export const apis = {
    development: {
        apiServerUrl: 'http://localhost:8888/dms-api/api/',
        pdfServiceUrl: 'http://localhost/medical-screening-pdf.php'
    },
    test: {
        apiServerUrl: 'http://52.34.172.213/dms-api/api/',
        pdfServiceUrl: 'http://52.34.172.213:8080/medical-screening-pdf.php'
    },
    stage: {
        apiServerUrl: 'http://dms-stage.allcells.com/dms-api/api/',
        pdfServiceUrl: 'http://dms-stage.allcells.com:8080/medical-screening-pdf.php'
    },
    production: {
        apiServerUrl: 'https://dms.allcells.com/api/api/',
        pdfServiceUrl: 'https://dms.allcells.com:8080/medical-screening-pdf.php'
    }

}