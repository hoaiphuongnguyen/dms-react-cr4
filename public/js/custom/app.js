//
// Package Name: AllCells DMS
// Package URI: http://www.ravendevelopers.com
// Subpackage: Site related Javascripts
// Author: Anirudh K. Mahant
// Author URI: http://www.ravendevelopers.com
// Description: AllCells DMS Javascripts.
// Version: 1.0
// License: Creative Commons 3.0 Attribution
// License URI: https://creativecommons.org/licenses/by/3.0/us/
// Tags: js
//

(function (dms, undefined) {

	dms.mediaQuerySize = function () {
		try {
			if (matchMedia('(min-device-width : 320px) and (max-device-width : 480px)').matches || matchMedia('(min-device-width : 360px) and (max-device-width : 640px)').matches) {
				return 'small';
			}
			else if (matchMedia('(min-device-width : 768px) and (max-device-width : 1024px) and (orientation: portrait)').matches) {
				return 'medium';
			}
			else if (matchMedia('(min-device-width : 768px) and (max-device-width : 1024px) and (orientation: landscape)').matches) {
				return 'medium-landscape';
			}
			else if (matchMedia('(min-device-width : 1025px) and (max-device-width : 1279px)').matches) {
				return 'medium-x';
			}
			else if (matchMedia('all and (min-width : 1224px)').matches) {
				return 'large';
			}
		} finally {
		}
	};

	dms.isJSON = function (str) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	};

	// We work under dms namespace
	dms.init = function () {
		jQuery('a[rel*=external]').each(function () {
			jQuery(this).attr('target', '_blank');
		});
	};

})(window.dms = window.dms || {});

(function ($) {
	$(document).ready(function () {
		dms.init();

		// Close popup tooltip button when clicking outside (html, body)
		$(".popup-tooltip").on('blur', function () {
			$(this).removeClass('active');
		});

		// Self close popup tooltip button
		$('[data-close-popup-tooltip]').click(function(){
			$(this).closest('.popup-tooltip').removeClass('active');
		});

		$('.dropdown-menu.static').on('focus click blur', function (e) {
			e.stopPropagation();
		});

		$('.dropdown-toggle.dropdown-extended').off('click').on('click', function (e) {
			var target = $(this).attr('id');
			var dropdown = $('[data-dropdown-for="' + target + '"]');
			dropdown.toggleClass('dropdown-active');
		});

		$('[data-toggle="tooltip"]').tooltip();

	});
})(jQuery);