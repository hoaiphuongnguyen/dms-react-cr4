import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import { API_STATUS } from '../common/constants';
import 'whatwg-fetch'; 


export function login(username, password) {
    var data = {
        username,
        password
    };

    var formBody = [];
    for (var property in data) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(data[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    const request = new Request(apiUtils.getLoginUrl(), {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        }),
        redirect: 'follow',
        body: formBody,
        credentials: 'same-origin' //allow fetch to send/save login cookie
    });

    return dispatch => {
        dispatch({
            type: actionTypes.LOGIN,
            status: API_STATUS.IN_PROGRESS
        });
        fetch(request)
            .then(response => {
                if (response.status == 401) {
                    dispatchLoginFail(dispatch);
                }
                else if (response.status == 200) {
                    return response.json();
                }
                else {
                    dispatchLoginError(dispatch);
                }
            })
            .then((responseData) => {
                if (responseData) {
                    dispatchLoginSuccess(dispatch, responseData);
                }
            })
            .catch(function (err) {
                dispatchLoginError(dispatch);
            });
    }
}

function dispatchLoginError(dispatch) {
    dispatch({
        type: actionTypes.LOGIN,
        status: API_STATUS.ERROR,
        errorMessage: 'Cannot connect to the server.'
    });
}

function dispatchLoginFail(dispatch) {
    dispatch({
        type: actionTypes.LOGIN,
        status: API_STATUS.ERROR,
        errorMessage: 'Invalid username or password.'
    });
}

function dispatchLoginSuccess(dispatch, responseData) {
    localStorage.setItem("userInfo", JSON.stringify(responseData));
    dispatch({
        type: actionTypes.LOGIN,
        status: API_STATUS.DONE,
        data: responseData
    });
}

export function setLoginFromLocalStore(data)
{
    return {
        type: actionTypes.LOGIN,
        status: API_STATUS.DONE,
        data: data
    }
}

export function logout() {
    return dispatch => {
        dispatch({
            type: actionTypes.LOGOUT,
            status: API_STATUS.IN_PROGRESS
        });
        fetch(apiUtils.getLogoutUrl(), {
            method: 'GET',
            credentials: 'same-origin' //allow fetch to send login cookie
        })
            .then(response => {
                if (response.status == 200) {
                    dispatchLogoutSuccess(dispatch);
                }
                else {
                    dispatchLogoutError(dispatch);
                }
            })
            .catch(err => {
                dispatchLogoutError(dispatch);
            });
    }
}


function dispatchLogoutError(dispatch) {
    dispatch({
        type: actionTypes.LOGOUT,
        status: API_STATUS.ERROR,
        errorMessage: 'Cannot connect to the server.'
    });
}


function dispatchLogoutSuccess(dispatch) {
    localStorage.removeItem("userInfo");
    dispatch({
        type: actionTypes.LOGOUT,
        status: API_STATUS.DONE
    });
}