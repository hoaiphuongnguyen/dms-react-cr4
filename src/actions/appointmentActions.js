import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {API_STATUS} from '../common/constants';
import {fetchExtended} from './fetchWrapper';

export function getAppointments(location, donorId, startTime, endTime) {
    var url = apiUtils.getAppointmentsUrl(location, donorId, startTime, endTime);
    return callGetAllApi(actionTypes.GET_APPOINTMENTS, url);
}

export function searchDonor(collectionSite, name, email, donorId) {
    var url = apiUtils.getSearchDonorForAppointmentUrl(collectionSite, name, email, donorId)
    return callGetAllApi(actionTypes.SEARCH_DONOR_FOR_APPOINTMENT, url);
}

export function callGetAllApi(actionType, url) {

    var request = new Request(url, {method: 'GET'});
    return dispatch => fetchExtended(dispatch, request, () => {
        dispatch({type: actionType, status: API_STATUS.IN_PROGRESS});
    }, {
        fNot200: () => {
            dispatch({type: actionType, status: API_STATUS.ERROR, message: response.cause});
        }
    }, (responseData) => {
        if (responseData) {
            dispatch({type: actionType, status: API_STATUS.DONE, items: responseData});
        }
    }, (err) => {
        dispatch({type: actionType, status: API_STATUS.ERROR, message: err});
    });
}

function callApiSaveAppointment(url, method, data, confirmSchedule) {
    var actionType = url.indexOf("checkout") > 0
        ? actionTypes.CHECK_OUT_APPOINTMENT
        : url.indexOf("cancellations") > 0
            ? actionTypes.CANCEL_APPOINTMENT
            : actionTypes.SAVE_APPOINTMENT

    var request;
    if (data != null) {
        var submitData = Object.assign({}, data);
        if (data.donor && data.donor.id) {
            submitData.donor = {
                id: data.donor.id
            };
        }

        if (data.procedureInfo && data.procedureInfo.id) {
            submitData.procedureInfo = {
                id: data.procedureInfo.id
            };
        }

        if (data.scheduleRequest && data.scheduleRequest.id) {
            submitData.scheduleRequest = {
                id: data.scheduleRequest.id
            };
        }

        request = new Request(url, {
            method: method,
            headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'}),
            body: JSON.stringify(submitData)
        });
    } else {
        request = new Request(url, {
            method: method,
            headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
        });
    }

    return dispatch => fetchExtended(dispatch, request, () => {
        dispatch({type: actionType, status: API_STATUS.IN_PROGRESS, data: data});
    }, {
        f204: () => {
            dispatch({type: actionType, status: API_STATUS.DONE, data: data, confirmSchedule: confirmSchedule});
        },
        f200: () => {
            if (data != null) {
                dispatch({type: actionType, status: API_STATUS.DONE, data: data, confirmSchedule: confirmSchedule});
            } else {
                dispatch({type: actionType, status: API_STATUS.DONE, confirmSchedule: confirmSchedule});
            }
        }
    }, (responseData) => {
        if (responseData) {
            if (responseData.errorCode || responseData.status == '404' || responseData.status == '500') {
                dispatch({type: actionType, status: API_STATUS.ERROR, data: responseData, confirmSchedule: confirmSchedule});
            }
        }
    }, (err) => {
        dispatch({
            type: actionType,
            status: API_STATUS.ERROR,
            data: {
                cause: err
            }
        });
    });
}

export function saveAppointment(appointment, isCancel, confirmSchedule) {
    var url;
    var method;
    var data;

    if (appointment.id) {
        method = "PUT";
        url = apiUtils.getOneAppointmentUrl(appointment.id);
        if (isCancel) {
            url = url + "/cancellations";
        }

        data = appointment;

    } else {
        url = apiUtils.getAppointmentUrl();
        method = "POST";
        data = appointment;
        data.id = null;

    }
    return callApiSaveAppointment(url, method, data, confirmSchedule);
}

export function updateAppointmentTime(appointment) {
    var url = apiUtils.getOneAppointmentUrl(appointment.id)
    return callApiSaveAppointment(url, "PATCH", appointment);
}

export function checkOut(appointmentId) {
    var url = apiUtils.getOneAppointmentUrl(appointmentId) + "/checkout";
    return callApiSaveAppointment(url, "PUT", null);
}

export function getAppointment(appointmentId) {
    var request = new Request(apiUtils.getOneAppointmentUrl(appointmentId), {
        method: 'GET',
        headers: new Headers({'Accept': 'application/json'})
    });

    return dispatch => fetchExtended(dispatch, request, () => {
        dispatch({type: actionTypes.GET_APPOINTMENT, status: API_STATUS.IN_PROGRESS});
    }, {
        fNot200: () => {
            dispatch({type: actionTypes.GET_APPOINTMENT, status: API_STATUS.ERROR, message: 'Unknown error'});
        }
    }, (responseData) => {
        if (responseData) {
            dispatch({type: actionTypes.GET_APPOINTMENT, status: API_STATUS.DONE, data: responseData});
        }
    }, (err) => {
        dispatch({type: actionTypes.GET_APPOINTMENT, status: API_STATUS.ERROR, message: err});
    });
}