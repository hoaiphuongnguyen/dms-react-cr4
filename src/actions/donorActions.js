import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {API_STATUS} from '../common/constants';
import {fetchExtended} from './fetchWrapper';

function callApiSaveDonor(tabName, url, method, data) {
    const request = new Request(url, {
        method: method,
        headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'}),
        body: JSON.stringify(data)
    });

    return dispatch => fetchExtended(dispatch, request, () => {}, {
        f204: () => {
            dispatch({tabName: tabName, type: actionTypes.SAVE_DONOR, status: API_STATUS.DONE});
        }
    }, (responseData) => {
        if (responseData) {
            if (responseData.errorCode || responseData.status == '500' || responseData.status == '400') {
                dispatch({tabName: tabName, type: actionTypes.SAVE_DONOR, status: API_STATUS.ERROR, data: responseData})
            } else {
                dispatch({tabName: tabName, type: actionTypes.SAVE_DONOR, status: API_STATUS.DONE, data: responseData})
            }
        }
    }, (err) => {
        dispatch({
            tabName: tabName,
            type: actionTypes.SAVE_DONOR,
            status: API_STATUS.ERROR,
            data: {
                cause: err
            }
        });
    });
}

export function savePayment(payment) {
    var url = apiUtils.getOneDonorUrl(payment.donor.id);
    url = url + "/payments";
    var method = "POST";
    var data = payment;
    return callApiSaveDonor("Payment", url, method, data);
}

export function saveDonor(tabName, donor) {
    var url;
    var method;
    var data;

    if (donor.id) {
        url = apiUtils.getOneDonorUrl(donor.id);
        if (tabName == "Profile") {
            method = "PUT";
            data = jQuery.extend(true, {}, donor);
            if (data.address.state == 'CA') 
                data.location.id = 'CA';
            else 
                data.location.id = 'MA';
            data.donorMedicalData = null;
            data.donorViralTest = null;
            data.donorProcedureAvailabilities = [];
            data.recruitment = null;
            data.outreachLogs = [];
            data.paymentTransactions = [];
            data.hlaTypings = [];
        } else if (tabName == "Medical") {
            url = url + "/medicals";
            if (donor.donorMedicalData && donor.donorMedicalData.id) {
                method = "PUT";
            } else {
                method = "POST";
            }
            data = new Object();
            if (donor.donorMedicalData) {
                data.donorMedicalData = donor.donorMedicalData;
                data.donorMedicalData.height = donor.donorMedicalData.feet;
                if (donor.donorMedicalData.feet) {
                    data.donorMedicalData.height = data.donorMedicalData.height;
                    if (donor.donorMedicalData.inch) 
                        data.donorMedicalData.height = data.donorMedicalData.height + "-" + donor.donorMedicalData.inch;
                    }
                }

            data.donorViralTest = donor.donorViralTest;
            data.donorProcedureAvailabilities = donor.donorProcedureAvailabilities;
        } else if (tabName == "Recruitment") {
            url = url + "/recruitments";
            if (donor.recruitment && donor.recruitment.id) {
                method = "PUT";
            } else {
                method = "POST";
            }
            data = donor.recruitment;
        }
    } else {
        url = apiUtils.getDonorUrl();
        method = "POST";
        data = donor;
        data.id = null;
        data.location = {};
        if (data.address.state == 'CA') 
            data.location.id = 'CA';
        else 
            data.location.id = 'MA';
        data.donorMedicalData = null;
        data.donorViralTest = null;
        data.donorProcedureAvailabilities = [];
        data.recruitment = null;
        data.outreachLogs = [];
        data.paymentTransactions = [];
        data.hlaTypings = [];
    }
    return callApiSaveDonor(tabName, url, method, data);
}

export function savePaymentVolume(payment) {

    const request = new Request(apiUtils.getSavePaymentVolumeUrl(payment.id, payment.actualCollectedWbVol), {
        method: 'PUT',
        headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
    });
    var tabName = "Payment";

    return dispatch => fetchExtended(dispatch, request, () => {}, {
        f204: () => {
            dispatch({tabName: tabName, type: actionTypes.SAVE_PAYMENT_VOLUME, status: API_STATUS.DONE});
        }
    }, (responseData) => {
        if (responseData) {
            if (responseData.errorCode || responseData.status == '500' || responseData.status == '400') {
                dispatch({tabName: tabName, type: actionTypes.SAVE_PAYMENT_VOLUME, status: API_STATUS.ERROR, data: responseData})
            } else {
                dispatch({tabName: tabName, type: actionTypes.SAVE_PAYMENT_VOLUME, status: API_STATUS.DONE, data: responseData})
            }
        }
    }, (err) => {
        dispatch({
            tabName: tabName,
            type: actionTypes.SAVE_PAYMENT_VOLUME,
            status: API_STATUS.ERROR,
            data: {
                cause: err
            }
        });
    });
}

export function getDonor(donorId) {
    var request = new Request(apiUtils.getOneDonorUrl(donorId), {
        method: 'GET',
        headers: new Headers({'Accept': 'application/json'})
    });
    return dispatch => fetchExtended(dispatch, request, () => {}, {
        fNot200: function () {
            dispatch({type: actionTypes.GET_DONOR, status: API_STATUS.ERROR, error: 'Unknown error'});
        }
    }, responseData => {
        if (responseData) {
            dispatch({type: actionTypes.GET_DONOR, status: API_STATUS.DONE, data: responseData});
        }
    }, err => {
        dispatch({type: actionTypes.GET_DONOR, status: API_STATUS.ERROR, error: err});
    });
}

export function printPdf(donor) {
    var data = {
        id: donor.id,
        name: (donor.firstName
            ? donor.firstName
            : "") + " " + (donor.lastName
            ? donor.lastName
            : ""),
        dob: donor.dateOfBirth,
        ethnicity: donor.donorMedicalData.ethnicity
            ? donor.donorMedicalData.ethnicity
            : "not_specified",
        age: donor.donorMedicalData.age,
        address: donor.address.addressLine1 + (donor.address.addressLine2
            ? (", " + donor.address.addressLine2)
            : "") + ", " + donor.address.city + ", " + donor.address.state + ", " + donor.address.zip,
        primaryPhone: donor.primaryPhone,
        secondaryPhone: donor.secondaryPhone,
        email: donor.email,
        height: donor.donorMedicalData.height
            ? donor.donorMedicalData.height
            : "",
        weight: donor.donorMedicalData.weight
            ? donor.donorMedicalData.weight
            : "",
        gender: donor.donorMedicalData.gender == "m"
            ? "male"
            : "female"
    };

    return {
        type: actionTypes.PRINT_PDF,
        status: API_STATUS.IN_PROGRESS,
        data: data,
        url: apiUtils.getPdfPrintingUrl()
    };
}

export function completePrintPdf() {
    return {type: actionTypes.PRINT_PDF, status: API_STATUS.DONE};
}