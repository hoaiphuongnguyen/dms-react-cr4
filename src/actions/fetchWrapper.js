import * as actionTypes from '../constants/actionTypes';
import { API_STATUS } from '../common/constants';


export function fetchExtended(dispatch, request,beforeRequestFunc, headerFuncs, bodyFunc, errFunc)
{
    var newRequest = new Request(request, {
        credentials: 'same-origin' //allow fetch to send/save login cookie
    })

    if(beforeRequestFunc)
        beforeRequestFunc();
    
    fetch(newRequest).then(response =>{

        if(response.status ==401 )
        {
            if(headerFuncs && headerFuncs.f401) //not login
            {
                return headerFuncs.f401();
            }
            else{
                localStorage.removeItem("userInfo");
                
                 dispatch({
                     type: actionTypes.SESSION_EXPIRED
                 });
                return null;
            }
        }
        if(response.status == 403  && headerFuncs && headerFuncs.f403) //not allowed
        {
            return headerFuncs.f403();
        }
        if(response.status == 500  && headerFuncs && headerFuncs.f500) //server error
        {
            return headerFuncs.f500();
        }
        if(response.status == 400 && headerFuncs && headerFuncs.f400) //bad request
        {
            return headerFuncs.f400();
        }
        if(response.status == 204  && headerFuncs && headerFuncs.f204) //no content
        {
            return headerFuncs.f204();
        }
        if(response.status != 200 && headerFuncs && headerFuncs.fNot200) //not 200 ok
        {
            return headerFuncs.fNot200();
        }
        if(response.status == 200 && headerFuncs && headerFuncs.f200) //200 ok
        {
            return headerFuncs.f200();
        }
        return response.json();
        

    }).then(responseData=>{
        bodyFunc(responseData);
    }).catch(err => {
            errFunc(err);
    });
}