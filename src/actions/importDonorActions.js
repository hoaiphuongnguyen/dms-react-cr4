import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import { API_STATUS } from '../common/constants';

export function importDonor(serverData) {
    return {
        type: actionTypes.IMPORT_DONORS,
        status: API_STATUS.DONE,
        totalRecords:serverData.totalRecords,
        successRecords:serverData.successRecords,
        failedRecords:serverData.failedRecords
    };
}
