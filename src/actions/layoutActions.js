import * as actionTypes from '../constants/actionTypes';
import {hashHistory} from 'react-router';

export function getDonorFromQuickSeach(donorId) {
    return {type: actionTypes.VIEW_DONOR_QUICK_SEARCH, donorId: donorId};
}

export function openSuccessDialog() {
    return {type: actionTypes.OPEN_SUCCESS_DIALOG};
}

export function closeSuccessDialog(nextUrl) {
    if (nextUrl) {
        hashHistory.push(nextUrl);
    }
    return {type: actionTypes.CLOSE_SUCCESS_DIALOG};
}

export function openAddUserDialog() {
    return {
        type: actionTypes.OPEN_ADD_USER_DIALOG,
        dialogData: {
            id: '',
            firstName: '',
            lastName: '',
            email: '',
            status: '',
            role: {
                id: ''
            }
        }
    };
}

export function openEditUserDialog(dialogData) {
    return {type: actionTypes.OPEN_EDIT_USER_DIALOG, dialogData: dialogData};
}

export function openAddOutReachDialog(donorId, scheduleId) {
    return {type: actionTypes.OPEN_ADD_OUT_REACH_DIALOG, donorId: donorId, scheduleId: scheduleId};
}

export function openAccountSettingDialog() {
    return {type: actionTypes.OPEN_ACCOUNT_SETTING_DIALOG};
}

export function openAddProcedureDialog() {
    return {type: actionTypes.OPEN_ADD_PROCEDURE_DIALOG};
}

export function openEditProcedureDialog(dialogData) {
    return {type: actionTypes.OPEN_EDIT_PROCEDURE_DIALOG, dialogData: dialogData};
}

export function openAppointmentPopup(dialogData) {
    return {type: actionTypes.OPEN_APPOINTMENT_POPUP, dialogData: dialogData};
}

export function openAddAppointmentDialog(collectionSite, donorId, procedureCategory, startTime, endTime) {
    return {
        type: actionTypes.OPEN_ADD_APPOINMENT_DIALOG,
        collectionSite: collectionSite,
        donorId: donorId,
        procedureCategory: procedureCategory,
        startTime: startTime,
        endTime: endTime
    };
}

export function openEditAppointmentDialog(dialogData) {
    return {type: actionTypes.OPEN_EDIT_APPOINMENT_DIALOG, dialogData: dialogData};
}

export function openCancelAppointmentDialog(dialogData) {
    return {type: actionTypes.OPEN_CANCEL_APPOINMENT_DIALOG, dialogData: dialogData};
}

export function closeDialog(needRefresh, data) {
    return {type: actionTypes.CLOSE_DIALOG, needRefresh: needRefresh, data: data};
}

export function closeDialogAccountSetting(needRefresh) {
    return {type: actionTypes.CLOSE_DIALOG_ACCOUNT_SETTING, needRefresh: needRefresh};
}

export function closeAppointmentDialog(needRefresh, data) {
    return {type: actionTypes.CLOSE_APPOINTMENT_DIALOG, needRefresh: needRefresh, data: data};
}

export function closeAppointmentPopup() {
    return {type: actionTypes.CLOSE_APPOINMENT_POPUP};
}

export function changeRoute(prevRoute, nextRoute) {
    return {type: actionTypes.CHANG_ROUTE, prevRoute, nextRoute};
}
