import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {fetchExtended} from './fetchWrapper';
import {API_STATUS} from '../common/constants';

export function getProcedure(procedureId) {
    var request = new Request(apiUtils.getOneProcedureUrl(ProcedureId),{method: 'GET'});

    return dispatch => fetchExtended(dispatch, request, ()=>{
        dispatch({type: actionTypes.GET_PROCEDURE, status: API_STATUS.IN_PROGRESS});
    },{fNot200:()=>{
        dispatch({type: actionTypes.GET_PROCEDURE, status: API_STATUS.ERROR, errorMessage: 'Unknown error'});
    }},(responseData)=>{
         if (responseData) {
            dispatch({type: actionTypes.GET_PROCEDURE, status: API_STATUS.DONE, Procedure: responseData});
        }
    },(err)=>{
        dispatch({type: actionTypes.GET_PROCEDURE, status: API_STATUS.ERROR, errorMessage: err});
    });
}

function callApiSaveProcedure(url, method, procedure) {
    var actionType = actionTypes.SAVE_PROCEDURE;

    var request = new Request(url, {
        method: method,
        headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'}),
        body: JSON.stringify(procedure)
    });

    return dispatch => fetchExtended(dispatch, request,()=>{
        dispatch({type: actionType, status: API_STATUS.IN_PROGRESS});
    },{f200:()=>{
        dispatch({type: actionType, status: API_STATUS.DONE});
    },f204:()=>{
        dispatch({type: actionType, status: API_STATUS.DONE});
    }},(responseData)=>{
        if (responseData) {
            dispatch({type: actionType, status: API_STATUS.ERROR, data: responseData});
        }
    },(err)=>{
         dispatch({
                type: actionType,
                status: API_STATUS.ERROR,
                data: {
                    cause: err.message
                }
            });
    });
}

export function saveProcedure(procedure) {
    if (procedure.id) {
        return callApiSaveProcedure(apiUtils.getOneProcedureUrl(procedure.id), "PUT", procedure);
    } else {
        return callApiSaveProcedure(apiUtils.getProcedureUrl(), "POST", procedure);
    }
}