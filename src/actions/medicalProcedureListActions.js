import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import { API_STATUS } from '../common/constants';
import {fetchExtended} from './fetchWrapper';

export function loadAllMedicalProcedures() {

    var request = new Request(apiUtils.getLoadActiveMedicalProceduresUrl(1, 1000, 'code', 'ASC'), {
            method: 'GET',
            headers: new Headers({'Accept': 'application/json'})
        });

    return dispatch => fetchExtended(dispatch,request,()=>{
        dispatch({type: actionTypes.LOAD_ALL_PROCEDURES,status: API_STATUS.IN_PROGRESS});
    },{fNot200:()=>{
        dispatch({type: actionTypes.LOAD_ALL_PROCEDURES, status: API_STATUS.ERROR, errorMessage: 'Unknown error' });
    }},(responseData)=>{
         if (responseData) {
                        dispatch({
                        type: actionTypes.LOAD_ALL_PROCEDURES,
                        status: API_STATUS.DONE,
                        page: responseData.page,
                        items: responseData.items,
                        totalRecords: responseData.totalRecords
                    });
                }
    },(err)=>{
         dispatch({
                    type: actionTypes.LOAD_ALL_PROCEDURES,
                    status: API_STATUS.ERROR,
                    errorMessage: err
                });
    });
}

export function loadMedicalProcedures(pageNumber,pageSize, sortBy, sortOrder, reload) {
    
     var request = new Request((apiUtils.getLoadMedicalProceduresUrl(pageNumber, pageSize, sortBy, sortOrder)), {
            method: 'GET',
            headers: new Headers({'Accept': 'application/json'})
        });

    return dispatch => fetchExtended(dispatch, request, ()=>{
    },{fNot200:()=>{
         dispatch({ type: actionTypes.LOAD_PROCEDURES, status: API_STATUS.ERROR, errorMessage: 'Unknown error' });
    }},(responseData)=>{
        if (responseData) {
                        dispatch({
                        type: reload ? actionTypes.RELOAD_PROCEDURES: actionTypes.LOAD_PROCEDURES,
                        status: API_STATUS.DONE,
                        page: responseData.page,
                        items: responseData.items,
                        sortBy: sortBy,
                        sortOrder: sortOrder,
                        totalRecords: responseData.totalRecords
                    });
                }
    },(err)=>{
         dispatch({
                    type: actionTypes.LOAD_PROCEDURES,
                    status: API_STATUS.ERROR,
                    errorMessage: err
                });
    });
}
