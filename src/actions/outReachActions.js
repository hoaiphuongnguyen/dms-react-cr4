import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {API_STATUS} from '../common/constants';
import {callGetAllApi} from './appointmentActions';
import {fetchExtended} from './fetchWrapper';

export function getListOutreachReason() {
    return callGetAllApi(actionTypes.GET_OUT_REACH_REASON, apiUtils.getOutreachReasonUrl());
}

export function getListOutreachOutcome() {
    return callGetAllApi(actionTypes.GET_OUT_REACH_OUTCOME, apiUtils.getOutreachOutcomeUrl());
}

function callApiSaveOutreach(url, method, data) {
    var actionType = actionTypes.SAVE_OUT_REACH;
    var request = new Request(url, {
        method: method,
        headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'}),
        body: JSON.stringify(data)
    });

    return dispatch=> fetchExtended(dispatch, request,()=>{
        dispatch({type: actionType, status: API_STATUS.IN_PROGRESS});
    },{f204:()=>{
        dispatch({type: actionType, status: API_STATUS.DONE, data: data});
    }},(responseData)=>{
        if (responseData.errorCode || responseData.status == '500') {
                dispatch({type: actionType, status: API_STATUS.ERROR, data: responseData});
            } else {
                dispatch({type: actionType, status: API_STATUS.DONE, data: responseData})
            }
    },(err)=>{
        dispatch({
                type: actionType,
                status: API_STATUS.ERROR,
                data: {
                    cause: err.message
                }
            });
    });
}

export function saveOutReach(donorId, data) {
    return callApiSaveOutreach(apiUtils.getOneDonorUrl(donorId) + "/outreachs", "POST", data);
}