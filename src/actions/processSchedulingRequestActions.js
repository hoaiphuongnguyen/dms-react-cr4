import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {API_STATUS} from '../common/constants';
import {fetchExtended} from './fetchWrapper';

export function processSchedulingRequest(data, pageNumber, pageSize, sortBy, sortOrder, reload) {
    var request= new Request(apiUtils.getProcessSchedulingRequestUrl(data, pageNumber, pageSize, sortBy, sortOrder),{
            method: 'POST',
            headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'}),
            body: JSON.stringify(data)
        });

    return dispatch=> fetchExtended(dispatch, request,()=>{
         dispatch({type: actionTypes.MATCH_DONORS, status: API_STATUS.IN_PROGRESS});
    },{fNot200:()=>{
        dispatch({
                    type: actionTypes.MATCH_DONORS,
                    status: API_STATUS.ERROR,
                    errorMessage: 'Unknown error'
                });
    }},(responseData)=>{
            if (responseData) {
                dispatch({
                    type: reload ? actionTypes.MATCH_DONORS_RELOAD: actionTypes.MATCH_DONORS,
                    status: API_STATUS.DONE,
                    page: responseData.page,
                    items: responseData.items
                        .map(function (x) {
                            if(x.outreachLogs && x.outreachLogs.length) {
                                var lastlog = x.outreachLogs[0];
                                x.lastRequestId= lastlog.scheduleRequest ? lastlog.scheduleRequest.id:'';
                                x.lastOutReachDate = lastlog.createdAt;
                                x.lastOutReachReason= lastlog.outreachReason ?lastlog.outreachReason.name:'';
                            }
                            else {
                                x.lastRequestId='';
                                x.lastOutReachDate ='';
                                x.lastOutReachReason='';
                            }
                            return x; 
                        }),
                    sortBy: sortBy,
                    sortOrder: sortOrder,
                    totalRecords: responseData.totalRecords
                });
            }

    },(err)=>{
         dispatch({
                type: actionTypes.MATCH_DONORS,
                status: API_STATUS.ERROR,
                errorMessage: err
            });
    });

}

export function clearProcessRequestParams()
{
    return {
        type: actionTypes.CLEAR_MATCH_DONORS_PARAMS
    };
}

export function matchDonorWithRequest(donorId, scheduleId, hiddenBackScheduleRequest)
{
    return{
        type: actionTypes.MATCH_DONOR_TO_SCHEDULE,
        donorId: donorId,
        scheduleId: scheduleId,
        hiddenBackScheduleRequest: hiddenBackScheduleRequest
    };
}


