import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {API_STATUS} from '../common/constants';
import {fetchExtended} from './fetchWrapper';

function callApiReport(url, pageNumber, pageSize, sortBy, sortOrder, reload) {
    var request = new Request(url, {
        method: 'GET',
        headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
    });

    return dispatch => fetchExtended(dispatch, request, () => {
        dispatch({type: actionTypes.LOAD_REPORT, status: API_STATUS.IN_PROGRESS});
    }, {
        fNot200: () => {
            dispatch({type: actionTypes.LOAD_REPORT, status: API_STATUS.ERROR, errorMessage: 'Unknown error'});
        }
    }, (responseData) => {
        if (responseData) {
            dispatch({
                type: reload
                    ? actionTypes.RELOAD_REPORT
                    : actionTypes.LOAD_REPORT,
                status: API_STATUS.DONE,
                page: responseData.page,
                items: responseData.items,
                sortBy: sortBy,
                sortOrder: sortOrder,
                totalRecords: responseData.totalRecords
            });
        }
    }, (err) => {
        dispatch({type: actionTypes.LOAD_REPORT, status: API_STATUS.ERROR, errorMessage: err});
    });
}

export function reportDonorStatus(searchParams, pageNumber, pageSize, sortBy, sortOrder, reload) {
    if (sortBy == "category" || sortBy == "status") {
        sortBy = "donorRecruitments." + sortBy;
    } else if (sortBy == "location") {
        sortBy = "location.state"
    } else if (sortBy == "age" || sortBy == "gender" || sortBy == "ethnicity" || sortBy == "race" || sortBy == "bloodType" || sortBy == "hasHpv") {
        sortBy = "donorMedicalDatas." + sortBy;
    }

    var url = apiUtils.getReportDonorStatusUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder);
    return callApiReport(url, pageNumber, pageSize, sortBy, sortOrder, reload)
}

export function reportDonorCancellation(searchParams, pageNumber, pageSize, sortBy, sortOrder, reload) {
    if (sortBy == "category" || sortBy == "status") {
        sortBy = "donor.donorRecruitments." + sortBy;
    } else if (sortBy == "location") {
        sortBy = "donor.location.state"
    } else if (sortBy == "procedure") {
        sortBy = "procedureInfo.name";
    } else if (sortBy == "woNumber") {
        sortBy = "scheduleRequest.woNumber";
    }

    var url = apiUtils.getReportDonorCancellationUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder);
    return callApiReport(url, pageNumber, pageSize, sortBy, sortOrder, reload)
}

export function reportAppointment(searchParams, pageNumber, pageSize, sortBy, sortOrder, reload) {
    if (sortBy == "donorId") {
        sortBy = "donor.id";
    } else if (sortBy == "location") {
        sortBy = "donor.location.state"
    } else if (sortBy == "procedure") {
        sortBy = "procedureInfo.name";
    } else if (sortBy == "procedureVolume") {
        sortBy = "procedureInfo.volumeUnit";
    } else if (sortBy == "procedureType") {
        sortBy = "procedureInfo.category";
    }

    var url = apiUtils.getReportAppointmentUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder);
    return callApiReport(url, pageNumber, pageSize, sortBy, sortOrder, reload)
}

export function reportPayment(searchParams, pageNumber, pageSize, sortBy, sortOrder, reload) {
    if (sortBy == "donorId") {
        sortBy = "donor.id";
    } else if (sortBy == "donorName") {
        sortBy = "donor.firstName"
    } else if (sortBy == "procedure") {
        sortBy = "procedureInfo.name";
    }
    var url = apiUtils.getReportPaymentUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder);
    return callApiReport(url, pageNumber, pageSize, sortBy, sortOrder, reload)
}

export function clearSearchParams() {
    return {type: actionTypes.CLEAR_SEARCH_PARAMS};
}
