import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {API_STATUS} from '../common/constants';
import {hashHistory} from 'react-router';
import {fetchExtended} from './fetchWrapper';

function callApiSaveSchedulingRequest(tabName, url, method, data) {
    const request = new Request(url, {
        method: method,
        headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'}),
        body: JSON.stringify(data)
    });
    return dispatch => fetchExtended(dispatch, request, () => {}, {
        f204: () => {
            dispatch({tabName: tabName, type: actionTypes.SAVE_SCHEDULING_REQUEST, status: API_STATUS.DONE});
        }
    }, (responseData) => {
        if (responseData.errorCode || responseData.status == '404' || responseData.status == '500') {
            dispatch({tabName: tabName, type: actionTypes.SAVE_SCHEDULING_REQUEST, status: API_STATUS.ERROR, data: responseData});
        } else {
            dispatch({tabName: tabName, type: actionTypes.SAVE_SCHEDULING_REQUEST, status: API_STATUS.DONE, data: responseData});
            if (method == "POST") {
                setTimeout(function () {
                    //redirect to edit form if create new request
                    hashHistory.push("/schedule-request/edit-schedule-request/" + responseData.id);
                }, 100);
            } else {
                dispatch({type: actionTypes.GET_SCHEDULING_REQUEST, status: API_STATUS.DONE, data: responseData});
            }
        }
    }, (err) => {
        dispatch({
            tabName: tabName,
            type: actionTypes.SAVE_SCHEDULING_REQUEST,
            status: API_STATUS.ERROR,
            data: {
                cause: err
            }
        });
    });
}

export function saveSchedulingRequest(tabName, schedulingRequest) {
    var url;
    var method;
    var data;

    if (schedulingRequest.id) {
        method = "PUT";
        url = apiUtils.getOneSchedulingRequestUrl(schedulingRequest.id);
        data = schedulingRequest;

    } else {
        url = apiUtils.getCreateSchedulingRequestUrl();
        method = "POST";
        data = schedulingRequest;
        data.id = null;

    }
    return callApiSaveSchedulingRequest(tabName, url, method, data);
}

export function getSchedulingRequest(schedulingRequestId) {
    var request = new Request(apiUtils.getOneSchedulingRequestUrl(schedulingRequestId), {
        method: 'GET',
        headers: new Headers({'Accept': 'application/json'})
    });
    return dispatch => fetchExtended(dispatch, request, () => {}, {
        f400: () => {
            dispatch({
                type: actionTypes.GET_SCHEDULING_REQUEST,
                status: API_STATUS.ERROR,
                error: 'Schedule request ' + schedulingRequestId + ' not found'
            });
        }
    }, (responseData) => {
        if (responseData) {
            dispatch({type: actionTypes.GET_SCHEDULING_REQUEST, status: API_STATUS.DONE, data: responseData});
        }
    }, (err) => {
        dispatch({type: actionTypes.GET_SCHEDULING_REQUEST, status: API_STATUS.ERROR, error: err});
    });
}

export function cancelSchedulingRequest(schedulingRequestId) {
    var url = apiUtils.getOneSchedulingRequestUrl(schedulingRequestId) + "/cancellation";
    var request = new Request(url, {
        method: 'PUT',
        headers: new Headers({'Accept': 'application/json'})
    });

    return dispatch => fetchExtended(dispatch, request, () => {}, {
        f200: () => {
            dispatch({type: actionTypes.CANCEL_SCHEDULING_REQUEST, status: API_STATUS.DONE});
        }
    }, (responseData) => {
        if (responseData) {
            if (responseData.errorCode || responseData.status == '404' || responseData.status == '500') {
                dispatch({tabName: tabName, type: actionTypes.CANCEL_SCHEDULING_REQUEST, status: API_STATUS.ERROR, data: responseData});
            }
        }
    }, (err) => {
        dispatch({type: actionTypes.CANCEL_SCHEDULING_REQUEST, status: API_STATUS.ERROR, error: err});
    });
}

export function unAssignedDonor(scheduleId, donorId) {
    var url = apiUtils.getUnAssignedDonorUrl(scheduleId, donorId);
    var request = new Request(url, {
        method: 'PUT',
        headers: new Headers({'Accept': 'application/json'})
    });

    return dispatch => fetchExtended(dispatch, request, () => {}, {
        fNot200: () => {
            dispatch({type: actionTypes.UNASSIGN_DONOR, status: API_STATUS.ERROR, error: 'Unknown error'});
        },
        f200: () => {
            dispatch({type: actionTypes.UNASSIGN_DONOR, status: API_STATUS.DONE});
        }
    }, (responseData) => {}, (err) => {
        dispatch({type: actionTypes.UNASSIGN_DONOR, status: API_STATUS.ERROR, error: err});
    });
}