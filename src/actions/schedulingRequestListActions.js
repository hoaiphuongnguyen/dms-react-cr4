import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {API_STATUS} from '../common/constants';
import {fetchExtended} from './fetchWrapper';

export function loadSchedulingRequestList(showOnlyProcessRequest, pageNumber, pageSize, sortBy, sortOrder, reload) {

    var request = new Request(apiUtils.getLoadSchedulingRequestUrl(showOnlyProcessRequest, pageNumber, pageSize, sortBy, sortOrder), {
        method: 'GET',
        headers: new Headers({'Accept': 'application/json'})
    });

    return dispatch => fetchExtended(dispatch, request, () => {}, {
        fNot200: () => {
            dispatch({type: actionTypes.LOAD_SCHEDULING_REQUEST, status: API_STATUS.ERROR, errorMessage: 'Unknown error'});
        }
    }, (responseData) => {
        if (responseData) {
            dispatch({
                type: reload
                    ? actionTypes.RELOAD_SCHEDULING_REQUEST
                    : actionTypes.LOAD_SCHEDULING_REQUEST,
                status: API_STATUS.DONE,
                page: responseData.page,
                items: responseData
                    .items
                    .map(function (x) {
                        if (x.donorToSchedulestatus == "CANCELLED") {
                            x.donorId = '';
                        }
                        return x;
                    }),
                sortBy: sortBy,
                sortOrder: sortOrder,
                totalRecords: responseData.totalRecords
            });
        }
    }, (err) => {
        dispatch({type: actionTypes.LOAD_SCHEDULING_REQUEST, status: API_STATUS.ERROR, errorMessage: err});
    });
}

export function getSchedulingRequestDonorMatch(donorId, pageNumber, pageSize, sortBy, sortOrder, reload) {

    var request = new Request(apiUtils.getSchedulingRequestDonorMatch(donorId, pageNumber, pageSize, sortBy, sortOrder), {
        method: 'GET',
        headers: new Headers({'Accept': 'application/json'})
    });

    return dispatch => fetchExtended(dispatch, request, () => {}, {
        fNot200: () => {
            dispatch({type: actionTypes.LOAD_SCHEDULING_REQUEST_DONOR_MATCH, status: API_STATUS.ERROR, errorMessage: 'Unknown error'});
        }
    }, (responseData) => {
        if (responseData) {
            dispatch({
                type: reload
                    ? actionTypes.RELOAD_SCHEDULING_REQUEST_DONOR_MATCH
                    : actionTypes.LOAD_SCHEDULING_REQUEST_DONOR_MATCH,
                status: API_STATUS.DONE,
                page: responseData.page,
                  items: responseData
                        .items
                        .map(function (x) {
                            return jQuery.extend(true, {}, x, {procedure: x.procedureInfo.name});
                        }),
                sortBy: sortBy,
                sortOrder: sortOrder,
                totalRecords: responseData.totalRecords
            });
        }
    }, (err) => {
        dispatch({type: actionTypes.LOAD_SCHEDULING_REQUEST_DONOR_MATCH, status: API_STATUS.ERROR, errorMessage: err});
    });
}
