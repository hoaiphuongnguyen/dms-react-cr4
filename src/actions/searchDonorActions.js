import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {API_STATUS} from '../common/constants';
import {fetchExtended} from './fetchWrapper';

export function searchDonor(searchParams, pageNumber, pageSize, sortBy, sortOrder, reload, returnDataOnly) {
    if (sortBy == "category" || sortBy == "status") {
        sortBy = "donorRecruitments." + sortBy;
    }
    var request = new Request(apiUtils.getSearchDonorUrl(pageNumber, pageSize, sortBy, sortOrder), {
        method: 'POST',
        headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'}),
        body: JSON.stringify(searchParams)
    });
    return dispatch => fetchExtended(dispatch, request, () => {
        if (!returnDataOnly) {
            dispatch({type: actionTypes.SEARCH_DONORS, status: API_STATUS.IN_PROGRESS});
        }

    }, {}, (responseData) => {

        if (responseData) {
            if (responseData.errorCode || responseData.status == '500' || responseData.status == '400') {
                dispatch({
                    type: reload
                        ? actionTypes.SEARCH_DONORS_RELOAD
                        : actionTypes.SEARCH_DONORS,
                    status: API_STATUS.ERROR,
                    errorMessage: responseData.cause
                        ? responseData.cause
                        : responseData
                })
            } else {
                if (!returnDataOnly) {
                    dispatch({
                        type: reload
                            ? actionTypes.SEARCH_DONORS_RELOAD
                            : actionTypes.SEARCH_DONORS,
                        status: API_STATUS.DONE,
                        page: responseData.page,
                        items: responseData.items,
                        sortBy: sortBy,
                        sortOrder: sortOrder,
                        totalRecords: responseData.totalRecords
                    });
                } else {
                    return responseData.items;
                }
            }
        }
    }, err => {
        if (!returnDataOnly) {
            dispatch({type: actionTypes.SEARCH_DONORS, status: API_STATUS.ERROR, errorMessage: err});
        }
    });
}

export function clearSearchParams() {
    return {type: actionTypes.CLEAR_SEARCH_PARAMS};
}
