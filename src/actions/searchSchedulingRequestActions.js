import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {API_STATUS} from '../common/constants';
import {fetchExtended} from './fetchWrapper';

export function searchSchedulingRequest(searchParams, pageNumber, pageSize, sortBy, sortOrder, reload) {
    var request = new Request(apiUtils.getSearchSchedulingRequestUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder), {
        method: 'GET',
        headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
    });

    return dispatch => fetchExtended(dispatch, request, () => {}, {
        fNot200: () => {
            dispatch({type: actionTypes.SEARCH_SCHEDULING_REQUEST, status: API_STATUS.ERROR, errorMessage: 'Unknown error'});
        }
    }, (responseData) => {
        if (responseData) {
            dispatch({
                type: reload
                    ? actionTypes.SEARCH_SCHEDULING_REQUEST_RELOAD
                    : actionTypes.SEARCH_SCHEDULING_REQUEST,
                status: API_STATUS.DONE,
                page: responseData.page,
                items: responseData
                    .items
                    .map(function (x) {
                        return jQuery.extend(true, {}, x, {procedure: x.procedureInfo.name});
                    }),
                sortBy: sortBy,
                sortOrder: sortOrder,
                totalRecords: responseData.totalRecords
            });
        }
    }, (err) => {
        dispatch({type: actionTypes.SEARCH_SCHEDULING_REQUEST, status: API_STATUS.ERROR, errorMessage: err});
    });
}

export function clearSearchParams() {
    return {type: actionTypes.CLEAR_SEARCH_PARAMS};
}