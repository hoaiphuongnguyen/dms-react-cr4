import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import {fetchExtended} from './fetchWrapper';
import {API_STATUS} from '../common/constants';

export function getUser(userId) {
    var request = new Request(apiUtils.getOneUserUrl(userId), {method: 'GET'});
    return dispatch => fetchExtended(dispatch, request, () => {
        dispatch({type: actionTypes.GET_USER, status: API_STATUS.IN_PROGRESS});
    }, {
        fNot200: () => {
            dispatch({type: actionTypes.GET_USER, status: API_STATUS.ERROR, errorMessage: 'Unknown error'});
        }
    }, (responseData) => {
        if (responseData) {
            dispatch({type: actionTypes.GET_USER, status: API_STATUS.DONE, user: responseData});
        }
    }, (err) => {
        dispatch({type: actionTypes.GET_USER, status: API_STATUS.ERROR, errorMessage: err});
    });
}

function callApiSaveUser(url, method, user, actionType) {

    var request;
    if (user) {
        request = new Request(url, {
            method: method,
            headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'}),
            body: JSON.stringify(user),
            credentials: 'same-origin' //allow fetch to send/save login cookie
        });
    } else {
        request = new Request(url, {
            method: method,
            headers: new Headers({'Accept': 'application/json', 'Content-Type': 'application/json'}),
            credentials: 'same-origin' //allow fetch to send/save login cookie
        });
    }

    return dispatch => fetchExtended(dispatch, request, () => {
        dispatch({type: actionType, status: API_STATUS.IN_PROGRESS});
    }, {
        f204: () => {
            dispatch({type: actionType, status: API_STATUS.DONE});
        },
        f200: () => {
            dispatch({type: actionType, status: API_STATUS.DONE});
        }
    }, (responseData) => {
        if (responseData) {
            dispatch({type: actionType, status: API_STATUS.ERROR, data: responseData});
        }
    }, (err) => {
        dispatch({
            type: actionType,
            status: API_STATUS.ERROR,
            data: {
                cause: err.message
            }
        });
    });
}

export function saveUser(user) {
    var actionType = actionTypes.SAVE_USER;
    if (user.id) {
        return callApiSaveUser(apiUtils.getOneUserUrl(user.id), "PUT", user, actionType);
    } else {
        return callApiSaveUser(apiUtils.getUserUrl(), "POST", user, actionType);
    }
}

export function resetPassword(userId) {
    var actionType = actionTypes.RESET_PASSWORD;
    return callApiSaveUser(apiUtils.getOneUserUrl(userId), "PATCH", null, actionType);

}

export function accountSetting(user, isChangePassword) {
    var url = apiUtils.getUserUrl();
    if (isChangePassword) {
        url = url + "?isChangePassword=true"
    }
    var actionType = actionTypes.UPDATE_PROFILE;
    return callApiSaveUser(url, "PUT", user, actionType);
}