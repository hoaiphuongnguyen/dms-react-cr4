import * as actionTypes from '../constants/actionTypes';
import * as apiUtils from '../utils/apiUtils';
import { API_STATUS } from '../common/constants';
import {fetchExtended} from './fetchWrapper';

export function loadUsers(keyword, pageNumber,pageSize, sortBy, sortOrder, reload) {
    
    var  request = new Request(apiUtils.getLoadUsersUrl(keyword, pageNumber, pageSize, sortBy, sortOrder), {
            method: 'GET',
            headers: new Headers({'Accept': 'application/json'})
        });

    return dispatch=> fetchExtended(dispatch,request,()=>{
    //    dispatch({type: actionTypes.LOAD_USERS,status: API_STATUS.IN_PROGRESS});
    },{fNot200:()=>{
        dispatch({type: actionTypes.LOAD_USERS,status: API_STATUS.ERROR,errorMessage: 'Unknown error'});
    }}, (responseData)=>{
         if (responseData) {
                    dispatch({
                        type: reload ? actionTypes.RELOAD_USERS : actionTypes.LOAD_USERS ,
                        status: API_STATUS.DONE,
                        page: responseData.page,
                        items: responseData.items.map(function(x){
                            return jQuery.extend(true, {}, x, {roleName:x.role.roleName});
                        }),
                        sortBy: sortBy,
                        sortOrder: sortOrder,
                        totalRecords: responseData.totalRecords
                    });
                }
    }, (err)=>{
         dispatch({
                    type: actionTypes.LOAD_USERS,
                    status: API_STATUS.ERROR,
                    errorMessage: err
                });
    });
}
