import React, {Component} from 'react';
import {hashHistory, Router} from 'react-router';
import {API_STATUS, PATH_PREFIX} from '../../common/constants';
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import ErrorDialog from '../common/errorDialog'
import * as validations from '../../validations/common'

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            errors: {},
            processing: false
        }

        this.login = this
            .login
            .bind(this);
        this.onChange = this
            .onChange
            .bind(this);
        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);

        this.inputControls = {};
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.status == API_STATUS.DONE && nextProps.user != null) {
            hashHistory.push(PATH_PREFIX + '/');
        } else if (nextProps.status == API_STATUS.IN_PROGRESS) {
            this.setState({processing: true});
        } else if (nextProps.status == API_STATUS.ERROR) {
            $("#errorMessage").html(nextProps.message);
            $("#errorDil").modal('show');
            this.setState({processing: false});

        }
    }
    componentDidMount() {
        document
            .body
            .classList
            .add('login');
        document
            .body
            .classList
            .add('front');

        $("input[name=password]").keypress(function(e){
            if(e.keyCode ==13)
            {
                $("#submitButton").click();
            }
        })
    }
    componentWillUnmount() {
        document
            .body
            .classList
            .remove('login');
        document
            .body
            .classList
            .remove('front');
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {
        var loginData = {};
        loginData[name] = value;
        this.setState(loginData);
    }

    login() {
        this.setState({errorMessage: ""});

        const data = this.state;
        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (name) {
                controls[name].validateAllRules(name, data[name]);
            });

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        });

        if (numError == 0) {
            this
                .props
                .actions
                .login(this.state.username.trim(), this.state.password.trim());
        }
    }

    render() {
        return (
            <div>
                <div id="wrapper">
                    <div className="container-fluid">
                        <main className="row">
                            <section className="col-sm-24" id="home-login">
                                <div className="panel-login txt-center-xs">
                                    <h1>
                                        <a href="/"><img src="/img/branding.svg" alt="All Cells"/></a>
                                    </h1>
                                    <h2>Donor Management System</h2>
                                    <h3>Sign in to continue</h3>

                                    <ErrorArea
                                        className="error-list border-all clearfix"
                                        hideCloseReport={true}
                                        errorList={this.state.errors}/>
                                    <form className="form-allcells form-animated-labels">

                                        <InputControl
                                            containerClass="form-group"
                                            ref
                                            ={(input) => {
                                            this.inputControls["username"] = input
                                        }}
                                            type="username"
                                            name="username"
                                            label="Email"
                                            required={true}
                                            validate={validations.email}
                                            errorMessage="The &#39;Email&#39; does not seem to be valid. Please check and enter again."
                                            emptyMessage="The &#39;Email&#39; field cannot be left empty. Please enter an email to continue."
                                            labelClass="ucase"
                                            inputClass="form-control"
                                            maxLength="32"
                                            value={this.state.username}
                                            onChange={this.onChange}
                                            onInvalid={this.onInvalid}
                                            onValid={this.onValid}/>

                                        <InputControl
                                            containerClass="form-group"
                                            ref
                                            ={(input) => {
                                            this.inputControls["password"] = input
                                        }}
                                            type="password"
                                            name="password"
                                            label="Password"
                                            required={true}
                                            emptyMessage="The &#39;Password&#39; field cannot be left empty. Please enter a password to continue."
                                            labelClass="ucase"
                                            inputClass="form-control"
                                            maxLength="32"
                                            value={this.state.password}
                                            onChange={this.onChange}
                                            onInvalid={this.onInvalid}
                                            onValid={this.onValid}/>

                                        <div className="form-submit">
                                            <button
                                                id="submitButton"
                                                className="btn btn-default"
                                                type="button"
                                                onClick={this.login}
                                                disabled={this.state.processing}>Log In</button>
                                        </div>

                                    </form>

                                </div>
                            </section>
                        </main>
                    </div>
                </div>
                <ErrorDialog/>
            </div>
        );
    }
}

export default Login;
