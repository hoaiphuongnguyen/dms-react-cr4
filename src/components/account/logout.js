import { PATH_PREFIX} from '../../common/constants';
import React, {Component} from 'react';
import {hashHistory} from 'react-router';

class Logout extends Component {

    constructor(props) {
        super(props);
        this.logoutClick = this
            .logoutClick
            .bind(this);
    }

    logoutClick() {
        hashHistory.push(PATH_PREFIX + '/logout');
    }

    render() {
        return (
            <a href="javascript:void(0)" onClick={this.logoutClick}>Log out</a>
        );
    }
}

export default Logout;