import React from 'react';
import {logout} from '../../actions/accountActions';
import {hashHistory} from 'react-router';
import {API_STATUS, PATH_PREFIX} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';

class LogoutPage extends React.Component {
    componentWillMount() {
        this
            .props
            .dispatch(logout());
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.logout && nextProps.logout.status == API_STATUS.DONE && nextProps.logout.actionType == actionTypes.LOGOUT) {
            hashHistory.push(PATH_PREFIX + '/login');
        }
    }

    render() {
        return null;
    }
};

export default LogoutPage;