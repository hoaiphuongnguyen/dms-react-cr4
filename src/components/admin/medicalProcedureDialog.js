import React, {Component} from 'react';
import {API_STATUS, DIALOG_TYPE} from '../../common/constants';
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import InputControl from '../common/inputControl'
import * as actionTypes from '../../constants/actionTypes';
import * as validations from '../../validations/common'
import isEqual from 'lodash';

class MedicalProcedureDialog extends Component {

    constructor(props) {
        super(props);

        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);
        this.onChange = this
            .onChange
            .bind(this);
        this.onSave = this
            .onSave
            .bind(this);
        this.onCancel = this
            .onCancel
            .bind(this);

        this.state = {
            data: this.props.data
                ? this.props.data
                : {},
            orgData: this.props.data
                ? this.props.data
                : {},
            errors: {}
        }

        this.inputControls = {};
    }

    hasUnsavedChanges() {
        return !_.isEqual(this.state.data, this.state.orgData);
    }

    componentDidUpdate()
    {
        if (this.showErr) {
            $(".error-list").show();
            this.showErr = false;
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.dialogShowing) {
            this.closing = false;
            $('#myModal').modal('show');

            if (nextProps.status == API_STATUS.IN_PROGRESS) {
                //do nothing
            } else if (nextProps.actionType == actionTypes.SAVE_PROCEDURE && nextProps.status == API_STATUS.DONE) {
                this.setState({successMessage: "Save medical procedure successfully.", processing: true});
                var closeDialog = this.props.layoutActions.closeDialog;
                self = this;
                setTimeout(function () {
                    if (!self.closing) 
                        closeDialog(true);
                    }
                , 3000);

            } else if (nextProps.status == API_STATUS.ERROR) {
                var message = "";
                if (nextProps.message) {
                    message = nextProps
                        .message
                        .replace("AlreadyExistsException: ", "")
                        .replace("IllegalArgumentException: ", "");
                }

                if (message.indexOf("protocol code") > 0) {
                    var controls = this.inputControls;
                    controls["code"].setError(message);
                    this.showErr = true;
                    this.setState({processing: false});
                } else {
                    this.setState({errorMessage: message, processing: false});
                }

            } else if (nextProps.layout.status == undefined || nextProps.layout.status == '') {
                // new dialog?
                var controls = this.inputControls;
                Object
                    .keys(controls)
                    .forEach(function (key) {
                        controls[key].clearError();
                    });

                if (nextProps.data) {
                    this.showHideWbSupplemental(nextProps.data.category);
                } else {
                    $("#wbSupplemental").hide();
                }

                this.setState({
                    id: nextProps.data && nextProps.data.id
                        ? nextProps.data.id
                        : '',
                    data: jQuery.extend(true, {}, nextProps
                        ? nextProps.data
                        : {}),
                    orgData: jQuery.extend(true, {}, nextProps
                        ? nextProps.data
                        : {}),
                    errors: {},
                    errorMessage: '',
                    successMessage: '',
                    processing: false,
                    clickSave: false,
                    clickCancel: false
                });

            }

        } else {
            this.closing = true;
            $('#myModal').modal('hide');
            this.setState({status: "", errorMessage: "", successMessage: ""});
        }
    }

    showHideWbSupplemental(category) {
        if (category == "Whole Blood") {
            $("#wbSupplemental").show();
        } else {
            $("#wbSupplemental").hide();
        }
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {
        const data = this.state.data;
        data[name] = value;

        if (name == "category") {
            this.showHideWbSupplemental(value);
            if (category != "Whole Blood") {
                data.wbSupplementalCompensation = null;
            }
        }

        this.setState({data: data});

    }

    saveData(autoSave) {
        if (this.state.processing) 
            return;
        
        this.setState({autoSave: autoSave});

        const data = this.state.data;
        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (key) {
                controls[key].validateAllRules(key, data[key]);
            });

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })
        if (numError == 0) {
            this.setState({clickSave: true, processing: true, errorMessage: "", successMessage: ""});
            this
                .props
                .actions
                .saveProcedure(this.state.data);
        } else {
            this.setState({errorMessage: "", successMessage: ""});
            $(".error-list").show();
        }
    }

    autoSave() {
        this.saveData(true);
    }

    onSave()
    {
        this.saveData(false);
    }

    onCancel() {
        this.setState({clickCancel: true});

        setTimeout(function () {
            $(".close-modal").click();
        }, 10);
    }

    render() {
        return (
            <div
                className="modal fade"
                id="myModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="myModalLabel">
                <div
                    className="modal-dialog popup-modal"
                    id={this.props.dialogId}
                    role="document">
                    <div className="modal-content  edit-popup-tooltip">
                        <div className="modal-header">

                            <div className="row">
                                <div className="col-sm-18 alpha-lg">
                                    <h4>{this.props.dialogType == DIALOG_TYPE.ADD_DATA
                                            ? 'Add New Medical Protocol'
                                            : 'Edit  Medical Protocol'}</h4>
                                </div>

                                <div className="col-sm-6">

                                    <a href="javascript:void(0)" data-dismiss="modal" className="close-modal">
                                        <span aria-hidden="true">×</span>
                                    </a>
                                </div>

                            </div>

                        </div>
                        <div className="modal-body">
                            <ErrorArea errorList={this.state.errors}/>
                            <ComonInfoArea
                                successMessage={this.state.successMessage}
                                errorMessage={this.state.errorMessage}/>

                            <div className="row">
                                <InputControl
                                    containerClass="form-group col-sm-24"
                                    type="text"
                                    name="name"
                                    label="Procedure Name"
                                    required={true}
                                    maxLength="60"
                                    emptyMessage="Procedure Name is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.name}
                                    ref
                                    ={(input) => {
                                    this.inputControls["name"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    type="text"
                                    name="code"
                                    maxLength="15"
                                    label="Code"
                                    required={true}
                                    emptyMessage="Code is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.code}
                                    ref
                                    ={(input) => {
                                    this.inputControls["code"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass=" form-group col-sm-12"
                                    showSelectOption={true}
                                    type="select"
                                    name="category"
                                    label="Category"
                                    required={true}
                                    emptyMessage="Category is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.category}
                                    ref
                                    ={(input) => {
                                    this.inputControls["category"] = input
                                }}
                                    optionValues={[
                                    [
                                        "Whole Blood", "Whole Blood"
                                    ],
                                    [
                                        "Bone Marrow", "Bone Marrow"
                                    ],
                                    [
                                        "LeukoPak", "LeukoPak"
                                    ],
                                    [
                                        "LeukoPak Mobilized", "LeukoPak Mobilized"
                                    ],
                                    [
                                        "Screening & Backup", "Screening & Backup"
                                    ],
                                    ["Injections & Others", "Injection & Others"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass=" form-group col-sm-24"
                                    showSelectOption={true}
                                    type="select"
                                    name="displayType"
                                    label="Display Type"
                                    required={true}
                                    emptyMessage="Display Type is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.displayType}
                                    ref
                                    ={(input) => {
                                    this.inputControls["displayType"] = input
                                }}
                                    optionValues={[
                                    [
                                        "Procedure", "procedure"
                                    ],
                                    [
                                        "Grouped_Procedure", "grouped_procedure"
                                    ],
                                    ["Appointment_Only", "appointment_only"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass=" form-group col-sm-12"
                                    showSelectOption={true}
                                    type="select"
                                    name="volumeUnit"
                                    label="Volume Unit"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.volumeUnit}
                                    ref
                                    ={(input) => {
                                    this.inputControls["volumeUnit"] = input
                                }}
                                    optionValues={[
                                    [
                                        "Adjustable", "Adjustable"
                                    ],
                                    [
                                        "Half Pak", "Half Pak"
                                    ],
                                    [
                                        "Full Pak", "Full Pak"
                                    ],
                                    [
                                        "50 ML", "50"
                                    ],
                                    [
                                        "100 ML", "100"
                                    ],
                                    ["30 ML", "30"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    type="text"
                                    name="eligibilityPeriodDays"
                                    label="Eligibility Period (Day)"
                                    maxLength="3"
                                    validate={validations.number}
                                    errorMessage="Eligibility Period must be a number"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.eligibilityPeriodDays}
                                    ref
                                    ={(input) => {
                                    this.inputControls["eligibilityPeriodDays"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    type="text"
                                    maxLength="4"
                                    name="volumeLimitPerCollection"
                                    label="Volume Limit Per Collection"
                                    validate={validations.number}
                                    errorMessage="Volume Limit Per Collection must be a number"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.volumeLimitPerCollection}
                                    ref
                                    ={(input) => {
                                    this.inputControls["volumeLimitPerCollection"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    type="text"
                                    name="procedureDurationMinutes"
                                    maxLength="3"
                                    label="Procedure Duration (mins)"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    validate={validations.number}
                                    errorMessage="Procedure Duration must be a number"
                                    value={this.state.data.procedureDurationMinutes}
                                    ref
                                    ={(input) => {
                                    this.inputControls["procedureDurationMinutes"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    type="text"
                                    name="compensationPerVolumeUnit"
                                    label="Payment/Volume Unit ($)"
                                    maxLength="6"
                                    validate={validations.decimal}
                                    errorMessage="Payment/Volume Unit must be a number with 2 decimal place"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.compensationPerVolumeUnit}
                                    ref
                                    ={(input) => {
                                    this.inputControls["compensationPerVolumeUnit"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    type="text"
                                    maxLength="4"
                                    name="glAccountNumber"
                                    label="GL Account"
                                    labelClass="ucase"
                                    validate={validations.number}
                                    errorMessage="GL Account must be a number"
                                    inputClass="form-control"
                                    value={this.state.data.glAccountNumber}
                                    ref
                                    ={(input) => {
                                    this.inputControls["glAccountNumber"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerId="wbSupplemental"
                                    containerClass="form-group col-sm-24"
                                    type="text"
                                    name="wbSupplementalCompensation"
                                    label="Whole Blood Supplemental Compensation"
                                    maxLength="6"
                                    validate={validations.number}
                                    errorMessage="Whole Blood Supplemental Compensation must be a number"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.wbSupplementalCompensation}
                                    ref
                                    ={(input) => {
                                    this.inputControls["wbSupplementalCompensation"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>
                                <InputControl
                                    containerClass="form-group col-sm-24"
                                    type="radio"
                                    name="status"
                                    label="Status"
                                    required={true}
                                    emptyMessage="Status is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.status}
                                    optionValues={[
                                    [
                                        "Active", "active"
                                    ],
                                    ["Inactive", "inactive"]
                                ]}
                                    ref
                                    ={(input) => {
                                    this.inputControls["status"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                            </div>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-group">
                                <a className="btn btn-white" onClick={this.onCancel}>
                                    Cancel</a>
                                <a
                                    disabled={this.state.processing}
                                    onClick={this.onSave}
                                    className="btn btn-white">
                                    Save Changes</a>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        );
    }
}

export default MedicalProcedureDialog;