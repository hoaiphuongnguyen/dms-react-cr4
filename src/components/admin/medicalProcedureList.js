import React, {Component} from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import {displayDate} from '../../utils/date.js'

class MedicalProcedureList extends Component {

    constructor(props) {
        super(props);

        this.items = this.props.list.items;

        this.state = {
            data: this.items,
            keyword: '',
            sortName: this.props.list.sortBy,
            sortOrder: this.props.list.sortOrder
        };

        this.onShowMore = this
            .onShowMore
            .bind(this);

        this
            .props
            .actions
            .loadMedicalProcedures(1, this.props.list.pageSize, this.props.list.sortBy, this.props.list.sortOrder, true);
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('manage-medical-profile');
        document
            .body
            .classList
            .add('inner');

        document.title = 'Manage Medical Protocol';
    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('manage-medical-profile');
        document
            .body
            .classList
            .remove('inner');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.list.needRefresh) {
            this
                .props
                .actions
                .loadMedicalProcedures(1, nextProps.list.pageSize, nextProps.list.sortBy, nextProps.list.sortOrder, true);
        } else {
            this.items = nextProps.list.items;

            this.setState({data: this.items, sortName: this.props.list.sortBy, sortOrder: this.props.list.sortOrder});
        }

    }

    onShowMore() {
        if (this.props.list.items.length < this.props.list.totalRecords) {
            this
                .props
                .actions
                .loadMedicalProcedures(++this.props.list.page, this.props.list.pageSize, this.props.list.sortBy, this.props.list.sortOrder, false);
        }
    }

    onSortChange(sortName, sortOrder) {
        this
            .props
            .actions
            .loadMedicalProcedures(1, this.props.list.pageSize, sortName, sortOrder, true);
    }

    buttonEdit(cell, row) {
        var f = this.props.layoutActions.openEditProcedureDialog;

        return (
            <a
                href="javascript:void(0)"
                data-toggle="tooltip"
                title="Edit Medical Procedure"
                onClick={function () {
                f(row);
            }}>
                <img
                    className="icon-svg"
                    src="/img/icons/searchdonaricons/icon-edit.svg"
                    alt="edit"
                    width="18"/></a>
        );
    }

    render() {

        let options = {
            onSortChange: this
                .onSortChange
                .bind(this)
        };

        let table = null;
        let showMore = null;
        if (this.props.list.items) {
            table = <BootstrapTable
                ref='table'
                data={this.state.data}
                options={options}
                search
                tableContainerClass="donortable"
                bordered={false}>
                <TableHeaderColumn dataField='name' isKey dataSort>Procedure name</TableHeaderColumn>
                <TableHeaderColumn dataField='code' dataSort>Code</TableHeaderColumn>
                <TableHeaderColumn dataField='category' dataSort>Category</TableHeaderColumn>
                <TableHeaderColumn dataField='displayType' dataSort>Display Type</TableHeaderColumn>
                <TableHeaderColumn dataField='volumeUnit' dataSort>Vol unit</TableHeaderColumn>
                <TableHeaderColumn dataField='compensationPerVolumeUnit' dataSort>Payment/vol Unit(s)</TableHeaderColumn>
                <TableHeaderColumn dataField='glAccountNumber' dataSort>GL account</TableHeaderColumn>
                <TableHeaderColumn dataField='status' dataSort>Status</TableHeaderColumn>
                <TableHeaderColumn dataField='createdAt' dataFormat={displayDate} dataSort>Created on</TableHeaderColumn>
                <TableHeaderColumn
                    dataField="button"
                    width='5%'
                    dataFormat={this
                    .buttonEdit
                    .bind(this)}
                    dataAlign="center"
                    headerTitle={false}>&nbsp;</TableHeaderColumn>
            </BootstrapTable>

            if (this.props.list.items.length < this.props.list.totalRecords) {
                showMore = <div className="col-sm-24 text-right showmore hide-table">
                    <a href="javascript:void(0)" onClick={this.onShowMore}>Show More</a>
                </div>
            }

        }

        var o = this.props.layoutActions.openAddProcedureDialog;
        return (
            <div id="schedule-appointment" className="clearfix">

                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>
                            <a href="javascript:void(0)">System Administration</a>
                        </li>
                        <li className="active">Manage Medical Protocol</li>
                    </ol>
                </div>

                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Manage Medical Protocol</h2>
                    </div>
                </div>

                <div className="search-wrapper">
                    <div className="col-sm-24 collapse-sm">
                        <h4 className="icon-container">Medical Protocols
                            <a
                                href="javascript:void(0)"
                                title="Add Medical Procedure"
                                onClick={function () {
                                o();
                            }}
                                data-toggle="modal"
                                data-target="#myModal">
                                <img src="/img/icons/createdonoricons/icon_add.png" alt=""/></a>
                        </h4>
                    </div>
                    <div
                        className="col-xs-24 col-sm-24 col-md-24 col-lg-24 collapse-sm table-responsive">
                        {table}
                    </div>

                    {showMore}

                    <div className="clear"></div>

                </div>

            </div>

        );
    }

}

export default MedicalProcedureList;