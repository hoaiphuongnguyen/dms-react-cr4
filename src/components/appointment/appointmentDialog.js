import React, {Component} from 'react';
import {API_STATUS, DIALOG_TYPE} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import InputControl from '../common/inputControl'
import * as validations from '../../validations/common'
import isEqual from 'lodash';

class AppointmentDialog extends Component {

    constructor(props) {
        super(props);

        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);
        this.onChange = this
            .onChange
            .bind(this);
        this.onBlur = this
            .onBlur
            .bind(this);
        this.onEnter = this
            .onEnter
            .bind(this);

        this.onSearchDonor = this
            .onSearchDonor
            .bind(this);

        this.onSave = this
            .onSave
            .bind(this);

        this.onSaveCancelAppointment = this
            .onSaveCancelAppointment
            .bind(this);

        this.onCancel = this
            .onCancel
            .bind(this);

        this.state = {
            searchDonor: {
                name: null,
                email: null,
                donorId: null
            },
            data: this.props.data
                ? this.props.data
                : {},
            orgData: this.props.data
                ? this.props.data
                : {},
            errors: {},
            allMedicalProcedures: []
        }

        this.id = null;
        this.loadingScheduleResquest = false;
        this.cancelLoadingScheduleResquest = false;
        this.changeScheduleResquest = false;
        this.inputControls = {};

        this.wbProcedureId = '';
        this.procedures = [];
        this.procedureId = null;
        this.procedureLabel = "Procedure Name";
        this.procedureEmptyMessage = "Procedure Name is required";

        this.listDonor = this.props.donors
            ? this.props.donors
            : [];
    }

    hasUnsavedChanges() {
        return !_.isEqual(this.state.data, this.state.orgData);
    }

    componentDidMount() {
        var self = this;
        $('#appointmentModal').on('hide.bs.modal', function (e) {
            if (self.loadingScheduleResquest || self.changeScheduleResquest) 
                self.cancelLoadingScheduleResquest = true;
            else 
                self.cancelLoadingScheduleResquest = false;
            
            self.changeScheduleResquest = false;
        })
    }

    componentDidUpdate()
    {
        if (this.showErr) {
            $(".error-list").show();
            this.showErr = false;
        }
    }

    buildProcedureOptions(allMedicalProcedures) {

        this.procedures = [];
        var self = this;
        allMedicalProcedures.forEach(function (element) {
            if (self.procedureCategory == element.category) {
                if (self.procedureCategory == "Whole Blood") {
                    self.wbProcedureId = element.id;
                } else {
                    self
                        .procedures
                        .push([element.name, element.id]);
                }
            }
        });

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dialogShowing) {
            this.closing = false;
            $('#appointmentModal').modal('show');

            if (nextProps.status == API_STATUS.IN_PROGRESS) {
                //do nothing?
            } else if (nextProps.actionType == actionTypes.SEARCH_DONOR_FOR_APPOINTMENT && nextProps.status == API_STATUS.DONE) {
                this.listDonor = nextProps.donors;
                $(".hide-table").show();
            } else if (!this.cancelLoadingScheduleResquest && nextProps.actionType == actionTypes.GET_SCHEDULING_REQUEST && nextProps.status == API_STATUS.DONE) {

                const data = this.state.data;
                data.scheduleRequest = jQuery.extend(true, {}, nextProps.scheduleRequest.data);
                this.loadingScheduleResquest = false;

                if (this.procedureCategory != "Injection & Others") {
                    this.procedureId = nextProps.scheduleRequest.data.procedureInfo.id;
                    data.procedureInfo = jQuery.extend(true, {}, nextProps.scheduleRequest.data.procedureInfo);
                    this.procedureCategory = data.scheduleRequest.procedureInfo.category;
                    this.showHideVolumnOrSubType(this.procedureCategory);
                }

                this.setState({data: data});

                if (this.waitingToSave) {
                    this.saveData(false);
                }

            } else if (!this.cancelLoadingScheduleResquest && nextProps.actionType == actionTypes.GET_SCHEDULING_REQUEST && nextProps.status == API_STATUS.ERROR) {
                if (nextProps.scheduleRequest.data.message) {
                    this
                        .inputControls["requestId"]
                        .setError(nextProps.scheduleRequest.data.message);
                } else {
                    this
                        .inputControls["requestId"]
                        .setError(nextProps.scheduleRequest.data);
                }

                this.showErr = true;

                this.loadingScheduleResquest = false;
                if (this.waitingToSave) {
                    this.waitingToSave = false;
                    this.setState({processing: false});
                }
            } else if (nextProps.actionType == actionTypes.SAVE_APPOINTMENT && nextProps.status == API_STATUS.DONE) {
                var closeDialog = this.props.layoutActions.closeAppointmentDialog;
                this.setState({successMessage: "Save appointment successfully."});
                setTimeout(function () {
                    closeDialog(true, nextProps.data);
                }, 3000);

            } else if (nextProps.actionType == actionTypes.CANCEL_APPOINTMENT && nextProps.status == API_STATUS.DONE) {
                var closeDialog = this.props.layoutActions.closeAppointmentDialog;
                this.setState({successMessage: "Cancel appointment successfully."});
                setTimeout(function () {
                    closeDialog(true, nextProps.data);
                }, 3000);

            } else if ((nextProps.actionType == actionTypes.SAVE_APPOINTMENT || nextProps.actionType == actionTypes.CANCEL_APPOINTMENT) && nextProps.status == API_STATUS.ERROR) {
                var message = "";
                if (typeof(nextProps.message) == "string") {

                    message = nextProps
                        .message
                        .replace("AlreadyExistsException: ", "")
                        .replace("IllegalArgumentException: ", "");

                    if (message.indexOf("request") >= 0) {
                        this
                            .inputControls["requestId"]
                            .setError(message);
                        this.setState({processing: false});
                        this.showErr = true;
                    } else {
                        this.setState({errorMessage: message, processing: false});
                    }
                }

            } else if (nextProps.layout.status == undefined || nextProps.layout.status == '') {
                //new dialog?
                if (!this.didMountCalling && this.props.needReloadAllProcedures) {
                    this
                        .props
                        .loadAllMedicalProcedures();
                }
                this.didMountCalling = false;
                this.loadProcedure = false;
                this.loadingScheduleResquest = false;

                if (nextProps.dialogData) {
                    this.id = nextProps.dialogData.id;
                    this.procedureCategory = nextProps.dialogData.procedureInfo.category;
                    this.procedureId = nextProps.dialogData.procedureInfo.id;

                } else {
                    this.collectionSite = nextProps.collectionSite;
                    this.id = null;
                    this.donorId = nextProps.donorId;
                    this.procedureCategory = nextProps.procedureCategory;
                    this.procedureId = null;
                }
                this.setState({
                    searchDonor: {
                        name: null,
                        email: null,
                        donorId: null
                    },
                    id: nextProps.dialogData && nextProps.dialogData.id
                        ? nextProps.dialogData.id
                        : '',
                    data: nextProps.dialogData
                        ? jQuery.extend(true, {}, nextProps.dialogData)
                        : {
                            donor: {
                                id: nextProps.donorId
                            },
                            procedureInfo: {},
                            scheduleRequest: {},
                            startTime: nextProps.startTime,
                            endTime: nextProps.endTime
                        },
                    orgData: nextProps.dialogData
                        ? jQuery.extend(true, {}, nextProps.dialogData)
                        : {
                            donor: {
                                id: nextProps.donorId
                            },
                            procedureInfo: {},
                            scheduleRequest: {},
                            startTime: nextProps.startTime,
                            endTime: nextProps.endTime
                        },
                    errors: {},
                    errorMessage: '',
                    successMessage: '',
                    processing: false,
                    clickSave: false,
                    clickCancel: false
                });

                var procedureCategory = this.procedureCategory;

                this.showHideVolumnOrSubType(procedureCategory);
                setTimeout(function () {
                    $(".hide-table").hide();
                }, 50);

                var controls = this.inputControls;
                Object
                    .keys(controls)
                    .forEach(function (key) {
                        if (controls[key]) 
                            controls[key].clearError();
                        }
                    );

                if (nextProps.allMedicalProcedures && nextProps.allMedicalProcedures.length) {
                    this.setState({allMedicalProcedures: nextProps.allMedicalProcedures});
                    this.buildProcedureOptions(nextProps.allMedicalProcedures);
                } else {
                    this.buildProcedureOptions(this.state.allMedicalProcedures);
                }

            }

        } else {
            this.closing = true;
            $('#appointmentModal').modal('hide');
            this.setState({status: "", successMessage: "", errorMessage: ""});
        }
    }

    showHideVolumnOrSubType(procedureCategory) {
        setTimeout(function () {
            $("#resource_title").text(procedureCategory);
            if (procedureCategory == "Screening & Backup") {
                $("#labelVolume").hide();
                $("#other").hide();
                $("#screeningSubType").show();
            } else if (procedureCategory == "Injection & Others") {
                $("#labelVolume").hide();
                $("#other").show();
                $("#screeningSubType").hide();
            } else {
                $("#labelVolume").show();
                $("#other").hide();
                $("#screeningSubType").hide();
            }
        }, 50);
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        if (name != "requestId" || this.changeScheduleResquest || (this.state.errors[name] && this.state.errors[name].indexOf("not found") < 0)) {
            const errors = this.state.errors;
            errors[name] = "";
            this.setState({errors: errors})
        }
    }

    getScheduleResquest(name, value) {
        if (this.cancelLoadingScheduleResquest) {
            return;
        }

        if (this.changeScheduleResquest) {
            this.changeScheduleResquest = false;
            this
                .inputControls[name]
                .validateAllRules(name, value);
            if (this.state.errors[name]) {
                $(".error-list").show();
            } else {
                this.loadingScheduleResquest = true;
                this
                    .props
                    .getSchedulingRequest(value);
            }
        } else {
            if (this.state.errors[name]) {
                $(".error-list").show();
            }
        }
    }

    onBlur(name, value) {
        if (name == "requestId" && value != '') {
            this.getScheduleResquest(name, value);
        }
    }

    onEnter(name, value) {
        if (name == "searchDonorName" || name == "searchDonorEmail" || name == "searchDonorId") {
            this.onSearchDonor();
        } else if (name == "requestId") {
            this.getScheduleResquest(name, value);
        }
    }

    onChange(name, value) {
        if (name == "searchDonorName" || name == "searchDonorEmail" || name == "searchDonorId") {
            const searchDonor = this.state.searchDonor;
            if (name == "searchDonorName") {
                searchDonor["name"] = value;
            } else if (name == "searchDonorEmail") {
                searchDonor["email"] = value;
            } else {
                searchDonor["donorId"] = value;
            }
            this.setState({searchDonor: searchDonor});
        } else {
            const data = this.state.data;
            if (name == "donorId") {
                data.donor["id"] = value;
            } else if (name == "requestId") {
                data.scheduleRequest["id"] = value.trim();
                this.changeScheduleResquest = true;
            } else if (name == "procedureId") {
                data.procedureInfo["id"] = value;
            } else {
                data[name] = value;
            }
            this.setState({data: data});
        }
    }

    onSearchDonor() {
        $(".hide-table").show();
        this
            .props
            .actions
            .searchDonor(this.collectionSite, this.state.searchDonor.name, this.state.searchDonor.email, this.state.searchDonor.donorId);
    }

    saveData(autoSave, isCancel) {

        this.setState({autoSave: autoSave});

        var data = this.state.data;
        var controls = this.inputControls;

        var isNeedScheduleRequest;

        if (this.procedureCategory == "Injection & Others") {
            isNeedScheduleRequest = false;
        } else {
            isNeedScheduleRequest = true;
        }

        Object
            .keys(controls)
            .forEach(function (key) {
                if (controls[key]) {
                    if (key == "procedureId") {
                        controls[key].validateAllRules(key, data.procedureInfo.id
                            ? data.procedureInfo.id.toString()
                            : null);
                    } else if (key == "requestId" && !isCancel) {
                        if (isNeedScheduleRequest && (!data.scheduleRequest.id || data.scheduleRequest.id == "")) {
                            controls["requestId"].setError("Please input schedule request");
                        }
                    } else {
                        controls[key].validateAllRules(key, data[key]);
                    }
                }
            });

        if (!isCancel && (!data.donor.id || data.donor.id == "")) {
            this.onInvalid("donorId", "Please search and select a donor");
        }

        var errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })

        if (numError == 0) {
            this.setState({clickSave: true, processing: true, errorMessage: "", successMessage: ""});
            if (!data.id && this.id && this.id != "") {
                data.id = this.id;
            }

            if (!this.loadingScheduleResquest && !this.changeScheduleResquest) {
                this.waitingToSave = false;
                this
                    .props
                    .actions
                    .saveAppointment(data, isCancel, false);
            } else {
                this.waitingToSave = true;
            }
        } else {
            this.setState({errorMessage: "", successMessage: ""});
            $(".error-list").show();
        }
    }

    autoSave() {
        if ($("#cancelComment").is(":visible ")) {
            this.saveData(true, true);
        } else {
            this.saveData(true, false);
        }
    }

    onSave() {
        this.saveData(false, false);
    }

    onSaveCancelAppointment() {
        this.saveData(false, true);
    }

    onCancel() {
        this.setState({clickCancel: true});

        setTimeout(function () {
            $("#close-modal-appoiment").click();
        }, 10);
    }

    renderDonorRows() {
        const items = this.listDonor;

        if (items) {
            var self = this;
            var chooseDonor = function (donorId) {
                self.onChange("donorId", donorId);
                self.onValid("donorId");
                $(".hide-check").show();
            };

            return (items.map((item, i) => (
                <tr key={item.id}>
                    <td
                        style={{
                        whiteSpace: "nowrap"
                    }}><input
                        name="donorId"
                        type="radio"
                        onClick={function () {
                    chooseDonor(item.id);
                }}
                        value={item.id}/>
                        <span className="check-text">
                            {item.id}</span>
                    </td>
                    <td
                        style={{
                        wordBreak: "break-all"
                    }}>{item.email}</td>
                    <td
                        style={{
                        wordBreak: "break-work"
                    }}>{item.firstName} {item.lastName}</td>
                </tr>

            )))
        }
        return null;
    }

    renderSearchDonorResult() {
        return (
            <div className="hide-table">
                <div className="table-responsive">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.renderDonorRows()}
                        </tbody>
                    </table>
                </div>
            </div>

        );
    }

    renderSearchDonorForm() {
        if (!this.donorId || this.donorId == '') {
            return (
                <div className="form-group">
                    <label className="ucase pull-left-lg">Select Donor For Appointment</label>
                    <a
                        href="javascript:void(0)"
                        className="pull-right-lg"
                        id="open_table"
                        onClick={this.onSearchDonor}>
                        <i className="fa fa-search search-icon"></i>
                    </a>

                    <InputControl
                        containerClass="form-group"
                        ref
                        ={(input) => {
                        this.inputControls["searchDonorName"] = input
                    }}
                        name="searchDonorName"
                        placeholder="Name"
                        label=""
                        labelClass="ucase"
                        inputClass="form-control"
                        maxLength="35"
                        value={this.state.searchDonor && this.state.searchDonor.name
                        ? this.state.searchDonor.name
                        : ''}
                        onChange={this.onChange}
                        onEnter={this.onEnter}
                        onInvalid={this.onInvalid}
                        onValid={this.onValid}/>

                    <InputControl
                        containerClass="form-group"
                        ref
                        ={(input) => {
                        this.inputControls["searchDonorEmail"] = input
                    }}
                        name="searchDonorEmail"
                        label=""
                        placeholder="Email"
                        labelClass="ucase"
                        inputClass="form-control"
                        maxLength="32"
                        value={this.state.searchDonor.email
                        ? this.state.searchDonor.email
                        : ''}
                        onChange={this.onChange}
                        onEnter={this.onEnter}
                        onInvalid={this.onInvalid}
                        onValid={this.onValid}/>
                    <label className="text-center">or</label>
                    <InputControl
                        containerClass="form-group"
                        ref
                        ={(input) => {
                        this.inputControls["searchDonorId"] = input
                    }}
                        name="searchDonorId"
                        label=""
                        placeholder="Donor ID (Forced Assignment)"
                        labelClass="ucase"
                        inputClass="form-control"
                        maxLength="32"
                        value={this.state.searchDonor.donorId
                        ? this.state.searchDonor.donorId
                        : ''}
                        onChange={this.onChange}
                        onEnter={this.onEnter}
                        onInvalid={this.onInvalid}
                        onValid={this.onValid}/> {this.renderSearchDonorResult()}

                </div>
            );
        } else {
            setTimeout(function () {
                $(".hide-check").show();
            }, 50);

        }
    }

    renderProcedureVolume() {
        if (this.state.data.scheduleRequest) {
            var request = this.state.data.scheduleRequest;
            if (request.procedureInfo && (request.procedureInfo.category == "Whole Blood" || request.procedureInfo.category == "Bone Marrow")) {
                return request.volume + " ml";
            } else if (request.procedureInfo && (request.procedureInfo.category == "Injection & Others" || request.procedureInfo.category == "Screening & Backup")) {
                return request.procedureInfo.name;
            } else {
                if (request.volume) 
                    return request.volume;
                else if (request.procedureInfo && request.procedureInfo.volumeUnit) 
                    return request.procedureInfo.volumeUnit;
                }
            }
        return null;

    }

    renderAddAppointment() {
        var requestIdlabel;
        if (this.procedureCategory == "Injection & Others") {
            requestIdlabel = "Related Schedule Request # (if any)";
        } else {
            requestIdlabel = "Related Schedule Request #";
        }
        return (
            <div
                className="modal fade"
                id="appointmentModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="appointmentModalLabel">
                <div
                    className="modal-dialog popup-modal"
                    id={this.props.dialogId}
                    role="document">
                    <div className="modal-content">
                        <div>
                            <div className="modal-header">
                                <div className="row">
                                    <div className="col-sm-20 alpha-lg">
                                        <h4>Add New Appointment</h4>
                                    </div>

                                    <div className="col-sm-4">
                                        <a
                                            href="javascript:void(0)"
                                            data-dismiss="modal"
                                            id="close-modal-appoiment"
                                            className="close-modal">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>

                                </div>

                            </div>
                            <div className="modal-body">
                                <ErrorArea errorList={this.state.errors}/>
                                <ComonInfoArea
                                    successMessage={this.state.successMessage}
                                    errorMessage={this.state.errorMessage}/>
                                <div className="form-group">
                                    <label className="ucase">Procedure
                                    </label>
                                    <label className="fs-16" id="resource_title"></label>
                                </div>

                                <InputControl
                                    containerClass="form-group"
                                    ref
                                    ={(input) => {
                                    this.inputControls["requestId"] = input
                                }}
                                    name="requestId"
                                    label={requestIdlabel}
                                    validate={validations.number}
                                    errorMessage="Related Schedule Request must be a number"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="10"
                                    value={this.state.data.scheduleRequest && this.state.data.scheduleRequest.id
                                    ? this.state.data.scheduleRequest.id
                                    : ''}
                                    onBlur={this.onBlur}
                                    onChange={this.onChange}
                                    onEnter={this.onEnter}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="form-group" id="labelVolume">
                                    <label className="ucase">Volume
                                    </label>
                                    <label className="fs-16" id="volume">{this.renderProcedureVolume()}</label>
                                </div>

                                <div className="form-group" id="screeningSubType">
                                    <label className="ucase">Procedure Name
                                    </label>
                                    <label className="fs-16" id="volume">{this.renderProcedureVolume()}</label>
                                </div>

                                <InputControl
                                    containerId="other"
                                    containerClass="form-group"
                                    type="select"
                                    showSelectOption={true}
                                    ref
                                    ={(input) => {
                                    this.inputControls["procedureId"] = input
                                }}
                                    name="procedureId"
                                    label={this.procedureLabel}
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    required={true}
                                    emptyMessage={this.procedureEmptyMessage}
                                    optionValues={this.procedures}
                                    value={this.state.data && this.state.data.procedureInfo
                                    ? this.state.data.procedureInfo.id
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/> {this.renderSearchDonorForm()}

                            </div>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-group">
                                <a
                                    disabled={this.state.processing}
                                    onClick={this.onCancel}
                                    className={"btn btn-white" + (this.state.processing
                                    ? " disabled"
                                    : "")}>
                                    Cancel</a>
                                <a
                                    disabled={this.state.processing}
                                    onClick={this.onSave}
                                    className={"btn btn-white" + (this.state.processing
                                    ? " disabled"
                                    : "")}>
                                    Add Appointment</a>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    }

    renderEditAppointment() {

        return (
            <div
                className="modal fade"
                id="appointmentModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="appointmentModalLabel">
                <div
                    className="modal-dialog popup-modal"
                    id={this.props.dialogId}
                    role="document">
                    <div className="modal-content">
                        <div>
                            <div className="modal-header">
                                <div className="row">
                                    <div className="col-sm-18 alpha-lg">
                                        <h4>Edit Appointment</h4>
                                    </div>

                                    <div className="col-sm-6">
                                        <a
                                            href="javascript:void(0)"
                                            data-dismiss="modal"
                                            id="close-modal-appoiment"
                                            className="close-modal">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>

                                </div>

                            </div>
                            <div className="modal-body">
                                <ErrorArea errorList={this.state.errors}/>
                                <ComonInfoArea
                                    successMessage={this.state.successMessage}
                                    errorMessage={this.state.errorMessage}/>
                                <div className="form-group">
                                    <label className="ucase">Procedure
                                    </label>
                                    <label className="fs-16" id="resource_title">
                                        {this.state.data.procedureInfo
                                            ? this.state.data.procedureInfo.name
                                            : ''}</label>
                                </div>

                                <div className="form-group" id="labelVolume">
                                    <label className="ucase">Volume
                                    </label>
                                    <label className="fs-16" id="volume">{this.renderProcedureVolume()}</label>
                                </div>

                                <InputControl
                                    containerId="other"
                                    containerClass="form-group"
                                    type="select"
                                    showSelectOption={true}
                                    ref
                                    ={(input) => {
                                    this.inputControls["procedureId"] = input
                                }}
                                    name="procedureId"
                                    label={this.procedureLabel}
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    required={true}
                                    emptyMessage={this.procedureEmptyMessage}
                                    optionValues={this.procedures}
                                    defaultValue={this.procedureId}
                                    value={this.state.data && this.state.data.procedureInfo
                                    ? this.state.data.procedureInfo.id
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group"
                                    type="textarea"
                                    ref
                                    ={(input) => {
                                    this.inputControls["comment"] = input
                                }}
                                    name="comment"
                                    label="Comments"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="100"
                                    value={this.state.data.comment}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                            </div>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-group">
                                <a
                                    disabled={this.state.processing}
                                    onClick={this.onCancel}
                                    className={"btn btn-white" + (this.state.processing
                                    ? " disabled"
                                    : "")}>
                                    Cancel</a>
                                <a
                                    disabled={this.state.processing}
                                    onClick={this.onSave}
                                    className={"btn btn-white" + (this.state.processing
                                    ? " disabled"
                                    : "")}>
                                    Save Changes</a>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        );

    }

    renderCancelAppointment() {
        return (
            <div
                className="modal fade"
                id="appointmentModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="appointmentModalLabel">
                <div
                    className="modal-dialog popup-modal"
                    id={this.props.dialogId}
                    role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <div className="row">
                                <div className="col-sm-18 alpha-lg">
                                    <h4>Cancel Appointment</h4>
                                </div>

                                <div className="col-sm-6">
                                    <a
                                        href="javascript:void(0)"
                                        data-dismiss="modal"
                                        id="close-modal-appoiment"
                                        className="close-modal">
                                        <span aria-hidden="true">&times;</span>
                                    </a>
                                </div>

                            </div>

                        </div>
                        <div className="modal-body">
                            <ErrorArea errorList={this.state.errors}/>
                            <ComonInfoArea
                                successMessage={this.state.successMessage}
                                errorMessage={this.state.errorMessage}/>
                            <div className="form-group">
                                <label className="ucase">Procedure
                                </label>
                                <label className="fs-16">
                                    {this.state.data.scheduleRequest && this.state.data.scheduleRequest.procedureInfo
                                        ? this.state.data.scheduleRequest.procedureInfo.name
                                        : ''}</label>
                            </div>

                            <InputControl
                                containerClass="form-group"
                                showSelectOption={true}
                                type="select"
                                ref
                                ={(input) => {
                                this.inputControls["cancelReason"] = input
                            }}
                                name="cancelReason"
                                label="Reason For Cancellation"
                                required={true}
                                emptyMessage="Reason For Cancellation is required"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.data.cancelReason}
                                optionValues={[
                                [
                                    "Cancelled By Donor", "Cancelled By Donor"
                                ],
                                [
                                    "Cancelled Work Order", "Cancelled Work Order"
                                ],
                                [
                                    "Work Order Fullfilled", "Work Order Fullfilled"
                                ],
                                [
                                    "Donor Failed Screening", "Donor Failed Screening"
                                ],
                                ["Donor Disqualified", "Donor Disqualified"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass=" form-group"
                                type="textarea"
                                ref
                                ={(input) => {
                                this.inputControls["cancelComment"] = input
                            }}
                                name="cancelComment"
                                label="Comments"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="100"
                                value={this.state.data.cancelComment}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-group">
                                <a
                                    onClick={this.onCancel}
                                    className={"btn btn-white" + (this.state.processing
                                    ? " disabled"
                                    : "")}>
                                    Cancel</a>
                                <a
                                    disabled={this.state.processing}
                                    onClick={this.onSaveCancelAppointment}
                                    className={"btn btn-white" + (this.state.processing
                                    ? " disabled"
                                    : "")}>
                                    Save Changes</a>

                            </div>
                        </div>

                    </div>

                </div>
            </div>

        );
    }

    render() {
        if (this.props.dialogType == DIALOG_TYPE.ADD_DATA) {
            return this.renderAddAppointment();
        } else if (this.props.dialogType == DIALOG_TYPE.EDIT_DATA) {
            return this.renderEditAppointment();
        } else {
            return this.renderCancelAppointment();
        }
    }
}

export default AppointmentDialog