import React, {Component} from 'react';
import {API_STATUS, DIALOG_TYPE, PATH_PREFIX} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import InputControl from '../common/inputControl'
import {hashHistory} from 'react-router';
import ComonInfoArea from '../common/comonInfoArea'

class AppointmentPopup extends Component {

    constructor(props) {
        super(props);

        this.state = {
            processing: false,
            id: '',
            data: this.props.data
                ? this.props.data
                : {}
        }

        this.onCheckOut = this
            .onCheckOut
            .bind(this);

        this.onClose = this
            .onClose
            .bind(this);

        this.inputControls = {};

        this.getAppointmentComplete = false;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dialogShowing) {
            $(".popup-tooltip").addClass('active');
            if (nextProps.status == API_STATUS.IN_PROGRESS) {
                //donothing
            }
            if (nextProps.actionType == actionTypes.CHECK_OUT_APPOINTMENT && nextProps.status == API_STATUS.DONE) {
                let data = this.state.data;
                data.status = "checked_out";
                this.setState({data: data, successMessage: "Check out appointment successfully.", processing: false});

                $("#check_out").hide();
                var closeAppointmentPopup = this.props.layoutActions.closeAppointmentPopup;
                setTimeout(function () {
                    $(".popup-tooltip").removeClass('active');
                    closeAppointmentPopup(true);
                }, 3000);
            } else if (!this.getAppointmentComplete && nextProps.actionType == actionTypes.GET_APPOINTMENT && nextProps.status == API_STATUS.DONE) {
                this.setState({
                    data: jQuery.extend(true, {}, nextProps.data)
                });

                this.getAppointmentComplete = true;
                $("#check_out").show();
            } else if ((nextProps.actionType == actionTypes.GET_APPOINTMENT || nextProps.actionType == actionTypes.CHECK_OUT_APPOINTMENT) && nextProps.status == API_STATUS.ERROR) {
                if (nextProps.actionType == actionTypes.CHECK_OUT_APPOINTMENT) 
                    $("#check_out").toggleClass("pointer-cursor");
                
                var message = "";
                if (typeof(nextProps.message) == "string") {
                    message = nextProps
                        .message
                        .replace("AlreadyExistsException: ", "")
                        .replace("IllegalArgumentException: ", "");
                    this.setState({errorMessage: message, processing: false});
                }
            } else if ((typeof(nextProps.dialogData) == "string" || typeof(nextProps.dialogData) == "number") && nextProps.dialogData != this.state.id) {
                //new popup?
                var appointmentId = nextProps.dialogData;
                if (appointmentId) {
                    this.setState({id: appointmentId, processing: false});
                    this.getAppointmentComplete = false
                    this
                        .props
                        .actions
                        .getAppointment(appointmentId);
                }
            } else {
                if (nextProps.needRefresh && nextProps.data) {
                    /*  this.setState({
                        data: jQuery.extend(true, {}, nextProps.data)
                    });*/
                    this.getAppointmentComplete = false;
                    if (nextProps.data.id) {
                        this
                            .props
                            .actions
                            .getAppointment(nextProps.data.id);
                    }
                }
            }

        } else {
            this.closing = true;
            $(".popup-tooltip").removeClass('active');
            this.setState({status: "", successMessage: "", errorMessage: ""});
        }

    }

    onClose() {
        $(".popup-tooltip").removeClass('active');
        this
            .props
            .layoutActions
            .closeAppointmentPopup();
    }

    onCheckOut() {
        if (this.state.processing) 
            return;
        
        $("#check_out").toggleClass("pointer-cursor");
        this.setState({processing: true, errorMessage: "", successMessage: ""});
        this
            .props
            .actions
            .checkOut(this.state.id);
    }

    renderCheckOutButton() {
        var canCheckin = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 2;

        if (canCheckin && this.state.data.startTime && this.state.data.status && this.state.data.status.toLowerCase() != 'checked_out' && this.state.data.status.toLowerCase() != "cancelled" && this.state.data.status.toLowerCase() != "completed") {
            var startDate = moment(this.state.data.startTime)
                .clone()
                .hour(0)
                .minute(0)
                .second(0)
                .millisecond(0);

            if ((startDate.format("MM/DD/YYYY") == moment().format("MM/DD/YYYY") && (this.state.data.status.toLowerCase() != 'checked_out'))) {
                return (
                    <div className="col-sm-12 omega-lg">
                        <div className="btn-group-x pull-right">
                            <a
                                id="check_out"
                                className="btn btn-grouped pointer-cursor"
                                data-toggle="tooltip"
                                data-placement="top"
                                onClick={this.onCheckOut}
                                title="Check Out">
                                <img
                                    src="/img/icons/createschedulingrequest/icon-check-in.svg"
                                    alt=""
                                    width="22"/>
                            </a>

                        </div>
                    </div>
                )
            }
        }
        return null;

    }

    renderCancelAppointmentButton(data) {
        var canEditAppt = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 2;

        if (canEditAppt && !this.state.processing && this.state.data.status && this.state.data.status.toLowerCase() != 'checked_out' && this.state.data.status.toLowerCase() != "cancelled" && this.state.data.status.toLowerCase() != "completed") {
            var openCancelAppointmentDialog = this.props.layoutActions.openCancelAppointmentDialog;
            return (
                <a
                    href="javascript:void(0)"
                    className="btn btn-white"
                    onClick={function () {
                    openCancelAppointmentDialog(data);
                }}>
                    Cancel Appointment</a>
            );
        } else {
            return (
                <a className="btn btn-white disabled">
                    Cancel Appointment</a>
            );
        }
    }

    renderEditAppoitmentButton(openEditAppointmentDialog, data, canEditAppt)
    {
        if (canEditAppt && !this.state.processing && data.status && data.status.toLowerCase() != "cancelled") {
            return (
                <a
                    href="javascript:void(0)"
                    className="btn btn-white"
                    onClick={function () {
                    openEditAppointmentDialog(data);
                }}>
                    Edit Appointment
                </a>
            );
        } else {
            return (
                <a className="btn btn-white disabled">
                    Edit Appointment
                </a>
            );
        }
    }
    renderProcedureVolume() {
        if (this.state.data.scheduleRequest) {
            var volume;
            if (this.state.data.procedureInfo.category == "Whole Blood" || this.state.data.procedureInfo.category == "Bone Marrow") {
                volume = this.state.data.scheduleRequest.volume + " ml";
            } else {
                volume = this.state.data.scheduleRequest.volume;
            }
            return (
                <p>Volume:&nbsp;
                    <strong>
                        {volume}</strong>
                </p>
            );
        }

    }

    render() {
        var self = this;
        var data = this.state.data;

        var openEditAppointmentDialog = this.props.layoutActions.openEditAppointmentDialog;

        var donorId = data.donor
            ? data.donor.id
            : '';

        var editDonor = function () {
            self.setState({id: null});
            self
                .props
                .layoutActions
                .closeAppointmentPopup();
            hashHistory.push(PATH_PREFIX + '/donor/edit-donor/' + donorId);
        };

        var canEditAppt = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 2;

        var editDonorText = null;
        var nameAndAge = null;
        if (canEditAppt) {
            editDonorText = (
                <a
                    className="underline"
                    href="javascript:void(0)"
                    onClick={function () {
                    editDonor();
                }}>Edit Donor</a>
            );
            var name = this.state.data.donor
                ? (this.state.data.donor.firstName + ' ' + this.state.data.donor.lastName)
                : '';
            var age = this.state.data.donor && this.state.data.donor.donorMedicalData && this.state.data.donor.donorMedicalData.age
                ? this.state.data.donor.donorMedicalData.age
                : '';
            nameAndAge = (
                <strong>{name.toUpperCase()}
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{this.state.data.donor && this.state.data.donor.donorMedicalData
                        ? age + ' ' + this
                            .state
                            .data
                            .donor
                            .donorMedicalData
                            .gender
                            .toUpperCase()
                        : ''}
                </strong>
            );
        }

        return (
            <div className="popup-tooltip" id="myPopup">

                <a href="javascript:void(0)" className="close-tip" onClick={this.onClose}>
                    <span aria-hidden="true">&times;</span>
                </a>

                <header className="header-new">

                    <div className="row">
                        <div className="col-sm-12 alpha-lg">
                            <p>
                                ID:
                                <strong>{this.state.data.donor
                                        ? this.state.data.donor.id
                                        : ''}</strong><br/> {nameAndAge}
                            </p>
                        </div>

                        {this.renderCheckOutButton()}

                    </div>

                </header>
                <div
                    style={{
                    padding: "15px 0px 0px 30px"
                }}>
                    <ComonInfoArea
                        successMessage={this.state.successMessage}
                        errorMessage={this.state.errorMessage}/>
                </div>

                <section className="sch-tool">
                    <p>ABO:&nbsp;
                        <strong>{this.state.data.donor && this.state.data.donor.donorMedicalData
                                ? this.state.data.donor.donorMedicalData.bloodType
                                : ''}</strong>
                    </p>
                    <p>Ethnicity:&nbsp;
                        <strong>{this.state.data.donor && this.state.data.donor.donorMedicalData
                                ? this.state.data.donor.donorMedicalData.ethnicity
                                : ''}</strong>
                    </p>
                    <p>BMI:&nbsp;
                        <strong>{this.state.data.donor && this.state.data.donor.donorMedicalData
                                ? this.state.data.donor.donorMedicalData.bmi
                                : ''}</strong>
                    </p>
                    <p>Smoker:&nbsp;
                        <strong>{this.state.data.donor && this.state.data.donor.donorMedicalData && this.state.data.donor.donorMedicalData.isSmoker
                                ? 'Y'
                                : 'N'}</strong>
                    </p>
                    <p>Medication:&nbsp;
                        <strong>{this.state.data.donor && this.state.data.donor.donorMedicalData && this.state.data.donor.donorMedicalData.onMedication
                                ? 'Y'
                                : 'N'}</strong>
                    </p>
                    <p>Procedure:&nbsp;
                        <strong>
                            {this.state.data.procedureInfo
                                ? this.state.data.procedureInfo.name
                                : ''}
                        </strong>
                    </p>

                    {this.renderProcedureVolume()}

                    <p>Request Specs:&nbsp;
                        <strong>{this.state.data && this.state.data.scheduleRequest && this.state.data.scheduleRequest.additionalNotes && this.state.data.scheduleRequest.additionalNotes != ''
                                ? this.state.data.scheduleRequest.additionalNotes
                                : 'N/A'}</strong>
                    </p>
                    <p>Request #:&nbsp;

                        <strong>{this.state.data && this.state.data.scheduleRequest && this.state.data.scheduleRequest.id
                                ? this.state.data.scheduleRequest.id
                                : 'N/A'}</strong>
                    </p>
                    <br/>
                    <br/>
                    <p className="pull-right ucase">
                        {editDonorText}
                    </p>
                </section>

                <footer>
                    <div className="btn-group">
                        {this.renderCancelAppointmentButton(data)}
                        {this.renderEditAppoitmentButton(openEditAppointmentDialog, data, canEditAppt)}
                    </div>
                </footer>

            </div>
        );

    }
}

export default AppointmentPopup