import React, {Component} from 'react'
import {API_STATUS} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import InputControl from '../common/inputControl'
import {localDateToUTCString, utcStringToLocalDate} from '../../utils/date.js'

class ScheduleAppointmentForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: {
                collectionSite: 'CA'
            },
            items: {},
            processing: false
        }

        this.onChange = this
            .onChange
            .bind(this);

        this.inputControls = {};

        this.hasInitQuincyCal = false;
        this.isViewToday = true;
        this.numCATodayEvent = 0;
        this.numMATodayEvent = 0;

    }

    updateCalVariable(view) {
        let startTimeLocal = view
            .intervalStart
            .clone()
            .local();

        this.startTime = localDateToUTCString(startTimeLocal);
        this.endTime = localDateToUTCString(view.intervalEnd.clone().local());

        this.isViewToday = false;
        if (view.name == "agendaDay") {
            var now = moment()
                .hour(0)
                .minute(0)
                .second(0)
                .millisecond(0);
            var duration = moment.duration(startTimeLocal.diff(now));
            if (Math.round(duration.asDays()) == 0) {
                this.isViewToday = true;
            }
        }
    }

    onChange(name, value) {
        const data = this.state.data;
        data[name] = value;
        this.setState({data: data});

        if ($(".popup-tooltip").hasClass('active')) {
            $(".popup-tooltip").removeClass('active');
            if (this.props.layoutActions) {
                this
                    .props
                    .layoutActions
                    .closeAppointmentPopup(false);
            }
        }
        let calenderId;
        if (value == 'CA') {
            calenderId = "#alamedacalendar";
            $('#alamedacalendar').show(1000);
            $('#quincycalendar').hide(1000);
            $(".fc-EventCount-button").text(this.numCATodayEvent);

        } else {
            calenderId = "#quincycalendar";
            if (!this.hasInitQuincyCal) {
                this.hasInitQuincyCal = true;
                this.jQInit("#quincycalendar");
            } else {
                $(".fc-EventCount-button").text(this.numMATodayEvent);
            }
            $('#quincycalendar').show(1000);
            $('#alamedacalendar').hide(1000);
        }

        this.updateCalVariable($(calenderId).fullCalendar("getView"));
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('schedule-appointment');
        document
            .body
            .classList
            .add('inner')

        document.title = 'Schedule Appointment';

        this.jQInit("#alamedacalendar");
    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('schedule-appointment');
        document
            .body
            .classList
            .remove('inner');

        if ($(".popup-tooltip").hasClass('active')) {
            $(".popup-tooltip").removeClass('active');
            if (this.props.layoutActions) {
                this
                    .props
                    .layoutActions
                    .closeAppointmentPopup(false);
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.actionType == "CHANG_ROUTE") {
            $('#alamedacalendar').fullCalendar('changeView', 'agendaDay', moment().format("YYYY-MM-DD "));
        } else if (!this.getAppointmentsComplete && nextProps.actionType == actionTypes.GET_APPOINTMENTS && nextProps.status == API_STATUS.DONE) {
            this.setState({items: nextProps.items});
            this.getAppointmentsComplete = true;

            var items = nextProps.items;
            let numEvent = items.length;
            if (this.isViewToday) {
                $(".fc-EventCount-button").text(numEvent);
                if (this.state.data.collectionSite == 'CA') 
                    this.numCATodayEvent = numEvent;
                else 
                    this.numMATodayEvent = numEvent;
                }
            
            var events = [];
            for (var i = 0; i < numEvent; i++) {
                var item = items[i];
                var status = item.status;
                var category = item.procedureInfo.category;
                var resourceId;
                var color;
                var className;
                if (status == "cancelled" || status == "CANCELLED") {
                    color = "#CCCCCC";
                    className = "fc-event-cancelled";
                    if (category == "Whole Blood") {
                        resourceId = "p2";
                    } else if (category == "Bone Marrow") {
                        resourceId = "p1";
                    } else if (category == "LeukoPak") {
                        resourceId = "p3";
                    } else if (category == "LeukoPak Mobilized") {
                        resourceId = "p4";

                    } else if (category == "Screening & Backup") {
                        resourceId = "p5";
                    } else {
                        resourceId = "p6";
                    }
                } else {
                    className = "";
                    if (category == "Whole Blood") {
                        resourceId = "p2";
                        color = "#a564d3";
                    } else if (category == "Bone Marrow") {
                        resourceId = "p1";
                        color = "#9dc15c";
                    } else if (category == "LeukoPak") {
                        resourceId = "p3";
                        color = "#ee5b5b";
                    } else if (category == "LeukoPak Mobilized") {
                        resourceId = "p4";
                        color = "#f9a231";

                    } else if (category == "Screening & Backup") {
                        resourceId = "p5";
                        color = "#289dea";
                    } else {
                        resourceId = "p6";
                        color = "#d3ca41";
                    }
                }

                var event = {
                    id: item.id,
                    resourceId: resourceId,
                    eventResourceEditable: false,
                    title: item.donor.firstName + ' ' + item.donor.lastName,
                    start: utcStringToLocalDate(item.startTime),
                    end: utcStringToLocalDate(item.endTime),
                    color: color,
                    borderColor: "#eaebf5",
                    className: className
                }
                events.push(event);
            }

            var calenderId = (this.state.data.collectionSite == 'CA')
                ? "#alamedacalendar"
                : "#quincycalendar";

            $(calenderId).fullCalendar('addEventSource', events);

            $(calenderId + " .fc-event-container").each(function () {
                const eventElements = ($(this).find(".fc-event"));
                const numElement = eventElements.length;
                if (numElement > 0) {
                    for (i = 0; i < numElement; i++) {
                        $(eventElements[i]).css("color", ((i % 2) == 0)
                            ? "white"
                            : "black");
                    }
                }
            });

        } else if (nextProps.status == API_STATUS.IN_PROGRESS) {
            if (nextProps.actionType == actionTypes.GET_APPOINTMENTS) {
                var calenderId = (this.state.data.collectionSite == 'CA')
                    ? "#alamedacalendar"
                    : "#quincycalendar";

                $(calenderId).fullCalendar('removeEvents');
            }
        } else if (nextProps.needRefresh) {
            var seft = this;
            setTimeout(function () {
                seft.getAppointmentsComplete = false;
                seft
                    .props
                    .actions
                    .getAppointments(seft.state.data.collectionSite, null, seft.startTime, seft.endTime);
            }, 50);
        }
    }

    jQInit(calenderId) {
        var canAddAppt = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 2;

        var openAddAppointmentDialog = this.props.layoutActions.openAddAppointmentDialog;
        var openAppointmentPopup = this.props.layoutActions.openAppointmentPopup;
        var self = this;
        var updateAppointmentTime = function (apointmentId, startTime, endTime) {
            var appointment = {
                id: apointmentId,
                startTime: startTime,
                endTime: endTime
            }
            self
                .props
                .actions
                .updateAppointmentTime(appointment);
        }

        // FullCalendar jQuery in no conflict mode
        $(calenderId).fullCalendar({
            slotDuration: '00:15:00',
            timezone: 'local',
            dayNamesShort: [
                'Sunday',
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday'
            ],
            defaultView: 'agendaDay',
            editable: true,
            selectable: true,
            customButtons: {
                EventCount: {
                    text: '0'
                }
            },

            header: {
                left: 'prev  title next today EventCount',
                right: 'agendaDay,agendaWeek,month'
            },

            schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
            eventLimit: true, // allow "more" link when too many events
            minTime: "07:00:00",
            maxTime: "17:00:00",
            height: "auto",
            allDaySlot: false,
            resources: [
                {
                    id: 'p1',
                    title: 'Bone Marrow'
                }, {
                    id: 'p2',
                    title: 'Whole Blood'
                }, {
                    id: 'p3',
                    title: 'LeukoPak'
                }, {
                    id: 'p4',
                    title: 'LeukoPak Mobilized'
                }, {
                    id: 'p5',
                    title: 'Screening & Backup'
                }, {
                    id: 'p6',
                    title: 'Injection & Others'
                }
            ],

            /**
         * OnEventClick call the PopupTooltip
         * @see https://fullcalendar.io/docs/mouse/eventClick/
         */
            eventClick: function (calEvent, jsEvent, view) {
                openAppointmentPopup(calEvent.id);
                var toolTip = $(".popup-tooltip");
                toolTip.position({
                    of: $(this),
                    my: 'left+20 center',
                    at: 'right center',
                    collision: 'flipfit flipfit',
                    using: function (obj, info) {
                        if (info.vertical != "top") {
                            $(this).addClass("flipped top");
                        } else {
                            $(this).removeClass("flipped top");
                        }
                        if (info.horizontal != "left") {
                            $(this).addClass("flipped left");
                        } else {
                            $(this).removeClass("flipped left");
                        }
                        $(this).css({
                            left: obj.left + 'px',
                            top: obj.top + 'px'
                        })
                    }
                })
            },

            select: function (start, end, evet, view, resource) {
                if (!canAddAppt) {
                    return;
                }
                if (resource) {
                    var now = moment();
                    if (start > now) {
                        openAddAppointmentDialog(self.state.data.collectionSite, null, resource.title, localDateToUTCString(start), localDateToUTCString(end));
                    }
                }
            },

            viewRender: function (view, element) {

                if ($(".popup-tooltip").hasClass('active')) {
                    $(".popup-tooltip").removeClass('active');
                    if (self.props.layoutActions) {
                        self
                            .props
                            .layoutActions
                            .closeAppointmentPopup(false);
                    }
                }

                self.updateCalVariable(view);

                self.getAppointmentsComplete = false;
                const data = self.state.data;
                self
                    .props
                    .actions
                    .getAppointments(data.collectionSite, null, self.startTime, self.endTime);
            },

            eventResize: function (event, delta, revertFunc) {
                if (!canAddAppt) {
                    revertFunc();
                    return;
                }
                updateAppointmentTime(event.id, localDateToUTCString(event.start), localDateToUTCString(event.end));

            },

            eventDrop: function (event, delta, revertFunc) {
                if (!canAddAppt) {
                    revertFunc();
                    return;
                }
                updateAppointmentTime(event.id, localDateToUTCString(event.start), localDateToUTCString(event.end));
            }

        });
    }

    render() {

        return (
            <div>

                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>
                            <a href="javascript:void(0)">Manage Donors</a>
                        </li>
                        <li className="active">Schedules &amp; Appointments</li>
                    </ol>
                </div>

                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Schedules &amp; Appointments</h2>
                    </div>

                    <InputControl
                        containerClass="pull-right-lg txt-center-xs filters"
                        type="select"
                        name="collectionSite"
                        ref
                        ={(input) => {
                        this.inputControls["collectionSite"] = input
                    }}
                        value={this.state.data.collectionSite}
                        optionValues={[
                        [
                            "Alameda", "CA"
                        ],
                        ["Quincy", "MA"]
                    ]}
                        onChange={this.onChange}/>

                </div>
                <div id='alamedacalendar' className="schedule-caldendar"></div>
                <div id='quincycalendar' className="schedule-caldendar"></div>

            </div>
        );
    }

}

export default ScheduleAppointmentForm;