import React, {Component} from 'react';

class ComonInfoArea extends Component {

  constructor(props) {
    super(props);
  }

  renderErrorMessage() {
    if (this.props.errorMessage) {
      return (
        <div className="col-sm-24 has-error server-error">
          <label>{this.props.errorMessage}</label>
        </div>
      )
    }
  }

  renderSuccessMessage() {
    if (this.props.successMessage) {
      if (this.props.resetPassword) {
        return (
          <div className="form-group col-sm-24 pass-re">
            <label className="blue message">{this.props.successMessage}</label>
          </div>
        )
      } else {
        return (
          <label className="text-success">{this.props.successMessage}</label>
        )
      }
    }
  }

  render() {

    return (
      <div>
        {this.renderErrorMessage()}
        {this.renderSuccessMessage()}
      </div>
    )

  }
}

export default ComonInfoArea