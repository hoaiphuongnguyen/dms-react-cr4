import React, {Component} from 'react';

class ErrorArea extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {

    $(".dismiss-errors")
      .click(function () {
        $(this)
          .closest(".error-list")
          .slideUp(500);
      })
  }

  ucwords(str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
      return $1.toUpperCase();
    })
  }

  render() {
    if (this.props.errorList == null) {
      return null;
    }
    
    const errorList = this.props.errorList;
    var errorArray = Object.keys(errorList);
    var errorVisible = false;

    var elements = [];
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        elements.push(errorList[key]);
      }
    })

    var converter = require('number-to-words');
    var numError = elements.length;
    var className = "error-list border-all clearfix";
    if (numError > 0) {
      errorVisible = true;
      var numErrorWords = this.ucwords(converter.toWords(numError));
    } else {
      className = 'margin-error-list';
    }

    return (
      <div className={className}>
        <div
          id="errorArea"
          className={errorVisible
          ? ''
          : 'hidden'}>
          <h5>{numErrorWords}&nbsp;Error{numError > 1
              ? 's'
              : ''}
            &nbsp;Found (as listed below)</h5>
          <ol className="error-list-group">
            {elements.map((text, i) => (
              <li key={i}>
                {text}
              </li>
            ))
}
          </ol>

          {!this.props.hideCloseReport
            ? <a className="dismiss-errors" role="button">Close Error Report</a>
            : ''
}

        </div>
      </div>
    );

  }
}

export default ErrorArea