import React, {Component} from 'react';

class ErrorDialog extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="modal fade" id="errorDil" role="dialog">
                <div className="modal-dialog">

                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-sm-24 text-center">
                                    <img src="/img/icons/reportsicons/icon-error.svg" alt="" width="50"/>
                                </div>
                                <div className="col-sm-24 text-center success-text error">
                                    <p id="errorMessage">Something went Wrong.
                                        <br/>
                                        Let's try again.</p>
                                </div>
                                <div className="clear"></div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        );
    }
}

export default ErrorDialog