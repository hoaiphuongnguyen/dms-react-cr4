import React, {Component} from 'react';
import Select2 from 'react-select2-wrapper';

class InputControl extends Component {
  constructor(props) {
    super(props);
    this.state = {}

    this.validateAllRules = this
      .validateAllRules
      .bind(this);
    this.clearData = this
      .clearData
      .bind(this);

    this.handleKeyPress = this
      .handleKeyPress
      .bind(this);
    this.handleChange = this
      .handleChange
      .bind(this);

    this.handleChangeSelect2 = this
      .handleChangeSelect2
      .bind(this);
    this.handleBlur = this
      .handleBlur
      .bind(this);

    this.jQueryPluginInited = false;
    this.initedjQueryPlugin =null
  }

  componentWillReceiveProps(nextProps) {

    if (this.props.type == "radio") {
      $('[name=' + this.props.name + ']').prop('checked', false);

      if (nextProps.value == "undefined") //missing typeof ??
        return;
      
      var value = nextProps.value;
      if (value) {
        $('[name=' + this.props.name + '][value="' + value + '"]').prop('checked', true);
        value = value.toLowerCase();
        $('[name=' + this.props.name + '][value="' + value + '"]').prop('checked', true);
      }
      return;
    }

    //we have to re-init the inner jquery plugin if underlying value has changed from outside of this component
    //so that the jquery component can update its own selected value(s)
    if(this.props.value != nextProps.value || 
        (!this.props.selectedValues && nextProps.selectedValues ) ||
        (this.props.selectedValues && !nextProps.selectedValues) ||
          (this.props.selectedValues  && nextProps.selectedValues && 
            this.props.selectedValues.length != nextProps.selectedValues.length)) //should do really deep comparision for selected values arrays
          {
            this.jQueryPluginInited= false;
          }

    if ($("#" + this.props.name).hasClass("showhidedrop")) {
      var selectedValue = nextProps.value;
      var name = this.props.name;
      var nextContainer = $("#" + name + "hide");
      if (selectedValue == true || selectedValue == "true" || selectedValue == "y" || selectedValue == "reserved" || selectedValue == "disqualified") {
        $(nextContainer).show();

      } else {
        $(nextContainer).hide();

      }
    }
  }

  initJQueryPlugin(){
      if (this.props.type == "date" && !this.props.readOnly) {
          this.initDatePicker();
      } else if (this.props.type == "time" && !this.props.readOnly) {
        this.initTimePicker();
      }
      else if (this.props.type == "select" && this.props.multiple) {
        this.initMultiValueDropDowns();
      }
  }

  componentDidMount() {
     this.jQueryPluginInited = false;
     this.initJQueryPlugin();
  }
  componentDidUpdate() {
      this.initJQueryPlugin();

      if (this.props.type == "radio" || this.props.type == "searchRadio") {

        var selectedValues = [];
        if(this.props.value)
         selectedValues.push(this.props.value);
        else if (this.props.selectedValues)
          selectedValues  = selectedValues.concat(this.props.selectedValues);
        var self = this;
        if(selectedValues.length ==0)
        {
            $('[name=' + self.props.name + ']:checked').prop('checked', false);
        }
        else{
          selectedValues.forEach(function(value){
            if($('[name=' + self.props.name + '][value="' + value + '"]').length
            && $('[name=' + self.props.name + '][value="' + value + '"]:checked').length==0)
            {
              $('[name=' + self.props.name + '][value="' + value + '"]').prop('checked', true);
            }
          });
        }
      }
      if (this.props.type == "searchCheckbox") {

        var selectedValues = [];
        if(this.props.value)
         selectedValues.push(this.props.value);
        else if (this.props.selectedValues)
          selectedValues  = selectedValues.concat(this.props.selectedValues);
        var self = this;

         if(selectedValues.length ==0)
        {
            $('[name=' + self.props.name + ']:checked').prop('checked', false);
        }
        else{
          selectedValues.forEach(function(value){
            if($('input[value="' + value + '"]').length
            && $('input[value="' + value + '"]:checked').length==0)
            {
              $('input[value="' + value + '"]').prop('checked', true);
            }
          });
        }
      }
  }

  initDatePicker(){
    if(this.jQueryPluginInited)
        return;
     var self = this;
      var raiseChangeDateEvent = function (value) {
        self.state = {
          errorVisible: false
        };
        if (self.props.onValid) {
          self
            .props
            .onValid(self.props.name);
        }
        self.handlePickerDate(value);
      }

      var name = this.props.name;
   //   setTimeout(function () {
        if (name == "dateOfBirth" || name == "standardTestDate") {
          var date = new Date();
          //  date.setDate(date.getDate() - 1);
          $('#' + name).datetimepicker({
            endDate: date,
            format: 'mm/dd/yyyy',
            autoclose: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
          });
        } else {
          self.initedjQueryPlugin =  $('#' + name).datetimepicker({format: 'mm/dd/yyyy', autoclose: 1, startView: 2, minView: 2, startDate: self.props.onlyFutureDate ? new Date():null,  forceParse: 0 });
        }
        $('#' + name).off('change');
        $('#' + name)
          .on("change", function (e) {
            var value = e.target.value;
            raiseChangeDateEvent(value);
          });
        self.jQueryPluginInited= true;
     // }, 100);
  }

  initTimePicker(){
     if(this.jQueryPluginInited)
        return;

     var self = this;
     var name = this.props.name;
   //   setTimeout(function () {
      
      self.initedjQueryPlugin = $('#' + name).timepicker({ showMeridian: false /*show 24h*/});
      
      $('#' + name).timepicker().off('changeTime.timepicker');
      $('#' + name).timepicker().on('changeTime.timepicker', function (e) {
          self.handleChange({target:{name:name, value: e.time.value}});
        });
      self.jQueryPluginInited= true;
  //  }, 5);
  }

  initMultiValueDropDowns() {
    var self = this;
    
    var func = self
      .initMultiValueDropDowns
      .bind(self);

    setTimeout(function () {
      if (self.jQueryPluginInited) 
        return;
      
      if (self.props.selectedValues && self.props.selectedValues.length > 0) { //existing data;
        if ($('[name=' + self.props.name + '] option:selected').length == 0) {
          func();
        } else {
          $('[name=' + self.props.name + ']').selectpicker({noneSelectedText:'Select', style: 'btn-mulselect'});
          $('[name=' + self.props.name + ']').selectpicker('refresh');
          self.jQueryPluginInited = true;
        }
      } else { //new data
        $('[name=' + self.props.name + ']').selectpicker({noneSelectedText:'Select', style: 'btn-mulselect'});
        $('[name=' + self.props.name + ']').selectpicker('refresh');
        self.jQueryPluginInited = true;
      }
    }, 100);
  }

  setError(message) {
    this.setState({errorMessage: message, errorVisible: true});
    if (this.props.onInvalid) {
      this
        .props
        .onInvalid(this.props.name, message);
    }
  }

  clearError() {
    this.setState({errorVisible: false});
  }

  clearData() {
    if (this.props.type == "radio") {
      $('input:radio')
        .each(function () {
          $(this).prop('checked', false);
        })
      this.setState({errorVisible: false});
    } else {
      this.state = {
        isEmpty: true,
        value: "",
        valid: true,
        errorMessage: "Input is invalid",
        errorVisible: false
      };
    }

    if (this.props.onChange) {
      this
        .props
        .onChange(this.props.name, '');
    }
  }

  validateAllRules(name, value, validateHidden) {
    if (!validateHidden && $("#" + name).is(":hidden")) {
      return; //no validation if control is hidden
    }

    if (this.props.readOnly){
      return;
    }

    var valid = true;
    if (typeof this.props.validate !== 'undefined') 
      valid = this.props.validate(value);
    
    if (valid && this.props.type == "date") {
      if (name == "dateOfBirth" || name == "standardTestDate") {
        var now = new Date();
        now.setHours(0);
        now.setMinutes(0);
        now.setSeconds(0);
        if (value) {
          var arrayDate = value.split("/");
          var date = new Date(arrayDate[2], arrayDate[0] - 1, arrayDate[1]);
          if (date > now) {
            valid = false;
            this.setState({errorVisible: true});
            var message = "";
            if (name == "dateOfBirth") {
              message = "The 'DOB' field cannot be future date. Please check and enter again."
            } else {
              message = "The 'Standard Viral Test Date' field cannot be future date. Please check and ent" +
                  "er again."
            }
            if (this.props.onInvalid) {
              this
                .props
                .onInvalid(name, message);
            }
            return;
          }
        }
      } else if (name == "fromDate" && value && value != "") {
        var toDateString = $('[name=fromDate]').val();
        if (toDateString != "") {
          var arrayDate = value.split("/");
          var formDate = new Date(arrayDate[2], arrayDate[0] - 1, arrayDate[1]);
          var arrayDate = toDateString.split("/");
          var toDate = new Date(arrayDate[2], arrayDate[0] - 1, arrayDate[1]);
          if (formDate > toDate) {
            this.setState({errorVisible: true});
            if (this.props.onInvalid) {
              this
                .props
                .onInvalid(name, "Form date cannot be greater than To date.");
            }
            return;
          }
        }
      } else if (name == "toDate" && value  && value != "") {
        var fromDateString = $('[name=fromDate]').val();
        if (fromDateString != "") {
          var arrayDate = fromDateString.split("/");
          var formDate = new Date(arrayDate[2], arrayDate[0] - 1, arrayDate[1]);
          var arrayDate = value.split("/");
          var toDate = new Date(arrayDate[2], arrayDate[0] - 1, arrayDate[1]);
          if (formDate > toDate) {
            this.setState({errorVisible: true});
            if (this.props.onInvalid) {
              this
                .props
                .onInvalid(name, "To date cannot be less than Form date.");
            }
            return;
          }
        }
      }

    } else if (name == "confirmPassword") {
      var newPassword = $("#newPassword").val();
      if (value != newPassword) {
        this.setState({errorVisible: true});
        if (this.props.onInvalid) {
          this
            .props
            .onInvalid(name, "Confirm Password does not match the Password.");
        }
        return;
      }
    }

    this.checkRequired_MinLength(name, value, valid, validateHidden);
  }

  checkRequired_MinLength(name, value, hasOtherError, validateHidden) {
    if (!validateHidden && $("#" + name).is(":hidden")) {
      return; //no validation if control is hidden
    }

    var valid;
    //The hasOtherError variable is optional, and true if not passed in:
    if (typeof hasOtherError === 'undefined') {
      valid = true;
    } else {
      valid = hasOtherError;
    }
    var message = "";

    // we skip required & minlength check if other hasNoOtherErroration returned
    // error already
    if (!valid) {
      // This happens when the user leaves the field, but it is not valid (we do final
      // validation in the parent component, then pass the result here for display)
      message = this.props.errorMessage;
    } else if (this.props.type == 'radio' && this.props.required && !value) {
      // this happens when we have a required field with checked we want the
      // "emptyMessage" error message
      message = this.props.emptyMessage;
      valid = false;
    } else if (this.props.required && (jQuery.isEmptyObject(value)  && !(typeof(value)=="number" && value!=null))) {
      // this happens when we have a required field with no text entered in this case,
      // we want the "emptyMessage" error message
      message = this.props.emptyMessage;
      valid = false;
    } else if (!jQuery.isEmptyObject(value) && value.length < this.props.minCharacters) {
      // This happens when the text entered is not the required length, in which case
      // we show the regular error message
      message = this.props.errorMessage;
      valid = false;
    }

    // setting the state will update the display, causing the error message to
    // display if there is one.
    this.setState({
      value: value,
      isEmpty: jQuery.isEmptyObject(value)  && !(typeof(value)=="number" && value!=null),
      valid: valid,
      errorMessage: message,
      errorVisible: !valid
    });

    if (!valid) {
      if (this.props.onInvalid) {
        this
          .props
          .onInvalid(name, message);
      }
    } else {
      if (this.props.onValid) {
        this
          .props
          .onValid(name);
      }
    }

  }

  handleBlur(event) {
    if (this.props.type == "radio") {
      if (!this.state.checked) {
        this.setState({errorVisible: true});
        if (this.props.onInvalid) {
          this
            .props
            .onInvalid(this.props.name, this.props.emptyMessage);
        }
      }
    } else {
      this.validateAllRules(event.target.name, event.target.value);
      if (this.state.valid && this.props.onBlur) {
          this
            .props
            .onBlur(event.target.name || event.target.id, event.target.value);
      }
    }
  }

   handleKeyPress(event) {
       if(event.charCode ==13)
       {
          this.validateAllRules(event.target.name, event.target.value);
          if (this.state.valid && this.props.onEnter){
              this
              .props
              .onEnter(event.target.name || event.target.id, event.target.value);
          }
        }
   }

  handleChange(event) {
    if (this.props.readOnly)
      return;
    var value = event.target.value;
    if (this.props.type == "searchCheckbox") {
      if (event.target.checked) {
        value = "+" + value;
      } else {
        value = "-" + value;
      }

      if (this.props.onChange) {
        this
          .props
          .onChange(event.target.name, value);
      }
      return;
    }

    if (this.props.type == "radio" || this.props.type == "searchRadio") {
      this.setState({checked: true, errorVisible: false})

      if(this.props.type == "searchRadio" ) 
      {
        if (!event.target.checked) {//uncheck, clear it
            value="";
        }
      }
      else if (this.props.onValid) {
        this
          .props
          .onValid(event.target.name);
      }
    } else {
      this.checkRequired_MinLength(event.target.name, value);
      if (this.props.type == "date") {
        this.handlePickerDate(value);
      } else if (this.props.type == "eligible" || this.props.type == "switch") {
        value = event.target.checked;
      } else if (event.target.name == "hlaGeneTypes") {
        var selectedOptions = event.target.selectedOptions;
        var selectedGeneTypes =[];
        for (var i = 0; i < selectedOptions.length; i++) {
           selectedGeneTypes.push(selectedOptions[i].value);
        }
        value =selectedGeneTypes;
      } else if (this.props.type == "select") {

        var selectedOptions = event.target.selectedOptions;
        const elementName = this.props.elementName;
        value = [];
        for (var i = 0; i < selectedOptions.length; i++) {
          var element = new Object();
          if (this.props.selectedValues && this.props.selectedValues[i]) {
            element.id = this.props.selectedValues[i].id;
          }
          if (elementName) {
            element[elementName] = selectedOptions[i].value;
            value.push(element);
          } else {
            value.push(selectedOptions[i].value);
          }
        }
      }
    }

    if (this.props.onChange) {
      this
        .props
        .onChange(event.target.name || event.target.id, value);
    }
  }

  renderRadio(wrapperClass) {
    const inputs = this.props.optionValues ? this.props.optionValues:[];
    var self= this;
    return (
      <div
        className={wrapperClass}
        onChange={this.handleChange}
        onBlur={this.handleBlur}>
        {this.props.useH4AsLabel ? (<h4>{this.props.label}</h4>) :(<label
          className="ucase"
          title={this.state.errorVisible
          ? this.state.errorMessage
          : ''}>{this.props.label}
        </label>)}
        {inputs.map(([
          text, value
        ], i) => {
          
          if(self.props.value == value) {
             return (
              <span key={i}>
                <input
                  type="radio"
                  readOnly={this.props.readOnly}
                  value={value
                  ? value
                  : ''}
                  selected={true}
                  defaultChecked={true}
                  name={this.props.name}/>
                <span className="radio-text">{text}</span>
              </span>
              );
          }else {
            return (
            <span key={i}>
              <input
                type="radio"
                readOnly={this.props.readOnly}
                value={value
                ? value
                : ''}
                selected={false}
                 defaultChecked={false}
                name={this.props.name}/>
              <span className="radio-text">{text}</span>
            </span>
            );
          }

          
        })
}
      </div>
    );
  }

  renderSearchRadio(wrapperClass) {
    const inputs = this.props.optionValues?this.props.optionValues:[];
    var self = this;
    return (
      <div
        id={this.props.containerId}
        className={wrapperClass}
        onChange={this.handleChange}
        onBlur={this.handleBlur}>
        <h4
          title={this.state.errorVisible
          ? this.state.errorMessage
          : ''}>{this.props.label}</h4>

        {inputs.map(([text, value, label], i) => {
        
          if(self.props.value == value)
          {
              return (
                <label key={i}>
                  <input
                    type="radio"
                    value={value
                    ? value
                    : ''}
                    selected={true}
                    defaultChecked={true}
                    name={this.props.name}
                    data-label={label?label:text}
                    data-group={this.props.name}/> {text}
                </label>
              );
          }
          else
          {
               return (
                <label key={i}>
                  <input
                    type="radio"
                    value={value
                    ? value
                    : ''}
                    name={this.props.name}
                    data-label={label?label:text}
                    selected={false}
                    defaultChecked={false}
                    data-group={this.props.name}/> {text}
                </label>
              );

          }
        })}
      </div>
    );
  }

  renderSearchCheckbox(wrapperClass) {
    const inputs = this.props.optionValues?this.props.optionValues:[];
    var defaultValues = [];
    if (this.props.selectedValues && typeof (this.props.selectedValues) === "string" ) {
      defaultValues = this
        .props
        .selectedValues
        .split("|");
    }

    return (
      <div
        id={this.props.containerId}
        className={wrapperClass}
        onChange={this.handleChange}
        onBlur={this.handleBlur}>
        <h4
          title={this.state.errorVisible
          ? this.state.errorMessage
          : ''}>{this.props.label}</h4>

        {inputs.map(([
          text, value
        ], i) => {

          if (defaultValues.find(e => e == value)) {

            return (
              <label key={i}>
                <input
                  type="checkbox"
                  value={value
                  ? value
                  : ''}
                  selected={true}
                  checked={true}
                  name={this.props.name}
                  data-label={text}
                  data-group={this.props.name}/> {text}
              </label>

            )
          } else {
            return (

              <label key={i}>
                <input
                  type="checkbox"
                  value={value
                  ? value
                  : ''}
                  name={this.props.name}
                  data-label={text}
                  data-group={this.props.name}/> {text}
              </label>
            )
          }

        })}
      </div>
    );
  }

  renderSelect(wrapperClass) {
    const elementName = this.props.elementName;
    var inputs = this.props.optionValues?this.props.optionValues:[];
    var selectedValues = this.props.selectedValues;

    if (this.props.multiple) {
      var defaultValues = [];
      if (selectedValues && selectedValues != '') {
        if (elementName) {
          var numSelected = selectedValues.length;
          for (var i = 0; i < numSelected; i++) {
            defaultValues[i] = selectedValues[i][elementName];
          }
        } else if (typeof selectedValues === 'string') {
          defaultValues = selectedValues.split("|");
        } else {
          defaultValues = selectedValues;
        }
      } else if (this.props.defaultValue) {
        defaultValues = this.props.defaultValue;
      }

      return (
        <div className={wrapperClass} id={this.props.containerId}>
          {this.props.useH4AsLabel ? (<h4>
            {this.props.label}</h4>) : (<label
            htmlFor={this.props.name}
            className={this.props.labelClass}
            title={this.state.errorVisible
            ? this.state.errorMessage
            : ''}>
            {this.props.label}</label>)}
          <select
            multiple={this.props.multiple}
            disabled={this.props.readOnly}
            readOnly={this.props.readOnly}
            onChange={this.handleChange}
            className={this.props.inputClass}
            name={this.props.name}
            id={this.props.name}>
            {inputs.map(([
              text, value
            ], i) => {

              if (defaultValues.find(e => e == value)) {

                return (
                  <option
                    key={i}
                    value={value
                    ? value
                    : ''}
                    selected={true}
                    checked={true}>{text}</option>
                )
              } else {
                return (
                  <option
                    key={i}
                    value={value
                    ? value
                    : ''}>{text}</option>
                )
              }

            })}
          </select>
        </div >
      )
    } else {
      var dataArray = [];
      var data = new Object();
      if (this.props.showSelectOption) {
        if (this.props.selectText) {
          data.text = this.props.selectText;
        } else {
          data.text = 'Select';
        }
        data.id = '';
        dataArray.push(data);
      }

      inputs.map(([
        text, value
      ], i) => {
        var data = new Object();
        data.text = text;
        data.id = value;
        dataArray.push(data);
      })

      var value = this.props.value
        ? this.props.value
        : '';

      if (value == "undefined") {
        value = ''
      }

      return (

        <div className={wrapperClass} id={this.props.containerId}>
          {this.props.useH4AsLabel ? (<h4>{this.props.label}</h4>) : (<label
            htmlFor={this.props.name}
            className={this.props.labelClass}
            title={this.state.errorVisible
            ? this.state.errorMessage
            : ''}>
            {this.props.label}</label>)}
          <Select2
            className={this.props.inputClass}
            disabled={this.props.readOnly}
            defaultValue={this.props.defaultValue
            ? this.props.defaultValue
            : ''}
            value={value
            ? value
            : ''}
            id={this.props.name}
            name={this.props.name}
            data={dataArray}
            options={{
            minimumResultsForSearch: 'Infinity'
          }}
            onSelect={this.handleChangeSelect2}/>
        </div >
      )

    }
  }

  handleChangeSelect2(event) {
    var selectedValue = event.target.value;
    this.setState({value: selectedValue});
    this.checkRequired_MinLength(event.target.name, selectedValue);

    if ($(event.target).hasClass("showhidedrop")) {

      var name = event.target.name;
      var nextContainer = $("#" + name + "hide");
      var nextInput = $(nextContainer).find(".form-control");
      var nextInputName;
      if ($(nextInput).attr("name")) 
        nextInputName = $(nextInput).attr("name");
      else 
        nextInputName = $(nextInput)
          .find(".form-control")
          .attr("name");
      
      if (selectedValue == true || selectedValue == "true" || selectedValue == "y" || selectedValue == "reserved" || selectedValue == "disqualified") {
        $(nextContainer).show();
        if (this.props.onChange) {
          this
            .props
            .onChange(nextInputName, $(nextInput).val());
        }
      } else {
        $(nextContainer).hide();
        if (this.props.onValid) {
          this
            .props
            .onValid(nextInputName);
        }
        if (this.props.onChange) {
          this
            .props
            .onChange(nextInputName, "");
        }
      }
    }

    if (this.props.onChange) {
      this
        .props
        .onChange(event.target.name, selectedValue);
    }
  }

  renderTime(wrapperClass) {
    var label = this.props.useH4AsLabel ? (<h4>{this.props.label}</h4>) : (<label
        htmlFor={this.props.name}
        className={this.props.labelClass}
        title={this.state.errorVisible
        ? this.state.errorMessage
        : ''}>
        {this.props.label}
      </label>);

    if (this.props.hideLabel) {
       label = ''
     }

    return (

        <div className={wrapperClass} id={this.props.containerId}>
          {label}
          <div
            className="input-group bootstrap-timepicker timepicker">
            <input
               id={this.props.name}
              className={this.props.inputClass}
              readOnly={this.props.readOnly}
              name={this.props.name}
              type="text"
              value={this.props.value
              ? this.props.value
              : ''}/>
              
              <span className={this.props.readOnly?"input-group-addon disabled":"input-group-addon "}>
                <img
                  src="/img/icons/createdonoricons/icon-clock.svg"/>
                <span className="glyphicon glyphicon-time"></span>
              </span>
           
          </div>

        </div>
      )
  }

  renderDate(wrapperClass) {
    var label;
    if (this.props.searchControl || this.props.useH4AsLabel) {
      label = <h4
        htmlFor={this.props.name}
        className={this.props.labelClass}
        title={this.state.errorVisible
        ? this.state.errorMessage
        : ''}>
        {this.props.label}
      </h4>

    } else {
      label = <label
        htmlFor={this.props.name}
        className={this.props.labelClass}
        title={this.state.errorVisible
        ? this.state.errorMessage
        : ''}>
        {this.props.label}
      </label>
    }

    let inputClassName = this.props.inputClass;
      let picker;
      if (this.props.hidePicker) {
          picker =null;
          inputClassName=inputClassName + " hide-picker";
      }
       else {
        picker = <span className={this.props.readOnly?"input-group-addon disabled":"input-group-addon "}>
              <img
                src="/img/icons/createdonoricons/icon_calender.svg"
                alt="Personal Profile Icon"/>
              <span className="glyphicon glyphicon-calendar"></span>
            </span>
      }

     if (this.props.hideLabel) {
       label=''
     }

    if (this.props.viralType) {
    
      return (
        <div className={wrapperClass} id={this.props.containerId}>
          {label}
          <div className="col-sm-24 collapse-sm">
            <div id={this.props.name} className="input-group date form_date">
              <input
                className={this.props.inputClass}
                readOnly={this.props.readOnly}
                name={this.props.name}
                type="text"
                maxLength="10"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                value={this.props.value
                ? this.props.value
                : ''}/>

             {picker}
            </div>
          </div>

          <div className="col-sm-24 collapse-sm">
            <div className="col-sm-24 viral-lbl">
              <span >{this.props.secondLabel}
              </span>
              <span id={this.props.name + 'SecondLabel'}>
                {this.props.secondValue}</span>
              <span></span>
            </div>
          </div>

        </div>
      )

    } else {
     
      return (

        <div className={wrapperClass} id={this.props.containerId}>
          {label}
          <div
            id={this.props.name}
            className="input-group date"
            data-date=""
            data-date-format="dd MM yyyy"
            data-link-field="dtp_input2"
            data-link-format="yyyy-mm-dd">
            <input
              className={this.props.inputClass}
              readOnly={this.props.readOnly}
              name={this.props.name}
              maxLength="10"
              type="text"
              onChange={this.handleChange}
              onBlur={this.handleBlur}
              value={this.props.value
              ? this.props.value
              : ''}/>

            {picker}

           
          </div>

        </div>
      )
    }
  }

  handlePickerDate(value) {
    this.setState({
      value: value
        ? value
        : ''
    });

    if (this.props.onChange) {
      this
        .props
        .onChange(this.props.name, value);
    }
    if (this.props.viralType) {
      if (value != "") {
        var inputDate = moment(value, 'MM/DD/YYYY');
        if (this.props.name == "standardTestDate") {
          var expirationDate = inputDate.add(90, 'days');
          if (this.props.onChange) {
            this
              .props
              .onChange(this.props.secondName, expirationDate.format('MM/DD/YYYY'));
          }
        } else {
          var dateTime = moment();
          var dateValue = moment({
            year: dateTime.year(),
            month: dateTime.month(),
            day: dateTime.date()
          });
          var duration = moment.duration(dateValue.diff(inputDate));
          var days = duration.asDays();
           if (this.props.onChange) {
            this
              .props
              .onChange(this.props.secondName, days);
            }
        }
      } else {
         if (this.props.onChange) {
            this
              .props
              .onChange(this.props.secondName, '');
          }
      }
    }
  }

  renderTextArea(wrapperClass) {
    return (
      <div className={wrapperClass}>

        {this.props.useH4AsLabel ? (<h4>{this.props.label}</h4>) : (<label htmlFor={this.props.name} className="ucase">{this.props.label}</label>)}
        <textarea
          maxLength={this.props.maxLength}
          className={this.props.inputClass}
          readOnly={this.props.readOnly}
          name={this.props.name}
          id={this.props.name}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={this.props.value
          ? this.props.value
          : ''}
          rows={this.props.rows?this.props.rows:8}></textarea>
      </div>
    )

  }

  renderEligible(wrapperClass) {
    return (
      <div>
        <input
          readOnly={this.props.readOnly}
          type="checkbox"
          className={this.props.inputClass}
          id={this.props.name}
          name={this.props.name}
          onChange={this.handleChange}
          checked={this.props.checked
          ? this.props.checked
          : false}/>
      </div>
    )
  }

  renderSwitch(wrapperClass) {
    return (
      <div className={wrapperClass}>
        <div className="switch">
          <label className="ucase">{this.props.label}</label>
          <input
            id={this.props.name}
            name={this.props.name}
            className="cmn-toggle cmn-toggle-round"
            onChange={this.handleChange}
            checked={this.props.checked
            ? this.props.checked
            : false}
            type="checkbox"/>
          <label htmlFor={this.props.name}></label>
        </div>
      </div>
    )
  }

  renderUsername(wrapperClass) {
    return (
      <div className={wrapperClass}>
        <input
          type="text"
          className={this.props.inputClass}
          id={this.props.name}
          name={this.props.name}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={this.props.value
          ? this.props.value
          : ''}/>
        <label htmlFor={this.props.name}>{this.props.label}</label>
      </div>
    )
  }

  renderPassword(wrapperClass) {
    return (
      <div className={wrapperClass}>
        <input
          type="password"
          className={this.props.inputClass}
          autoComplete="new-password"
          id={this.props.name}
          name={this.props.name}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={this.props.value
          ? this.props.value
          : ''}/>
        <label htmlFor={this.props.name}>
          {this.props.label}
        </label>
      </div>

    )
  }

  render() {
   
   if (this.props.hidden){
       return null;
   }

    var wrapperClass = this.props.containerClass
    if (this.state.errorVisible) {
      wrapperClass += " has-error";
    }
    var type = this.props.type;
    if (!type) {
      type = "text";
    }

    if (type == "radio") {
      return this.renderRadio(wrapperClass);
    } else if (type == "searchRadio") {
      return this.renderSearchRadio(wrapperClass);
    } else if (type == "searchCheckbox") {
      return this.renderSearchCheckbox(wrapperClass);
    } else if (type == "textarea") {
      return this.renderTextArea(wrapperClass);
    } else if (this.props.type == "eligible") {
      return this.renderEligible(wrapperClass);
    } else if (this.props.type == "switch") {
      return this.renderSwitch(wrapperClass);
    } else if (this.props.type == "select") {
      return this.renderSelect(wrapperClass);
    } else if (this.props.type == "date") {
      return this.renderDate(wrapperClass);
    } else if (this.props.type == "time") {
      return this.renderTime(wrapperClass);
    }
    else if (this.props.type == "username") {
      return this.renderUsername(wrapperClass);
    } else if (this.props.type == "password") {
      return this.renderPassword(wrapperClass);
    } else {
      if (type == "changepassword") {
        type = "password";
      }

      var label = null;
      if (this.props.hideLabel){

      }
      else if (this.props.searchControl || this.props.useH4AsLabel) {
        label = <h4
          htmlFor={this.props.name}
          className={this.props.labelClass}
          title={this.state.errorVisible
          ? this.state.errorMessage
          : ''}>
          {this.props.label}
        </h4>

      } else {
        label = <label
          htmlFor={this.props.name}
          className={this.props.labelClass}
          title={this.state.errorVisible
          ? this.state.errorMessage
          : ''}>
          {this.props.label}
        </label>;
      }

      return (
        <div className={wrapperClass} id={this.props.containerId}>
          {label}

          <input
            placeholder
            ={this.props.placeholder
            ? this.props.placeholder
            : ''}
            readOnly={this.props.readOnly}
            id={this.props.name}
            name={this.props.name}
            type={type}
            className={this.props.inputClass}
            maxLength={this.props.maxLength}
            onChange={this.handleChange}
            onKeyPress={this.handleKeyPress}
            onBlur={this.handleBlur}
            value={this.props.value!=null
            ? this.props.value
            : ''}/>
        </div>
      )
    }
  }
}

export default InputControl