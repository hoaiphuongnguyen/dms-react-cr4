import React, {Component} from 'react';
import * as actionTypes from '../../constants/actionTypes';
import {API_STATUS} from '../../common/constants';

class SuccessDialog extends Component {

    constructor(props) {
        super(props);
        this.state = ({
            schedulingRequest: jQuery.extend(true, {}, this.props.schedulingRequest)
        });

    }

    componentDidMount() {

        if (this.props.dialogShowing) {
            $('#successDil').modal('show');

        } else {
            $('#successDil').modal({show: false});
        }
        var self = this;

        $('#successDil').on('hide.bs.modal', function (e) {

            if (!self.closing) {
                e.preventDefault();
                e.stopImmediatePropagation();
                //raise close actions
                var closeDialog = self.props.layoutActions.closeSuccessDialog;
                var nextUrl = (self.state.schedulingRequest && self.state.schedulingRequest.actionType == actionTypes.CANCEL_SCHEDULING_REQUEST)
                    ? "/schedule-request/search-schedule-request"
                    : null;

                if (closeDialog(nextUrl)) {
                    self.closing = true;
                }
                return false;
            }
            self.closing = false;

        });

    }

    componentWillUnmount() {
        $('#successDil').off('hide.bs.modal');
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.dialogShowing) {
            this.closing = false;
            $('#successDil').modal('show');
            this.setState({schedulingRequest: nextProps.schedulingRequest});
            var nextUrl = (nextProps.schedulingRequest && nextProps.schedulingRequest.actionType == actionTypes.CANCEL_SCHEDULING_REQUEST)
                ? "/schedule-request/search-schedule-request"
                : null;

            var closeDialog = this.props.layoutActions.closeSuccessDialog;
            self = this;
            setTimeout(function () {
                if (!self.closing) 
                    closeDialog(nextUrl);
                }
            , 3000);

        } else {
            this.closing = true;
            $('#successDil').modal('hide');
        }

    }

    render() {

        return (
            <div className="modal fade" id="successDil" role="dialog">
                <div className="modal-dialog">

                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-sm-24 text-center">
                                    <img src="/img/icons/reportsicons/icon-success.svg" alt="" width="50"/>
                                </div>
                                <div className="col-sm-8 col-sm-offset-8 success-text">
                                    <p id="successMessage"></p>
                                </div>
                                <div className="clear"></div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default SuccessDialog