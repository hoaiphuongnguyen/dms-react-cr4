import React, {Component} from 'react';

class DonorCreateTab extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div id="donorCreateTabs">
                <div className="col-sm-6">
                    <div  id ="step1" className="tab-pil active"  href="#profile" >
                        <div className="step">1</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createdonoricons/icon_personal_profile.svg"
                                alt="Personal Profile Icon"/>
                        </div>
                        <p className="ucase">Personal Profile</p>
                    </div>
                </div>

                <div className="col-sm-6">
                    <div id ="step2" className="tab-pil disable"  href="#medical" >
                        <div className="step">2</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createdonoricons/icon_medical_profile.svg"
                                alt="Personal Profile Icon"/>
                        </div>
                        <p className="ucase">Medical Profile</p>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div id ="step3" className="tab-pil disable"  href="#schedule" >
                        <div className="step">3</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createdonoricons/icon_schedule.svg"
                                alt="Personal Profile Icon"/>
                        </div>
                        <p className="ucase">Schedules</p>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div id ="step4" className="tab-pil disable"  href="#recruitment" >
                        <div className="step">4</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createdonoricons/icon-recruitment-profile.svg"
                                alt="Personal Profile Icon"/>
                        </div>
                        <p className="ucase">Recruitment Profile</p>
                    </div>
                </div>
            </div>

        );
    }
}

export default DonorCreateTab;
