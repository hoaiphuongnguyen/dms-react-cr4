import React, {Component} from 'react';

class DonorEditTab extends Component {
    constructor(props) {
        super(props);

    }

    render() {

        let profileTab = null;
      
            profileTab = <div className="col-sm-4 collapse-sm">
                <div id="step1" className="tab-pil" data-toggle="tab" href="#profile">
                    <div className="step">1</div>
                    <div className="icon">
                        <img
                            src="/img/icons/createdonoricons/icon_personal_profile.svg"
                            alt="Personal Profile Icon"/>
                    </div>
                    <p className="ucase">Personal Profile</p>
                </div>
            </div>
        

        return (
            <div>
                {profileTab}
                <div className="col-sm-4 collapse-sm">
                    <div id="step2" className="tab-pil" data-toggle="tab" href="#medical">
                        <div className="step">2</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createdonoricons/icon_medical_profile.svg"
                                alt="Personal Profile Icon"/>
                        </div>
                        <p className="ucase">Medical Profile</p>
                    </div>
                </div>
                <div className="col-sm-4 collapse-sm">
                    <div id="step3" className="tab-pil" data-toggle="tab" href="#recruitment">
                        <div className="step">3</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createdonoricons/icon-recruitment-profile.svg"
                                alt="Personal Profile Icon"/>
                        </div>
                        <p className="ucase">Recruitment Profile</p>
                    </div>
                </div>
                <div className="col-sm-4 collapse-sm">
                    <div id="step4" className="tab-pil" data-toggle="tab" href="#schedule">
                        <div className="step">4</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createdonoricons/icon_schedule.svg"
                                alt="Personal Profile Icon"/>
                        </div>
                        <p className="ucase">Schedules</p>
                    </div>
                </div>
                <div className="col-sm-4 collapse-sm">
                    <div id="step5" className="tab-pil" data-toggle="tab" href="#payments">
                        <div className="step">5</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createdonoricons/icon-payments.svg"
                                alt="Personal Profile Icon"/>
                        </div>
                        <p className="ucase">Payments</p>
                    </div>
                </div>
                <div className="col-sm-4 collapse-sm">
                    <div id="step6" className="tab-pil" data-toggle="tab" href="#outreach">
                        <div className="step">6</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createdonoricons/icon-outreach.svg"
                                alt="Personal Profile Icon"/>
                        </div>
                        <p className="ucase">OutReach</p>
                    </div>
                </div>
            </div>

        );
    }
}

export default DonorEditTab;
