import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import {API_STATUS} from '../../common/constants';
import DonorCreateTab from './donorCreateTab'
import DonorEditTab from './donorEditTab'
import DonorProfile from './donorProfile'
import DonorMedical from './donorMedical'
import DonorSchedule from './donorSchedule'
import DonorRecruitment from './donorRecruitment'
import DonorPayment from './donorPayment'
import DonorOutReach from './donorOutReach'
import DonorPdfPrintForm from './donorPdfPrintForm'
import isEqual from 'lodash';
import * as validations from '../../validations/common'
import * as actionTypes from '../../constants/actionTypes';

class DonorForm extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            donor: jQuery.extend(true, {}, this.props.donor)
        });

        this.onSave = this
            .onSave
            .bind(this);
        this.onSaveVolume = this
            .onSaveVolume
            .bind(this);
        this.printPdf = this
            .printPdf
            .bind(this);
        this.completePrintPdf = this
            .completePrintPdf
            .bind(this);

        this.hasUnsavedChanges = this
            .hasUnsavedChanges
            .bind(this);
        this.autoSave = this
            .autoSave
            .bind(this);
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('inner');

        this.hasInitCreateForm = false;
        this.hasInitEditForm = false;
        this.id = this.props.routeParams.id;
        if (this.id) {
            this.initEditForm(this.id);
        } else {
            document
                .body
                .classList
                .add('create-donor');
            document.title = 'Create Donor';
        }

        var self = this;
        setTimeout(function () {
            self.jqDidMount();
        }, 100);
    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('create-donor');
        document
            .body
            .classList
            .remove('edit-donor');
        document
            .body
            .classList
            .remove('inner');
    }

    setActiveTab() {
        var changed;
        if ($("#profile").is(":visible ")) {
            $('div[href="profile"]').addClass("active");
        } else if ($("#medical").is(":visible ")) {
            $('div[href="medical"]').addClass("active");
        } else if ($("#payments").is(":visible ")) {
            $('div[href="payments"]').addClass("active");
        } else if ($("#schedule").is(":visible ")) {
            $('div[href="schedule"]').addClass("active");
        } else if ($("#recruitment").is(":visible ")) {
            $('div[href="recruitment"]').addClass("active");
        }
        return changed;
    }

    hasUnsavedChanges() {
        var changed;
        if ($("#profile").is(":visible ")) {
            changed = this
                .refs
                .profile
                .hasUnsavedChanges();
        } else if ($("#medical").is(":visible ")) {
            changed = this
                .refs
                .medical
                .hasUnsavedChanges();
        } else if ($("#payments").is(":visible ")) {
            changed = this
                .refs
                .payments
                .hasUnsavedChanges();
        } else if ($("#schedule").is(":visible ")) {
            changed = this
                .refs
                .schedule
                .hasUnsavedChanges();
        } else if ($("#recruitment").is(":visible ")) {
            changed = this
                .refs
                .recruitment
                .hasUnsavedChanges();
        }
        return changed;
    }

    autoSave(nextTarget) {
        var changed;
        if ($("#profile").is(":visible ")) {
            this
                .refs
                .profile
                .autoSave(nextTarget);
        } else if ($("#medical").is(":visible ")) {
            this
                .refs
                .medical
                .autoSave(nextTarget);
        } else if ($("#payments").is(":visible ")) {
            this
                .refs
                .payments
                .autoSave(nextTarget);
        } else if ($("#recruitment").is(":visible ")) {
            this
                .refs
                .recruitment
                .autoSave(nextTarget);
        } else if ($("#schedule").is(":visible ")) {
            this
                .refs
                .schedule
                .autoSave(nextTarget);
        }
    }

    onCancel() {
        var changed;
        if ($("#profile").is(":visible ")) {
            this
                .refs
                .profile
                .onCancel();
        } else if ($("#medical").is(":visible ")) {
            this
                .refs
                .medical
                .onCancel();
        } else if ($("#payments").is(":visible ")) {
            this
                .refs
                .payments
                .onCancel();
        } else if ($("#recruitment").is(":visible ")) {
            this
                .refs
                .recruitment
                .onCancel();
        } else if ($("#schedule").is(":visible ")) {
            this
                .refs
                .schedule
                .onCancel();
        }
    }

    initCreateForm() {
        this.jqTabClick();

        if (this.hasInitCreateForm) 
            return;
        
        this.id = null; //reset donor id
        this.hasInitCreateForm = true;
        this.hasInitEditForm = false;
        document
            .body
            .classList
            .remove('edit-donor');
        document
            .body
            .classList
            .add('create-donor');
        document.title = 'Create Donor';

        //reset
        this
            .refs
            .profile
            .clearData();
        this
            .refs
            .schedule
            .clearData();

        setTimeout(function () {
            $('.date')
                .each(function () {
                    $(this).data('date', null);
                    $(this).datetimepicker('update');
                })
        }, 20);

        $("#donorCreateTabs .tab-pil").removeAttr("data-toggle");
        $("#step1").removeClass("disable");
        $("#step2").addClass("disable");
        $("#step3").addClass("disable");
        $("#step4").addClass("disable");

        $(".tab-pil").removeClass("active");
        $("#step1").addClass("active");
        $("#step1").tab("show");

    }

    initEditForm(donorId) {
        this.jqTabClick();

        if (this.hasInitEditForm) 
            return;
        
        this.hasInitEditForm = true;
        this.hasInitCreateForm = false;

        document
            .body
            .classList
            .remove('create-donor');
        document
            .body
            .classList
            .add('edit-donor');
        document.title = 'Edit Donor';

        this.id = donorId;
        this
            .props
            .actions
            .getDonor(this.id);

        var didOnly = (this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id >= 3);
        $(".tab-pil").removeClass("active");
        if (didOnly) {
            $("#step1").addClass("disable");
            $("#step1").removeClass("active")
            $("#step2").addClass("active");
            $("#step2").tab("show");
        } else {
            $("#step1").removeClass("disable");
            $("#step1").addClass("active");
            $("#step1").tab("show");
        }

    }

    viewDonorFromQuickSearch(donorId) {
        window
            .history
            .replaceState(null, null, "#/donor/edit-donor/" + donorId);

        if (!this.hasInitEditForm) {
            this.initEditForm(donorId);
        } else {
            this.id = donorId; //update global donor id value
            this
                .props
                .actions
                .getDonor(donorId);
        }

        //Check if has confirm appointment form for previous donor
        if (this.state.donor.matchedScheduleId) {
            //clear matchedScheduleId
            this
                .refs
                .schedules
                .clearMatchedSchedule();
        }

        //check canlendar is rendered?
        if (this.refs.schedule.hasRenderCalendar) {
            $("#create_donor_schedule").fullCalendar('removeEvents');
            $("#create_donor_schedule").hide();
            $(".fc-EventCount-button").text("0");
            if (this.refs.isViewToday) {
                // Reload list appointment for today view because viewRender event not raise
                // when reopen and view today (view no change). In other cases, view is change
                // so viewRender event is raise and api getAppointments is auto called
                this.refs.schedule.getAppointmentsComplete = false;
                this
                    .props
                    .appointmentActions
                    .getAppointments(null, this.id, this.refs.schedule.startTime, this.refs.schedule.endTime);
            }
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.donor.actionType == actionTypes.VIEW_DONOR_QUICK_SEARCH || (location.hash.indexOf("#/donor/edit-donor/") < 0 && nextProps.donor.actionType == actionTypes.CHANG_ROUTE)) {
            let donorId = null;
            if (nextProps.donor.actionType == actionTypes.VIEW_DONOR_QUICK_SEARCH) {
                donorId = nextProps.donor.donorId;
            }
            if (this.hasUnsavedChanges()) {
                if (!$(".jconfirm").hasClass("jconfirm-open") && !this.showConfirm) {
                    const self = this;
                    this.showConfirm = true;
                    $.confirm({
                        title: 'Confirm leave',
                        content: 'Changes were made. Would you like to leave and discard them or stay to save?',
                        buttons: {
                            No: {
                                text: 'Leave',
                                btnClass: 'btn-danger',
                                action: function () {
                                    self.showConfirm = false;
                                    if (donorId == null || donorId == undefined || donorId == "undefined") {
                                        self.hasInitCreateForm = false;
                                        self.initCreateForm();
                                    } else {
                                        //view donor from quick search
                                        self.viewDonorFromQuickSearch(donorId);
                                    }
                                }

                            },
                            cancel: {
                                text: 'Stay',
                                btnClass: 'btn-blue',
                                action: function () {
                                    self.showConfirm = false;
                                }
                            }
                        }
                    });
                }
            } else { //no change
                if (donorId == null || donorId == undefined || donorId == "undefined") {
                    this.initCreateForm();
                } else {
                    this.viewDonorFromQuickSearch(donorId);
                }
            }
        } else if (nextProps.donor.actionType == actionTypes.CHANG_ROUTE) {
            //edit donor from search donor
            this.setState({
                donor: jQuery.extend(true, {}, nextProps.donor)
            })

            if (location.hash.indexOf("#/donor/edit-donor/") >= 0) {
                let donorId = location
                    .hash
                    .replace("#/donor/edit-donor/", "");

                this.initEditForm(donorId);
                let matchedScheduleId = nextProps.donor.matchedScheduleId;
                if (matchedScheduleId && matchedScheduleId != '') {
                    $(".tab-pil").removeClass("active");
                    $("#step4").tab("show");
                    $("#step4").addClass("active");
                    if (!this.loadSchedulingRequest) {
                        let seft = this;
                        setTimeout(function () {
                            seft
                                .props
                                .getSchedulingRequestAction(matchedScheduleId);
                        }, 20);

                        this.loadSchedulingRequest = true;
                    }
                }
            }
        } else {
            //(important) need set donor state for all action type and status
            this.setState({
                donor: jQuery.extend(true, {}, nextProps.donor)
            })

            if (nextProps.donor.actionType == actionTypes.GET_DONOR && nextProps.donor.status == API_STATUS.DONE) {
                this.jqDatePickerUpdate(nextProps.donor.data);
            } else if (nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.status == API_STATUS.DONE) {
                if (this.id == null) { //create donor
                     this.jqDatePickerUpdate(nextProps.donor.data);
                     
                    // reset flag hasInitCreateForm to allow reset data to blank when click link
                    // create donor
                    this.hasInitCreateForm = false;
                    if (nextProps.donor && nextProps.donor.data.id) {
                        //set new id
                        this.id = nextProps.donor.data.id;
                    }
                }
            } else if ((nextProps.donor.actionType == actionTypes.SAVE_APPOINTMENT || nextProps.donor.actionType == actionTypes.CANCEL_APPOINTMENT || nextProps.donor.actionType == actionTypes.CHECK_OUT_APPOINTMENT) && nextProps.donor.status == API_STATUS.DONE) {
                const self = this;
                setTimeout(function () {
                    //Reload donor
                    self
                        .props
                        .actions
                        .getDonor(self.id);

                    //Reload list appointment on calendar
                    if ((nextProps.donor.actionType == actionTypes.SAVE_APPOINTMENT && !nextProps.donor.confirmSchedule) || nextProps.donor.actionType == actionTypes.CANCEL_APPOINTMENT) {
                        self.refs.schedule.getAppointmentsComplete = false;
                        self
                            .props
                            .appointmentActions
                            .getAppointments(null, self.id, self.refs.schedule.startTime, self.refs.schedule.endTime);
                    }
                }, 3000);
            }
        }
    }

    onSaveVolume(payment)
    {
        this
            .props
            .actions
            .savePaymentVolume(payment);
    }

    onSave(tabName, data) {
        if (tabName == "Payment") {
            this
                .props
                .actions
                .savePayment(data, "Payment");
        } else {
            this
                .props
                .actions
                .saveDonor(tabName, data);
        }
    }

    jqTabClick() {
        const self = this;

        //tab click event
        $(".tab-pil").off("click");
        $(".tab-pil").click(function (event) {
            var target = $(event.target).closest(".tab-pil");
            if ($(target).hasClass("disable") || $($(target).attr("href")).is(":visible ")) {
                event.preventDefault()
                event.stopPropagation();
            } else if (!$(".jconfirm").hasClass("jconfirm-open") && self.hasUnsavedChanges()) {
                event.preventDefault()
                event.stopPropagation();

                setTimeout(function () {
                    $(target).removeClass("active");
                    self.setActiveTab();
                }, 100);

                $.confirm({
                    title: 'Confirm leave',
                    content: 'Changes were made. Would you like to leave and discard them or stay to save?',
                    buttons: {
                        No: {
                            text: 'Leave',
                            btnClass: 'btn-danger',
                            action: function () {
                                self.onCancel();
                                $(".tab-pil").removeClass("active");
                                $(target).addClass("active");
                                $(target).tab("show");
                            }
                        },
                        cancel: {
                            text: 'Stay',
                            btnClass: 'btn-blue',
                            action: function () {}
                        }
                    }
                });
            } else {
                $(".tab-pil").removeClass("active");
                $(this).addClass("active");

                if ($(target).attr("id") != "#step4") {
                    if ($(".popup-tooltip").hasClass('active')) {
                        $(".popup-tooltip").removeClass('active');
                        if (self.props.layoutActions) {
                            self
                                .props
                                .layoutActions
                                .closeAppointmentPopup(false);
                        }
                    }
                }
            }

        });
    }

    jqDidMount() {
        $(".select2-container").css("width", "");
        this.jqTabClick();
    }

    jqDatePickerUpdate(donor) {
        this
            .refs
            .profile
            .jqDatePickerUpdate(donor);
        this
            .refs
            .medical
            .jqDatePickerUpdate(donor);
        this
            .refs
            .recruitment
            .jqDatePickerUpdate(donor);

    }

    printPdf()
    {
        if (this.state.donor && this.state.donor.data && this.state.donor.data.id) {
            this
                .props
                .actions
                .printPdf(this.state.donor.data);
        }
    }

    completePrintPdf()
    {
        this
            .props
            .actions
            .completePrintPdf();
    }

    render() {
        var canSaveDonor = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 2;

        var didOnly = (this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id >= 3);

        return (
            <div>
                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>Manage Donors</li>
                        <li className="active">{this.props.routeParams.id
                                ? 'Edit Donor'
                                : 'Create Donor'}</li>
                    </ol>
                </div>
                <div id="section-title" className="clearfix">
                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">{this.props.routeParams.id
                                ? 'Edit Donor'
                                : 'Create Donor'}</h2>
                    </div>

                    {this.props.routeParams.id && this.props.donor.pdfPrintUrl == null && !didOnly
                        ? <div className="pull-right-lg txt-center-xs filters">
                                <button className="btn btn-print btn-hollow" onClick={this.printPdf}>Print Screening Form</button>
                            </div>
                        : null}
                    {this.props.donor.pdfPrintUrl
                        ? <DonorPdfPrintForm
                                actionPath={this.props.donor.pdfPrintUrl}
                                data={this.props.donor.pdfPrintData}
                                onDownloadComplete={this.completePrintPdf}/>
                        : null
}

                </div>

                <div className="panel">
                    <div className="panel-body">
                        {this.props.routeParams.id
                            ? <DonorEditTab didOnly={didOnly}/>
                            : <DonorCreateTab/>
}
                        <div className="tab-content col-sm-24">
                            <DonorProfile
                                ref="profile"
                                onSave={this.onSave}
                                donor={this.state.donor}
                                didOnly={didOnly}
                                layoutActions={this.props.layoutActions}
                                getDonorAction={this.props.actions.getDonor}
                                canSaveDonor={canSaveDonor}/>
                            <DonorMedical
                                ref="medical"
                                onSave={this.onSave}
                                donor={this.state.donor}
                                didOnly={didOnly}
                                formType={this.props.routeParams.id
                                ? "Edit"
                                : "Add"}
                                user={this.props.user}
                                layoutActions={this.props.layoutActions}
                                openEditDonorProcedureAvailability={this.props.layoutActions.openEditDonorProcedureAvailability}
                                getDonorAction={this.props.actions.getDonor}
                                canSaveDonor={canSaveDonor}/>
                            <DonorSchedule
                                ref="schedule"
                                donor={this.state.donor}
                                user={this.props.user}
                                didOnly={didOnly}
                                layoutActions={this.props.layoutActions}
                                getDonorAction={this.props.actions.getDonor}
                                getSchedulingRequestDonorMatchAction={this.props.getSchedulingRequestDonorMatchAction}
                                getSchedulingRequestAction={this.props.getSchedulingRequestAction}
                                processScheduleActions={this.props.processScheduleActions}
                                appointmentActions={this.props.appointmentActions}
                                layoutActions={this.props.layoutActions}
                                canSaveDonor={canSaveDonor}/>
                            <DonorRecruitment
                                ref="recruitment"
                                onSave={this.onSave}
                                donor={this.state.donor}
                                didOnly={didOnly}
                                layoutActions={this.props.layoutActions}
                                getDonorAction={this.props.actions.getDonor}
                                canSaveDonor={canSaveDonor}/> {this.props.routeParams.id
                                ? <DonorPayment
                                        ref="payments"
                                        onSave={this.onSave}
                                        onSaveVolume={this.onSaveVolume}
                                        donor={this.state.donor}
                                        didOnly={didOnly}
                                        layoutActions={this.props.layoutActions}
                                        canSaveDonor={canSaveDonor}
                                        getDonorAction={this.props.actions.getDonor}/>
                                : ''}
                            {this.props.routeParams.id
                                ? <DonorOutReach
                                        donor={this.state.donor}
                                        didOnly={didOnly}
                                        canSaveDonor={canSaveDonor}
                                        openAddUserDialog={this.props.layoutActions.openAddOutReachDialog}
                                        user={this.props.user}/>
                                : ''}

                        </div>

                    </div>
                </div>

            </div>

        )
    }
}

export default DonorForm