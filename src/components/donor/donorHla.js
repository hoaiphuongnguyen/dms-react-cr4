import React, {Component} from 'react';

class DonorHla extends Component {
    constructor(props) {
        super(props);        
    }
     
    showAlleleValue(type, value) {
        if (value == '') {
            return type;
        } else {
            return type + ":" + value;
        }
    }

    render() {
        const items = this.props.donor.hlaTypings;
        var geneTypes = [];
        geneTypes.push("A");
        geneTypes.push("B");
        geneTypes.push("C");
        geneTypes.push("DPA1");
        geneTypes.push("DPB1");
        geneTypes.push("DQA1");
        geneTypes.push("DQB1");
        geneTypes.push("DRB1");
        geneTypes.push("DRB3");
        geneTypes.push("DRB4");
        geneTypes.push("DRB5");

        var listHla = [];
        var hla;
        geneTypes.map(function (geneType, i) {
            var aa1 = '';
            var aa2 = '';
            var bb1 = '';
            var bb2 = '';
            var cc1 = '';
            var cc2 = '';
            var dd1 = '';
            var dd2 = '';

            if (items && items.length > 0) {
                items
                    .map(function (item, j) {
                        if (item.geneType == geneType) {
                            aa1 = item.aa1 == null
                                ? ''
                                : item.aa1 < 10
                                    ? ' ' + item.aa1
                                    : item.aa1;
                            aa2 = item.aa2 == null
                                ? ''
                                : item.aa2 < 10
                                    ? ' ' + item.aa2
                                    : item.aa2;
                            bb1 = item.bb1 == null
                                ? ''
                                : item.bb1 < 10
                                    ? ' ' + item.bb1
                                    : item.bb1;
                            bb2 = item.bb2 == null
                                ? ''
                                : item.bb2 < 10
                                    ? ' ' + item.bb2
                                    : item.bb2;
                            cc1 = item.cc1 == null
                                ? ''
                                : item.cc1 < 10
                                    ? ' ' + item.cc1
                                    : item.cc1;
                            cc2 = item.cc2 == null
                                ? ''
                                : item.cc2 < 10
                                    ? ' ' + item.cc2
                                    : item.cc2;
                            dd1 = item.dd1 == null
                                ? ''
                                : item.dd1 < 10
                                    ? ' ' + item.dd1
                                    : item.dd1;
                            dd2 = item.dd2 == null
                                ? ''
                                : item.dd2 < 10
                                    ? ' ' + item.dd2
                                    : item.dd2;
                            return;
                        }
                    })
            }

            hla = new Object();
            hla.id = i * 2;
            hla.geneType = geneType;
            hla.allele = 1;
            hla.aa = aa1;
            hla.bb = bb1;
            hla.cc = cc1;
            hla.dd = dd1;
            listHla.push(hla);

            hla = new Object();
            hla.id = i * 2 + 1;
            hla.geneType = geneType;
            hla.allele = 2;
            hla.aa = aa2;
            hla.bb = bb2;
            hla.cc = cc2;
            hla.dd = dd2;
            listHla.push(hla);
        });

        return (
            <div>

                <div className="col-sm-24 ">
                    <h4>HLA</h4>
                </div>

                <div className="col-sm-24">
                    <div id="hlatyping" className="table-responsive">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th colSpan="5">
                                        HLA TYPING
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {listHla.map((item, i) => (
                                    <tr key={item.id}>
                                        <td>Gene Type {item.geneType}, Allele {item.allele}</td>
                                        <td>{this.showAlleleValue('AA', item.aa)}</td>
                                        <td>{this.showAlleleValue('BB', item.bb)}</td>
                                        <td>{this.showAlleleValue('CC', item.cc)}</td>
                                        <td>{this.showAlleleValue('DD', item.dd)}</td>
                                    </tr>

                                ))}

                            </tbody>
                        </table>

                    </div>

                </div>
            </div>

        );

    }
}

export default DonorHla;