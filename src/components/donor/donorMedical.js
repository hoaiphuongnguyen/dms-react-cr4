import React, {Component} from 'react';
import DonorPersonInfo from './donorPersonInfo';
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import * as validations from '../../validations/common'
import DonorHla from './donorHla'
import {API_STATUS} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import isEqual from 'lodash';
import {hashHistory} from 'react-router';

class DonorMedical extends Component {
  constructor(props) {
    super(props);
    this.state = {
      donor: jQuery.extend(true, {}, this.props.donor.data),
      orgDonor: jQuery.extend(true, {}, this.props.donor.data),
      errors: {},
      processing: false,
      autoSave: false
    }

    this.onInvalid = this
      .onInvalid
      .bind(this);
    this.onValid = this
      .onValid
      .bind(this);
    this.onChange = this
      .onChange
      .bind(this);

    this.autoSave = this
      .autoSave
      .bind(this);
    this.onSave = this
      .onSave
      .bind(this);
    this.onCancel = this
      .onCancel
      .bind(this);

    this.inputControls = [];
  }

  hasUnsavedChanges() {
    if (this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.height == undefined) {
      this.state.donor.donorMedicalData.height = null;
    }
    if (this.state.orgDonor.donorMedicalData && this.state.orgDonor.donorMedicalData.height == undefined) {
      this.state.orgDonor.donorMedicalData.height = null;
    }

    var changed = !_.isEqual(this.state.donor.donorViralTest, this.state.orgDonor.donorViralTest) || !_.isEqual(this.state.donor.donorProcedureAvailabilities, this.state.orgDonor.donorProcedureAvailabilities) || !_.isEqual(this.state.donor.donorMedicalData, this.state.orgDonor.donorMedicalData);
    return changed;
  }

  componentWillReceiveProps(nextProps) {

    var controls = this.inputControls;
    if (controls && controls.length > 0) {
      Object
        .keys(controls)
        .forEach(function (name) {
          if (controls[name]) 
            controls[name].clearError();
          }
        );
    }

    if ((nextProps.donor.status == API_STATUS.DONE && nextProps.donor.actionType == actionTypes.GET_DONOR) || (nextProps.donor.status == API_STATUS.DONE && nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.tabName == 'Profile')) {
      this.setState({
        donor: jQuery.extend(true, {}, nextProps.donor.data),
        orgDonor: jQuery.extend(true, {}, nextProps.donor.data)
      });

    } else if (nextProps.donor.status == API_STATUS.DONE && nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.tabName == 'Medical' && this.state.processing) {
      this.setState({
        processing: false,
        donor: jQuery.extend(true, {}, nextProps.donor.data),
        orgDonor: jQuery.extend(true, {}, nextProps.donor.data),
        errors: {},
        autoSave: false
      });

      if (this.state.autoSave) {
        if (this.state.nextTarget) {
          if (typeof(this.state.nextTarget) == "object") {
            $(".tab-pil").removeClass("active");
            $(this.state.nextTarget).addClass("active");
            $(this.state.nextTarget).tab("show");
          } else {
            const donorId = parseInt(this.state.nextTarget);
            if (donorId) {
              this
                .props
                .getDonorAction(donorId);
            } else {
              hashHistory.push(this.state.nextTarget);
            }
          }
          this.setState({nextTarget: null});
        }
      } else {
        $("#successMessage").html("Save medical profile successfully.");
        this
          .props
          .layoutActions
          .openSuccessDialog();
      }

    } else if (nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.status == API_STATUS.ERROR && nextProps.donor.tabName == 'Medical' && this.state.processing) {
      var message = "";
      if (nextProps.donor.data.cause) {
        message = nextProps.donor.data.cause;
        if (message.message) 
          message = message.message;
        message = message
          .replace("AlreadyExistsException: ", "")
          .replace("IllegalArgumentException: ", "");
      } else if (nextProps.donor.data.message) {
        message = nextProps.donor.data.message;
      }

      $("#errorDil").modal('show');
      this.setState({errorMessage: message, processing: false});
    }

  }

  onInvalid(name, message) {
    const errors = this.state.errors;
    errors[name] = message;
    this.setState({errors: errors})
  }

  onValid(name) {
    const errors = this.state.errors;
    errors[name] = "";
    this.setState({errors: errors})
  }

  onChange(name, value) {
    const donorData = this.state.donor;
    if (name.startsWith("eligible") || name.startsWith("wbAccumulatedVol") || name.startsWith("wbVolLeft") || name.startsWith("lastProcedureDate") || name.startsWith("lastProcedureVol") || name.startsWith("currentAvailability") || name.startsWith("nextAvailableDate") || name.startsWith("donationCount")) {
      var index = parseInt(name.substr(name.length - 1));
      donorData.donorProcedureAvailabilities[index][name.substr(0, name.length - 1)] = value;
    } else if (name.endsWith("TestDate") || name.endsWith("ElapsedDay") || name == "standardTestExpirationDate") {
      if (!donorData.donorViralTest) {
        donorData.donorViralTest = new Object();
      }
      donorData.donorViralTest[name] = value;
    } else {
      if (!donorData.donorMedicalData) {
        donorData.donorMedicalData = new Object();
      }

      if (value == "y") {
        donorData.donorMedicalData[name] = true;
      } else if (value == "n") {
        donorData.donorMedicalData[name] = false;
      } else if (value == '') {
        donorData.donorMedicalData[name] = null;
      } else {
        donorData.donorMedicalData[name] = value;
        if (name == "feet" || name == "inch" || name == "weight") {
          var feet = donorData.donorMedicalData["feet"];
          var inch = donorData.donorMedicalData["inch"];
          var weight = donorData.donorMedicalData["weight"];
          if (feet && feet != "" && weight && weight != "") {
            var height = parseInt(feet * 12);
            if (inch && inch != "") {
              height = height + parseInt(inch);
            }
            var bmi = 703 * weight / (height * height);
            donorData.donorMedicalData["bmi"] = bmi.toFixed(1);
          }
        }

      }
    }

    this.setState({donor: donorData});
  }

  saveData(autoSave) {
    this.setState({autoSave: autoSave});

    const donorData = this.state.donor;
    const self = this;
    let controls = this.inputControls;

    Object
      .keys(controls)
      .forEach(function (name) {
        if (name.startsWith("eligible") || name.startsWith("wbAccumulatedVol") || name.startsWith("wbVolLeft") || name.startsWith("lastProcedureDate") || name.startsWith("lastProcedureVol") || name.startsWith("currentAvailability") || name.startsWith("nextAvailableDate") || name.startsWith("donationCount")) {
          var index = parseInt(name.substr(name.length - 1));
          var value = donorData.donorProcedureAvailabilities[index][name.substr(0, name.length - 1)];
          controls[name].validateAllRules(name, value);
          if (name.startsWith("wbAccumulatedVol") || name.startsWith("wbVolLeft")) {
            var errorMessage = self.state.errors[name];
            if (!errorMessage || errorMessage.length == 0) {
              var vol = parseInt(value);
              if (vol < 0 || vol > 500) {
                if (name.startsWith("wbAccumulatedVol")) 
                  controls[name].setError("The 'Accumulated Vol' field should be between 0 - 500");
                else 
                  controls[name].setError("The 'Vol. Left to Donate' should be between 0 - 500");
                }
              }
          }
        } else if (name.endsWith("TestDate")) {
          controls[name].validateAllRules(name, donorData.donorViralTest
            ? donorData.donorViralTest[name]
            : '');
        } else {
          controls[name].validateAllRules(name, donorData.donorMedicalData
            ? donorData.donorMedicalData[name]
            : '');
        }
      });

    const errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })
    if (numError == 0) {
      this.setState({processing: true, errorMessage: "", successMessage: ""});
      this
        .props
        .onSave("Medical", this.state.donor);
    } else {
      this.setState({errorMessage: "", successMessage: ""});
      $(".error-list").show();
      //scroll top
      $('html, body').animate({
        scrollTop: 200
      }, 700);
    }
  }

  autoSave(nextTarget) {
    this.setState({nextTarget: nextTarget});
    this.saveData(true);
  }

  onSave() {
    this.saveData(false);
  }

  jqDatePickerUpdate(donor) {
    setTimeout(function () {
      $('#medical .date')
        .each(function () {
          let date = null;
          if (this.id.endsWith("TestDate")) {
            if (donor.donorViralTest) 
              date = donor.donorViralTest[this.id];
            }
          else if (this.id.startsWith("lastProcedureDate") || this.id.startsWith("nextAvailableDate")) {
            if (donor.donorProcedureAvailabilities) 
              date = donor.donorProcedureAvailabilities[this.id];
            }
          else if (donor.donorMedicalData) {
            date = donor.donorMedicalData[this.id];
          }
          $(this).data('date', date);
          $(this).datetimepicker('update');
        });
    }, 20);
  }

  onCancel() {
    this.setState({
      donor: jQuery.extend(true, {}, this.state.orgDonor),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (controls[name]) 
          controls[name].clearError();
        }
      );

    this.jqDatePickerUpdate(this.state.orgDonor);
  }

  renderProcedureAvailabilities() {
    if (this.state.donor && this.state.donor.donorProcedureAvailabilities) {
      const canEditAvailability = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <=2;

      const items = this.state.donor.donorProcedureAvailabilities;

      return (
        <div className="col-sm-24">
          <div className="table-responsive">
            <table className="table table-bordered">
              <thead>
                <tr>
                  <th>Procedure</th>
                  <th
                    width="2%"
                    style={{
                    maxWidth: "58px"
                  }}>Eligible</th>
                  <th style={{
                    maxWidth: "95px"
                  }}>Accumulated Vol.</th>
                  <th>Vol. Left To Donate</th>
                  <th style={{
                    minWidth: "142px"
                  }}>Last Procedure Date</th>
                  <th>Last Procedure Vol.</th>
                  <th
                    width="2%"
                    style={{
                    maxWidth: "88px"
                  }}>Current Availability</th>
                  <th style={{
                    minWidth: "142px"
                  }}>Next Availability Date</th>
                  <th># Of Lifetime Donations</th>

                </tr>
              </thead>
              <tbody>
                {items.map((item, i) => (
                  <tr key={item.procedureCategory}>
                    <td>{item.procedureCategory}</td>
                    <td style={{
                      textAlign: "center"
                    }}>
                      <InputControl
                        readOnly={!this.props.canSaveDonor}
                        type="eligible"
                        name={'eligible' + i}
                        checked={item.eligible}
                        onChange={this.onChange}/>

                    </td>
                    <td
                      id={'procedureCategory' + i}
                      style={{
                      textAlign: "center"
                    }}>{item.procedureCategory == "Whole Blood"
                        ? canEditAvailability
                          ? (<InputControl
                            hideLabel
                            type="text"
                            name={'wbAccumulatedVol' + i}
                            validate={validations.number}
                            maxLength="3"
                            errorMessage="The &#39;Accumulated Vol&#39; field must be a number"
                            value={item.wbAccumulatedVol}
                            inputClass="form-control text-right"
                            ref
                            ={(input) => {
                            this.inputControls['wbAccumulatedVol' + i] = input
                          }}
                            onChange={this.onChange}
                            onInvalid={this.onInvalid}
                            onValid={this.onValid}/>)
                          : (item.wbAccumulatedVol
                            ? item.wbAccumulatedVol
                            : '-')
                        : 'N/A'}
                    </td>
                    <td
                      id={'wbVolLeft' + i}
                      style={{
                      textAlign: "center"
                    }}>{item.procedureCategory == "Whole Blood"
                        ? canEditAvailability
                          ? (<InputControl
                            hideLabel
                            type="text"
                            name={'wbVolLeft' + i}
                            validate={validations.number}
                            maxLength="3"
                            errorMessage="The &#39;Vol. Left to Donate&#39; field must be a number"
                            value={item.wbVolLeft}
                            inputClass="form-control text-right"
                            ref
                            ={(input) => {
                            this.inputControls['wbVolLeft' + i] = input
                          }}
                            onChange={this.onChange}
                            onInvalid={this.onInvalid}
                            onValid={this.onValid}/>)
                          : (item.wbVolLeft
                            ? item.wbVolLeft
                            : '-')

                        : 'N/A'}

                    </td>
                    <td style={{
                      textAlign: "center"
                    }}>
                      {canEditAvailability
                        ? (<InputControl
                          type="date"
                          hideLabel={true}
                          name={'lastProcedureDate' + i}
                          validate={validations.date}
                          errorMessage={"The '" + item.procedureCategory + " Last Procedure Date' field does not seem to be valid."}
                          value={item.lastProcedureDate}
                          inputClass="form-control"
                          ref
                          ={(input) => {
                          this.inputControls['lastProcedureDate' + i] = input
                        }}
                          onChange={this.onChange}
                          onInvalid={this.onInvalid}
                          onValid={this.onValid}/>)
                        : (item.lastProcedureDate
                          ? item.lastProcedureDate
                          : '-')}</td>
                    <td
                      id={'lastProcedureVol' + i}
                      style={{
                      textAlign: "center"
                    }}>
                      {canEditAvailability
                        ? (<InputControl
                          hideLabel
                          type="text"
                          maxLength="25"
                          name={'lastProcedureVol' + i}
                          value={item.lastProcedureVol == 'N/A'
                          ? ''
                          : item.lastProcedureVol}
                          inputClass="form-control"
                          ref
                          ={(input) => {
                          this.inputControls['lastProcedureVol' + i] = input
                        }}
                          onChange={this.onChange}
                          onInvalid={this.onInvalid}
                          onValid={this.onValid}/>)
                        : (item.lastProcedureVol
                          ? item.lastProcedureVol
                          : '-')}</td>
                    <td
                      style={{
                      textAlign: "center"
                    }}
                      id={'currentAvailability' + i}>
                      {canEditAvailability
                        ? (<InputControl
                          type="eligible"
                          name={'currentAvailability' + i}
                          checked={item.currentAvailability}
                          onChange={this.onChange}/>)
                        : (item.currentAvailability != null
                          ? item.currentAvailability
                            ? 'Available'
                            : 'Not Available'
                          : '-')}</td>
                    <td style={{
                      textAlign: "center"
                    }}>
                      {canEditAvailability
                        ? (<InputControl
                          type="date"
                          hideLabel={true}
                          name={'nextAvailableDate' + i}
                          validate={validations.date}
                          errorMessage={"The '" + item.procedureCategory + " Next Availability Date' field does not seem to be valid."}
                          value={item.nextAvailableDate}
                          inputClass="form-control"
                          ref
                          ={(input) => {
                          this.inputControls['nextAvailableDate' + i] = input
                        }}
                          onChange={this.onChange}
                          onInvalid={this.onInvalid}
                          onValid={this.onValid}/>)
                        : (item.nextAvailableDate
                          ? item.nextAvailableDate
                          : '-')}</td>
                    <td
                      id={'donationCount' + i}
                      style={{
                      textAlign: "center"
                    }}>
                      {canEditAvailability
                        ? (<InputControl
                          type="text"
                          hideLabel
                          validate={validations.number}
                          errorMessage={"The '" + item.procedureCategory + " Number of Lifetime Donations' field must be a number."}
                          name={'donationCount' + i}
                          value={item.donationCount}
                          inputClass="form-control text-right"
                          ref
                          ={(input) => {
                          this.inputControls['donationCount' + i] = input
                        }}
                          onChange={this.onChange}
                          onInvalid={this.onInvalid}
                          onValid={this.onValid}/>)
                        : (item.donationCount
                          ? item.donationCount
                          : '0')}</td>
                  </tr>

                ))}
              </tbody>
            </table>
          </div>
        </div>
      );
    }
    return null;

  }

  render() {
    var cssHideSaveButton = this.props.canSaveDonor === false
      ? " hide"
      : "";

    return (
      <div id="medical" className="tab-pane fade">
        <div className="donor-form">
          <div className="col-sm-24 collapse-sm info clearfix">
            <div className="col-sm-3 content-div">
              <label className="ucase">Id:</label>
              <label>{this.state.donor.id}</label>
            </div>
          </div>

          <div className="col-sm-24 no-padding">
            <ErrorArea errorList={this.state.errors}/>
          </div>

          <div className="col-sm-24 tab-container">

            <div className="col-sm-24">
              <h4>Viral Testing</h4>
            </div>
            <div className="col-sm-24 viraltest collapse-sm">

              <InputControl
                readOnly
                ={!this.props.canSaveDonor}
                containerClass="col-sm-8 form-group"
                ref
                ={(input) => {
                this.inputControls["standardTestDate"] = input
              }}
                value={this.state.donor.donorViralTest && this.state.donor.donorViralTest.standardTestDate
                ? this.state.donor.donorViralTest.standardTestDate
                : ''}
                secondName="standardTestExpirationDate"
                secondValue={this.state.donor.donorViralTest && this.state.donor.donorViralTest.standardTestExpirationDate
                ? this.state.donor.donorViralTest.standardTestExpirationDate
                : 'N/A'}
                type="date"
                viralType="Standard"
                name="standardTestDate"
                label="Standard Viral test Date"
                secondLabel="Expiration Date"
                validate={validations.date}
                errorMessage="The &#39;Standard Viral test Date&#39; does not seem to be valid. Please check and enter again."
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-8 form-group"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["expandedTestDate"] = input
              }}
                value={this.state.donor.donorViralTest && this.state.donor.donorViralTest.expandedTestDate
                ? this.state.donor.donorViralTest.expandedTestDate
                : ''}
                secondName="expandedTestElapsedDay"
                secondValue={this.state.donor.donorViralTest && this.state.donor.donorViralTest.expandedTestElapsedDay
                ? this.state.donor.donorViralTest.expandedTestElapsedDay
                : 'N/A'}
                type="date"
                validate={validations.date}
                viralType="Elapsed"
                name="expandedTestDate"
                label="Expanded Virals Test Date"
                secondLabel="Elapsed Days Since Test"
                errorMessage="The &#39;Elapsed Days Since Test&#39; does not seem to be valid. Please check and enter again."
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                readOnly={!this.props.canSaveDonor}
                containerClass="col-sm-8 form-group"
                ref
                ={(input) => {
                this.inputControls["clinicalGradeTestDate"] = input
              }}
                type="date"
                viralType="Grade"
                name="clinicalGradeTestDate"
                value={this.state.donor.donorViralTest
                ? this.state.donor.donorViralTest.clinicalGradeTestDate
                : ''}
                secondName="clinicGradeTestElapsedDay"
                secondValue={this.state.donor.donorViralTest && this.state.donor.donorViralTest.clinicGradeTestElapsedDay
                ? this.state.donor.donorViralTest.clinicGradeTestElapsedDay
                : 'N/A'}
                label="Clinical Grade Virals Test Date "
                secondLabel="Elapsed Days Since Test"
                validate={validations.date}
                errorMessage="The &#39;Elapsed Days Since Test&#39; does not seem to be valid. Please check and enter again."
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

            </div>
            <div className="col-sm-24">
              <h4>Procedure Eligibility
              </h4>
            </div>
            {this.renderProcedureAvailabilities()}
            <div className="col-sm-24">
              <h4>Ethnic Profile</h4>
            </div>

            <InputControl
              containerClass="col-sm-5 form-group"
              type="select"
              name="ethnicity"
              readOnly
              ={!this.props.canSaveDonor}
              ref
              ={(input) => {
              this.inputControls["ethnicity"] = input
            }}
              value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.ethnicity
              ? this.state.donor.donorMedicalData.ethnicity
              : ''}
              label="EthniCity"
              emptyMessage="The &#39;EthniCity&#39; field cannot be left blank. Please select a EthniCity from the dropdown to continue."
              labelClass="ucase"
              inputClass="form-control cityselect"
              optionValues={[
              [
                "Hispanic or Latino ", "hispanic"
              ],
              [
                "Not Hispanic or Latino", "non_hispanic"
              ],
              ["Not Specified", "not_specified"]
            ]}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-5 form-group"
              type="select"
              name="race"
              readOnly
              ={!this.props.canSaveDonor}
              ref
              ={(input) => {
              this.inputControls["race"] = input
            }}
              value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.race
              ? this.state.donor.donorMedicalData.race
              : ''}
              label="Race"
              emptyMessage="The &#39;Race&#39; field cannot be left blank. Please select a Race from the dropdown to continue."
              labelClass="ucase"
              inputClass="form-control cityselect"
              optionValues={[
              [
                "American Indian/Alaskan Native", "american_indian"
              ],
              [
                "Asian", "asian"
              ],
              [
                "Black or African American", "black"
              ],
              [
                "Native Hawaiian or Other Pacific Islander", "hawaiian"
              ],
              [
                "White", "white"
              ],
              ["Other Race", "other"]
            ]}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <div className="col-sm-24">
              <h4>Vitals</h4>
            </div>

            <InputControl
              containerClass="col-sm-3 form-group"
              name="age"
              ref
              ={(input) => {
              this.inputControls["age"] = input
            }}
              value={this.state.donor.donorMedicalData
              ? this.state.donor.donorMedicalData.age
              : ''}
              label="Age"
              labelClass="ucase"
              inputClass="form-control"
              readOnly={true}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-3 form-group"
              type="select"
              name="gender"
              readOnly
              ={!this.props.canSaveDonor}
              ref
              ={(input) => {
              this.inputControls["gender"] = input
            }}
              value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.gender
              ? this.state.donor.donorMedicalData.gender
              : ''}
              label="Gender"
              emptyMessage="The &#39;Gender&#39; field cannot be left blank. Please select a Gender from the dropdown to continue."
              labelClass="ucase"
              inputClass="form-control cityselect"
              optionValues={[
              [
                "Male", "m"
              ],
              ["Female", "f"]
            ]}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <div className="col-sm-5 form-group">
              <div className="col-sm-24 collapse-sm">
                <label htmlFor="height" className="ucase">Height</label>
              </div>

              <InputControl
                containerClass="col-sm-12 collapse-l form-group"
                type="select"
                name="feet"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["feet"] = input
              }}
                value={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.feet
                : ''}
                label=""
                selectText="Feet"
                labelClass="hidden"
                inputClass="form-control cityselect"
                optionValues={[
                [
                  "4", "4"
                ],
                [
                  "5", "5"
                ],
                [
                  "6", "6"
                ],
                ["7", "7"]
              ]}
                showSelectOption={true}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-12 collapse-l form-group"
                type="select"
                name="inch"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["inch"] = input
              }}
                value={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.inch
                : ''}
                label=""
                selectText="Inch"
                showSelectOption={true}
                labelClass="hidden"
                inputClass="form-control cityselect"
                optionValues={[
                [
                  "0", "0"
                ],
                [
                  "1", "1"
                ],
                [
                  "2", "2"
                ],
                [
                  "3", "3"
                ],
                [
                  "4", "4"
                ],
                [
                  "5", "5"
                ],
                [
                  "6", "6"
                ],
                [
                  "7", "7"
                ],
                [
                  "8", "8"
                ],
                [
                  "9", "9"
                ],
                [
                  "10", "10"
                ],
                ["11", "11"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>
            </div>

            <InputControl
              readOnly
              ={!this.props.canSaveDonor}
              containerClass="col-sm-3 form-group"
              name="weight"
              ref
              ={(input) => {
              this.inputControls["weight"] = input
            }}
              value={this.state.donor.donorMedicalData
              ? this.state.donor.donorMedicalData.weight
              : ''}
              label="Weight(LBS)"
              validate={validations.weight}
              errorMessage="The &#39;Weight&#39; should be number. Please check the Weight(LBS) and enter again."
              labelClass="ucase"
              inputClass="form-control"
              maxLength="5"
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-3 form-group"
              name="bmi"
              ref
              ={(input) => {
              this.inputControls["bmi"] = input
            }}
              value={this.state.donor.donorMedicalData
              ? this.state.donor.donorMedicalData.bmi
              : ''}
              label="BMI"
              readOnly={true}
              validate={validations.bmi}
              errorMessage="The &#39;BMI&#39; should be number. Please check the BMI and enter again."
              labelClass="ucase"
              inputClass="form-control"
              maxLength="5"
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <div className="col-sm-24">
              <h4>Stats</h4>
            </div>
            <div className="col-sm-24 collapse-sm">
              <InputControl
                containerClass="col-sm-5 form-group"
                type="select"
                name="bloodType"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["bloodType"] = input
              }}
                value={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.bloodType
                : ''}
                label="Blood Type"
                labelClass="ucase"
                inputClass="form-control cityselect"
                showSelectOption={true}
                optionValues={[
                [
                  'A\uFF0B', "A+"
                ],
                [
                  'A \u2013', "A-"
                ],
                [
                  'B\uFF0B', "B+"
                ],
                [
                  'B \u2013', "B-"
                ],
                [
                  'O\uFF0B', "O+"
                ],
                [
                  'O \u2013', "O-"
                ],
                [
                  'AB \u2013', "AB-"
                ],
                ['AB\uFF0B', "AB+"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-5 form-group"
                type="select"
                name="isSmoker"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["isSmoker"] = input
              }}
                value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.isSmoker
                ? 'y'
                : (!this.state.donor.donorMedicalData || this.state.donor.donorMedicalData.isSmoker == null)
                  ? ''
                  : 'n'}
                label="Smoker"
                labelClass="ucase"
                inputClass="form-control cityselect"
                showSelectOption={true}
                optionValues={[
                [
                  "Yes", "y"
                ],
                ["No", "n"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-5 form-group"
                type="select"
                readOnly
                ={!this.props.canSaveDonor}
                name="cmvStatus"
                ref
                ={(input) => {
                this.inputControls["cmvStatus"] = input
              }}
                value={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.cmvStatus
                : ''}
                label="CMV Status"
                labelClass="ucase"
                inputClass="form-control cityselect"
                showSelectOption={true}
                optionValues={[
                [
                  "Positive", "positive"
                ],
                [
                  "Negative", "negative"
                ],
                ["Equivalent", "equivalent"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-5 form-group"
                type="select"
                readOnly
                ={!this.props.canSaveDonor}
                name="hasAllergy"
                ref
                ={(input) => {
                this.inputControls["hasAllergy"] = input
              }}
                value=
                {this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.hasAllergy ? 'y' :(!this.state.donor.donorMedicalData ||this.state.donor.donorMedicalData.hasAllergy==null) ? '' : 'n'}
                label="Allergies"
                labelClass="ucase"
                inputClass="form-control cityselect showhidedrop"
                showSelectOption={true}
                optionValues={[
                [
                  "Yes", "y"
                ],
                ["No", "n"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group"
                containerId="hasAllergyhide"
                type="select"
                readOnly
                ={!this.props.canSaveDonor}
                multiple
                name="donorMedicalAllergies"
                elementName="allergyType"
                ref
                ={(input) => {
                this.inputControls["donorMedicalAllergies"] = input
              }}
                label="Category"
                labelClass="ucase"
                inputClass="form-control allergyselectpicker"
                showSelectOption={true}
                optionValues={[
                [
                  "Food", "food"
                ],
                [
                  "Environment", "environmental"
                ],
                ["Medication", "medication"]
              ]}
                selectedValues={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.donorMedicalAllergies
                : ''}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

            </div>
            <div className="col-sm-24 collapse-sm">
              <InputControl
                containerClass="col-sm-4 form-group"
                type="select"
                readOnly
                ={!this.props.canSaveDonor}
                name="onMedication"
                ref
                ={(input) => {
                this.inputControls["onMedication"] = input
              }}
                value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.onMedication
                ? 'y'
                : (!this.state.donor.donorMedicalData || this.state.donor.donorMedicalData.onMedication == null)
                  ? ''
                  : 'n'}
                label="Medication"
                labelClass="ucase"
                inputClass="form-control cityselect showhidedrop"
                showSelectOption={true}
                optionValues={[
                [
                  "Yes", "y"
                ],
                ["No", "n"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group"
                containerId="onMedicationhide"
                name="donorMedications"
                elementName="medicationType"
                ref
                ={(input) => {
                this.inputControls["donorMedications"] = input
              }}
                label="Category"
                type="select"
                readOnly
                ={!this.props.canSaveDonor}
                multiple
                labelClass="ucase"
                inputClass="form-control medicalselectpicker"
                showSelectOption={true}
                optionValues={[
                [
                  "Birth Control", "birth_control"
                ],
                [
                  "Vitamin/Supplement", "supplement"
                ],
                [
                  "Over-Counter", "over_counter"
                ],
                [
                  "Prescription", "prescription"
                ],
                ["Steroid", "steroid"]
              ]}
                selectedValues={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.donorMedications
                : ''}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>
            </div>
            <div className="col-sm-24 ">
              <h4>Shots</h4>
            </div>

            <div className="col-sm-24 collapse-sm">

              <InputControl
                containerClass="col-sm-4 form-group"
                type="select"
                readOnly
                ={!this.props.canSaveDonor}
                name="hasTetanus"
                ref
                ={(input) => {
                this.inputControls["hasTetanus"] = input
              }}
                value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.hasTetanus
                ? 'y'
                : (!this.state.donor.donorMedicalData || this.state.donor.donorMedicalData.hasTetanus == null)
                  ? ''
                  : 'n'}
                label="Tetanus"
                labelClass="ucase"
                inputClass="form-control cityselect showhidedrop"
                showSelectOption={true}
                optionValues={[
                [
                  "Yes", "y"
                ],
                ["No", "n"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group"
                containerId="hasTetanushide"
                type="date"
                name="lastTetanusDate"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["lastTetanusDate"] = input
              }}
                value={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.lastTetanusDate
                : ''}
                label="Tetanus Date"
                validate={validations.date}
                errorMessage="The &#39;Tetanus Date&#39; does not seem to be valid. Please check and enter again."
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group"
                type="select"
                name="hasFlu"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["hasFlu"] = input
              }}
                value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.hasFlu
                ? 'y'
                : (!this.state.donor.donorMedicalData || this.state.donor.donorMedicalData.hasFlu == null)
                  ? ''
                  : 'n'}
                label="Flu"
                labelClass="ucase"
                inputClass="form-control cityselect showhidedrop"
                showSelectOption={true}
                optionValues={[
                [
                  "Yes", "y"
                ],
                ["No", "n"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group"
                containerId="hasFluhide"
                type="date"
                name="lastFluDate"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["lastFluDate"] = input
              }}
                value={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.lastFluDate
                : ''}
                label="Flu Date"
                validate={validations.date}
                errorMessage="The &#39;Flu Date&#39; does not seem to be valid. Please check and enter again."
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>
            </div>

            <div className="col-sm-24 collapse-sm">

              <InputControl
                containerClass="col-sm-4 form-group"
                type="select"
                name="hasHepB"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["hasHepB"] = input
              }}
                value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.hasHepB
                ? 'y'
                : (!this.state.donor.donorMedicalData || this.state.donor.donorMedicalData.hasHepB == null)
                  ? ''
                  : 'n'}
                label="Hep B"
                labelClass="ucase"
                inputClass="form-control cityselect showhidedrop"
                showSelectOption={true}
                optionValues={[
                [
                  "Yes", "y"
                ],
                ["No", "n"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group"
                containerId="hasHepBhide"
                type="date"
                name="lastHepBDate"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["lastHepBDate"] = input
              }}
                value={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.lastHepBDate
                : ''}
                label="Hep B Date"
                validate={validations.date}
                errorMessage="The &#39;Hep B Date&#39; does not seem to be valid. Please check and enter again."
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group"
                type="select"
                name="hasPneumonia"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["hasPneumonia"] = input
              }}
                value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.hasPneumonia
                ? 'y'
                : (!this.state.donor.donorMedicalData || this.state.donor.donorMedicalData.hasPneumonia == null)
                  ? ''
                  : 'n'}
                label="Pneumonia"
                labelClass="ucase"
                inputClass="form-control cityselect showhidedrop"
                showSelectOption={true}
                optionValues={[
                [
                  "Yes", "y"
                ],
                ["No", "n"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group"
                containerId="hasPneumoniahide"
                type="date"
                name="lastPneumoniaDate"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["lastPneumoniaDate"] = input
              }}
                value={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.lastPneumoniaDate
                : ''}
                label="Pneumonia Date"
                validate={validations.date}
                errorMessage="The &#39;Pneumonia Date&#39; does not seem to be valid. Please check and enter again."
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group"
                type="select"
                name="hasHpv"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["hasHpv"] = input
              }}
                value={this.state.donor.donorMedicalData && this.state.donor.donorMedicalData.hasHpv
                ? 'y'
                : (!this.state.donor.donorMedicalData || this.state.donor.donorMedicalData.hasHpv == null)
                  ? ''
                  : 'n'}
                label="Hpv"
                labelClass="ucase"
                inputClass="form-control cityselect showhidedrop"
                showSelectOption={true}
                optionValues={[
                [
                  "Yes", "y"
                ],
                ["No", "n"]
              ]}
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-4 form-group "
                containerId="hasHpvhide"
                type="date"
                readOnly
                ={!this.props.canSaveDonor}
                name="lastHpvDate"
                ref
                ={(input) => {
                this.inputControls["lastHpvDate"] = input
              }}
                value={this.state.donor.donorMedicalData
                ? this.state.donor.donorMedicalData.lastHpvDate
                : ''}
                label="Hpv Date"
                validate={validations.date}
                errorMessage="The &#39;Hpv Date&#39; does not seem to be valid. Please check and enter again."
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>
            </div>

            {this.state.donor && this.state.donor.id && this.state.donor.id != '' && this.props.formType == "Edit"
              ? <DonorHla donor={this.state.donor}/>
              : <div/>}

            <div className="col-sm-24">
              <button
                className={"btn btn-default" + cssHideSaveButton}
                disabled={this.state.processing}
                onClick={this.onSave}>Save</button>
              <button
                className={"btn btn-default btn-hollow" + cssHideSaveButton}
                onClick={this.onCancel}
                disabled={this.state.processing}>Cancel</button>
            </div>

          </div>
        </div>

      </div>

    )
  }
}

export default DonorMedical