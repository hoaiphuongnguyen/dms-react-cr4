import React, {Component} from 'react';
import DonorPersonInfo from './donorPersonInfo';
import {API_STATUS} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import {displayDate} from '../../utils/date.js'

class DonorOutReach extends Component {
    constructor(props) {
        super(props);
        this.state = {
            donor: jQuery.extend(true, {}, this.props.donor)
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.donor.status == API_STATUS.DONE) {
            this.setState({
                donor: jQuery.extend(true, {}, nextProps.donor.data)
            });

        }
    }

    renderCreatedBy(createdBy) {
        if (createdBy == null) {
            createdBy = localStorage.getItem("userInfo");;
        }
        if (createdBy) {
            var name = createdBy.firstName;
            if (createdBy.lastName) {
                name = name + ' ' + createdBy.lastName;
            }
            return name;
        } else {
            return '';
        }

    }

    renderOutreachRows() {
        const items = this.state.donor.outreachLogs;

        if (items) {
            return (items.map((item, i) => (
                <tr key={item.id}>
                    <td>{displayDate(item.createdAt)}</td>
                    <td>{item.scheduleRequest
                            ? item.scheduleRequest.id
                            : ''}</td>
                    <td>{item.outreachType}</td>
                    <td>{item.outreachReason.name}</td>
                    <td>
                        {this.renderCreatedBy(item.createdBy)}</td>
                    <td>{item.outreachOutcome.name}</td>
                    <td>{item.comment}</td>
                </tr>

            )))
        }
    }

    openOutReach()
    {
        var canAddLog = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 3;
        if (canAddLog) {
            var openAddOutReachDialog = this.props.openAddUserDialog;
            var donorId = this.state.donor.id;
            openAddOutReachDialog(donorId);
        }
    }

    renderOutreachLogs() {

        var openAddOutReachDialog = this.props.openAddUserDialog;
        var donorId = this.state.donor.id;
        return (
            <div className="tab-container col-sm-24 collapse-sm">
                <h4 className="icon-container">Outreach Log
                    <a
                        href="javascript:void(0)"
                        className={!this.props.canSaveDonor
                        ? "hide"
                        : " add-top"}
                        id="outreach2"
                        onClick={this
                        .openOutReach
                        .bind(this)}
                        title="Add Outreach Log"><img src="/img/icons/createdonoricons/icon_add.png" alt=""/></a>
                </h4>
                <div className="table-responsive">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Schedule Request No.</th>
                                <th>Type</th>
                                <th>Reason</th>
                                <th>Created By</th>
                                <th>Outcome</th>
                                <th>Comment</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderOutreachRows()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
    render() {

        return (
            <div id="outreach" className="tab-pane fade">
                <div className="donor-form">
                    {!this.props.didOnly
                        ? <DonorPersonInfo donor={this.state.donor}/>
                        : null}
                    {this.renderOutreachLogs()}
                </div>
            </div>
        );
    }
}

export default DonorOutReach;