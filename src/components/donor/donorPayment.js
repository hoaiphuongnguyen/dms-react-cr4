import React, {Component} from 'react'
import DonorPersonInfo from './donorPersonInfo'
import {API_STATUS} from '../../common/constants'
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import * as actionTypes from '../../constants/actionTypes'
import * as validations from '../../validations/common'
import {hashHistory} from 'react-router';
import isEqual from 'lodash';
import {displayDate} from '../../utils/date.js';

class DonorPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      donor: jQuery.extend(true, {}, this.props.donor.data),
      activePayments: [],
      orgActivePayments: [],
      errors: {},
      processing: false,
      autoSave: false
    }

    this.onInvalid = this
      .onInvalid
      .bind(this);
    this.onValid = this
      .onValid
      .bind(this);
    this.onChange = this
      .onChange
      .bind(this);
    this.onSave = this
      .onSave
      .bind(this);

    this.onCancel = this
      .onCancel
      .bind(this);

    this.inputControls = [];
  }

  hasUnsavedChanges() {
    var changed = !_.isEqual(this.state.activePayments, this.state.orgActivePayments);
    return changed;
  }

  onInvalid(name, message) {
    const errors = this.state.errors;
    errors[name] = message;
    this.setState({errors: errors})
  }

  onValid(name) {
    const errors = this.state.errors;
    errors[name] = "";
    this.setState({errors: errors})
  }

  onChange(name, value) {
    const payments = this.state.activePayments;
    if (name.startsWith("actualCollectedWbVol")) {
      var index = name.replace("actualCollectedWbVol", "");
      payments[index].actualCollectedWbVol = value;
    } else if (name.startsWith("cardNumber")) {
      var index = name.replace("cardNumber", "");
      payments[index].cardNumber = value;
    } else if (name.startsWith("amount")) {
      var index = name.replace("amount", "");
      payments[index].amount = value;
    }
    this.setState({activePayments: payments});
  }

  onSaveVolume(index) {
    var payment = this.state.activePayments[index];
    payment.donor = {
      id: this.state.donor.id
    };

    var controls = this.inputControls;
    if (!controls) 
      return;
    
    controls["actualCollectedWbVol" + index].validateAllRules("actualCollectedWbVol" + index, payment["actualCollectedWbVol"]);

    const errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })

    if (numError == 0) {
      this.setState({saveIndex: index, processing: true, errorMessage: "", successMessage: ""});

      this
        .props
        .onSaveVolume(payment);
    } else {
      this.setState({processing: false, errorMessage: "", successMessage: ""});
      $(".error-list").show();
      //scroll top
      $('html, body').animate({
        scrollTop: 200
      }, 700);
    }
  }

  saveData(index, autoSave) {
    this.setState({autoSave: autoSave});

    var payment = this.state.activePayments[index];
    payment.donor = {
      id: this.state.donor.id
    };

    var controls = this.inputControls;
    if (!controls) 
      return;
    
    controls["actualCollectedWbVol" + index].validateAllRules("actualCollectedWbVol" + index, payment["actualCollectedWbVol"]);
    controls["cardNumber" + index].validateAllRules("cardNumber" + index, payment["cardNumber"]);
    controls["amount" + index].validateAllRules("amount" + index, payment["amount"]);

    const errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })

    if (numError == 0) {
      this.setState({processing: true, errorMessage: "", successMessage: ""});

      var p = jQuery.extend(true, {}, payment);
      p.actualCollectedWbVol = null; //ignore this value when send to API payment
      this
        .props
        .onSave("Payment", p);
    } else {
      this.setState({processing: false, errorMessage: "", successMessage: ""});
      $(".error-list").show();
      //scroll top
      $('html, body').animate({
        scrollTop: 200
      }, 700);
    }
  }

  autoSave(nextTarget) {
    this.setState({nextTarget: nextTarget});
    var payments = this.state.activePayments;
    for (var i = 0; i < payments.length; i++) {
      if (!_.isEqual(payments[i], this.state.orgActivePayments[i])) {
        this.saveData(i, true);
      }
    }
  }

  onSave(index) {
    this.saveData(index, false);
  }

  onCancel(index) {
    this.setState({errors: {}, activePayments: this.state.orgActivePayments});
    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (controls[name]) 
          controls[name].clearError();
        }
      );
  }

  componentWillReceiveProps(nextProps) {
    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (controls[name]) 
          controls[name].clearError();
        }
      );

    if (nextProps.donor.status == API_STATUS.DONE && nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.tabName == 'Profile') {
      this.setState({
        donor: jQuery.extend(true, {}, nextProps.donor.data)
      });
    } else if (nextProps.donor.actionType == actionTypes.GET_DONOR && nextProps.donor.status == API_STATUS.DONE) {
      this.setState({
        donor: jQuery.extend(true, {}, nextProps.donor.data),
        activePayments: jQuery.extend(true, [], nextProps.donor.data.activePayments),
        orgActivePayments: jQuery.extend(true, [], nextProps.donor.data.activePayments)
      });
    } else if (nextProps.donor.actionType == actionTypes.SAVE_PAYMENT_VOLUME && nextProps.donor.tabName == 'Payment' && nextProps.donor.status == API_STATUS.DONE && this.state.processing) {
      this.setState({processing: false});
      $("#successMessage").html("Set payment volume successfully.");
      this
        .props
        .layoutActions
        .openSuccessDialog();
        
      if (this.state.donor.activePayments[this.state.saveIndex].amount == 0) {
        this
          .props
          .getDonorAction(this.state.donor.id);
      }

    } else if (nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.status == API_STATUS.DONE && nextProps.donor.tabName == 'Payment' && this.state.processing) {
      this.setState({processing: false});
      if (this.state.autoSave) {
        if (this.state.nextTarget) {
          if (typeof(this.state.nextTarget) == "object") {
            $(".tab-pil").removeClass("active");
            $(this.state.nextTarget).addClass("active");
            $(this.state.nextTarget).tab("show");
          } else {
            const donorId = parseInt(this.state.nextTarget);
            if (donorId) {
              this
                .props
                .getDonorAction(donorId);
            } else {
              hashHistory.push(this.state.nextTarget);
            }
          }
          this.setState({nextTarget: null});
        }
      } else {
        $("#successMessage").html("Complete Work Order successfully.");
        this
          .props
          .layoutActions
          .openSuccessDialog();
      }

      this
        .props
        .getDonorAction(this.state.donor.id);
    } else if ((nextProps.donor.actionType == actionTypes.SAVE_DONOR || nextProps.donor.actionType == actionTypes.SAVE_PAYMENT_VOLUME) && nextProps.donor.status == API_STATUS.ERROR && nextProps.donor.tabName == 'Payment' && this.state.processing) {
      var message = "";
      if (nextProps.donor.data.cause) {
        message = nextProps.donor.data.cause;
        if (message.message) 
          message = message.message;
        message = message
          .replace("AlreadyExistsException: ", "")
          .replace("IllegalArgumentException: ", "");
      } else if (nextProps.donor.data.message) {
        message = nextProps.donor.data.message;
      }
      if (message) {
        $("#errorDil #errorMessage").html(message);
      }
      $("#errorDil").modal('show');
      this.setState({errorMessage: message, processing: false});
    }
  }

  renderActivePayment() {
    var disabledCss = "";
    if (this.state.processing) 
      disabledCss = " disabled";
    const items = this.state.activePayments;
    if (items && items.length) {
      var self = this;
      var completePayment = function (index) {
        self.onSave(index);
      };

      var saveVolume = function (index) {
        self.onSaveVolume(index);
      };

      return (
        <div className="work-orders float-left-lastcol">

          <ErrorArea errorList={this.state.errors}/> {items.map((item, i) => (
            <article key={item.id} className="work-order">
              <div className="row">
                <div className="col-lg-5 col-sm-24 form-group">
                  <label className="ucase">Work Order Number</label>
                  <p>{item.workOrder}</p>
                </div>

                <div className="col-lg-4 col-sm-24 form-group">
                  <label className="ucase">Amount ($)</label>
                  <p>{item.amount}</p>
                </div>

                <div className="col-lg-4 col-sm-24 form-group">
                  <label className="ucase">Procedure</label>
                  <p className="icase">{item.procedureInfo.name}</p>
                </div>

                <InputControl
                  containerClass="col-lg-4 col-sm-24 form-group"
                  label="Collected Whole Blood Volume (ML)"
                  labelClass="ucase"
                  type="text"
                  name={'actualCollectedWbVol' + i}
                  readOnly
                  ={!this.props.canSaveDonor}
                  ref
                  ={(input) => {
                  this.inputControls['actualCollectedWbVol' + i] = input
                }}
                  value={this.state.activePayments[i].actualCollectedWbVol}
                  maxLength="3"
                  validate={validations.number}
                  errorMessage="The &#39;Collected Whole Blood Volume #&#39; does not seem to be valid. Please check and enter again."
                  labelClass="ucase"
                  inputClass="form-control "
                  onChange={this.onChange}
                  onInvalid={this.onInvalid}
                  onValid={this.onValid}/>

                <div className="col-lg-7 col-sm-24 form-group">
                  <label className="ucase">&nbsp;</label>
                  <a
                    className={!this.props.canSaveDonor
                    ? "hide"
                    : "btn btn-default" + disabledCss}
                    style={{
                    backgroundColor: '#4387FD',
                    minWidth: '200px'
                  }}
                    onClick={function () {
                    saveVolume(i);
                  }}>Set Volume/Confirm Attributes</a>
                </div>
              </div>
              <div className="row">

                <InputControl
                  containerClass="col-lg-5 col-sm-24 form-group"
                  hideLabel={true}
                  type="text"
                  name={'cardNumber' + i}
                  readOnly
                  ={!this.props.canSaveDonor}
                  ref
                  ={(input) => {
                  this.inputControls['cardNumber' + i] = input
                }}
                  value={this.state.activePayments[i].cardNumber}
                  maxLength="16"
                  placeholder="Swift Card #"
                  required={true}
                  emptyMessage="The &#39;Swift Card #&#39; field cannot be left empty. Please enter a value to continue."
                  validate={validations.number}
                  errorMessage="The &#39;Swift Card #&#39; does not seem to be valid. Please check and enter again."
                  labelClass="ucase"
                  inputClass="form-control full"
                  onChange={this.onChange}
                  onInvalid={this.onInvalid}
                  onValid={this.onValid}/>

                <InputControl
                  containerClass="col-lg-4 col-sm-24 form-group"
                  hideLabel={true}
                  type="text"
                  name={'amount' + i}
                  readOnly
                  ={!this.props.canSaveDonor}
                  ref
                  ={(input) => {
                  this.inputControls['amount' + i] = input
                }}
                  value={this.state.activePayments[i].amount}
                  maxLength="9"
                  placeholder={item.amount}
                  defaultValue={item.amount}
                  required={true}
                  emptyMessage="The &#39;Amount&#39; field cannot be left empty. Please enter a value to continue."
                  validate={validations.decimal}
                  errorMessage="The &#39;Amount&#39; does not seem to be valid. Please check and enter again."
                  labelClass="ucase"
                  inputClass="form-control"
                  onChange={this.onChange}
                  onInvalid={this.onInvalid}
                  onValid={this.onValid}/>

                <div className="col-lg-15 col-sm-24 form-group">
                  <a
                    className={!this.props.canSaveDonor
                    ? "hide"
                    : "btn btn-default" + disabledCss}
                    style={{
                    minWidth: '180px'
                  }}
                    onClick={function () {
                    completePayment(i);
                  }}>
                    <img src="/img/icons/editdonor/icon-payment.svg" alt="Funds"/>
                    Fund Card
                  </a>
                </div>
              </div>

            </article>

          ))}

        </div>
      );
    }
  }

  getTotalPastPayment() {
    const items = this.state.donor.paymentTransactions;
    if (!items || items.length == 0) {
      return '';
    }

    if (items.length < 10) {
      return '0' + items.length;
    } else {
      return items.length;
    }
  }

  getTotalAmountPastPayment() {
    const items = this.state.donor.paymentTransactions;
    if (!items || items.length == 0) {
      return '';
    }

    var total = 0;
    items.map(function (item) {
      total = total + item.amount;
    });
    return total;
  }

  renderPastPayment() {
    const items = this.state.donor.paymentTransactions;
    if (!items || items.length == 0) {
      return (
        <div>
          <h4>Past Payments
          </h4>
          <label>
            No past payment
          </label>
        </div>
      );
    }

    return (
      <div>
        <h4>Past Payments
        </h4>
        <div className="clear"></div>

        <div className="table-responsive allcells-responsive-tables">
          <table
            id="search_donor_dtab4"
            className="table allcells-tables"
            cellSpacing="0"
            width="100%">
            <thead>
              <tr>
                <th>Date</th>
                <th>Swift Card Number</th>
                <th>Work Order Number</th>
                <th>Amount</th>
                <th>Procedure</th>
                <th>COLLECTED WHOLE BLOOD VOL</th>
                <th>Paid By</th>
              </tr>
            </thead>
            <tbody>
              {items.map((item, i) => (

                <tr key={item.id}>
                  <td>{displayDate(item.createdAt)}</td>
                  <td>{item.cardNumber}</td>
                  <td>{item.workOrder}</td>
                  <td>{item.amount}</td>
                  <td>{item.procedureInfo.name}</td>
                  <td>{item.actualCollectedWbVol}</td>
                  <td>{item.createdBy.firstName} {item.createdBy.lastName
                      ? ' ' + item.createdBy.lastName
                      : ''}</td>
                </tr>

              ))}
            </tbody>
          </table>

        </div>

      </div>

    );
  }

  render() {
    return (
      <div id="payments" className="tab-pane fade">
        <div className="donor-form">
          {!this.props.didOnly
            ? <DonorPersonInfo donor={this.state.donor}/>
            : null}
          <div className="tab-container col-sm-24 collapse-sm">

            <div className="col-sm-24">
              <h4 className="secondary">Active Work Orders Requiring Payment</h4>
              {this.renderActivePayment()}

              <div className="clear"></div>

              <div className="spacer large"></div>

              {this.renderPastPayment()}
            </div>
          </div>

        </div>
      </div>

    );
  }
}

export default DonorPayment;