import React, {Component} from 'react'
import {API_STATUS} from '../../common/constants'
import * as actionTypes from '../../constants/actionTypes'
import ReactDOM from 'react-dom'

class DonorPdfPrintForm extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
        var element = ReactDOM.findDOMNode(this);
        var c = $(element).children(":first").submit();
        var self =this;
        setTimeout(function(){
            self.props.onDownloadComplete();
        }, 300);
    }

  render() {
      return (<div>
                <form
                  action={this.props.actionPath}
                  className="hidden"
                  method="POST"
                  target="pdf_iframe">
                  <input name="donorDID" type="text" value={this.props.data.id}/>
                  <input name="donorName" type="text" value={this.props.data.name}/>
                  <input name="donorDOB" type="text" value={this.props.data.dob}/>
                  <input name="donorEthnicity" type="text" value={this.props.data.ethnicity}/>
                  <input name="donorAge" type="text" value={this.props.data.age}/>
                  <input name="donorAddress" type="text" value={this.props.data.address}/>
                  <input name="donorHomePhone" type="text" value={this.props.data.primaryPhone}/>
                  <input name="donorCellPhone" type="text" value={this.props.data.secondaryPhone}/>
                  <input name="donorEmail" type="text" value={this.props.data.email}/>
                  <input name="donorHeight" type="text" value={this.props.data.height}/>
                  <input name="donorWeight" type="text" value={this.props.data.weight}/>
                  <input name="donorGender" type="text" value={this.props.data.gender}/>
              </form>
              <iframe id="pdf_iframe"></iframe>

      </div>)
      //return (<iframe  className="hidden" src={this.props.actionPath+"?"+ this.props.data} />)

     /*return (
            <form
                action={this.props.actionPath}
                className="hidden"
                method="POST"
            >
                <input name="donorDID" type="text" value={this.props.data.id}/>
                <input name="donorName" type="text" value={this.props.data.name}/>
                <input name="donorDOB" type="text" value={this.props.data.dob}/>
                <input name="donorEthnicity" type="text" value={this.props.data.ethnicity}/>
                <input name="donorAge" type="text" value={this.props.data.age}/>
                <input name="donorAddress" type="text" value={this.props.data.address}/>
                <input name="donorHomePhone" type="text" value={this.props.data.primaryPhone}/>
                <input name="donorCellPhone" type="text" value={this.props.data.secondaryPhone}/>
                <input name="donorEmail" type="text" value={this.props.data.email}/>
                <input name="donorHeight" type="text" value={this.props.data.height}/>
                <input name="donorWeight" type="text" value={this.props.data.weight}/>
                <input name="donorGender" type="text" value={this.props.data.gender}/>
            </form>
        );*/

      
  }
}

export default DonorPdfPrintForm;