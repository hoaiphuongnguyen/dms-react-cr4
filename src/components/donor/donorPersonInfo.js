import React, {Component} from 'react';
import {API_STATUS} from '../../common/constants';
class DonorPersonInfo extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        let address = '';
        if (this.props.donor.address) {
            address = this.props.donor.address.addressLine1;
            if (this.props.donor.address.addressLine2 && this.props.donor.address.addressLine2 != '') {
                address = address + ", " + this.props.donor.address.addressLine2;
            }

            if (this.props.donor.address.city && this.props.donor.address.city != '') {
                address = address + ", " + this.props.donor.address.city;
            }

            if (this.props.donor.address.state && this.props.donor.address.state != '') {
                address = address + ", " + this.props.donor.address.state;
            }

              if (this.props.donor.address.zip && this.props.donor.address.zip != '') {
                address = address + " " + this.props.donor.address.zip;
            }

        }

        return (
            <div className="col-sm-24 collapse-sm info clearfix">
                <div className="col-sm-5 content-div">
                    <label className="ucase">Donor:</label>
                    <label>{this.props.donor.firstName}&nbsp;{this.props.donor.lastName}</label>
                </div>
                <div className="col-sm-3 content-div">
                    <label className="ucase">Id:</label>
                    <label>{this.props.donor.id}</label>
                </div>
                <div className="col-sm-6 content-div">
                    <label className="ucase">Email:</label>
                    <label>{this.props.donor.email}</label>
                </div>
                <div className="col-sm-3 content-div">
                    <label className="ucase">Phone:</label>
                    <label>{this.props.donor.primaryPhone}</label>
                </div>
                <div className="col-sm-7 content-div">
                    <label className="ucase">Address:</label>
                    <label>{address}</label>
                </div>
            </div>

        );
    }
}

export default DonorPersonInfo;