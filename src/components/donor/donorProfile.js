import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import * as validations from '../../validations/common'
import {API_STATUS} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import {hashHistory} from 'react-router';
import isEqual from 'lodash';

class DonorProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      donor: jQuery.extend(true, {}, this.props.donor.data),
      initDonor: jQuery.extend(true, {}, this.props.donor.data),
      orgDonor: jQuery.extend(true, {}, this.props.donor.data),
      errors: {},
      processing: false,
      autoSave: false
    }

    this.onInvalid = this
      .onInvalid
      .bind(this);
    this.onValid = this
      .onValid
      .bind(this);
    this.onChangeProfile = this
      .onChangeProfile
      .bind(this);
    this.onChangeAddress = this
      .onChangeAddress
      .bind(this);

    this.autoSave = this
      .autoSave
      .bind(this);
    this.onSave = this
      .onSave
      .bind(this);
    this.onCancel = this
      .onCancel
      .bind(this);

    this.inputControls = {};

  }

  componentDidUpdate()
  {
    if (this.showErr) {
      $(".error-list").show();
      this.showErr = false;
    }
  }

  hasUnsavedChanges() {
    var changed = !_.isEqual(this.state.donor.firstName, this.state.orgDonor.firstName) || !_.isEqual(this.state.donor.lastName, this.state.orgDonor.lastName) || !_.isEqual(this.state.donor.email, this.state.orgDonor.email) || !_.isEqual(this.state.donor.dateOfBirth, this.state.orgDonor.dateOfBirth) || !_.isEqual(this.state.donor.primaryPhone, this.state.orgDonor.primaryPhone) || !_.isEqual(this.state.donor.secondaryPhone, this.state.orgDonor.secondaryPhone) || !_.isEqual(this.state.donor.address, this.state.orgDonor.address);
    return changed;
  }

  componentWillReceiveProps(nextProps) {

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (controls[name]) 
          controls[name].clearError();
        }
      );

    if (nextProps.donor.status == API_STATUS.DONE && nextProps.donor.actionType == actionTypes.GET_DONOR) {
      this.setState({
        donor: jQuery.extend(true, {}, nextProps.donor.data),
        orgDonor: jQuery.extend(true, {}, nextProps.donor.data)
      });
    } else if (nextProps.donor.status == API_STATUS.DONE && nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.tabName == 'Profile' && this.state.processing) {
      this.setState({
        donor: jQuery.extend(true, {}, nextProps.donor.data),
        orgDonor: jQuery.extend(true, {}, nextProps.donor.data),
        errors: {},
        processing: false,
        autoSave: false
      });
      if (this.state.autoSave) {
        if (this.state.nextTarget) {
          if (typeof(this.state.nextTarget) == "object") {
            $(".tab-pil").removeClass("active");
            $(this.state.nextTarget).addClass("active");
            $(this.state.nextTarget).tab("show");
          } else {
            const donorId = parseInt(this.state.nextTarget);
            if (donorId) {
              this
                .props
                .getDonorAction(donorId);
            } else {
              hashHistory.push(this.state.nextTarget);
            }
          }
          this.setState({nextTarget: null});
        }
      } else {
        $("#successMessage").html("Save personal profile successfully.");
        this
          .props
          .layoutActions
          .openSuccessDialog();
      }

      $(".tab-pil").removeClass("disable");
      $("#donorCreateTabs .tab-pil").attr("data-toggle", "tab");
      $("#donorCreateTabs .tab-pil").tab();

    } else if (nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.status == API_STATUS.ERROR && nextProps.donor.tabName == 'Profile' && this.state.processing) {
      var message = "";
      if (nextProps.donor.data.cause) {
        message = nextProps.donor.data.cause;
        if (message.message) 
          message = message.message;
        message = message
          .replace("AlreadyExistsException: ", "")
          .replace("IllegalArgumentException: ", "");
      } else if (nextProps.donor.data.message) {
        message = nextProps.donor.data.message;
      }
      if (message.indexOf("email") > 0) {
        var controls = this.inputControls;
        controls["email"].setError(message);
        this.showErr = true;
        //scroll top
        $('html, body').animate({
          scrollTop: 200
        }, 700);
      } else {
        $("#errorDil").modal('show');
      }
      this.setState({processing: false});
    }
  }

  onInvalid(name, message) {
    const errors = this.state.errors;
    errors[name] = message;
    this.setState({errors: errors})
  }

  onValid(name) {
    const errors = this.state.errors;
    errors[name] = "";
    this.setState({errors: errors})
  }

  onChangeProfile(name, value) {
    const donorData = this.state.donor;
    donorData[name] = value;
    this.setState({donor: donorData});
  }

  onChangeAddress(name, value) {
    const donorData = this.state.donor;
    donorData.address[name] = value;
    this.setState({donor: donorData});
  }

  saveData(autoSave) {
    this.setState({autoSave: autoSave});

    const donorData = this.state.donor;
    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (name == "firstName" || name == "lastName" || name == "email" || name == "dateOfBirth" || name == "primaryPhone" || name == "secondaryPhone") {
          controls[name].validateAllRules(name, donorData[name]);
        } else {
          controls[name].validateAllRules(name, donorData.address[name]);
        }
      });

    const errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })
    if (numError == 0) {
      this.setState({processing: true, errorMessage: "", successMessage: ""});
      this
        .props
        .onSave("Profile", this.state.donor);
    } else {

      this.setState({errorMessage: "", successMessage: ""});
      $(".error-list").show();
      //scroll top
      $('html, body').animate({
        scrollTop: 200
      }, 700);
    }
  }

  autoSave(nextTarget) {
    this.setState({nextTarget: nextTarget});
    this.saveData(true);
  }

  onSave() {
    this.saveData(false);
  }

  clearData() {
    this.setState({
      donor: jQuery.extend(true, {}, this.state.initDonor),
      orgDonor: jQuery.extend(true, {}, this.state.initDonor),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (controls[name]) 
          controls[name].clearError();
        }
      );
  }

  jqDatePickerUpdate(donor) {
    setTimeout(function () {
      $('#profile .date')
        .each(function () {
          $(this).data('date', donor[this.id]);
          $(this).datetimepicker('update');
        })
    }, 20);
  }

  onCancel() {
    this.setState({
      donor: jQuery.extend(true, {}, this.state.orgDonor),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].clearError();
      });

    this.jqDatePickerUpdate(this.state.orgDonor);
  }

  render() {
    if (this.props.didOnly) {
      return null;
    }

    var cssHideSaveButton = this.props.canSaveDonor === false
      ? " hide"
      : "";
    return (

      <div id="profile" className="tab-pane fade in active">
        <ErrorArea errorList={this.state.errors}/>
        <div className="donor-form">
          <div className="tab-container">
            <div className="col-sm-24">
              <h4>General Information</h4>
            </div>
            <div className="col-sm-5 form-group">
              <label htmlFor="id" className="ucase">ID</label>
              <input
                type="text"
                className="form-control"
                placeholder=""
                id="id"
                value={this.state.donor.id
                ? this.state.donor.id
                : ''}
                disabled/>
            </div>

            <InputControl
              containerClass="col-sm-5 form-group"
              ref
              ={(input) => {
              this.inputControls["firstName"] = input
            }}
              name="firstName"
              label="First Name"
              required={true}
              validate={validations.aplha}
              errorMessage="The &#39;First name&#39; does not seem to be valid. Please check and enter again."
              emptyMessage="The &#39;First name&#39; field cannot be left empty. Please enter a value to continue."
              labelClass="ucase"
              inputClass="form-control"
              maxLength="35"
              value={this.state.donor && this.state.donor.firstName
              ? this.state.donor.firstName
              : ''}
              onChange={this.onChangeProfile}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-5 form-group"
              ref
              ={(input) => {
              this.inputControls["lastName"] = input
            }}
              name="lastName"
              label="Last Name"
              required={true}
              validate={validations.aplha}
              errorMessage="The &#39;Last name&#39; does not seem to be valid. Please check and enter again."
              emptyMessage="The &#39;Last name&#39; field cannot be left empty. Please enter a value to continue."
              labelClass="ucase"
              inputClass="form-control"
              maxLength="35"
              value={this.state.donor.lastName}
              onChange={this.onChangeProfile}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-5 form-group"
              ref
              ={(input) => {
              this.inputControls["dateOfBirth"] = input
            }}
              type="date"
              name="dateOfBirth"
              label="DOB"
              required={true}
              validate={validations.date}
              errorMessage="The &#39;DOB&#39; does not seem to be valid. Please check and enter again."
              emptyMessage="The &#39;DOB&#39; field cannot be left empty. Please enter a date to continue."
              labelClass="ucase"
              inputClass="form-control"
              maxLength="35"
              value={this.state.donor.dateOfBirth}
              onChange={this.onChangeProfile}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <div className="col-sm-24">
              <h4>Contact Information</h4>
            </div>

            <InputControl
              containerClass="col-sm-5 form-group"
              ref
              ={(input) => {
              this.inputControls["email"] = input
            }}
              name="email"
              label="Email"
              required={true}
              validate={validations.email}
              errorMessage="The &#39;Email&#39; does not seem to be valid. Please check and enter again."
              emptyMessage="The &#39;Email&#39; field cannot be left empty. Please enter an email to continue."
              labelClass="ucase"
              inputClass="form-control"
              maxLength="32"
              value={this.state.donor.email}
              onChange={this.onChangeProfile}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-5 form-group"
              ref
              ={(input) => {
              this.inputControls["primaryPhone"] = input
            }}
              name="primaryPhone"
              label="Primary phone"
              required={true}
              validate={validations.phone}
              errorMessage="Primary phone does not seem to be valid. Please check and enter again."
              emptyMessage="The &#39;Primary phone&#39; field cannot be left empty. Please enter a value to continue."
              labelClass="ucase"
              inputClass="form-control"
              maxLength="32"
              value={this.state.donor.primaryPhone}
              onChange={this.onChangeProfile}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-5 form-group"
              type="text"
              name="secondaryPhone"
              ref
              ={(input) => {
              this.inputControls["secondaryPhone"] = input
            }}
              label="Secondary phone"
              validate={validations.phone}
              errorMessage="The &#39;Secondary phone&#39; does not seem to be valid. Please check and enter again."
              labelClass="ucase"
              inputClass="form-control"
              maxLength="32"
              value={this.state.donor.secondaryPhone}
              onChange={this.onChangeProfile}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-10 form-group"
              type="text"
              name="addressLine1"
              ref
              ={(input) => {
              this.inputControls["addressLine1"] = input
            }}
              label="Address Line 1"
              required={true}
              emptyMessage="The &#39;Address&#39; 1 field cannot be left empty. Please enter a value to continue."
              labelClass="ucase"
              inputClass="form-control"
              maxLength="255"
              value={this.state.donor.address
              ? this.state.donor.address.addressLine1
              : ''}
              onChange={this.onChangeAddress}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-10 form-group"
              type="text"
              name="addressLine2"
              ref
              ={(input) => {
              this.inputControls["addressLine2"] = input
            }}
              label="Address Line 2"
              labelClass="ucase"
              inputClass="form-control"
              maxLength="255"
              value={this.state.donor.address
              ? this.state.donor.address.addressLine2
              : ''}
              onChange={this.onChangeAddress}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <div className="col-sm-24 collapse-sm">
              <InputControl
                containerClass="col-sm-5 form-group"
                name="city"
                ref
                ={(input) => {
                this.inputControls["city"] = input
              }}
                label="City"
                required={true}
                validate={validations.aplha}
                errorMessage="The &#39;City&#39; does not seem to be valid. Please check and enter again."
                emptyMessage="The &#39;City&#39; field cannot be left empty. Please enter a value to continue."
                labelClass="ucase"
                inputClass="form-control"
                maxLength="32"
                value={this.state.donor.address
                ? this.state.donor.address.city
                : ''}
                onChange={this.onChangeAddress}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-5 form-group"
                type="select"
                name="state"
                ref
                ={(input) => {
                this.inputControls["state"] = input
              }}
                label="State"
                required={true}
                emptyMessage="The &#39;State&#39; field cannot be left blank. Please select a State from the dropdown to continue."
                labelClass="ucase"
                inputClass="form-control cityselect"
                optionValues={[
                [
                  "California", "CA"
                ],
                [
                  "Massachusetts", "MA"
                ],
                [
                  "Rhode Island", "RI"
                ],
                ["New Hampshire", "NH"]
              ]}
                value={this.state.donor.address
                ? this.state.donor.address.state
                : ''}
                onChange={this.onChangeAddress}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-5 form-group"
                type="text"
                name="zip"
                ref
                ={(input) => {
                this.inputControls["zip"] = input
              }}
                label="Zip"
                required={true}
                validate={validations.zip}
                errorMessage="The &#39;Zip&#39; code should be 5 digit long. Please check the code and enter again."
                emptyMessage="The &#39;Zip&#39; code field cannot be left empty. Please enter a value to continue."
                labelClass="ucase"
                inputClass="form-control"
                maxLength="5"
                value={this.state.donor.address
                ? this.state.donor.address.zip
                : ''}
                onChange={this.onChangeAddress}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

            </div>
            <div className="col-sm-24">
              <button
                className={"btn btn-default" + cssHideSaveButton}
                disabled={this.state.processing}
                onClick={this.onSave}>Save</button>
              <button
                onClick={this.onCancel}
                disabled={this.state.processing}
                className="btn btn-default btn-hollow">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default DonorProfile