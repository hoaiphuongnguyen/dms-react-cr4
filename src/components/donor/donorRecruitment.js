import React, {Component} from 'react';
import DonorPersonInfo from './donorPersonInfo';
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import * as validations from '../../validations/common'
import {API_STATUS} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import {hashHistory} from 'react-router';
import isEqual from 'lodash';

class DonorRecruitment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      donor: jQuery.extend(true, {}, this.props.donor.data),
      orgDonor: jQuery.extend(true, {}, this.props.donor.data),
      errors: {},
      processing: false,
      autoSave: false
    }

    this.onInvalid = this
      .onInvalid
      .bind(this);
    this.onValid = this
      .onValid
      .bind(this);
    this.onChange = this
      .onChange
      .bind(this);

    this.autoSave = this
      .autoSave
      .bind(this);
    this.onSave = this
      .onSave
      .bind(this);
    this.onCancel = this
      .onCancel
      .bind(this);

    this.inputControls = [];
  }

  hasUnsavedChanges() {
    var changed = !_.isEqual(this.state.donor.recruitment, this.state.orgDonor.recruitment);
    return changed;
  }

  componentWillReceiveProps(nextProps) {
    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (controls[name]) 
          controls[name].clearError();
        }
      );

    if ((nextProps.donor.status == API_STATUS.DONE && nextProps.donor.actionType == actionTypes.GET_DONOR) || (nextProps.donor.status == API_STATUS.DONE && nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.tabName == 'Profile')) {
      this.setState({
        donor: jQuery.extend(true, {}, nextProps.donor.data),
        orgDonor: jQuery.extend(true, {}, nextProps.donor.data)
      });
    } else if (nextProps.donor.status == API_STATUS.DONE && nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.tabName == 'Recruitment' && this.state.processing) {
      this.setState({
        processing: false,
        orgDonor: jQuery.extend(true, {}, this.state.donor),
        errors: {},
        autoSave: false
      });
      if (this.state.autoSave) {
        if (this.state.nextTarget) {
          if (typeof(this.state.nextTarget) == "object") {
            $(".tab-pil").removeClass("active");
            $(this.state.nextTarget).addClass("active");
            $(this.state.nextTarget).tab("show");
          } else {
            const donorId = parseInt(this.state.nextTarget);
            if (donorId) {
              this
                .props
                .getDonorAction(donorId);
            } else {
              hashHistory.push(this.state.nextTarget);
            }
          }
          this.setState({nextTarget: null});
        }
      } else {
        $("#successMessage").html("Save recruitment profile successfully.");
        this
          .props
          .layoutActions
          .openSuccessDialog();
      }
    } else if (nextProps.donor.actionType == actionTypes.SAVE_DONOR && nextProps.donor.status == API_STATUS.ERROR && nextProps.donor.tabName == 'Recruitment' && this.state.processing) {
      var message = "";
      if (nextProps.donor.data.cause) {
        message = nextProps.donor.data.cause;
        if (message.message) 
          message = message.message;
        message = message
          .replace("AlreadyExistsException: ", "")
          .replace("IllegalArgumentException: ", "");
      } else if (nextProps.donor.data.message) {
        message = nextProps.donor.data.message;
      }
      $("#errorDil").modal('show');
      this.setState({errorMessage: message, processing: false});
    }
  }

  onInvalid(name, message) {
    const errors = this.state.errors;
    errors[name] = message;
    this.setState({errors: errors})
  }

  onValid(name) {
    const errors = this.state.errors;
    errors[name] = "";
    this.setState({errors: errors})
  }

  onChange(name, value) {
    const donorData = this.state.donor;
    if (!donorData.recruitment) 
      donorData.recruitment = new Object();
    
    donorData.recruitment[name] = value;
    this.setState({data: donorData});
  }

  saveData(autoSave) {
    this.setState({autoSave: autoSave});

    const donorData = this.state.donor;

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].validateAllRules(name, donorData.recruitment
          ? donorData.recruitment[name]
          : '');
      });

    const errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })

    if (numError == 0) {
      this.setState({processing: true, errorMessage: "", successMessage: ""});

      this
        .props
        .onSave("Recruitment", this.state.donor);
    } else {
      this.setState({errorMessage: "", successMessage: ""});
      $(".error-list").show();
      //scroll top
      $('html, body').animate({
        scrollTop: 200
      }, 700);
    }
  }

  autoSave(nextTarget) {
    this.setState({nextTarget: nextTarget});
    this.saveData(true);
  }

  onSave() {
    this.saveData(false);
  }

  jqDatePickerUpdate(donor) {
    setTimeout(function () {
      $('#recruitment .date')
        .each(function () {
          if (donor.recruitment) {
            $(this).data('date', donor.recruitment[this.id]);
            $(this).datetimepicker('update');
          }
        });
    }, 20);
  }

  onCancel() {
    this.setState({
      donor: jQuery.extend(true, {}, this.state.orgDonor),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (controls[name]) 
          controls[name].clearError();
        }
      );

    this.jqDatePickerUpdate(this.state.orgDonor);
  }

  render() {
    var cssHideSaveButton = this.props.canSaveDonor === false
      ? " hide"
      : "";
    return (

      <div id="recruitment" className="tab-pane fade">
        <div className="donor-form">

          {!this.props.didOnly
            ? <DonorPersonInfo donor={this.state.donor}/>
            : null}
          <div className="col-sm-24 no-padding">
            <ErrorArea errorList={this.state.errors}/>
          </div>

          <div className="col-sm-24 tab-container">

            <InputControl
              containerClass="col-sm-7 form-group"
              type="select"
              name="whereHear"
              readOnly
              ={!this.props.canSaveDonor}
              ref
              ={(input) => {
              this.inputControls["whereHear"] = input
            }}
              value={this.state.donor.recruitment
              ? this.state.donor.recruitment.whereHear
              : ''}
              label="Where did you hear about us"
              labelClass="ucase"
              inputClass="form-control cityselect"
              showSelectOption={true}
              optionValues={[
              [
                "Radio", "radio"
              ],
              [
                "Word Of Mouth", "word_of_mouth"
              ],
              [
                "Employee", "employee"
              ],
              [
                "Flyer", "flyer"
              ],
              [
                "Facebook", "facebook"
              ],
              [
                "Yelp", "yelp"
              ],
              [
                "Google", "google"
              ],
              [
                "College", "college"
              ],
              [
                "Table Setup", "table_setup"
              ],
              ["Other", "other"]
            ]}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-7 form-group"
              type="select"
              name="preferCommunicateMethod"
              readOnly
              ={!this.props.canSaveDonor}
              ref
              ={(input) => {
              this.inputControls["preferCommunicateMethod"] = input
            }}
              value={this.state.donor.recruitment
              ? this.state.donor.recruitment.preferCommunicateMethod
              : ''}
              label="Preferred mode of communication"
              labelClass="ucase"
              inputClass="form-control cityselect"
              optionValues={[
              [
                "Email", "email"
              ],
              [
                "Text", "text"
              ],
              ["Phone", "phone"]
            ]}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <div className="col-sm-24">
              <h4>Donor Attributes</h4>
            </div>

            <InputControl
              containerClass="col-sm-7 form-group"
              type="select"
              name="category"
              readOnly
              ={!this.props.canSaveDonor}
              ref
              ={(input) => {
              this.inputControls["category"] = input
            }}
              value={this.state.donor.recruitment
              ? this.state.donor.recruitment.category
              : ''}
              label="Category"
              labelClass="ucase"
              inputClass="form-control cityselect showhidedrop"
              optionValues={[
              [
                "Prospect", "prospect"
              ],
              [
                "New", "new"
              ],
              [
                "Active", "active"
              ],
              [
                "Lapsed", "lapsed"
              ],
              [
                "Inactive", "inactive"
              ],
              ["Disqualified", "disqualified"]
            ]}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-7 form-group"
              containerId="categoryhide"
              type="select"
              name="disqualificationReason"
              readOnly
              ={!this.props.canSaveDonor}
              ref
              ={(input) => {
              this.inputControls["disqualificationReason"] = input
            }}
              value={this.state.donor.recruitment
              ? this.state.donor.recruitment.disqualificationReason
              : ''}
              label="Disqualification Reason"
              labelClass="ucase"
              inputClass="form-control cityselect"
              optionValues={[
              [
                "Suitability", "suitability"
              ],
              ["Medical", "medical"]
            ]}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-5 form-group"
              type="select"
              name="status"
              readOnly
              ={!this.props.canSaveDonor}
              ref
              ={(input) => {
              this.inputControls["status"] = input
            }}
              value={this.state.donor.recruitment
              ? this.state.donor.recruitment.status
              : ''}
              label="Status"
              labelClass="ucase"
              inputClass="form-control cityselect showhidedrop"
              optionValues={[
              [
                "Open", "open"
              ],
              [
                "Reserved", "reserved"
              ],
              ["Scheduled", "scheduled"]
            ]}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-5 form-group"
              containerId="statushide"
              type="date"
              name="reservationExpirationDate"
              readOnly
              ={!this.props.canSaveDonor}
              ref
              ={(input) => {
              this.inputControls["reservationExpirationDate"] = input
            }}
              value={this.state.donor.recruitment
              ? this.state.donor.recruitment.reservationExpirationDate
              : ''}
              label="Expiration Date"
              required={true}
              validate={validations.date}
              errorMessage="The &#39;Reservation Expiration Date&#39; does not seem to be valid. Please check and enter again."
              emptyMessage="The &#39;Reservation Expiration Date&#39; field cannot be left empty. Please enter a date to continue."
              labelClass="ucase"
              inputClass="form-control"
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <div className="col-sm-24 collapse-sm mar-t-15">
              <InputControl
                containerClass="col-sm-7"
                readOnly
                ={!this.props.canSaveDonor}
                type="switch"
                name="isOnCall"
                ref
                ={(input) => {
                this.inputControls["isOnCall"] = input
              }}
                checked={this.state.donor.recruitment
                ? this.state.donor.recruitment.isOnCall
                : false}
                label="On Call Donor?"
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-7"
                type="switch"
                name="isClinicalGrade"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["isClinicalGrade"] = input
              }}
                checked={this.state.donor.recruitment
                ? this.state.donor.recruitment.isClinicalGrade
                : false}
                label="Clinical grade donor?"
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-10"
                type="switch"
                name="isClinicalAssessment1Completed"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["isClinicalAssessment1Completed"] = input
              }}
                checked={this.state.donor.recruitment
                ? this.state.donor.recruitment.isClinicalAssessment1Completed
                : false}
                label="Clinical Grade Assessment 1 Completed?"
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>
            </div>

            <div className="col-sm-24 collapse-sm mar-t-15">
              <InputControl
                containerClass="col-sm-7"
                type="switch"
                name="isFlexible"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["isFlexible"] = input
              }}
                checked={this.state.donor.recruitment
                ? this.state.donor.recruitment.isFlexible
                : false}
                label="Flexible Donor?"
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-7"
                type="switch"
                name="isInflexible"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["isInflexible"] = input
              }}
                checked={this.state.donor.recruitment
                ? this.state.donor.recruitment.isInflexible
                : false}
                label="Inflexible Donor?"
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-10"
                type="switch"
                ref
                ={(input) => {
                this.inputControls["isClinicalAssessment2Completed"] = input
              }}
                name="isClinicalAssessment2Completed"
                readOnly
                ={!this.props.canSaveDonor}
                checked={this.state.donor.recruitment
                ? this.state.donor.recruitment.isClinicalAssessment2Completed
                : false}
                label="Clinical Grade Assessment 2 Completed?"
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

            </div>

            <div className="col-sm-24 collapse-sm">
              <InputControl
                containerClass="col-sm-7"
                type="switch"
                name="isLowDemographic"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["isLowDemographic"] = input
              }}
                checked={this.state.donor.recruitment
                ? this.state.donor.recruitment.isLowDemographic
                : false}
                label="Low Demographic Donor?"
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>

              <InputControl
                containerClass="col-sm-7"
                type="switch"
                name="isDonorAssessmentNeeded"
                readOnly
                ={!this.props.canSaveDonor}
                ref
                ={(input) => {
                this.inputControls["isDonorAssessmentNeeded"] = input
              }}
                checked={this.state.donor.recruitment
                ? this.state.donor.recruitment.isDonorAssessmentNeeded
                : false}
                label="Donor Assessment Needed?"
                labelClass="ucase"
                inputClass="form-control"
                onChange={this.onChange}
                onInvalid={this.onInvalid}
                onValid={this.onValid}/>
            </div>

            <InputControl
              containerClass="col-sm-24 form-group"
              type="textarea"
              maxLength="200"
              name="comment"
              readOnly
              ={!this.props.canSaveDonor}
              max
              ref
              ={(input) => {
              this.inputControls["comment"] = input
            }}
              value={this.state.donor && this.state.donor.recruitment && this.state.donor.recruitment.comment
              ? this.state.donor.recruitment.comment
              : ''}
              label="Comments"
              labelClass="ucase"
              inputClass="form-control"
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <div className="col-sm-24">
              <button
                className={"btn btn-default" + cssHideSaveButton}
                disabled={this.state.processing}
                onClick={this.onSave}>Save</button>
              <button
                className={"btn btn-default btn-hollow" + cssHideSaveButton}
                onClick={this.onCancel}
                disabled={this.state.processing}>Cancel</button>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default DonorRecruitment;