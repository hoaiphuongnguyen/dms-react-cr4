import React, {Component} from 'react'
import DonorPersonInfo from './donorPersonInfo'
import {API_STATUS, PATH_PREFIX} from '../../common/constants'
import * as actionTypes from '../../constants/actionTypes'
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import * as validations from '../../validations/common'
import {hashHistory} from 'react-router';
import isEqual from 'lodash';
import {
  displayDate,
  displayDate2,
  displayDate3,
  displayTime,
  displayTime2,
  localDateToUTCString,
  utcStringToLocalDate
} from '../../utils/date.js';

class DonorSchedule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      donor: jQuery.extend(true, {}, this.props.donor.data),
      scheduleRequest: jQuery.extend(true, {}, this.props.scheduleRequest),
      procedureInfo: {},
      appointment: {
        appointmentDate: '',
        appointmentTime: ''
      },
      orgAppointment: {
        appointmentDate: '',
        appointmentTime: ''
      },
      errors: {},
      processing: false
    }

    this.onShowMore = this
      .onShowMore
      .bind(this);

    this.onInvalid = this
      .onInvalid
      .bind(this);
    this.onValid = this
      .onValid
      .bind(this);
    this.onChange = this
      .onChange
      .bind(this);

    this.onConfirmAppointment = this
      .onConfirmAppointment
      .bind(this);

    this.onBackScheduleRequest = this
      .onBackScheduleRequest
      .bind(this);

    this.onCancel = this
      .onCancel
      .bind(this);

    this.inputControls = [];

    var isViewToday = true;
    var canAddAppt;
    var hasRenderCalendar = false;

  }

  componentDidMount() {
    this.canAddAppt = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 2;

    this.jQInit(); //no need setTimeout when call jQInit
  }

  componentWillUnmount() {
    if ($(".popup-tooltip").hasClass('active')) {
      $(".popup-tooltip").removeClass('active');
      if (this.props.layoutActions) {
        this
          .props
          .layoutActions
          .closeAppointmentPopup(false);
      }
    }
  }

  onInvalid(name, message) {
    const errors = this.state.errors;
    errors[name] = message;
    this.setState({errors: errors});
  }

  onValid(name) {
    const errors = this.state.errors;
    errors[name] = "";
    this.setState({errors: errors});
  }

  onChange(name, value) {
    const appointmentData = this.state.appointment;
    appointmentData[name] = value;
    this.setState({appointment: appointmentData});
  }

  saveAppointment(autoSave)
  {
    this.setState({autoSave: autoSave});

    const data = this.state.appointment;
    var controls = this.inputControls;
    controls["appointmentDate"].validateAllRules("appointmentDate", data["appointmentDate"]);
    controls["appointmentTime"].validateAllRules("appointmentTime", data["appointmentTime"]);

    var errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })

    if (numError == 0) {
      var startTime = moment(this.state.appointment.appointmentDate + ' ' + this.state.appointment.appointmentTime);
      var now = moment();
      if (startTime < now) {
        if (startTime.format("YYYY/MM/DD") < now.format("YYYY/MM/DD")) {
          controls["appointmentDate"].setError("Please input date in future.");
        } else {
          controls["appointmentTime"].setError("Please input time in future.");
        }
        this.setState({processing: false, errorMessage: "", successMessage: ""});
        $(".error-list").show();
      } else {
        this.setState({processing: true, errorMessage: "", successMessage: ""});
        var appointment = {
          startTime: localDateToUTCString(startTime),
          endTime: localDateToUTCString(startTime.add(30, 'minutes')),
          scheduleRequest: {
            id: this.state.scheduleRequest.id
          },
          procedureInfo: {
            id: this.state.procedureInfo.id
          },
          donor: {
            id: this.state.donor.id
          }
        }

        this
          .props
          .appointmentActions
          .saveAppointment(appointment, null, true);
      }
    } else {
      this.setState({errorMessage: "", successMessage: ""});
      $(".error-list").show();
    }
  }

  onCancel() {
    this.setState({errors: {}, appointment: this.state.orgAppointment});
    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (controls[name]) 
          controls[name].clearError();
        }
      );
  }

  autoSave(nextTarget) {
    this.setState({nextTarget: nextTarget});
    this.saveAppointment(true);
  }

  onConfirmAppointment() {
    this.saveAppointment(false);
  }

  onBackScheduleRequest() {
    hashHistory.push(PATH_PREFIX + '/schedule-request/process-schedule-request/' + this.state.scheduleRequest.id);
  }

  hasUnsavedChanges() {
    var changed = !_.isEqual(this.state.appointment, this.state.orgAppointment);
    return changed;
  }

  clearData() {
    $("#create_donor_schedule").fullCalendar('removeEvents');
    $("#create_donor_schedule").hide();
    $(".fc-EventCount-button").text("0");
    if (this.props.donor.loadMatchRequests && this.props.donor.loadMatchRequests.items) {
      this.props.donor.loadMatchRequests.items = [];
    }
  }

  reloadCalendarAfterConfirmAppointment() {
    if (this.hasRenderCalendar) {
      $("#create_donor_schedule").show();
      //calendar has render, call api getAppointments
      this.getAppointmentsComplete = false;
      this
        .props
        .appointmentActions
        .getAppointments(null, this.state.donor.id, this.startTime, this.endTime);
    } else {
      // show and render calendar, getAppointments will auto call on viewRender event
      $("#create_donor_schedule").show();
      $('#create_donor_schedule').fullCalendar('render');
      $("#create_donor_schedule").fullCalendar('rerenderEvents');
    }
  }

  componentWillReceiveProps(nextProps) {

    var controls = this.inputControls;
    if (controls && controls.length > 0) {
      Object
        .keys(controls)
        .forEach(function (name) {
          if (controls[name]) 
            controls[name].clearError();
          }
        );
    }

    if (nextProps.donor.actionType == actionTypes.SAVE_APPOINTMENT && nextProps.donor.status == API_STATUS.DONE && nextProps.donor.confirmSchedule && this.state.processing) {
      this.setState({processing: false, appointment: {}, orgAppointment: {}});

      $("#successMessage").html("Confirm appointment successfully.");
      this
        .props
        .layoutActions
        .openSuccessDialog();

      let self = this;
      setTimeout(function () {
        self.reloadCalendarAfterConfirmAppointment();
      }, 3000);

    } else if ((nextProps.donor.actionType == actionTypes.GET_DONOR || nextProps.donor.actionType == actionTypes.SAVE_DONOR) && nextProps.donor.status == API_STATUS.DONE) {
      this.setState({
        donor: jQuery.extend(true, {}, nextProps.donor.data)
      });

      if (this.canAddAppt) {
        this
          .props
          .getSchedulingRequestDonorMatchAction(nextProps.donor.data.id, 1, this.props.donor.loadMatchRequests.pageSize, this.props.donor.loadMatchRequests.sortBy, this.props.donor.loadMatchRequests.sortOrder, true);
      }

    } else if (nextProps.donor.actionType == actionTypes.GET_SCHEDULING_REQUEST && nextProps.donor.status == API_STATUS.DONE) {
      this.setState({
        scheduleRequest: jQuery.extend(true, {}, nextProps.donor.scheduleRequest),
        procedureInfo: jQuery.extend(true, {}, nextProps.donor.scheduleRequest.procedureInfo)
      });

      let self = this;
      setTimeout(function () {
        $("#appointmentTime")
          .val("")
          .timepicker("update");
        self.onChange("appointmentTime", '');
      }, 20);
    } else if (nextProps.donor.actionType == actionTypes.SAVE_APPOINTMENT && nextProps.donor.status == API_STATUS.ERROR && nextProps.donor.confirmSchedule && this.state.processing) {
      var message = "";
      if (typeof(nextProps.donor.message) == "string") {
        message = nextProps
          .donor
          .message
          .replace("AlreadyExistsException: ", "")
          .replace("IllegalArgumentException: ", "");
        if (message) {
          $("#errorDil #errorMessage").html(message);
        }
        $("#errorDil").modal('show');
        this.setState({errorMessage: message, processing: false});
      }
    } else if (nextProps.donor.actionType == actionTypes.GET_APPOINTMENTS && nextProps.donor.status == API_STATUS.DONE) {
      this.getAppointmentsComplete = true;

      $("#create_donor_schedule").fullCalendar('removeEvents');

      var items = nextProps.donor.appointments;
      if (this.isViewToday) {
        $(".fc-EventCount-button").text(items.length);
      }

      var events = [];
      for (var i = 0; i < items.length; i++) {
        var item = items[i];
        var category = item.procedureInfo.category;
        var status = item.status;
        var resourceId;
        var color;
        var className;
        if (status == "cancelled" || status == "CANCELLED") {
          color = "#CCCCCC";
          className = "fc-event-cancelled";
          if (category == "Whole Blood") {
            resourceId = "p2";
          } else if (category == "Bone Marrow") {
            resourceId = "p1";
          } else if (category == "LeukoPak") {
            resourceId = "p3";
          } else if (category == "LeukoPak Mobilized") {
            resourceId = "p4";

          } else if (category == "Screening & Backup") {
            resourceId = "p5";
          } else {
            resourceId = "p6";
          }
        } else {
          className = "";
          if (category == "Whole Blood") {
            resourceId = "p2";
            color = "#a564d3";
          } else if (category == "Bone Marrow") {
            resourceId = "p1";
            color = "#9dc15c";
          } else if (category == "LeukoPak") {
            resourceId = "p3";
            color = "#ee5b5b";
          } else if (category == "LeukoPak Mobilized") {
            resourceId = "p4";
            color = "#f9a231";

          } else if (category == "Screening & Backup") {
            resourceId = "p5";
            color = "#289dea";
          } else {
            resourceId = "p6";
            color = "#d3ca41";
          }
        }

        var event = {
          id: item.id,
          resourceId: resourceId,
          eventResourceEditable: false,
          title: item.donor.firstName + ' ' + item.donor.lastName,
          start: utcStringToLocalDate(item.startTime),
          end: utcStringToLocalDate(item.endTime),
          color: color,
          borderColor: "#eaebf5",
          className: className
        }

        events.push(event);
      }

      $("#create_donor_schedule").fullCalendar('addEventSource', events);

      $("#create_donor_schedule .fc-event-container").each(function () {
        const eventElements = ($(this).find(".fc-event"));
        const numElement = eventElements.length;
        if (numElement > 0) {
          for (i = 0; i < numElement; i++) {
            $(eventElements[i]).css("color", ((i % 2) == 0)
              ? "white"
              : "black");
          }
        }
      });

    }

  }

  updateCalVariable(view) {
    let startTimeLocal = view
      .intervalStart
      .clone()
      .local();

    this.startTime = localDateToUTCString(startTimeLocal);
    this.endTime = localDateToUTCString(view.intervalEnd.clone().local());

    this.isViewToday = false;
    if (view.name == "agendaDay") {
      var now = moment()
        .hour(0)
        .minute(0)
        .second(0)
        .millisecond(0);
      var duration = moment.duration(startTimeLocal.diff(now));
      if (Math.round(duration.asDays()) == 0) {
        this.isViewToday = true;
      }
    }
  }

  jQInit() {

    var openAddAppointmentDialog = this.props.layoutActions.openAddAppointmentDialog;
    var openAppointmentPopup = this.props.layoutActions.openAppointmentPopup;
    var self = this;
    var updateAppointmentTime = function (apointmentId, startTime, endTime) {
      var appointment = {
        id: apointmentId,
        startTime: startTime,
        endTime: endTime
      }
      self
        .props
        .appointmentActions
        .updateAppointmentTime(appointment);
    }

    // FullCalendar jQuery in no conflict mode
    $('#create_donor_schedule').fullCalendar({
      slotDuration: '00:15:00',
      timezone: 'local',
      dayNamesShort: [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ],
      defaultView: 'agendaDay',
      editable: true,
      selectable: true,
      customButtons: {
        EventCount: {
          text: '0'
        }
      },

      header: {
        left: 'prev  title next today EventCount',
        right: 'agendaDay,agendaWeek,month'
      },

      schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
      eventLimit: true, // allow "more" link when too many events
      minTime: "07:00:00",
      maxTime: "17:00:00",
      height: "auto",
      allDaySlot: false,
      resources: [
        {
          id: 'p1',
          title: 'Bone Marrow'
        }, {
          id: 'p2',
          title: 'Whole Blood'
        }, {
          id: 'p3',
          title: 'LeukoPak'
        }, {
          id: 'p4',
          title: 'LeukoPak Mobilized'
        }, {
          id: 'p5',
          title: 'Screening & Backup'
        }, {
          id: 'p6',
          title: 'Injection & Others'
        }
      ],

      /**
         * OnEventClick call the PopupTooltip
         * @see https://fullcalendar.io/docs/mouse/eventClick/
         */
      eventClick: function (calEvent, jsEvent, view) {
        openAppointmentPopup(calEvent.id);
        var toolTip = $(".popup-tooltip");
        toolTip.position({
          of: $(this),
          my: 'left+20 center',
          at: 'right center',
          collision: 'flipfit flipfit',
          using: function (obj, info) {
            if (info.vertical != "top") {
              $(this).addClass("flipped top");
            } else {
              $(this).removeClass("flipped top");
            }
            if (info.horizontal != "left") {
              $(this).addClass("flipped left");
            } else {
              $(this).removeClass("flipped left");
            }
            $(this).css({
              left: obj.left + 'px',
              top: obj.top + 'px'
            })
          }
        })
      },

      select: function (start, end, evet, view, resource) {
        if (!self.canAddAppt) {
          return;
        }

        if (resource) {
          var now = moment();
          if (start > now) {
            openAddAppointmentDialog(null, self.state.donor.id, resource.title, localDateToUTCString(start), localDateToUTCString(end));
          }
        }
      },

      viewRender: function (view, element) {
        self.hasRenderCalendar = true;
        if ($(".popup-tooltip").hasClass('active')) {
          $(".popup-tooltip").removeClass('active');
          if (self.props.layoutActions) {
            self
              .props
              .layoutActions
              .closeAppointmentPopup(false);
          }
        }

        self.updateCalVariable(view);

        self.getAppointmentsComplete = false;
        self
          .props
          .appointmentActions
          .getAppointments(null, self.state.donor.id, self.startTime, self.endTime);
      },

      eventResize: function (event, delta, revertFunc) {
        if (!self.canAddAppt) {
          revertFunc();
          return;
        }
        updateAppointmentTime(event.id, localDateToUTCString(event.start), localDateToUTCString(event.end));
      },

      eventDrop: function (event, delta, revertFunc) {
        if (!self.canAddAppt) {
          revertFunc();
          return;
        }
        updateAppointmentTime(event.id, localDateToUTCString(event.start), localDateToUTCString(event.end));
      }

    });

    $("#create_donor_schedule").hide();
    $("#add_sched").click(function () {
      $("#create_donor_schedule").toggle();
      $("#create_donor_schedule").fullCalendar('changeView', 'agendaDay');
      $(".fc-today-button").click(); //set default view is today
      $('#create_donor_schedule').fullCalendar('render');
      $("#create_donor_schedule").fullCalendar('rerenderEvents');
    });
  }

  renderCancelAppointmentButton(item) {
    if (this.canAddAppt && item.status && item.status.toLowerCase() != 'checked_out' && item.status.toLowerCase() != "cancelled" && item.status.toLowerCase() != "completed") {
      var openCancelAppointmentDialog = this.props.layoutActions.openCancelAppointmentDialog;
      return (
        <td className="actions">
          <a
            href="javascript:void(0)"
            data-toggle="modal"
            title="Cancel Appointment"
            onClick={function () {
            openCancelAppointmentDialog(item);
          }}>
            <img
              className="icon-svg"
              src="/img/icons/editdonor/icon-delete.svg"
              alt="delete"
              width="18"
              data-pin-nopin="true"/>
          </a>
        </td>
      );
    } else {
      return (
        <td className="actions"></td>
      );
    }
  }

  renderEditAppointmentButton(item)
  {
    var openEditAppointmentDialog = this.props.layoutActions.openEditAppointmentDialog;
    if (this.canAddAppt && item.status && item.status.toLowerCase() != 'checked_out' && item.status.toLowerCase() != "cancelled" && item.status.toLowerCase() != "completed") {
      return (
        <td className="actions">
          <a
            href="javascript:void(0)"
            data-toggle="modal"
            title="Edit Appointment"
            onClick={function () {
            openEditAppointmentDialog(item);
          }}>
            <img
              className="icon-svg"
              src="/img/icons/editdonor/icon-edit.svg"
              alt="edit"
              width="18"
              data-pin-nopin="true"/>
          </a>
        </td>
      );
    } else {
      return (
        <td className="actions"></td>
      );
    }
  }

  renderUpcommingAppointments() {
    const items = this.state.donor.upcommingAppointments;
    let dataRows = null;
    if (items && items.length) {
      dataRows = (items.map((item, i) => (
        <tr key={item.id}>
          <td>{displayDate(item.startTime)}</td>
          <td>{displayTime(item.startTime)}</td>
          <td>{item.procedureInfo.name}</td>
          <td>{item.status}</td>
          {this.renderEditAppointmentButton(item)}

          {this.renderCancelAppointmentButton(item)}
        </tr>
      )))
    }

    return (
      <div className="table-responsive allcells-responsive-tables">
        <table
          id="search_donor_dtab"
          className="table allcells-tables"
          cellSpacing="0"
          width="100%">
          <thead>
            <tr>
              <th>Date</th>
              <th>Time</th>
              <th>Procedure</th>
              <th>Status</th>
              <th className="no_sort actions"></th>
              <th className="no_sort actions"></th>
            </tr>
          </thead>
          <tbody>
            {dataRows}
          </tbody>
        </table>
      </div>
    );
  }

  renderStatusColumn(status)
  {
    if (status.toLowerCase() == 'completed') {
      return (
        <td className="success" style={{
          whiteSpace: "nowrap"
        }}>
          <img src="/img/icons/editdonor/icon-greentick.svg" alt="Check"/>&nbsp;&nbsp;Completed</td>
      );
    }

    return (
      <td>{status}</td>
    )

  }

  renderPastAppointments() {
    const items = this.state.donor.pastAppointments;
    let dataRows = null;

    if (items && items.length) {
      dataRows = (items.map((item, i) => (
        <tr key={item.id}>
          <td>{displayDate(item.startTime)}</td>
          <td>{displayTime(item.startTime)}</td>
          <td>{item.checkOutTime
              ? displayTime(item.checkOutTime)
              : ''}</td>
          <td>{item.cancelAt
              ? displayTime(item.cancelAt)
              : ''}</td>
          <td>{item.procedureInfo.name}</td>
          {this.renderStatusColumn(item.status)}
        </tr>

      )))
    }

    return (
      <div className="table-responsive allcells-responsive-tables">
        <table
          id="search_donor_dtab2"
          className="table allcells-tables"
          cellSpacing="0"
          width="100%">
          <thead>
            <tr>
              <th>Date</th>
              <th>Time</th>
              <th>Check out Time</th>
              <th>Cancellation Time</th>
              <th>Procedure</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {dataRows}
          </tbody>
        </table>

      </div>
    );
  }

  clearMatchedSchedule() {
    this
      .props
      .processScheduleActions
      .matchDonorWithRequest(this.state.donor.id, null, true);

    this.setState({scheduleRequest: null});
  }

  renderConfirmAppointment() {
    if (this.state.scheduleRequest && this.state.scheduleRequest.id && this.props.donor.matchedScheduleId) {
      let backToScheduleRequest = null;
      if (!this.props.donor.hiddenBackScheduleRequest) {
        backToScheduleRequest = <div className="col-sm-6 showmore">
          <a href="javascript:void(0)" onClick={this.onBackScheduleRequest}>Back To Schedule Request</a>
        </div>
      }

      return (
        <div className="col-sm-24 collapse-sm info confirm-div">
          <ErrorArea errorList={this.state.errors}/>
          <h4 className="col-sm-24">Select &amp; Confirm</h4>
          <div className="col-sm-4 content-div">
            <label className="ucase">Collection Date:</label>
            <label>{this.state.scheduleRequest
                ? displayDate2(this.state.scheduleRequest.internalCollectionTime, this.state.scheduleRequest.collectionDate, this.state.scheduleRequest.collectionByTime)
                : ''}</label>
          </div>
          <div className="col-sm-4 content-div">
            <label className="ucase">Collection Site:</label>
            <label>{this.state.scheduleRequest
                ? this.state.scheduleRequest.collectionSite
                : ''}</label>
          </div>
          <div className="col-sm-4 content-div">
            <label className="ucase">Procedure:</label>
            <label>{this.state.scheduleRequest && this.state.scheduleRequest.procedureInfo
                ? this.state.scheduleRequest.procedureInfo.name
                : ''}</label>
          </div>
          <div className="col-sm-2 content-div">
            <label className="ucase">Volume:</label>
            <label>{this.state.scheduleRequest
                ? this.state.scheduleRequest.volume
                : ''}</label>
          </div>
          <div className="col-sm-4 content-div">
            <label className="ucase">Work Order:</label>
            <label>{this.state.scheduleRequest
                ? this.state.scheduleRequest.woNumber
                : ''}</label>
          </div>
          <div className="col-sm-6 content-div">
            <label className="ucase">Collection Must Completed By:</label>
            <label>{this.state.scheduleRequest
                ? displayTime2(this.state.scheduleRequest.internalCollectionTime, this.state.scheduleRequest.collectionDate, this.state.scheduleRequest.collectionByTime)
                : ''}</label>
          </div>
          <div className="col-sm-24 collapse-sm">

            <InputControl
              containerClass="col-sm-5"
              hideLabel={true}
              type="date"
              onlyFutureDate={true}
              name="appointmentDate"
              ref
              ={(input) => {
              this.inputControls["appointmentDate"] = input
            }}
              value={this.state.appointment.appointmentDate
              ? this.state.appointment.appointmentDate
              : ''}
              required={true}
              emptyMessage="The &#39;Appointment date&#39; field cannot be left empty. Please enter a value to continue."
              validate={validations.date}
              errorMessage="The &#39;Appointment date&#39; does not seem to be valid. Please check and enter again."
              labelClass="ucase"
              inputClass="form-control"
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-5"
              type="time"
              hideLabel={true}
              name="appointmentTime"
              ref
              ={(input) => {
              this.inputControls["appointmentTime"] = input
            }}
              value={this.state.appointment.appointmentTime
              ? this.state.appointment.appointmentTime
              : ''}
              required={true}
              emptyMessage="The &#39;Appointment time&#39; field cannot be left empty. Please enter a value to continue."
              errorMessage="The &#39;Appointment time&#39; does not seem to be valid. Please check and enter again."
              labelClass="ucase"
              inputClass="form-control"
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <div className="col-sm-3">
              <button
                className="btn btn-default btn-confirm margin-btm"
                disabled={this.state.processing}
                onClick={this.onConfirmAppointment}>Confirm Appointment</button>
            </div>
          </div>

          {backToScheduleRequest}

        </div>
      );
    }
    return null;
  }

  onShowMore() {
    if (this.props.donor.loadMatchRequests.items.length < this.props.donor.loadMatchRequests.totalRecords) {
      this
        .props
        .getSchedulingRequestDonorMatchAction(this.state.donor.id, ++this.props.donor.loadMatchRequests.page, this.props.donor.loadMatchRequests.pageSize, this.props.donor.loadMatchRequests.sortBy, this.props.donor.loadMatchRequests.sortOrder, false);
    }
  }

  onSortChange(sortName, sortOrder) {
    if (sortName == "procedure") 
      sortName = "procedureInfo.Name";
    
    this
      .props
      .getSchedulingRequestDonorMatchAction(this.state.donor.id, 1, this.props.donor.loadMatchRequests.pageSize, sortName, sortOrder, true);
  }

  buttonScheduleDonor(cell, row) {

    var self = this;

    if (!self.canAddAppt) 
      return null;
    
    var f = function (row) {
      var requestId = row.id;
      self
        .props
        .processScheduleActions
        .matchDonorWithRequest(self.state.donor.id, requestId, true);

      self
        .props
        .getSchedulingRequestAction(requestId);

      //scroll top
      $('html, body').animate({
        scrollTop: 200
      }, 700);
    };
    return (
      <a
        href="javascript:void(0)"
        data-toggle="tooltip"
        title="Schedule Donor"
        onClick={function () {
        f(row);
      }}>
        <img
          className="icon-svg"
          src="/img/icons/processschedulingrequest/icon-process-request.svg"
          alt="edit"
          width="18"/></a>
    );
  }

  buttonEditRequest(cell, row) {
    var f = function (row) {
      hashHistory.push(PATH_PREFIX + '/schedule-request/edit-schedule-request/' + row.id);
    };
    return (
      <a
        href="javascript:void(0)"
        data-toggle="tooltip"
        title="Edit Request"
        onClick={function () {
        f(row);
      }}>
        <img
          className="icon-svg"
          src="/img/icons/processschedulingrequest/icon-edit-scheduling-request.svg"
          alt="edit"
          width="18"/></a>
    );
  }

  renderMatchScheduleRequest() {

    if (!this.canAddAppt) 
      return null;
    
    let options = {
      onSortChange: this
        .onSortChange
        .bind(this)
    };

    let table = null;
    let showMore = null;
    if (this.props.donor.loadMatchRequests && this.props.donor.loadMatchRequests.items) {
      table = <BootstrapTable
        ref='table'
        data={this.props.donor.loadMatchRequests.items}
        options={options}
        tableContainerClass="donortable"
        bordered={false}>
        <TableHeaderColumn dataField='id' isKey dataSort>SCHEDULE Request#</TableHeaderColumn>
        <TableHeaderColumn dataField='woNumber' dataSort>WORK ORDER</TableHeaderColumn>
        <TableHeaderColumn dataField='procedure' dataSort>Procedure</TableHeaderColumn>
        <TableHeaderColumn
          dataField='internalCollectionTime'
          dataFormat={displayDate3}
          dataSort>REQUEST COLLECTION DATE</TableHeaderColumn>
        <TableHeaderColumn dataField='createdAt' dataFormat={displayDate} dataSort>Created On</TableHeaderColumn>
        <TableHeaderColumn dataField='requestedBy' dataSort>Requested By</TableHeaderColumn>
        <TableHeaderColumn
          dataField="button"
          width='5%'
          dataFormat={this
          .buttonScheduleDonor
          .bind(this)}
          dataAlign="center"
          headerTitle={false}>&nbsp;</TableHeaderColumn>
        <TableHeaderColumn
          dataField="button"
          width='5%'
          dataFormat={this
          .buttonEditRequest
          .bind(this)}
          dataAlign="center"
          headerTitle={false}>&nbsp;</TableHeaderColumn>
      </BootstrapTable>

      if (this.props.donor.loadMatchRequests.items.length < this.props.donor.loadMatchRequests.totalRecords) {
        showMore = <div className="col-sm-24 text-right showmore hide-table">
          <a href="javascript:void(0)" onClick={this.onShowMore}>Show More</a>
        </div>;

      }

    }

    return (

      <div >
        <h4>Matched Schedule Requests</h4>

        <div
          className="col-xs-24 col-sm-24 col-md-24 col-lg-24 collapse-sm table-responsive">
          {table}
        </div>
        {showMore}

        <div className="clear"></div>

      </div>

    );
  }

  render() {

    return (
      <div id="schedule" className="tab-pane fade">
        <div className="donor-form">
          {!this.props.didOnly
            ? <DonorPersonInfo donor={this.state.donor}/>
            : null}
          {this.renderConfirmAppointment()}
          <div className="col-sm-24">
            <div id="create_donor_schedule" className="schedule-caldendar"/>
          </div>

          <div className="tab-container col-sm-24 collapse-sm">

            <div className="col-sm-24">
              <h4 className="icon-container">Upcoming Appointments
                <a
                  href="javascript:void(0)"
                  data-toggle="tooltip"
                  id="add_sched"
                  title=""
                  data-original-title="Add Schedule">
                  <img src="/img/icons/createdonoricons/icon_add.png" alt=""/>
                </a>
              </h4>

              {this.renderUpcommingAppointments()}

              <div className="clear"></div>

              <div className="spacer"></div>

              <h4>Past Appointments</h4>

              {this.renderPastAppointments()}

              <div className="clear"></div>
              <div className="spacer"></div>

              {this.renderMatchScheduleRequest()}

              <div className="clear"></div>

            </div>

          </div>

        </div>
      </div>

    )
  }
}

export default DonorSchedule