import React, { Component } from 'react'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import * as apiUtils from '../../utils/apiUtils';
import { API_STATUS } from '../../common/constants';

class ImportDonor extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
           
        };

    }

    componentDidMount() {
        document.body.classList.add('import-donor');
        document.body.classList.add('inner');
        document.title = 'Import Donor';

        var self = this;
        var raiseMessage = function(data) { 
            self.props.actions.importDonor({totalRecords:data.totalRecordsProcessed,
                                            successRecords: data.totalRecordsSucceed,
                                            failedRecords: data.totalRecordsFailed });
        }

        setTimeout(function(){

            $("#input-fcount-1").fileinput({
                uploadUrl: apiUtils.getImportDonorUrl(),
                validateInitialCount: true,
                overwriteInitial: false,
                initialPreviewAsData: true,
                allowedFileExtensions: ["csv"],
                maxFileCount: 1
            });

            $('#input-fcount-1').on('fileuploaded', function(event, data, previewId, index) {
                var form = data.form, files = data.files, extra = data.extra,
                response = data.response, reader = data.reader;
                raiseMessage(response);
            });

        },200);
    }

    componentWillUnmount() {
        document.body.classList.remove('import-donor');
        document.body.classList.remove('inner');
    }

    render() {

        if(this.props.importDonor.status == API_STATUS.DONE)
        {
            var text=this.props.importDonor.totalRecords + " are found. Success:" + this.props.importDonor.successRecords + " .Failed:" + this.props.importDonor.failedRecords;
        }
        else
            text="";
        return (
            <div>

                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li><a href="javascript:void(0)">Schedules &amp; Appointments</a></li>
                        <li className="active">Import Donor</li>
                    </ol>
                </div>

                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Import Donor</h2>
                    </div>
                    
                </div>


                <div className="panel">
                    <div className="panel-body">
                        <div className="note">Please ensure that the new donor file to import is correctly formatted and has a file name that ends with a ".csv" extension.</div>
                            
                            <input id="input-fcount-1" name="donorFile" multiple="false" type="file" className="file-loading" />
                            <div> { text }</div>
                        </div>
                    </div>
            </div>
        );
    }

}

export default ImportDonor;