import React, {Component} from 'react';
import {API_STATUS, DIALOG_TYPE} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import InputControl from '../common/inputControl'
import isEqual from 'lodash';

class OutReachDialog extends Component {

    constructor(props) {
        super(props);

        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);
        this.onChange = this
            .onChange
            .bind(this);
        this.onSave = this
            .onSave
            .bind(this);
        this.onCancel = this
            .onCancel
            .bind(this);

        this.state = {
            donorId: this.props.donorId,
            scheduleId: this.props.scheduleId,
            data: this.props.data
                ? this.props.data
                : {},
            orgData: this.props.data
                ? this.props.data
                : {}
        }

        this.inputControls = {};
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dialogShowing) {
            this.closing = false;
            $('#myModal').modal('show');
            if (nextProps.status == API_STATUS.IN_PROGRESS) {
                if (nextProps.actionType == actionTypes.SAVE_OUT_REACH) {
                    this.setState({processing: true});
                }
            } else if (nextProps.actionType == actionTypes.GET_OUT_REACH_REASON || nextProps.actionType == actionTypes.GET_OUT_REACH_OUTCOME) {
                if (nextProps.status == API_STATUS.DONE) {
                    var items = nextProps.items;
                    var arrayElement = [];
                    items.map(function (item) {
                        var element = [item.name, item.id];
                        arrayElement.push(element);
                    })

                    if (nextProps.actionType == actionTypes.GET_OUT_REACH_REASON) {
                        this.setState({outReachReasonList: arrayElement});
                    } else {
                        this.setState({outReachOutcomeList: arrayElement});
                    }
                }

            } else if (nextProps.actionType == actionTypes.SAVE_OUT_REACH && nextProps.status == API_STATUS.DONE) {

                var closeDialog = this.props.layoutActions.closeDialog;
                this.setState({successMessage: "Save out reach successfully.", processing: true});
                setTimeout(function () {
                    closeDialog(true, nextProps.data);
                }, 3000);

            } else if (nextProps.status == API_STATUS.ERROR) {
                var message = "";
                if (nextProps.message) {
                    message = nextProps
                        .message
                        .replace("AlreadyExistsException: ", "")
                        .replace("IllegalArgumentException: ", "");
                }
                this.setState({errorMessage: message, processing: false});

            } else if (nextProps.layout.status == undefined || nextProps.layout.status == '') {
                //new dialog?
                this
                    .props
                    .actions
                    .getListOutreachReason();
                this
                    .props
                    .actions
                    .getListOutreachOutcome();

                var controls = this.inputControls;
                Object
                    .keys(controls)
                    .forEach(function (key) {
                        controls[key].clearError();
                    });

                this.setState({
                    donorId: nextProps.donorId,
                    scheduleId: nextProps.scheduleId,
                    data: {},
                    errors: {},
                    errorMessage: '',
                    successMessage: '',
                    processing: false,
                    clickSave: false,
                    clickCancel: false
                });

            }

        } else {
            this.closing = true;
            $('#myModal').modal('hide');
            this.setState({status: "", successMessage: "", errorMessage: ""});
        }
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {
        const data = this.state.data;
        if (name == "outreachReason") {
            data[name] = {
                id: value
            }
        } else if (name == "outreachOutcome") {
            data[name] = {
                id: value
            }
        } else {
            data[name] = value;
        }
        this.setState({data: data});
    }

    hasUnsavedChanges() {
        return !_.isEqual(this.state.data, this.state.orgData);
    }

    saveData(autoSave) {
        if (this.state.processing) 
            return;
        
        this.setState({autoSave: autoSave});

        const data = this.state.data;
        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (key) {
                controls[key].validateAllRules(key, data[key]);
            });

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })
        if (numError == 0) {
            this.setState({clickSave: true, processing: true, errorMessage: "", successMessage: ""});
            if (this.state.scheduleId) {
                this.state.data.scheduleRequest = {
                    id: this.state.scheduleId
                };
            }
            this
                .props
                .actions
                .saveOutReach(this.props.donorId, this.state.data);
        } else {
            this.setState({errorMessage: "", successMessage: ""});
            $(".error-list").show();
        }
    }

    autoSave() {
        this.saveData(true);
    }

    onSave()
    {
        this.saveData(false);
    }

    onCancel() {
        this.setState({clickCancel: true});

        setTimeout(function () {
            $(".close-modal").click();
        }, 10);
    }

    render() {
        var scheduleIdLabel = null;
        if (this.state.scheduleId) {
            scheduleIdLabel = (
                <div className="fom-group">
                    <label className="ucase">Schedule ID: {this.state.scheduleId}
                    </label>
                </div>
            );
        }
        return (
            <div
                className="modal fade"
                id="myModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="myModalLabel">
                <div
                    className="modal-dialog popup-modal"
                    id={this.props.dialogId}
                    role="document">
                    <div className="modal-content">
                        <div className="modal-header">

                            <div className="row">
                                <div className="col-sm-18 alpha-lg">
                                    <h4>Add Outreach Log</h4>
                                </div>

                                <div className="col-sm-6">

                                    <a href="javascript:void(0)" data-dismiss="modal" className="close-modal">
                                        <span aria-hidden="true">×</span>
                                    </a>
                                </div>

                            </div>

                        </div>
                        <div className="modal-body">
                            <ErrorArea errorList={this.state.errors}/>
                            <ComonInfoArea
                                successMessage={this.state.successMessage}
                                errorMessage={this.state.errorMessage}/>
                            <div>
                                <InputControl
                                    containerClass="form-group"
                                    type="radio"
                                    name="outreachType"
                                    label="Outreach Type"
                                    required={true}
                                    emptyMessage="Outreach Type is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "Phone", "Phone"
                                    ],
                                    [
                                        "Text", "Text"
                                    ],
                                    ["Email", "Email"]
                                ]}
                                    value={this.state.data.outreachType
                                    ? this.state.data.outreachType
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["outreachType"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group"
                                    type="select"
                                    name="outreachReason"
                                    label="Outreach Reason"
                                    required={true}
                                    emptyMessage="Outreach Reason is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    showSelectOption={true}
                                    value={this.state.data.outreachReason
                                    ? this.state.data.outreachReason.id
                                    : ''}
                                    optionValues={this.state.outReachReasonList
                                    ? this.state.outReachReasonList
                                    : []}
                                    ref
                                    ={(input) => {
                                    this.inputControls["outreachReason"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group"
                                    type="select"
                                    name="outreachOutcome"
                                    label="Outreach Outcome"
                                    required={true}
                                    showSelectOption={true}
                                    emptyMessage="Outreach Outcome is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    value={this.state.data.outreachOutcome
                                    ? this.state.data.outreachOutcome.id
                                    : ''}
                                    optionValues={this.state.outReachOutcomeList
                                    ? this.state.outReachOutcomeList
                                    : []}
                                    ref
                                    ={(input) => {
                                    this.inputControls["outreachOutcome"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/> {scheduleIdLabel}
                                <InputControl
                                    containerClass="form-group"
                                    type="textarea"
                                    name="comment"
                                    ref
                                    ={(input) => {
                                    this.inputControls["comment"] = input
                                }}
                                    value={this.state.data && this.state.data.comment
                                    ? this.state.data.comment
                                    : ''}
                                    label="Comments"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                            </div>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-group">
                                <a className="btn btn-white" onClick={this.onCancel}>
                                    Cancel</a>
                                <a
                                    disabled={this.state.processing}
                                    onClick={this.onSave}
                                    className="btn btn-white">
                                    Save Changes</a>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

export default OutReachDialog