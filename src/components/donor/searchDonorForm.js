import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import * as validations from '../../validations/common'
import {API_STATUS, PATH_PREFIX} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import {hashHistory, Router} from 'react-router';

class SearchDonorForm extends Component {
    constructor(props) {
        super(props);

        this.items = this.props.searchDonor.items;

        this.state = {
            searchParams: {
                "isMatchDonor": false
            },
            errors: {},
            keyword: '',
            sortName: this.props.searchDonor.sortBy,
            sortOrder: this.props.searchDonor.sortOrder,
            paramsEditted: false
        };

        this.showError = false;
        // assemble parameters
        this.defaultSearchParams = {
            "isMatchDonor": false,
            "did": null,
            "firstName": null,
            "lastName": null,
            "email": null,
            "primaryPhone": null,
            "locationId": null,
            "category": null,
            "status": null,
            "isOnCall": null,
            "isClinicalGrade": null,
            "isFlexible": null,
            "isInflexible": null,
            "isLowDemographic": null,
            "isClinicalAssessment1Completed": null,
            "isClinicalAssessment2Completed": null,
            "isDonorAssessmentNeeded": null,
            "procedureCategory": null,
            "collectionDate": null,
            "collectionTime": null,
            "donationCountLimit": null,
            "gender": null,
            "ethnicity": null,
            "ageRange": null,
            "bmiRange": null,
            "bloodType": null,
            "race": null,
            "smoker": null,
            "excludeMedication": null,
            "includeAllergy": null,
            "hasHpv": null,
            "hasHepB": null,
            "hasTetanus": null,
            "hasFlu": null,
            "hasPneumonia": null,
            "cmv": null,
            "hla": null
        };

        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);

        this.onChange = this
            .onChange
            .bind(this);

        this.onShowMore = this
            .onShowMore
            .bind(this);

        this.inputControls = {};
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {

        this.setState({paramsEditted: true});

        const data = this.state.searchParams;

        if (name == "hlaGeneTypes") {

            var value1 = value.substring(1);
            if (value[0] == "+") {
                this.fillHlaSpecific(value1);
            } else {
                $("#hla" + value1).remove();
            }

            if (name == "hlaGeneTypes" && value.length > 0 && !$('[name=hlaAlleleOption]:checked').val()) {
                $('[name=hlaAlleleOption][value="either"]')
                    .closest('label')
                    .click()
                    .addClass('checked');
                if ($('[name=hlaAlleleOption][value="either"]').length) {
                    this.radioButtonClick(($('[name=hlaAlleleOption][value="either"]')[0]), this);
                }
            }

        } else if (name == "procedureCategory") {
            if (typeof(value) === "string" && value.startsWith("Whole Blood")) {
                $("#whole").show();
                $("#bonemarrow,#lukopack").hide();
                data.volume = null;
            } else if (typeof(value) === "string" && value.startsWith("Bone Marrow")) {
                $("#bonemarrow").show();
                $("#whole,#lukopack").hide();
                if ($("#bonemarrow input").length > 0 && $("#bonemarrow input:checked").length == 0) {
                    $($("#bonemarrow input")[0])
                        .closest("label")
                        .click()
                        .addClass("checked");
                }
            } else if (typeof(value) === "string" && value.startsWith("LeukoPak")) {
                $("#lukopack").show();
                $("#bonemarrow,#whole").hide();
                if ($("#lukopack input").length > 0 && $("#lukopack input:checked").length == 0) {
                    $($("#lukopack input")[0])
                        .closest("label")
                        .click()
                        .addClass("checked");
                }
            } else {
                $("#whole,#bonemarrow, #lukopack").hide();
                data.volume = null;
            }

        } else if (name.indexOf("Volume") >= 0) {
            data.volume = value;
        } else if (name == "hlaAlleleOption") {
            if (value == "both") {
                $(".type_alle2").show();
                $("h4.alleTitle").each(function () {
                    var t = $(this).text();
                    $(this).text(t + " 1");
                });
            } else {
                $(".type_alle2").hide();
                $("h4.alleTitle").each(function () {
                    var t = $(this).text();
                    if (t.endsWith("1")) 
                        $(this).text(t.substring(0, t.length - 2));
                    }
                );
            }
        } else if (name == "smoker" || name.indexOf("has") >= 0 || name.indexOf("is") >= 0) {
            if (value == 'y') 
                value = true;
            else if (value == 'n') 
                value = false;
            else 
                value = null;
            }
        else if (value == '') 
            value = null;
        
        data[name] = value;
        this.setState({searchParams: data});

    }

    onSearch() {
        this.setState({paramsEditted: false});
        const data = jQuery.extend(true, {}, this.state.searchParams);

        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (key) {
                controls[key].validateAllRules(key, data[key]);
            });

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })
        if (numError == 0) {

            if (data.ageStart || data.ageEnd) 
                data.ageRange = data.ageStart + "-" + data.ageEnd;
            else 
                data.ageRange = null;
            
            if (data.bmiStart || data.bmiEnd) 
                data.bmiRange = data.bmiStart + "-" + data.bmiEnd;
            else 
                data.bmiRange = null;
            
            this.buildHla(data);

            this.setState({searchParams: data});

            this
                .props
                .actions
                .searchDonor(data, 1, this.props.searchDonor.pageSize, this.props.searchDonor.sortBy, this.props.searchDonor.sortOrder, true);
        } else {
            this.showError = true;
        }
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.searchDonor.actionType == "CHANG_ROUTE") {
            this.clearfields();
        } else if (nextProps.searchDonor.hideFilterIfResultsFound || nextProps.searchDonor.actionType == actionTypes.SEARCH_DONORS || nextProps.searchDonor.actionType == actionTypes.SEARCH_DONORS_RELOAD) {
            if (nextProps.searchDonor.hideFilterIfResultsFound) {
                $("#edit_search_filters").removeClass("dropdown-active");
            } else {
                $("#edit_search_filters").addClass("dropdown-active");
                $('html, body').animate({
                    scrollTop: $(document).height() - $(window).height()
                }, 700);
            }

        }

    }

    onShowMore() {
        if (this.props.searchDonor.items.length < this.props.searchDonor.totalRecords) {
            this
                .props
                .actions
                .searchDonor(this.state.searchParams, ++this.props.searchDonor.page, this.props.searchDonor.pageSize, this.props.searchDonor.sortBy, this.props.searchDonor.sortOrder, false);
        }
    }

    onSortChange(sortName, sortOrder) {
        this
            .props
            .actions
            .searchDonor(this.state.searchParams, 1, this.props.searchDonor.pageSize, sortName, sortOrder, true);
    }

    buttonEdit(cell, row) {
        var canEdit = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 3;

        var didOnly = (this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id >= 3);
        var iconSrc = didOnly
            ? "icon-view.svg"
            : "icon-edit.svg";
        iconSrc = "/img/icons/searchdonaricons/" + iconSrc

        var titleEditDonor = didOnly
            ? "View donor"
            : "Edit donor";

        var f = function (row) {
            if (canEdit) {
                hashHistory.push(PATH_PREFIX + '/donor/edit-donor/' + row.id);
            }
        };
        if (canEdit) {
            return (
                <a
                    href="javascript:void(0)"
                    data-toggle="tooltip"
                    title={titleEditDonor}
                    onClick={function () {
                    f(row);
                }}>
                    <img className="icon-svg" src={iconSrc} alt="edit" width="18"/></a>
            );
        }
        return null;
    }

    buttonOutreach(cell, row) {
        var canAddLog = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 2;
        var openAddOutReachDialog = this.props.layoutActions.openAddOutReachDialog;
        var f = function (row) {
            if (canAddLog) {
                openAddOutReachDialog(row.id);
            }
        };
        if (canAddLog) {
            return (
                <a
                    href="javascript:void(0)"
                    data-toggle="tooltip"
                    title="Add Outreach Log"
                    onClick={function () {
                    f(row);
                }}>
                    <img
                        className="icon-svg"
                        src="/img/icons/searchdonaricons/icon-add-outreach-log.svg"
                        alt="edit"
                        width="18"/></a>
            );
        }
        return null;
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('search-donor');
        document
            .body
            .classList
            .add('inner');
        document
            .body
            .classList
            .add('search-donor-results');
        document.title = 'Search Donor';

        var self = this;
        setTimeout(function () {

            $(".search-donor-form input")
                .keypress(function (e) {
                    if (e.keyCode == 13) {
                        self.onSearch();
                    }
                })

            $("#whole,#bonemarrow, #lukopack").hide();

            $("#ddAdvancedFilters").off("click");
            $("#ddAdvancedFilters").click(function (event) {
                $("#edit_search_filters").toggleClass("dropdown-active");
            });

            $("input:radio").off("click");
            $("input:checkbox").off("click");
            $("input:radio").click(function () {
                self.radioButtonClick(this, self);
            });

            $("input:checkbox").click(self.checkBoxClick);
        }, 200);
    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('search-donor');
        document
            .body
            .classList
            .remove('inner');
    }

    buildHla(returnData) {
        var hla = {};
        var reactComponent = this;

        hla.alleleMatchOption = this.state.searchParams.hlaAlleleOption
            ? this.state.searchParams.hlaAlleleOption
            : 'either';
        hla.geneTypes = [];

        $("div.alle1").each(function () {
            var result = {};
            result.geneType = $(this).attr("data-gene");
            result.allele1 = [];
            $(this)
                .find(".form-group.alle-select")
                .each(function () {
                    reactComponent.buildAlle($(this), result, true);
                });

            hla
                .geneTypes
                .push(result);
        });

        if (hla.alleleMatchOption == "both") {
            var index = 0;
            $("div.alle2").each(function () {
                var result = hla.geneTypes[index];
                result.geneType = $(this).attr("data-gene");
                result.allele2 = [];
                $(this)
                    .find(".form-group.alle-select")
                    .each(function () {
                        reactComponent.buildAlle($(this), result, false);
                    });
                index++;
            });
        }

        if (hla.geneTypes.length == 0) {
            hla = null;
        }

        if (!returnData) {
            const data = this.state.searchParams;
            data.hla = hla;
            this.setState({searchParams: data});
        } else {
            returnData.hla = hla;
        }
    }

    buildAlle(alleHtmlObject, result, addToAllele1)
    {
        if (alleHtmlObject.find("select#PredicateAA").length) {
            this.buildAlleGene(alleHtmlObject, "AA", result, addToAllele1);
        } else if (alleHtmlObject.find("select#PredicateBB").length) {
            this.buildAlleGene(alleHtmlObject, "BB", result, addToAllele1);
        } else if (alleHtmlObject.find("select#PredicateCC").length) {
            this.buildAlleGene(alleHtmlObject, "CC", result, addToAllele1);
        } else if (alleHtmlObject.find("select#PredicateDD").length) {
            this.buildAlleGene(alleHtmlObject, "DD", result, addToAllele1);
        }
    }

    buildAlleGene(alleHtmlObject, gene, result, addToAllele1)
    {
        gene = gene.replace("|", "");
        var alleleData = {
            alleleType: gene
        };
        alleleData.predicate = alleHtmlObject
            .find("select#Predicate" + gene + " option:checked")
            .val()
            .toLowerCase()
            .replace(" ", "_");
        if (alleleData.predicate != "predicate") {
            alleleData.value = alleHtmlObject
                .find("input")
                .val()
                .toLowerCase();

            if (addToAllele1) {
                result
                    .allele1
                    .push(alleleData);
            } else {
                result
                    .allele2
                    .push(alleleData);
            }
        }
    }

    fillHlaSpecific(hlaGeneValue, dontCreateSelect2) {
        if ($("#hla" + hlaGeneValue).length > 0 || this.fillingHla) 
            return;
        
        this.fillingHla = true;

        this.fillingHla = true;
        var v = $('[name=hlaAlleleOption]:checked').val();

        var alleNumber1 = "1";
        if (!v || v == "either" || v == "homozygous") {
            var alleNumber1 = "";
        }

        var options = '<div class="geneTypeContainer" id=hla' + hlaGeneValue + '><div class="col-sm-24"><h4 class="alleTitle">GENE TYPE ' + hlaGeneValue + ', ALLELE ' + alleNumber1 + '</h4></div><div id="gene_typea" class="alle1" data-gene=' + hlaGeneValue + '><div class="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm ' +
                'form-group"><select class="form-control cityselect" id="PredicateAA"><option val' +
                'ue="predicate">Predicate</option><option value="ignore">Ignore</option><option v' +
                'alue="equal">Equal</option><option value="not_equal">Not Equal</option></select>' +
                '</div><div class="col-sm-6 collapse-sm alle-type form-group"><input type="text" ' +
                'placeholder="AA" class="form-control"/></div></div><div class="col-sm-6 form-gro' +
                'up alle-select"><div class="col-sm-18 collapse-sm form-group"><select class="for' +
                'm-control cityselect" id="PredicateBB"><option value="predicate">Predicate</opti' +
                'on><option value="ignore">Ignore</option><option value="equal">Equal</option><op' +
                'tion value="not_equal">Not Equal</option></select></div><div class="col-sm-6 col' +
                'lapse-sm alle-type form-group"><input type="text" placeholder="BB" class="form-c' +
                'ontrol"/></div></div><div class="col-sm-6 form-group alle-select"><div class="co' +
                'l-sm-18 collapse-sm form-group"><select class="form-control cityselect" id="Pred' +
                'icateCC"><option value="predicate">Predicate</option><option value="ignore">Igno' +
                're</option><option value="equal">Equal</option><option value="not_equal">Not Equ' +
                'al</option></select></div><div class="col-sm-6 collapse-sm alle-type form-group"' +
                '><input type="text" placeholder="CC" class="form-control"/></div></div><div clas' +
                's="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm form-group' +
                '"><select class="form-control cityselect" id="PredicateDD"><option value="predic' +
                'ate">Predicate</option><option value="ignore">Ignore</option><option value="equa' +
                'l">Equal</option><option value="not_equal">Not Equal</option></select></div><div' +
                ' class="col-sm-6 collapse-sm alle-type form-group"><input type="text" placeholde' +
                'r="DD" class="form-control"/></div></div></div><div class="col-sm-24 collapse-sm' +
                ' type_alle2" id="gene_type_alle2"><div class="col-sm-24"><h4>GENE TYPE ' + hlaGeneValue + ', ALLELE 2</h4></div><div id="gene_typea" class="alle2" data-gene=' + hlaGeneValue + '><div class="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm ' +
                'form-group"><select class="form-control cityselect " id="PredicateAA"><option  v' +
                'alue="predicate">Predicate</option><option value="ignore">Ignore</option><option' +
                ' value="equal">Equal</option><option value="not_equal">Not Equal</option></selec' +
                't></div><div class="col-sm-6 collapse-sm alle-type form-group"><input type="text' +
                '" placeholder="AA" class="form-control"/></div></div><div class="col-sm-6 form-g' +
                'roup alle-select"><div class="col-sm-18 collapse-sm form-group"><select class="f' +
                'orm-control cityselect" id="PredicateBB"><option value="predicate">Predicate</op' +
                'tion><option value="ignore">Ignore</option><option value="equal">Equal</option><' +
                'option value="not_equal">Not Equal</option></select></div><div class="col-sm-6 c' +
                'ollapse-sm alle-type form-group"><input type="text" placeholder="BB" class="form' +
                '-control"/></div></div><div class="col-sm-6 form-group alle-select"><div class="' +
                'col-sm-18 form-group collapse-sm"><select class="form-control cityselect" id="Pr' +
                'edicateCC"><option value="predicate">Predicate</option><option value="ignore">Ig' +
                'nore</option><option value="equal">Equal</option><option value="not_equal">Not E' +
                'qual</option></select></div><div class="col-sm-6 collapse-sm alle-type form-grou' +
                'p"><input type="text" placeholder="CC" class="form-control"/></div></div><div cl' +
                'ass="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm form-gro' +
                'up"><select class="form-control cityselect" id="PredicateDD"><option value="pred' +
                'icate">Predicate</option><option value="ignore">Ignore</option><option value="eq' +
                'ual">Equal</option><option value="not_equal">Not Equal</option></select></div><d' +
                'iv class="col-sm-6 collapse-sm alle-type form-group"><input type="text" placehol' +
                'der="DD" class="form-control"/></div></div></div></div></div>';

        $('#gene_type_alle1').append(options);
        if (v == "both") {
            $(".type_alle2").show();
        } else {
            $(".type_alle2").hide();
        }

        if (!dontCreateSelect2) {
            $("#gene_type_alle1 .cityselect").select2({minimumResultsForSearch: Infinity});
        }

        this.fillingHla = false;

    }

    toogleAdvanceSearch() {
        if ($('.advanced_search').css('display') == 'none') {
            $('.advanced_search').show();
            $('#filtertext').html('<div class="hide-text">Hide Advanced Filters</div>');
        } else {
            $('.advanced_search').hide();
            $('#filtertext').html('<div class="hide-text">Show Advanced Filters</div>');
        }
    }

    clearfields() {
        $('#disp_collectionSite,#disp_category,#disp_status,#disp_isOnCall,#disp_isClinical' +
                'Grade,#disp_isFlexible,#disp_isInflexible,#disp_flexible,#disp_isLowDemographic,' +
                '#disp_isClinicalAssessment1Completed,#disp_isClinicalAssessment2Completed, #disp' +
                '_isDonorAssessmentNeeded, #disp_gender, #disp_ethnicity, #disp_race, #disp_blood' +
                'Type, #disp_smoker, #disp_excludeMedication, #disp_includeAllergy, #disp_hasHpv,' +
                ' #disp_hasHepB, #disp_hasTetanus, #disp_hasFlu, #disp_hasPneumonia, #disp_cmv,#d' +
                'isp_hlaGeneTypes,#disp_hlaAlleleOption, #disp_procedureCategory').html('');

        $('#gene_typea, #gene_type_alle1, #gene_typea2')
            .children()
            .remove();

        $('input').each(function () {
            $(this)
                .closest('label')
                .removeClass('checked');
        });
        $("input:checked").prop("checked", false);

        this.setState({
            searchParams: jQuery.extend(true, {}, this.defaultSearchParams)
        });

        $("input[type=text]").val("");
        $("input[type=email]").val("");

        this
            .props
            .actions
            .clearSearchParams();

        $("#edit_search_filters").addClass("dropdown-active");
    }

    radioButtonClick(obj, form) {
        //// Get the radio button value
        var res = $(obj).val();

        var grp = $(obj).attr("data-group");
        // onclick below conternt display in bar addclass to selected criteria
        $('input[name="' + obj.name + '"]')
            .not(obj)
            .closest('label')
            .removeClass('checked');
        $(obj)
            .closest('label')
            .addClass('checked');

        var repid;
        // replace spl char form value
        if (!/^[0-9A-Za-z]+$/.test(res)) {
            repid = res.replace(/[_\W]+/g, "");
        } else {
            repid = res;
        }

        var content_before = '<span data-value="' + res + '" data-group="' + grp + '" id="rem_' + repid + '" class=" res-display">';
        var content_after = '</span>';
        var content = content_before + $(obj).attr("data-label") + '<a  href="javascript:void(0)" data-value="' + res + '"   id="remove_term_' + repid + '">X</a>' + content_after;

        var elemExists = $('#disp_' + grp).has('#rem_' + repid);

        if (elemExists.length) {
            $('#disp_' + grp)
                .find('#rem_' + repid)
                .remove();
            $('input[name=' + grp + ']').prop("checked", false); // uncheck radio
            $("input[data-group='" + grp + "']")
                .closest('label')
                .removeClass('checked');

            form.onChange(grp, "");
        } else {
            $('#disp_' + grp).html('');
            $('#disp_' + grp).append(content);
        }

        function remove_terms(resObj, val, grp) { //type=radio
            $(resObj).remove(); // remove data in bar

            $('input[name=' + grp + '][value="' + val + '"]')
                .closest('label')
                .prop("checked", false)
                .removeClass('checked');
            form.onChange(grp, "");
        }

        $("#remove_term_" + repid + '[data-value="' + res + '"]')
            .click(function (e) {
                var rem = $(e.target).closest("span");
                remove_terms($(rem), $(rem).attr("data-value"), $(rem).attr("data-group"));
            });

    }

    checkBoxClick() {
        var res = $(this).val();
        var grp = $(this).attr("data-group")

        // get checkbox status as checked or not
        var chk_sts = $('input:checkbox[value="' + res + '"]').prop('checked');

        var repid;
        // replace spl char form value
        if (!/^[0-9A-Za-z]+$/.test(res)) {
            repid = res.replace(/[_\W]+/g, "");
        } else {
            repid = res;
        }

        // content formation for display in bar
        var content_before = '<span data-value="' + res + '" id="rem_' + repid + '" class="res-display">';
        var content_after = '</span>';
        var content = content_before + $(this).attr("data-label") + '<a  href="javascript:void(0)" data-value="' + res + '"  id="remove_term_' + repid + '">X</a>' + content_after;

        function remove_terms(resObj, val, grp) { //type=check box
            $(resObj).remove(); // remove data in bar

            $('input:checkbox[value="' + val + '"]')
                .closest('label')
                .click()
                .removeClass('checked');

        }

        if (chk_sts == true) {
            // by check chkbox add the content in bar
            $('#disp_' + grp).append(content);
            //addclass to selected criteria
            $(this)
                .closest('label')
                .addClass('checked');

            $("#remove_term_" + repid + '[data-value="' + res + '"]').click(function (e) {
                var rem = $(e.target).closest("span");
                remove_terms($(rem), $(rem).attr("data-value"), $(rem).attr("data-group"));
            });

        } else { // by uncheck chkbox remove the content in bar
            $("#rem_" + repid).remove();
            $(this)
                .closest('label')
                .removeClass('checked');

        }

    }

    renderCategory(cell, row) {
        var category = row.recruitment && row.recruitment.category;
        if (category) {
            var className = category.toLowerCase() + "-clr";
            return (
                <div className={className}>
                    <span></span>
                    {category}
                </div>
            );
        }
    }

    renderStatus(cell, row) {
        var status = row.recruitment && row.recruitment.status;
        return (
            <div>
                {status}
            </div>
        );
    }

    render() {

        let didOnly = (this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id >= 3);

        let options = {
            onSortChange: this
                .onSortChange
                .bind(this)
        };

        let table = null;
        let showMore = null;
        if (this.props.searchDonor.items && this.props.searchDonor.items.length) {
            table = <div className="searchResultsContainer">
                <BootstrapTable
                    ref='table'
                    data={this.props.searchDonor.items}
                    options={options}
                    search={false}
                    tableContainerClass="donortable"
                    bordered={false}>
                    <TableHeaderColumn dataField='id' isKey dataSort>Id</TableHeaderColumn>
                    <TableHeaderColumn dataField='email' dataSort>Email</TableHeaderColumn>
                    <TableHeaderColumn dataField='firstName' dataSort>First Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='lastName' dataSort>Last Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='primaryPhone' dataSort>Phone</TableHeaderColumn>

                    <TableHeaderColumn
                        dataField='category'
                        dataFormat={this
                        .renderCategory
                        .bind(this)}
                        dataSort>Catagory</TableHeaderColumn>
                    <TableHeaderColumn
                        dataField='status'
                        dataFormat={this
                        .renderStatus
                        .bind(this)}
                        dataSort>Status</TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="button"
                        width='5%'
                        dataFormat={this
                        .buttonOutreach
                        .bind(this)}
                        dataAlign="center"
                        headerTitle={false}>&nbsp;</TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="button"
                        width='5%'
                        dataFormat={this
                        .buttonEdit
                        .bind(this)}
                        dataAlign="center"
                        headerTitle={false}>&nbsp;</TableHeaderColumn>
                </BootstrapTable>
                <div className="clear"></div>

            </div>

            if (this.props.searchDonor.items.length < this.props.searchDonor.totalRecords) {

                showMore = <div className="text-right showmore">
                    <a
                        className="showmore_rows"
                        href="javascript:void(0)"
                        onClick={this.onShowMore}>Show More</a>
                </div>
            }

        }

        var errText = null;
        if (!this.state.paramsEditted && (this.props.searchDonor.actionType == actionTypes.SEARCH_DONORS || this.props.searchDonor.actionType == actionTypes.SEARCH_DONORS_RELOAD) && this.props.searchDonor.status == API_STATUS.ERROR) {
            var message = "";
            if (typeof(this.props.searchDonor.message) == "string") {
                message = this
                    .props
                    .searchDonor
                    .message
                    .replace("AlreadyExistsException: ", "")
                    .replace("IllegalArgumentException: ", "");

                if (message.startsWith("InvalidFormatException")) {
                    var i = message.indexOf("at [Source:");
                    var j = message.indexOf("from String");
                    if (i > -1 && j > -1) {
                        message = message.substring(j + 5, i);
                    }
                }

                if (message) {
                    $("#errorDil #errorMessage").html(message);
                }
            }
            $("#errorDil").modal('show');

        } else if (!this.showError && (this.props.searchDonor.actionType == actionTypes.SEARCH_DONORS || this.props.searchDonor.actionType == actionTypes.SEARCH_DONORS_RELOAD) && this.props.searchDonor.status == API_STATUS.DONE && this.props.searchDonor.items.length == 0) {
            errText = (
                <span
                    id="errorSpan"
                    style={{
                    color: 'red',
                    position: 'relative',
                    fontSize: '20px',
                    top: '18px',
                    marginRight: '15px'
                }}>No result found.</span>
            );
        }

        if (this.showError) {
            $(".error-list").show();
            this.showError = false;
            setTimeout(function () {
                //scroll top
                $('html, body').animate({
                    scrollTop: 100
                }, 700);
            }, 200);
        }

        return (
            <div id="schedule-appointment" className="clearfix">
                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>
                            <a href="javascript:void(0)">Manage Donors</a>
                        </li>
                        <li className="active">Search Donor</li>
                    </ol>
                </div>
                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Search Donor</h2>
                    </div>

                </div>
                <div className="search-wrapper">
                    <div className="col-xs-24 col-sm-24 col-md-24 col-lg-24">
                        <div className="results col-sm-19">
                            <span id="disp_hasHpv"></span>
                            <span id="disp_hasHepB"></span>
                            <span id="disp_hasTetanus"></span>
                            <span id="disp_hasFlu"></span>
                            <span id="disp_hasPneumonia"></span>
                            <span id="disp_isOnCall"></span>
                            <span id="disp_isClinicalGrade"></span>
                            <span id="disp_isFlexible"></span>
                            <span id="disp_isInflexible"></span>
                            <span id="disp_isLowDemographic"></span>
                            <span id="disp_isClinicalAssessment1Completed"></span>
                            <span id="disp_isClinicalAssessment2Completed"></span>
                            <span id="disp_isDonorAssessmentNeeded"></span>
                            <span id="disp_collectionSite"></span>
                            <span id="disp_category"></span>
                            <span id="disp_status"></span>
                            <span id="disp_procedureCategory"></span>
                            <span id="disp_gender"></span>
                            <span id="disp_ethnicity"></span>
                            <span id="disp_race"></span>
                            <span id="disp_bloodType"></span>
                            <span id="disp_smoker"></span>
                            <span id="disp_clinicalGrade"></span>
                            <span id="disp_excludeMedication"></span>
                            <span id="disp_includeAllergy"></span>

                            <span id="disp_cmv"></span>

                            <span id="disp_hlaGeneTypes"></span>
                            <span id="disp_hlaAlleleOption"></span>

                        </div>
                        <div className="reset-form col-sm-5">

                            <div className="btn-group" role="group" aria-label="actions">
                                <a
                                    type="button"
                                    className="btn btn-default"
                                    href="javascript:void(0)"
                                    onClick={this
                                    .clearfields
                                    .bind(this)}>
                                    <img src="/img/icons/searchdonaricons/reset.png" alt="reset"/>Reset
                                </a>
                                <a
                                    type="button"
                                    className="btn btn-default btn-sm dropdown-toggle dropdown-extended"
                                    href="javascript:void(0)"
                                    id="ddAdvancedFilters"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="true">
                                    <img src="/img/icons/searchdonaricons/icon-filters.png" alt="reset"/>Filters
                                </a>
                            </div>
                        </div>
                        <div className="clear"></div>
                        <hr/>
                    </div>
                    <div
                        id="edit_search_filters"
                        className="dropdown-active clearfix dropdown-content">
                        <div
                            name="search-donor-form"
                            id="search-donor-form"
                            className="search-donor-form">

                            <div className="clear"></div>

                            <ErrorArea errorList={this.state.errors}/>
                            <div className="clear"></div>
                            <div className="col-sm-24">
                                <h3>General Information</h3>
                            </div>
                            <div className="clear"></div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["did"] = input
                            }}
                                name="did"
                                label="ID"
                                validate={validations.did}
                                errorMessage="The &#39;ID&#39; should be number. Please check and enter again."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="8"
                                value={this.state.searchParams && this.state.searchParams.did
                                ? this.state.searchParams.did
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                hidden={didOnly}
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["email"] = input
                            }}
                                name="email"
                                label="Email"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.searchParams && this.state.searchParams.email
                                ? this.state.searchParams.email
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                hidden={didOnly}
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["firstName"] = input
                            }}
                                name="firstName"
                                label="First Name"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="35"
                                value={this.state.searchParams && this.state.searchParams.firstName
                                ? this.state.searchParams.firstName
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                hidden={didOnly}
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["lastName"] = input
                            }}
                                name="lastName"
                                label="Last Name"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="35"
                                value={this.state.searchParams && this.state.searchParams.lastName
                                ? this.state.searchParams.lastName
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                hidden={didOnly}
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["primaryPhone"] = input
                            }}
                                name="primaryPhone"
                                label="Primary Phone"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="35"
                                value={this.state.searchParams && this.state.searchParams.primaryPhone
                                ? this.state.searchParams.primaryPhone
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-7 col-lg-7 form-group"
                                type="searchRadio"
                                name="collectionSite"
                                label="Location"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.searchParams.collectionSite}
                                optionValues={[
                                [
                                    "California", "CA"
                                ],
                                ["Massachusetts", "MA"]
                            ]}
                                ref
                                ={(input) => {
                                this.inputControls["collectionSite"] = input
                            }}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>
                            <div className="col-sm-24">
                                <h3>Recruitment Profile Search Criteria</h3>
                            </div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-15 col-lg-15 form-group"
                                type="searchRadio"
                                name="category"
                                label="Category"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.searchParams.category}
                                optionValues={[
                                [
                                    "Prospect", "prospect"
                                ],
                                [
                                    "New", "new"
                                ],
                                [
                                    "Active", "active"
                                ],
                                [
                                    "Lapsed", "lapsed"
                                ],
                                [
                                    "Inactive", "inactive"
                                ],
                                ["Disqualified", "disqualified"]
                            ]}
                                ref
                                ={(input) => {
                                this.inputControls["category"] = input
                            }}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-9 col-lg-9 form-group"
                                type="searchRadio"
                                name="status"
                                label="Status"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.searchParams.status}
                                optionValues={[
                                [
                                    "Open", "open"
                                ],
                                [
                                    "Reserved", "reserved"
                                ],
                                ["Scheduled", "scheduled"]
                            ]}
                                ref
                                ={(input) => {
                                this.inputControls["status"] = input
                            }}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-15 col-lg-15 form-group"
                                type="searchRadio"
                                name="procedureCategory"
                                label="Procedure"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.searchParams.procedureCategory}
                                optionValues={[
                                [
                                    "Whole Blood", "Whole Blood"
                                ],
                                [
                                    "Bone Marrow", "Bone Marrow"
                                ],
                                [
                                    "LeukoPak", "LeukoPak"
                                ],
                                ["Mobilized LeukoPak", "Mobilized LeukoPak"]
                            ]}
                                ref
                                ={(input) => {
                                this.inputControls["procedureCategory"] = input
                            }}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                containerId="whole"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["wholeVolume"] = input
                            }}
                                name="wholeVolume"
                                label="Volume"
                                errorMessage="The &#39;Volume&#39; does not seem to be valid. Please check and enter again."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="9"
                                value={this.state.searchParams && this.state.searchParams.wholeVolume
                                ? this.state.searchParams.wholeVolume
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                containerId="bonemarrow"
                                type="searchRadio"
                                name="bonemarrowVolume"
                                label="Volume"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.searchParams.bonemarrowVolume}
                                optionValues={[
                                [
                                    "50 ML", "Bone Marrow 50 ML"
                                ],
                                ["100 ML", "Bone Marrow 100 ML"]
                            ]}
                                ref
                                ={(input) => {
                                this.inputControls["bonemarrowVolume"] = input
                            }}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                containerId="lukopack"
                                type="searchRadio"
                                name="lukopackVolume"
                                label="Volume"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.searchParams.lukopackVolume}
                                optionValues={[
                                [
                                    "Half", "LeukoPak Half"
                                ],
                                ["Full", "LeukoPak Full"]
                            ]}
                                ref
                                ={(input) => {
                                this.inputControls["lukopackVolume"] = input
                            }}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                type="date"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["collectionDate"] = input
                            }}
                                name="collectionDate"
                                label="Collection Date"
                                useH4AsLabel
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="10"
                                validate={validations.date}
                                errorMessage="The &#39;Collection Date&#39; does not seem to be valid. Please check and enter again."
                                emptyMessage="The &#39;Collection Date&#39; field cannot be left empty. Please enter a value to continue."
                                value={this.state.searchParams && this.state.searchParams.collectionDate
                                ? this.state.searchParams.collectionDate
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["donationCountLimit"] = input
                            }}
                                name="donationCountLimit"
                                label="Lifetime Collection Limit"
                                errorMessage="The &#39;Lifetime Collection Limit<&#39; does not seem to be valid. Please check and enter again."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="9"
                                value={this.state.searchParams && this.state.searchParams.donationCountLimit
                                ? this.state.searchParams.donationCountLimit
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-4 form-group"
                                type="searchRadio"
                                name="isOnCall"
                                ref
                                ={(input) => {
                                this.inputControls["isOnCall"] = input
                            }}
                                value={this.state.searchParams && this.state.searchParams.isOnCall === true
                                ? 'y'
                                : (this.state.searchParams && this.state.searchParams.isOnCall === false)
                                    ? 'n'
                                    : ''}
                                label="On Call Donor"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "On Call Donor"
                                ],
                                ["No", "n", "Not Call Donor"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-5 col-lg-5 form-group"
                                type="searchRadio"
                                name="isClinicalGrade"
                                ref
                                ={(input) => {
                                this.inputControls["isClinicalGrade"] = input
                            }}
                                value={this.state.searchParams && this.state.searchParams.isClinicalGrade === true
                                ? 'y'
                                : (this.state.searchParams && this.state.searchParams.isClinicalGrade === false)
                                    ? 'n'
                                    : ''}
                                label="Clinical Grade Donor"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "Clinical Grade Donor"
                                ],
                                ["No", "n", "Not Clinical Grade Donor"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-4 form-group"
                                type="searchRadio"
                                name="isFlexible"
                                ref
                                ={(input) => {
                                this.inputControls["isFlexible"] = input
                            }}
                                value={this.state.searchParams && this.state.searchParams.isFlexible === true
                                ? 'y'
                                : (this.state.searchParams && this.state.searchParams.isFlexible === false)
                                    ? 'n'
                                    : ''}
                                label="Flexible Donor"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "Flexible Donor"
                                ],
                                ["No", "n", "Not Flexible Donor"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-4 form-group"
                                type="searchRadio"
                                name="isInflexible"
                                ref
                                ={(input) => {
                                this.inputControls["isInflexible"] = input
                            }}
                                value={this.state.searchParams && this.state.searchParams.isInflexible === true
                                ? 'y'
                                : (this.state.searchParams && this.state.searchParams.isInflexible === false)
                                    ? 'n'
                                    : ''}
                                label="Inflexible Donor"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "Inflexible Donor"
                                ],
                                ["No", "n", "Not Inflexible Donor"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-5 col-lg-5 form-group"
                                type="searchRadio"
                                name="isLowDemographic"
                                ref
                                ={(input) => {
                                this.inputControls["isLowDemographic"] = input
                            }}
                                value={this.state.searchParams && this.state.searchParams.isLowDemographic === true
                                ? 'y'
                                : (this.state.searchParams && this.state.searchParams.isLowDemographic === false)
                                    ? 'n'
                                    : ''}
                                label="Low Demographic Donor"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "Low Demographic Donor"
                                ],
                                ["No", "n", "Not Low Demographic Donor"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-9 col-lg-9 form-group"
                                type="searchRadio"
                                name="isClinicalAssessment1Completed"
                                ref
                                ={(input) => {
                                this.inputControls["isClinicalAssessment1Completed"] = input
                            }}
                                value={this.state.searchParams && this.state.searchParams.isClinicalAssessment1Completed === true
                                ? 'y'
                                : (this.state.searchParams && this.state.searchParams.isClinicalAssessment1Completed === false)
                                    ? 'n'
                                    : ''}
                                label="Clinical Grade Assessment 1 Completed"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "Clinical Grade Assessment 1 Completed"
                                ],
                                ["No", "n", "Clinical Grade Assessment 1 Not Completed"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-8 col-lg-8 form-group"
                                type="searchRadio"
                                name="isClinicalAssessment2Completed"
                                ref
                                ={(input) => {
                                this.inputControls["isClinicalAssessment2Completed"] = input
                            }}
                                value={this.state.searchParams && this.state.searchParams.isClinicalAssessment2Completed === true
                                ? 'y'
                                : (this.state.searchParams && this.state.searchParams.isClinicalAssessment2Completed === false)
                                    ? 'n'
                                    : ''}
                                label="Clinical Grade Assessment 2 Completed"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "Clinical Grade Assessment 2 Completed"
                                ],
                                ["No", "n", "Clinical Grade Assessment 2 Not Completed"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                type="searchRadio"
                                name="isDonorAssessmentNeeded"
                                ref
                                ={(input) => {
                                this.inputControls["isDonorAssessmentNeeded"] = input
                            }}
                                value={this.state.searchParams && this.state.searchParams.isDonorAssessmentNeeded === true
                                ? 'y'
                                : (this.state.searchParams && this.state.searchParams.isDonorAssessmentNeeded === false)
                                    ? 'n'
                                    : ''}
                                label="Donor Assessment Needed"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "Donor Assessment Needed"
                                ],
                                ["No", "n", "Donor Assessment Not Needed"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div
                                className="advanced_search"
                                style={{
                                display: 'none'
                            }}>
                                <div className="col-sm-24">
                                    <h3>Medical Profile Search Criteria</h3>
                                </div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    type="searchRadio"
                                    name="gender"
                                    ref
                                    ={(input) => {
                                    this.inputControls["gender"] = input
                                }}
                                    value={this.state.searchParams && this.state.searchParams.gender
                                    ? this.state.searchParams.gender
                                    : ''}
                                    label="Gender"
                                    labelClass="ucase"
                                    inputClass="form-control "
                                    optionValues={[
                                    [
                                        "Male", "m"
                                    ],
                                    ["Female", "f"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-18 col-lg-18 form-group"
                                    type="searchRadio"
                                    name="ethnicity"
                                    ref
                                    ={(input) => {
                                    this.inputControls["ethnicity"] = input
                                }}
                                    value={this.state.searchParams && this.state.searchParams.ethnicity
                                    ? this.state.searchParams.ethnicity
                                    : ''}
                                    label="Ethnicity"
                                    labelClass="ucase"
                                    inputClass="form-control cityselect"
                                    optionValues={[
                                    [
                                        "Hispanic or Latino ", "hispanic"
                                    ],
                                    [
                                        "Not Hispanic or Latino", "non_hispanic"
                                    ],
                                    ["Not Specified", "not_specified"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    ref={(input) => {
                                    this.inputControls["ageStart"] = input
                                }}
                                    name="ageStart"
                                    useH4AsLabel
                                    label="From Age"
                                    validate={validations.age}
                                    errorMessage="The &#39;From Age&#39; should be number. Please check the From Age and enter again."
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="3"
                                    value={this.state.searchParams && this.state.searchParams.ageStart
                                    ? this.state.searchParams.ageStart
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    ref
                                    ={(input) => {
                                    this.inputControls["ageEnd"] = input
                                }}
                                    name="ageEnd"
                                    useH4AsLabel
                                    label="To Age"
                                    validate={validations.age}
                                    errorMessage="The &#39;To Age&#39; should be number. Please check the To Age and enter again."
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="3"
                                    value={this.state.searchParams.ageEnd && this.state.searchParams.ageEnd
                                    ? this.state.searchParams.ageEnd
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    ref={(input) => {
                                    this.inputControls["bmiStart"] = input
                                }}
                                    name="bmiStart"
                                    useH4AsLabel
                                    label="From Bmi"
                                    validate={validations.bmi}
                                    errorMessage="The &#39;From Bmi&#39; should be number and maximum 99. Please check the From  Bmi and enter again."
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="5"
                                    value={this.state.searchParams && this.state.searchParams.bmiStart
                                    ? this.state.searchParams.bmiStart
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    ref={(input) => {
                                    this.inputControls["bmiEnd"] = input
                                }}
                                    name="bmiEnd"
                                    useH4AsLabel
                                    label="To Bmi"
                                    validate={validations.bmi}
                                    errorMessage="The &#39;To Bmi&#39; should be number and maximum 99. Please check the To Bmi and enter again."
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="5"
                                    value={this.state.searchParams && this.state.searchParams.bmiEnd
                                    ? this.state.searchParams.bmiEnd
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-24 col-lg-24 form-group"
                                    type="searchRadio"
                                    name="race"
                                    ref
                                    ={(input) => {
                                    this.inputControls["race"] = input
                                }}
                                    label="Race"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "American Indian/Alaskan Native", "american_indian"
                                    ],
                                    [
                                        "Asian", "asian"
                                    ],
                                    [
                                        "Black or African American", "black"
                                    ],
                                    [
                                        "Native Hawaiian or Other Pacific Islander", "hawaiian"
                                    ],
                                    [
                                        "White", "white"
                                    ],
                                    ["Other Race", "other"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-14 col-lg-14 form-group"
                                    type="searchRadio"
                                    name="bloodType"
                                    ref
                                    ={(input) => {
                                    this.inputControls["bloodType"] = input
                                }}
                                    label="Blood Type"
                                    labelClass="ucase"
                                    optionValues={[
                                    [
                                        'A\uFF0B', "A+"
                                    ],
                                    [
                                        'A \u2013', "A-"
                                    ],
                                    [
                                        'B\uFF0B', "B+"
                                    ],
                                    [
                                        'B \u2013', "B-"
                                    ],
                                    [
                                        'O\uFF0B', "O+"
                                    ],
                                    [
                                        'O \u2013', "O-"
                                    ],
                                    [
                                        'AB\uFF0B', "AB+"
                                    ],
                                    ['AB \u2013', "AB-"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    type="searchRadio"
                                    name="smoker"
                                    ref
                                    ={(input) => {
                                    this.inputControls["smoker"] = input
                                }}
                                    value={this.state.searchParams && this.state.searchParams.smoker === true
                                    ? 'y'
                                    : (this.state.searchParams && this.state.searchParams.smoker === false)
                                        ? 'n'
                                        : ''}
                                    label="Smoker"
                                    labelClass="ucase"
                                    optionValues={[
                                    [
                                        "Yes", "y", "Smoker"
                                    ],
                                    ["No", "n", "Not Smoker"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-18 col-lg-18 form-group"
                                    name="excludeMedication"
                                    ref
                                    ={(input) => {
                                    this.inputControls["excludeMedication"] = input
                                }}
                                    label="Medication use"
                                    type="searchRadio"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "Birth Control", "birth_control"
                                    ],
                                    [
                                        "Vitamin/Supplement", "supplement"
                                    ],
                                    [
                                        "Over-Counter", "over_counter"
                                    ],
                                    [
                                        "Prescription", "prescription"
                                    ],
                                    ["Steroid", "steroid"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-12 col-lg-12  form-group"
                                    type="searchRadio"
                                    name="includeAllergy"
                                    ref
                                    ={(input) => {
                                    this.inputControls["includeAllergy"] = input
                                }}
                                    label="Allergy Restrictions"
                                    labelClass="ucase"
                                    inputClass="form-control allergyselectpicker"
                                    optionValues={[
                                    [
                                        "Food", "food"
                                    ],
                                    [
                                        "Environment", "environmental"
                                    ],
                                    ["Medication", "medication"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-4 form-group"
                                    type="searchRadio"
                                    name="hasHpv"
                                    ref
                                    ={(input) => {
                                    this.inputControls["hasHpv"] = input
                                }}
                                    value={this.state.searchParams && this.state.searchParams.hasHpv === true
                                    ? 'y'
                                    : (this.state.searchParams && this.state.searchParams.hasHpv === false)
                                        ? 'n'
                                        : ''}
                                    label="Hpv"
                                    labelClass="ucase"
                                    optionValues={[
                                    [
                                        "Yes", "y", "HPV"
                                    ],
                                    ["No", "n", "No&nbsp;HPV"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-4 form-group"
                                    type="searchRadio"
                                    name="hasHepB"
                                    ref
                                    ={(input) => {
                                    this.inputControls["hasHepB"] = input
                                }}
                                    value={this.state.searchParams && this.state.searchParams.hasHepB === true
                                    ? 'y'
                                    : (this.state.searchParams && this.state.searchParams.hasHepB === false)
                                        ? 'n'
                                        : ''}
                                    label="Hep B"
                                    labelClass="ucase"
                                    optionValues={[
                                    [
                                        "Yes", "y", "Help&nbsp;B"
                                    ],
                                    ["No", "n", "No&nbsp;Help&nbsp;B"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-4 form-group"
                                    type="searchRadio"
                                    name="hasTetanus"
                                    ref
                                    ={(input) => {
                                    this.inputControls["hasTetanus"] = input
                                }}
                                    value={this.state.searchParams && this.state.searchParams.hasTetanus === true
                                    ? 'y'
                                    : (this.state.searchParams && this.state.searchParams.hasTetanus === false)
                                        ? 'n'
                                        : ''}
                                    label="Tetanus"
                                    labelClass="ucase"
                                    optionValues={[
                                    [
                                        "Yes", "y", "Tetanus"
                                    ],
                                    ["No", "n", "No&nbsp;Tetanus"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-4 form-group"
                                    type="searchRadio"
                                    name="hasFlu"
                                    ref
                                    ={(input) => {
                                    this.inputControls["hasFlu"] = input
                                }}
                                    value={this.state.searchParams && this.state.searchParams.hasFlu === true
                                    ? 'y'
                                    : (this.state.searchParams && this.state.searchParams.hasFlu === false)
                                        ? 'n'
                                        : ''}
                                    label="Flu"
                                    labelClass="ucase"
                                    optionValues={[
                                    [
                                        "Yes", "y", "Flu"
                                    ],
                                    ["No", "n", "No&nbsp;Flu"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    type="searchRadio"
                                    name="hasPneumonia"
                                    ref
                                    ={(input) => {
                                    this.inputControls["hasPneumonia"] = input
                                }}
                                    value={this.state.searchParams && this.state.searchParams.hasPneumonia === true
                                    ? 'y'
                                    : (this.state.searchParams && this.state.searchParams.hasPneumonia === false)
                                        ? 'n'
                                        : ''}
                                    label="Pneumonia"
                                    labelClass="ucase"
                                    optionValues={[
                                    [
                                        "Yes", "y", "Pneumonia"
                                    ],
                                    ["No", "n", "No&nbsp;Pneumonia"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-10 col-lg-10 form-group"
                                    type="searchRadio"
                                    showSelectOption={true}
                                    name="cmv"
                                    ref
                                    ={(input) => {
                                    this.inputControls["cmv"] = input
                                }}
                                    value={this.state.searchParams && this.state.searchParams.cmv
                                    ? this.state.searchParams.cmv
                                    : ''}
                                    label="CMV Status"
                                    labelClass="ucase"
                                    inputClass="form-control cityselect"
                                    optionValues={[
                                    [
                                        "Positive", "positive"
                                    ],
                                    [
                                        "Negative", "negative"
                                    ],
                                    ["Equivalent", "equivalent"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>
                                <div className="col-sm-24">
                                    <h3>HLA Typing Search Criteria</h3>
                                </div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-24 col-lg-24 form-group gene_typea_select"
                                    type="searchCheckbox"
                                    ref
                                    ={(input) => {
                                    this.inputControls["hlaGeneTypes"] = input
                                }}
                                    name="hlaGeneTypes"
                                    label="Hla Typing-Gene Type"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "A", "A"
                                    ],
                                    [
                                        "B", "B"
                                    ],
                                    [
                                        "C", "C"
                                    ],
                                    [
                                        "DPA1", "DPA1"
                                    ],
                                    [
                                        "DPB1", "DPB1"
                                    ],
                                    [
                                        "DQA1", "DQA1"
                                    ],
                                    [
                                        "DQB1", "DQB1"
                                    ],
                                    [
                                        "DRB1", "DRB1"
                                    ],
                                    [
                                        "DRB3", "DRB3"
                                    ],
                                    [
                                        "DRB4", "DRB4"
                                    ],
                                    ["DRB5", "DRB5"]
                                ]}
                                    selectedValues={this.state.searchParams && this.state.searchParams.hlaGeneTypes
                                    ? this.state.searchParams.hlaGeneTypes
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-24 col-lg-24 form-group"
                                    type="searchRadio"
                                    name="hlaAlleleOption"
                                    ref
                                    ={(input) => {
                                    this.inputControls["hlaAlleleOption"] = input
                                }}
                                    label="Allele Option"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "Either Allele", "either"
                                    ],
                                    [
                                        "Homozygous Alleles", "homozygous"
                                    ],
                                    ["Both Alleles", "both"]
                                ]}
                                    value={this.state.searchParams && this.state.searchParams.hlaAlleleOption
                                    ? this.state.searchParams.hlaAlleleOption
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>
                                <div className="col-sm-24 collapse-sm" id="gene_type_alle1"></div>
                                <div className="clear"></div>
                            </div>
                            <div className="clear"></div>
                            <div className="clear"></div>
                            <div className="col-xs-24 col-sm-24 col-md-24 col-lg-24">
                                <div className="pull-left showhide">
                                    <a
                                        href="javascript:void(0)"
                                        onClick={this
                                        .toogleAdvanceSearch
                                        .bind(this)}
                                        id="filtertext">Show Advanced Filters</a>
                                </div>
                                <div className="pull-right">
                                    {errText}
                                    <button
                                        className="btn btn-default"
                                        onClick={this
                                        .onSearch
                                        .bind(this)}>Search</button>

                                </div>
                            </div>

                            {this.state.searchParams.length
                                ? (<hr/>)
                                : null}
                        </div>
                    </div>
                    <div className="clear"></div>
                    {table}

                    {showMore}

                </div>

            </div>
  
            );
      }
  }

 export default SearchDonorForm;