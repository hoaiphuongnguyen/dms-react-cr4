import React, {Component} from 'react';
import Logout from '../account/logout';
import {hashHistory, Router} from 'react-router';
import {PATH_PREFIX} from '../../common/constants';
import * as apiUtils from '../../utils/apiUtils';

class Header extends Component {
  constructor(props) {
    super(props);
    var userObj = this.props.account.userInfo;
    if (!userObj) {
      var localUser = localStorage.getItem("userInfo");
      if (localUser) {
        userObj = JSON.parse(localUser);
        this
          .props
          .actions
          .setLoginFromLocalStore(userObj);
      }
    }

    var userName = "";
    if (userObj) {
      userName = userObj.firstName + " " + (userObj.lastName
        ? userObj.lastName
        : '');
    }

    this.state = {
      userName: userName
    }

    this.onAccountSetting = this
      .onAccountSetting
      .bind(this);
  }

  onAccountSetting() {
    this
      .props
      .openAccountSettingDialog();
  }

  componentWillReceiveProps(nextProps) {
    var localUser = nextProps.account.userInfo || localStorage.getItem("userInfo");
    if (typeof localUser === "string") {
      var userObj = JSON.parse(localUser);
      var userName = userObj.firstName + " " + (userObj.lastName
        ? userObj.lastName
        : '');
      this.setState({userName: userName});
    } else if (localUser != null) {
      this.setState({
        userName: localUser.firstName + " " + localUser.lastName
      });
    }

    this.redirectIfNotLoggedIn(nextProps.account);
  }

  componentDidMount() {
    this.redirectIfNotLoggedIn(this.props.account);
    var self = this;
    setTimeout(function () {
      self.initAutoCompletePlugin();
    }, 100);
  }

  quickSearchDonor(idOrLastName)
  {
    if (!isNaN(idOrLastName)) {
      var searchParams = {
        id: idOrLastName
      };
      var sortBy = 'id';
      return apiUtils.getGlobalSearchDonorUrl(1, 10, sortBy, "ASC") + "&id=" + idOrLastName;
    } else {
      searchParams = {
        lastName: idOrLastName
      };
      sortBy = "lastName";
      return apiUtils.getGlobalSearchDonorUrl(1, 10, sortBy, "ASC") + "&lastName=" + idOrLastName;
    }
  }

  initAutoCompletePlugin(data)
  {
    var user = this.props.account && this.props.account.userInfo
      ? this.props.account.userInfo
      : null;
    var canSearchDonor = user == null || user.role == null || !user.role.id || user.role.id <= 2;
    if (!canSearchDonor) 
      return;
    var self = this;
    var options = {
      // Instead of data you can also specify url: example.com/example.json as
      // parameter Live data (ajax) will be only be fetched if you press enter key
      // NOTICE: DO NOT USE CDN VERSION OF EZAUTOCOMPLETE ITS REVERSE ENGINEERED TO
      // WORK WITH ENTER KEY
      url: function (phrase) {
        return self.quickSearchDonor(phrase);
      },
      ajaxCallback: function (data) {
        if (data && data.length == 1) {
          //auto goto single match item
          if (!$("body").hasClass("edit-donor")) {
            hashHistory.push(PATH_PREFIX + '/donor/edit-donor/' + data[0].id);
          } else {
            self
              .props
              .getDonorFromQuickSeach(data[0].id);
          }
        }
      },

      getValue: "lastName",
      list: {
        match: {
          enabled: true
        },
        onClickEvent: function (e) {
          var target = e.currentTarget;
          var donorId = $(target)
            .find("span.id")
            .text();
          if (donorId) {
            if (!$("body").hasClass("edit-donor")) {
              hashHistory.push(PATH_PREFIX + '/donor/edit-donor/' + donorId);
            } else {
              self
                .props
                .getDonorFromQuickSeach(donorId);
            }
          }
        }
      },

      highlightPhrase: false,
      template: {
        type: "custom",
        method: function (value, item) {
          return "<span class='icon'></span><span class='id'>" + item.id + "</span> <span class='name'>" + value + '</span>';
        }
      }
    };
    $("#head_search_donors").easyAutocomplete(options);
  }

  componentDidUpdate()
  {
  }

  redirectIfNotLoggedIn(account) {
    if (location.hash != (PATH_PREFIX + "#/login") && account.userInfo == null) {
      var localUser = localStorage.getItem("userInfo");
      if (localUser == null) {
        hashHistory.push(PATH_PREFIX + '/login');
      }
    }

  }
  render() {

    return (
      <header id="header-main-contents" className="clearfix">
        <div id="header-search-donors" className="clearfix">
          <div className="header_search_donors">
            <input
              type="search"
              name="head_search_donors"
              id="head_search_donors"
              className="form-control"/>
          </div>
          <div className="pull-right-lg" id="profile-dropdown">
            <div className="btn-group">
              <a
                href="javascript:void(0)"
                id="btn-profile"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false">
                {"Hello " + this.state.userName + " "}<i className="fa fa-angle-down" aria-hidden="true"></i>
              </a>
              <ul className="dropdown-menu dropdown-menu-right">
                <li>
                  <a href="javascript:void(0)" onClick={this.onAccountSetting}>Account Settings</a>
                </li>
                <li>
                  <Logout />
                </li>
              </ul>
            </div>
          </div>
          <hr className="divider"/>
        </div>
      </header>
    );
  }

}

export default Header;