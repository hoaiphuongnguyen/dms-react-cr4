import React, { Component } from 'react';
import Header from '../../containers/header';
import Nav from './nav'
import UserDialog from '../../containers/user/userDialog'
import AccountSettingDialog from '../../containers/user/accountSettingDialog'
import OutReachDialog from '../../containers/donor/outReachDialog'
import AppointmentDialog from '../../containers/appointment/appointmentDialog'
import AppointmentPopup from '../../containers/appointment/appointmentPopup'
import MedicalProcedureDialog from '../../containers/admin/medicalProcedureDialog'
import SuccessDialog from '../../containers/common/successDialog'
import ErrorDialog from '../common/errorDialog'
import {PATH_PREFIX, DIALOG_TYPE} from '../../common/constants'

class Layout extends Component {
	constructor(props) {
		super(props);
	}

	componentWillReceiveProps(nextProps) {
      if(this.props.user ==null && nextProps.user!=null)
      {
        this.setState({checkaccess:true});
      }
  	}
	
   renderDialog() {
	   var hash =location.hash.toLowerCase();
	   if (hash.startsWith('#/user')){
			return (
				<UserDialog user={this.props.layout.dialogData}
				 dialogShowing={this.props.layout.modalDialogShowing} 
				 dialogType={this.props.layout.activeDialogType} 
				 dialogId="userDialog" />
			);
	   }
	   else if (hash.startsWith('#/donor/search-donor')
	    ||hash.startsWith( '#/schedule-request/process-schedule-request')){
		   	return (
				<OutReachDialog 
				 donorId={this.props.layout.donorId}
				 scheduleId= {this.props.layout.scheduleId}
				 data={this.props.layout.dialogData}
				 dialogShowing={this.props.layout.modalDialogShowing}
				 dialogType={this.props.layout.activeDialogType} 
				 dialogId="outReachDialog"
				/>
			);
	   }
	   else if (hash.startsWith( '#/donor/create-donor')){
			return (
				<div>
				<AppointmentPopup
					dialogData={this.props.layout.dialogData}
					dialogShowing={this.props.layout.appointmentPopupShowing}
					dialogType={this.props.layout.activeDialogType} 
					dialogId="appointmentPopup"
					user= {this.props.user}
				/>
				
				<AppointmentDialog 
					collectionSite={this.props.layout.collectionSite}
					procedureCategory={this.props.layout.procedureCategory}
					donorId={this.props.layout.donorId}
					startTime={this.props.layout.startTime}
				    endTime={this.props.layout.endTime}
				    dialogData={this.props.layout.dialogData}
					modalId = "appointmentModal"
					closeDialog = "closeAppointmentDialog"
					dialogShowing={this.props.layout.appointmentDialogShowing}
					dialogType={this.props.layout.activeDialogType} 
					dialogId="appointmentDialog"
				/>
				</div>
			  )

		}
	   else if (hash.startsWith( '#/donor/edit-donor')){
		   	return (
				<div>
				<OutReachDialog 
				 donorId={this.props.layout.donorId}
				 data={this.props.layout.dialogData}
				 dialogShowing={this.props.layout.modalDialogShowing}
				 dialogType={this.props.layout.activeDialogType} 
				 dialogId="outReachDialog"
				/>

				<AppointmentPopup
					dialogData={this.props.layout.dialogData}
					dialogShowing={this.props.layout.appointmentPopupShowing}
					dialogType={this.props.layout.activeDialogType} 
					dialogId="appointmentPopup"
					user= {this.props.user}
				/>
				
				<AppointmentDialog 
					collectionSite={this.props.layout.collectionSite}
					procedureCategory={this.props.layout.procedureCategory}
					donorId={this.props.layout.donorId}
					startTime={this.props.layout.startTime}
				    endTime={this.props.layout.endTime}
				    dialogData={this.props.layout.dialogData}
					modalId = "appointmentModal"
					closeDialog = "closeAppointmentDialog"
					dialogShowing={this.props.layout.appointmentDialogShowing}
					dialogType={this.props.layout.activeDialogType} 
					dialogId="appointmentDialog"
				/>
				</div>
			);
	   }
	    else if (hash.startsWith( '#/medical-procedure')){

		   return (
				<MedicalProcedureDialog 
				 data={this.props.layout.dialogData}
				 dialogShowing={this.props.layout.modalDialogShowing}
				 dialogType={this.props.layout.activeDialogType} 	
				/>
			);

	   }
		else if (hash.startsWith(PATH_PREFIX + '#/')){
			return (
			<div>
				<AppointmentPopup
					dialogData={this.props.layout.dialogData}
					dialogShowing={this.props.layout.appointmentPopupShowing}
					dialogType={this.props.layout.activeDialogType} 
					dialogId="appointmentPopup"
					user= {this.props.user}
				/>
				<AppointmentDialog 
					collectionSite={this.props.layout.collectionSite}
					procedureCategory={this.props.layout.procedureCategory}
					donorId={this.props.layout.donorId}
					startTime={this.props.layout.startTime}
				    endTime={this.props.layout.endTime}
				    dialogData={this.props.layout.dialogData}
					modalId = "appointmentModal"
					closeDialog = "closeAppointmentDialog"
					dialogShowing={this.props.layout.appointmentDialogShowing}
					dialogType={this.props.layout.activeDialogType} 
					dialogId="appointmentDialog"
				/>
			
				</div>
			);
	   }
	  
    }

	renderASDialog() {

		return (
		<AccountSettingDialog  
		modalId = "myAccountSetting"
		closeDialog = "closeDialogAccountSetting"
		dialogShowing={this.props.layout.accountSettingDialogShowing} 
		dialogType={this.props.layout.activeDialogType} />
		)
	}

	render() {
		return (
			<div>
				<div id="wrapper">
					<div className="container-fluid">
						<main className="row">
							<aside id="sidebar-tasks" className="col-xs-24 col-sm-24 col-md-5 col-lg-5">
								<h1 className="branding">
									<a href="javascript:void(0)"><img src="/img/branding.svg" alt="" /></a>
								</h1>
								<Nav user={this.props.user}/>
							</aside>

							<section id="main-contents" className="col-xs-24 col-sm-24 col-md-19 col-lg-19">
								<Header />
								<section id="content" className="clearfix">
									{this.props.children}
								</section>
							</section>
						</main >
					</div>
				</div>

			{this.renderDialog()}

			{this.renderASDialog()}

			
			<ErrorDialog/>
		    <SuccessDialog 
				dialogShowing={this.props.layout.successDialogShowing}
			schedulingRequest={this.props.schedulingRequest}/>
			</div>
		);
	}
}

export default Layout;