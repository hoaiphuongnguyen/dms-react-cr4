import React, {Component} from 'react';
import NavItem from './navItem';
import NavItemGroup from './navItemGroup';
import {PATH_PREFIX} from '../../common/constants'

class Nav extends Component {
  constructor(props) {
    super(props);
  }

  shouldExpandManageDonors() {
    var result =  location
      .hash
      .toLowerCase()
      .startsWith('#/donor') || location.hash=="#/";
      return result;
  }

  shouldExpandScheduleRequests() {
    var result =  location
      .hash
      .toLowerCase()
      .startsWith(  '#/schedule-request');
      return result;
  }

  shouldExpandReports() {
    var result =  location
      .hash
      .toLowerCase()
      .startsWith('#/report');
      return result;
  }

  shouldExpandSystemAdministration() {
    var result =  location
      .hash
      .toLowerCase()
      .startsWith('#/user') || location
      .hash
      .toLowerCase()
      .startsWith('#/medical-procedure');
      return result;
  }

  componentWillReceiveProps(nextProps) {
      if(this.props.user ==null && nextProps.user!=null)
      {
        this.setState({checkaccess:true});
      }
  }

  checkacccess(){
    if(this.props.user){
      var roleId = this.props.user.role && this.props.user.role.id ? this.props.user.role.id: 5;

      if(roleId >1) //disable admin menu
      {
          $("#adminMenuGroup").hide();
      }

      if(roleId > 2) //disable menu items for DID Only
      {
          $("#reportsMenuGroup").hide();
          $("#createDonorMenu").hide();
          $("#importDonorMenu").hide();
          $("#processScheduleMenu").hide();
      }

      if(roleId >3) //did schedule
      {
          $("#createScheduleMenu").hide();
      }

      if(roleId >4) //calendar only view
      {
          $("#schedulesMenuGroup").hide();
          $("#searchDonorMenu").hide();
      }
    }
  }
  componentDidMount(){
    this.checkacccess();
  }
  componentDidUpdate(){
     this.checkacccess();
  }

  render() {

    return (
      <div className="panel-group" id="accordion">

        <NavItemGroup
          id="accManageDonors" containerId="donorsMenuGroup"
          title="Manage Donors"
          expanded={this.shouldExpandManageDonors()}>
          <NavItem to={PATH_PREFIX + '/'}><img src="/img/icons/sidebar/schedule_appointment.svg" alt=""/>Schedules &amp; Appointments</NavItem>
          <NavItem id="searchDonorMenu" to={PATH_PREFIX + '/donor/search-donor'}><img src="/img/icons/sidebar/search_donor.svg" alt=""/>Search Donor</NavItem>
          <NavItem id="createDonorMenu" to={PATH_PREFIX + '/donor/create-donor'}><img src="/img/icons/sidebar/create_donor.svg" alt=""/>Create Donor</NavItem>
          <NavItem id="importDonorMenu" to={PATH_PREFIX + '/donor/import-donor'}><img src="/img/icons/sidebar/import_donors.svg" alt=""/>Import Donors</NavItem>
        </NavItemGroup>

        <NavItemGroup
          id="accManageScheduleRequests" containerId="schedulesMenuGroup"
          title="Manage Schedule Requests"
          expanded={this.shouldExpandScheduleRequests()}>
          <NavItem id="processScheduleMenu" to={PATH_PREFIX + '/schedule-request/process-schedule-request'}><img src="/img/icons/sidebar/icon-process-schedule-request.svg" alt=""/>Process Scheduling Requests</NavItem>
          <NavItem to={PATH_PREFIX + '/schedule-request/search-schedule-request'}><img src="/img/icons/sidebar/icon-search-scheduling-request.svg" alt=""/>Search Scheduling Request</NavItem>
          <NavItem id="createScheduleMenu" to={PATH_PREFIX + '/schedule-request/create-scheduling-request'}><img src="/img/icons/sidebar/icon-create-scheduling-request.svg" alt=""/>Create Scheduling Request</NavItem>
        </NavItemGroup>

        <NavItemGroup
          id="accReports" containerId="reportsMenuGroup"
          title="Reports"
          expanded={this.shouldExpandReports()}>
          <NavItem to={PATH_PREFIX + '/report/donor-status'}><img src="/img/icons/sidebar/icon-donor-status.svg" alt=""/>Donor Status</NavItem>
          <NavItem to={PATH_PREFIX + '/report/donor-cancellation'}><img src="/img/icons/sidebar/icon-donor-cancellations.svg" alt=""/>Donor Cancellation</NavItem>
          <NavItem to={PATH_PREFIX + '/report/appointment'}><img src="/img/icons/sidebar/icon-scheduling-request.svg" alt=""/>Appointment</NavItem>
          <NavItem to={PATH_PREFIX + '/report/payment-transaction'}><img src="/img/icons/sidebar/icon-payment.svg" alt=""/>Payment Transaction</NavItem>
        </NavItemGroup>

        <NavItemGroup id="collapseFour" containerId="adminMenuGroup" title="System Administration" expanded={this.shouldExpandSystemAdministration()}>
          <NavItem to={PATH_PREFIX + '/user'}><img src="/img/icons/sidebar/icon-manage-user.svg" alt="" />Manage Users</NavItem>
          <NavItem to={PATH_PREFIX + '/medical-procedure'}><img src="/img/icons/sidebar/icon-manage-medical-protocol.svg" alt="" />Manage Medical Protocol</NavItem>
        </NavItemGroup>
      </div>
    );
  }
}

export default Nav;
