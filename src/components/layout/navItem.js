import React, {Component} from 'react';
import {Link, IndexLink, withRouter} from 'react-router'
import {PATH_PREFIX} from '../../common/constants'
class NavItem extends Component {
    render() {
        const {
            index,
            to,
            id,
            children,
            router,
            params,
            location,
            routes,
            ...props
        } = this.props
        let isActive
        if (router.isActive('/', true) && index) {
            isActive = true;
        } else if (to == PATH_PREFIX + "/donor/search-donor" && location.pathname.indexOf('/donor/edit-donor') >= 0) {
            isActive = true;
        } else {
            isActive = router.isActive(to);
        }

        return (
            <li
                id={id}
                className={isActive
                ? 'active'
                : ''}>
                <Link to={to} {...props}>{children}</Link>
            </li>
        )

    }
}

NavItem = withRouter(NavItem)

export default NavItem