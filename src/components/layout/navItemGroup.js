import React, { Component } from 'react';
import { Link, IndexLink, withRouter } from 'react-router'

class NavItemGroup extends Component {

     constructor(props) {
        super(props);
        this.state = {expanded:false};
     }

    componentWillReceiveProps(nextProps) {
        
        var self = this;
        if(nextProps.expanded)
        {
            setTimeout(function(){
                if(!self.unMounted)
                {
                    self.setState({expanded: nextProps.expanded});
                    if($("#" + self.props.containerId + " a").attr("aria-expanded") == "false")
                    {
                        $("#" + self.props.containerId + " a").click();
                    }
                }
            }, 500);
        }

    }

    componentDidMount()
    {
        this.unMounted=false;
        var self = this;
        if (this.props.expanded) {
            setTimeout(function () {
                if ($("#" + self.props.containerId + " a").attr("aria-expanded") == "false") {
                    $("#" + self.props.containerId + " a").click();
                }
            }, 500);
        }
    }

    componentWillUnmount() {
        this.unMounted=true;
    }

    render() {
        const { id, title, ...props } = this.props;
        var expanded = this.state.expanded;
        return (
            <div className="panel panel-default" id={this.props.containerId}>
                <div className="panel-heading">
                    <h4 className="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href={'#'+id} className='collapsed' aria-expanded={false}>{title}</a>
                    </h4>
                </div>
                <div id={id} className='panel-collapse collapse' aria-expanded={false}>
                    <div className="panel-body">
                        <ul>
                            {this.props.children}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default NavItemGroup
