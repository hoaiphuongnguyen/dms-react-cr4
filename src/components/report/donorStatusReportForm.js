import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import * as validations from '../../validations/common'
import {API_STATUS, PATH_PREFIX} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import {hashHistory, Router} from 'react-router';
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import * as apiUtils from '../../utils/apiUtils';
import {displayDate} from '../../utils/date.js';

class DonorStatusReportForm extends Component {
    constructor(props) {
        super(props);

        this.items = this.props.report.items;

        this.state = {
            searchParams: {},
            errors: {},
            data: this.items,
            sortName: this.props.report.sortBy,
            sortOrder: this.props.report.sortOrder,
            status: ''
        };

        this.onShowMore = this
            .onShowMore
            .bind(this);

        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);

        this.onChange = this
            .onChange
            .bind(this);

        this.onSearch = this
            .onSearch
            .bind(this);

        this.inputControls = {};
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.report.actionType == "CHANG_ROUTE") {
            this.clearfields();
        } else {
            this.items = nextProps.report.items;
            this.setState({data: this.items, errors: {}, sortName: this.props.report.sortBy, sortOrder: this.props.report.sortOrder, status: nextProps.report.status});
        }
    }

    clearfields() {
        $('#disp_collectionSite, #disp_category, #disp_status').html('');

        $('input').each(function () {
            $(this)
                .closest('label')
                .removeClass('checked');

        });

        this.setState({searchParams: {}, errors: {}, sortName: this.props.report.sortBy, sortOrder: this.props.report.sortOrder});

        this
            .props
            .actions
            .clearSearchParams();
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {
        const searchParams = this.state.searchParams;
        searchParams[name] = value;
        this.setState({searchParams: searchParams, status: ''});
    }

    onSearch()
    {
        const searchParams = this.state.searchParams;
        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (key) {
                controls[key].validateAllRules(key, searchParams[key]);
            });

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })
        if (numError == 0) {
            this
                .props
                .actions
                .reportDonorStatus(searchParams, 1, this.props.report.pageSize, this.props.report.sortBy, this.props.report.sortOrder, true);
        } else {
            $(".error-list").show();
        }

    }

    onShowMore() {
        if (this.props.report.items.length < this.props.report.totalRecords) {
            this
                .props
                .actions
                .reportDonorStatus(this.state.searchParams, ++this.props.report.page, this.props.report.pageSize, this.props.report.sortBy, this.props.report.sortOrder, false);
        }
    }

    onSortChange(sortName, sortOrder) {
        this
            .props
            .actions
            .reportDonorStatus(this.state.searchParams, 1, this.props.report.pageSize, sortName, sortOrder, true);
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('donor-status');
        document
            .body
            .classList
            .add('inner');

        document.title = 'Donor Status';
        setTimeout(this.jQInit(this), 200);
    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('donor-status');
        document
            .body
            .classList
            .remove('inner');
    }

    jQInit(form) {

        function remove_terms(resObj, value, grp) {
            $(resObj).remove(); // remove data in bar
            $('input[name=' + grp + '][value="' + value + '"]')
                .closest('label')
                .prop("checked", false)
                .removeClass('checked');

            form.onChange(grp, "");

        }

        $("input:radio")
            .click(function () {
                //// Get the radio button value
                var res = $(this).val();
                var grp = $(this).attr("data-group");
                // onclick below conternt display in bar addclass to selected criteria
                $('input[name="' + this.name + '"]')
                    .not(this)
                    .closest('label')
                    .removeClass('checked');
                $(this)
                    .closest('label')
                    .addClass('checked');

                var repid;
                // replace spl char form value
                if (!/^[0-9A-Za-z]+$/.test(res)) {
                    repid = res.replace(/[_\W]+/g, "");
                } else {
                    repid = res;
                }

                var content_before = '<span data-group="' + grp + '" data-value="' + res + '" id="rem_' + repid + '" class=" res-display">';
                var content_after = '</span>';
                var content = content_before + $(this).attr("data-label") + '<a  href="javascript:void(0)"  id="remove_term_' + repid + '">X</a>' + content_after;

                var elemExists = $('#disp_' + grp).has('#rem_' + repid);
                if (elemExists.length) {
                    $('#disp_' + grp)
                        .find('#rem_' + repid)
                        .remove();
                    $('input[name=' + grp + ']').prop("checked", false); // uncheck radio
                    $("input[data-group='" + grp + "']")
                        .closest('label')
                        .removeClass('checked');

                    form.onChange(grp, "");
                } else {
                    $('#disp_' + grp).html('');
                    $('#disp_' + grp).append(content);
                }

                $("#remove_term_" + repid)
                    .click(function (e) {
                        var rem = $(e.target).closest("span");
                        remove_terms($(rem), $(rem).attr("data-value"), $(rem).attr("data-group"));
                    });

            });

    }

    renderCategory(cell, row) {
        var category = row.category;
        var className = (category
            ? category.toLowerCase()
            : '') + "-clr";
        return (
            <div className={className}>
                <span></span>
                {category}
            </div>
        );
    }

    createExportCSVBtn = () => {
        return (
            <button
                onClick={this
                .onExportToCSV
                .bind(this)}
                type="button"
                className="btn btn-success react-bs-table-csv-btn  hidden-print">
                <span>
                    <i className="glyphicon glyphicon-export"></i>Export to CSV</span>
            </button>
        )
    }

    onExportToCSV() {
        let url = apiUtils.getReportDonorStatusUrl(this.state.searchParams, null, null, this.props.report.sortBy, this.props.report.sortOrder, true, true);
        window.open(url);
        return false;
    }

    renderSeachResult() {
        let options = {
            onSortChange: this
                .onSortChange
                .bind(this),
            exportCSVBtn: this
                .createExportCSVBtn
                .bind(this)
        };
        if (this.props.report.items && this.props.report.items.length) {
            let showMore = null;
            if (this.props.report.items.length < this.props.report.totalRecords) {
                showMore = <div className="text-right showmore">
                    <a
                        className="showmore_rows"
                        href="javascript:void(0)"
                        onClick={this.onShowMore}>Show More</a>
                </div>
            }

            return (
                <div className="clearfix" data-dropdown-for="search-btn">
                    <div className="searchResultsContainer">
                        <BootstrapTable
                            ref='table'
                            data={this.state.data}
                            options={options}
                            search
                            tableContainerClass="donortable"
                            exportCSV
                            csvFileName="donorStatusReport.csv"
                            bordered={false}>

                            <TableHeaderColumn dataField='id' isKey width='70px' dataSort>Id</TableHeaderColumn>
                            <TableHeaderColumn dataField='email' dataSort>Email</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='category'
                                dataFormat={this
                                .renderCategory
                                .bind(this)}
                                dataSort>Catagory</TableHeaderColumn>
                            <TableHeaderColumn dataField='status' width='70px' dataSort>Status</TableHeaderColumn>

                            <TableHeaderColumn
                                dataField='lastStatusUpdate'
                                width='82px'
                                dataFormat={displayDate}>Last status update</TableHeaderColumn>
                            <TableHeaderColumn dataField='location' dataSort>Location</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='createdAt'
                                dataFormat={displayDate}
                                width='82px'
                                dataSort>Sign-Up Date</TableHeaderColumn>
                            <TableHeaderColumn dataField='age' width='60px' dataSort>Age</TableHeaderColumn>
                            <TableHeaderColumn dataField='gender' width='60px' dataSort>Gender</TableHeaderColumn>
                            <TableHeaderColumn dataField='bloodType' width='60px' dataSort>Blood Type</TableHeaderColumn>
                            <TableHeaderColumn dataField='ethnicity' dataSort>Ethnicity
                            </TableHeaderColumn>
                            <TableHeaderColumn dataField='race' dataSort>Race</TableHeaderColumn>
                            <TableHeaderColumn dataField='hasHpv' width='60px' dataSort>HPV
                            </TableHeaderColumn>

                            <TableHeaderColumn
                                dataField='standardTestDate'
                                className='hidden'
                                columnClassName='hidden'>Standard Viral Test date</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='expandedTestDate'
                                className='hidden'
                                columnClassName='hidden'>Expanded Viral Test date</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='clinicalGradeTestDate'
                                className='hidden'
                                columnClassName='hidden'>Clinical Viral Test date</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='whereHear'
                                className='hidden'
                                columnClassName='hidden'>Where did you hear about us?</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='isOnCall'
                                className='hidden'
                                columnClassName='hidden'>On-Call Donor</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='isFlexible'
                                className='hidden'
                                columnClassName='hidden'>Flexible Donor</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='isClinicalGrade'
                                className='hidden'
                                columnClassName='hidden'>Clinical Grade Donor</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='isLowDemographic'
                                className='hidden'
                                columnClassName='hidden'>Low-Demographic Donor</TableHeaderColumn>

                            <TableHeaderColumn
                                dataField='wbEligibility'
                                className='hidden'
                                columnClassName='hidden'>Whole blood eligibility</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='wbAvailability'
                                className='hidden'
                                columnClassName='hidden'>Whole blood availability</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='wbDonationCount'
                                className='hidden'
                                columnClassName='hidden'>Number of WB donations</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='wbNextAvailableDate'
                                className='hidden'
                                columnClassName='hidden'>Next WB availablity date</TableHeaderColumn>

                            <TableHeaderColumn
                                dataField='bmEligibility'
                                className='hidden'
                                columnClassName='hidden'>Bone Marrow eligibility</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='bmAvailability'
                                className='hidden'
                                columnClassName='hidden'>Bone Marrow availability</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='bmDonationCount'
                                className='hidden'
                                columnClassName='hidden'>Number of Bone Marrow donations</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='bmNextAvailableDate'
                                columnClassName='hidden'
                                className='hidden'>Next BM availablity date</TableHeaderColumn>

                            <TableHeaderColumn
                                dataField='lpEligibility'
                                className='hidden'
                                columnClassName='hidden'>LeukoPak eligibility</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='lpAvailability'
                                className='hidden'
                                columnClassName='hidden'>LeukoPak availability</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='lpDonationCount'
                                className='hidden'
                                columnClassName='hidden'>Number of LeukoPak donations</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='lpNextAvailableDate'
                                className='hidden'
                                columnClassName='hidden'>Next LeukoPak availbility date</TableHeaderColumn>

                            <TableHeaderColumn
                                dataField='lpmEligibility'
                                className='hidden'
                                columnClassName='hidden'>Mobilized LeukoPak eligibility</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='lpmAvailability'
                                className='hidden'
                                columnClassName='hidden'>Mobilized LeukoPak availability</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='lpmDonationCount'
                                className='hidden'
                                columnClassName='hidden'>Number of Mob. LeukoPak donations</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='lpmNextAvailableDate'
                                className='hidden'
                                columnClassName='hidden'>Next Mob. LeukoPak availbility date</TableHeaderColumn>

                        </BootstrapTable>
                        <div className="clear"></div>
                        {showMore}
                    </div>
                </div>

            );
        }
    }

    render() {

        var errText = null;
        if (this.state.status == API_STATUS.ERROR) {

            $("#errorDil").modal('show');

        } else if (this.state.status == API_STATUS.DONE && this.props.report.items.length == 0) {
            errText = (
                <span
                    id="errorSpan"
                    style={{
                    color: 'red',
                    position: 'relative',
                    top: '15px',
                    marginRight: '15px'
                }}>No result found.</span>
            );
        }

        return (
            <div id="schedule-appointment" className="clearfix">

                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>
                            <a href="javascript:void(0)">
                                Reports</a>
                        </li>
                        <li className="active">Donor Status</li>
                    </ol>
                </div>

                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Donor Status</h2>
                    </div>

                </div>

                <div className="search-wrapper">

                    <div className="col-xs-24 col-sm-24 col-md-24 col-lg-24">
                        <p className="results">
                            <span id="disp_collectionSite"></span>
                            <span id="disp_category"></span>
                            <span id="disp_status"></span>
                        </p>

                        <div className="reset-form pull-right">
                            <img src="/img/icons/searchdonaricons/reset.png" alt="reset"/>
                            <a
                                href="javascript:void(0)"
                                onClick={this
                                .clearfields
                                .bind(this)}>Reset</a>
                            <div className="clear"></div>

                        </div>
                        <hr/>

                    </div>
                    <div
                        name="search-donar-form"
                        id="search-donar-form"
                        className="search-donar-form">
                        <ErrorArea errorList={this.state.errors}/>
                        <div className="col-xs-24 col-sm-24 col-md-24 col-lg-24 collapse-sm">
                            <InputControl
                                containerClass="col-xs-24 col-sm-6  form-group"
                                type="date"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["fromDate"] = input
                            }}
                                name="fromDate"
                                label="From Date"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="10"
                                validate={validations.date}
                                errorMessage="The &#39;From Date&#39; does not seem to be valid. Please check and enter again."
                                value={this.state.searchParams.fromDate}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-6  form-group"
                                type="date"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["toDate"] = input
                            }}
                                name="toDate"
                                label="To Date"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="10"
                                validate={validations.date}
                                errorMessage="The &#39;To Date&#39; does not seem to be valid. Please check and enter again."
                                value={this.state.searchParams.toDate}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>
                        </div>

                        <InputControl
                            containerClass="col-xs-24 col-sm-24 col-md-7 col-lg-7 form-group"
                            type="searchRadio"
                            name="collectionSite"
                            label="Location"
                            labelClass="ucase"
                            inputClass="form-control"
                            value={this.state.searchParams.collectionSite}
                            optionValues={[
                            [
                                "California", "CA"
                            ],
                            ["Massachusetts", "MA"]
                        ]}
                            ref
                            ={(input) => {
                            this.inputControls["collectionSite"] = input
                        }}
                            onChange={this.onChange}
                            onInvalid={this.onInvalid}
                            onValid={this.onValid}/>

                        <InputControl
                            containerClass="col-xs-24 col-sm-24 col-md-15 col-lg-15 form-group"
                            type="searchRadio"
                            name="category"
                            label="Category"
                            labelClass="ucase"
                            inputClass="form-control"
                            value={this.state.searchParams.category}
                            optionValues={[
                            [
                                "Prospect", "prospect"
                            ],
                            [
                                "New", "new"
                            ],
                            [
                                "Active", "active"
                            ],
                            [
                                "Lapsed", "lapsed"
                            ],
                            [
                                "Inactive", "inactive"
                            ],
                            ["Disqualified", "disqualified"]
                        ]}
                            ref
                            ={(input) => {
                            this.inputControls["category"] = input
                        }}
                            onChange={this.onChange}
                            onInvalid={this.onInvalid}
                            onValid={this.onValid}/>

                        <InputControl
                            containerClass="col-xs-24 col-sm-24 col-md-9 col-lg-9 form-group"
                            type="searchRadio"
                            name="status"
                            label="Status"
                            labelClass="ucase"
                            inputClass="form-control"
                            value={this.state.searchParams.status}
                            optionValues={[
                            [
                                "Open", "open"
                            ],
                            [
                                "Reserved", "reserved"
                            ],
                            ["Scheduled", "scheduled"]
                        ]}
                            ref
                            ={(input) => {
                            this.inputControls["status"] = input
                        }}
                            onChange={this.onChange}
                            onInvalid={this.onInvalid}
                            onValid={this.onValid}/>

                        <div className="clear"></div>

                        <div className="col-sm-24 form-group">

                            <button className="btn btn-default" onClick={this.onSearch}>Search</button>
                        </div>

                        {errText}

                        {this.state.data.length
                            ? (<hr/>)
                            : null}

                        <div className="clear"></div>

                        {this.renderSeachResult()}

                    </div>
                </div>
            </div>

        );
    }
}

export default DonorStatusReportForm;