import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import * as validations from '../../validations/common'
import {API_STATUS, PATH_PREFIX} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import {hashHistory, Router} from 'react-router';
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import * as apiUtils from '../../utils/apiUtils';
import {displayDate} from '../../utils/date.js';

class PaymentReportForm extends Component {
    constructor(props) {
        super(props);

        this.items = this.props.report.items;

        this.state = {
            searchParams: {},
            errors: {},
            data: this.items,
            sortName: this.props.report.sortBy,
            sortOrder: this.props.report.sortOrder,
            status: ''
        };

        this.onShowMore = this
            .onShowMore
            .bind(this);

        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);

        this.onChange = this
            .onChange
            .bind(this);

        this.onSearch = this
            .onSearch
            .bind(this);

        this.inputControls = {};
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.report.actionType == "CHANG_ROUTE") {
            this.clearfields();
        } else {
            this.items = nextProps
                .report
                .items
                .map(function (x) {
                    return jQuery.extend(true, {}, x, {
                        procedure: x.procedureInfo.name,
                        donorId: x.donor.id,
                        donorName: x.donor.firstName + ' ' + x.donor.lastName
                    })
                });
            this.setState({data: this.items, errors: {}, sortName: this.props.report.sortBy, sortOrder: this.props.report.sortOrder, status: nextProps.report.status});
        }
    }

    clearfields() {
        $('#disp_collectionSite, #disp_category, #disp_status').html('');

        $('input').each(function () {
            $(this)
                .closest('label')
                .removeClass('checked');

        });

        this.setState({searchParams: {}, errors: {}, sortName: this.props.report.sortBy, sortOrder: this.props.report.sortOrder});

        this
            .props
            .actions
            .clearSearchParams();
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {
        const searchParams = this.state.searchParams;
        searchParams[name] = value;
        this.setState({searchParams: searchParams, status: ''});
    }

    onSearch() {
        const searchParams = this.state.searchParams;
        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (key) {
                controls[key].validateAllRules(key, searchParams[key]);
            });

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })
        if (numError == 0) {
            this
                .props
                .actions
                .reportPayment(searchParams, 1, this.props.report.pageSize, this.props.report.sortBy, this.props.report.sortOrder, true);
        } else {
            $(".error-list").show();
        }
    }

    onShowMore() {
        if (this.props.report.items.length < this.props.report.totalRecords) {
            this
                .props
                .actions
                .reportPayment(this.state.searchParams, ++this.props.report.page, this.props.report.pageSize, this.props.report.sortBy, this.props.report.sortOrder, false);
        }
    }

    onSortChange(sortName, sortOrder) {
        this
            .props
            .actions
            .reportPayment(this.state.searchParams, 1, this.props.report.pageSize, sortName, sortOrder, true);
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('payments');
        document
            .body
            .classList
            .add('inner');

        document.title = 'Payments';
        setTimeout(this.jQInit(this), 200);

        var self = this;
        $(".search-wrapper.payments input").keypress(function (e) {
            if (e.keyCode == 13) {
                self.onSearch();
            }
        })
    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('payments');
        document
            .body
            .classList
            .remove('inner');
    }

    jQInit(form) {

        function remove_terms(resObj, value, grp) {
            $(resObj).remove(); // remove data in bar
            $('input[name=' + grp + '][value="' + value + '"]')
                .closest('label')
                .prop("checked", false)
                .removeClass('checked');

            form.onChange(grp, "");
        }

        $("input:radio")
            .click(function () {
                //// Get the radio button value
                var res = $(this).val();
                var grp = $(this).attr("data-group");
                // onclick below conternt display in bar addclass to selected criteria
                $('input[name="' + this.name + '"]')
                    .not(this)
                    .closest('label')
                    .removeClass('checked');
                $(this)
                    .closest('label')
                    .addClass('checked');

                var repid;
                // replace spl char form value
                if (!/^[0-9A-Za-z]+$/.test(res)) {
                    repid = res.replace(/[_\W]+/g, "");
                } else {
                    repid = res;
                }

                var content_before = '<span data-group="' + grp + '" data-value="' + res + '" id="rem_' + repid + '" class=" res-display">';
                var content_after = '</span>';
                var content = content_before + $(this).attr("data-label") + '<a  href="javascript:void(0)"  id="remove_term_' + repid + '">X</a>' + content_after;

                var elemExists = $('#disp_' + grp).has('#rem_' + repid);
                if (elemExists.length) {
                    $('#disp_' + grp)
                        .find('#rem_' + repid)
                        .remove();
                    $('input[name=' + grp + ']').prop("checked", false); // uncheck radio

                    $("input[data-group='" + grp + "']")
                        .closest('label')
                        .removeClass('checked');

                    form.onChange(grp, "");
                } else {
                    $('#disp_' + grp).html('');
                    $('#disp_' + grp).append(content);
                }

                $("#remove_term_" + repid)
                    .click(function (e) {
                        var rem = $(e.target).closest("span");
                        remove_terms($(rem), $(rem).attr("data-value"), $(rem).attr("data-group"));
                    });
            });

    }

    createExportCSVBtn = () => {
        return (
            <button
                onClick={this
                .onExportToCSV
                .bind(this)}
                type="button"
                className="btn btn-success react-bs-table-csv-btn  hidden-print">
                <span>
                    <i className="glyphicon glyphicon-export"></i>Export to CSV</span>
            </button>
        )
    }

    onExportToCSV() {
        let url = apiUtils.getReportPaymentUrl(this.state.searchParams, null, null, this.props.report.sortBy, this.props.report.sortOrder, true, true);
        window.open(url);
        return false;
    }

    renderSeachResult() {
        let options = {
            onSortChange: this
                .onSortChange
                .bind(this),
            exportCSVBtn: this
                .createExportCSVBtn
                .bind(this)
        };

        if (this.props.report.items && this.props.report.items.length) {
            let showMore = null;
            if (this.props.report.items.length < this.props.report.totalRecords) {
                showMore = <div className="text-right showmore">
                    <a
                        className="showmore_rows"
                        href="javascript:void(0)"
                        onClick={this.onShowMore}>Show More</a>
                </div>
            }
            return (
                <div className="clearfix" data-dropdown-for="search-btn">
                    <div className="searchResultsContainer">
                        <BootstrapTable
                            ref='table'
                            data={this.state.data}
                            options={options}
                            search
                            tableContainerClass="donortable"
                            exportCSV
                            csvFileName="paymentReport.csv"
                            bordered={false}>

                            <TableHeaderColumn dataField='id' isKey dataSort>Transaction Id</TableHeaderColumn>
                            <TableHeaderColumn dataField='refNo' dataSort>Reference Number</TableHeaderColumn>
                            <TableHeaderColumn dataField='workOrder' dataSort>Work Order</TableHeaderColumn>
                            <TableHeaderColumn dataField='donorId' dataSort>Donor Id</TableHeaderColumn>
                            <TableHeaderColumn dataField='donorName' dataSort>Donor Name</TableHeaderColumn>
                            <TableHeaderColumn dataField='procedure' dataSort>Procedure</TableHeaderColumn>
                            <TableHeaderColumn dataField='amount' dataSort>Amount($)</TableHeaderColumn>
                            <TableHeaderColumn dataField='createdAt' dataFormat={displayDate}  dataSort>Transaction Date</TableHeaderColumn>

                        </BootstrapTable>
                        <div className="clear"></div>
                        {showMore}
                    </div>
                </div>

            );
        }
    }

    render() {

        var errText = null;
        if (this.state.status == API_STATUS.ERROR) {

            $("#errorDil").modal('show');

        } else if (this.state.status == API_STATUS.DONE && this.props.report.items.length == 0) {
            errText = (
                <span
                    id="errorSpan"
                    style={{
                    color: 'red',
                    position: 'relative',
                    top: '15px',
                    marginRight: '15px'
                }}>No result found.</span>
            );
        }

        return (
            <div id="schedule-appointment" className="clearfix">

                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>
                            <a href="javascript:void(0)">
                                Reports</a>
                        </li>
                        <li className="active">Payments</li>
                    </ol>
                </div>

                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Payments</h2>
                    </div>

                </div>

                <div className="search-wrapper payments">

                    <div className="col-xs-24 col-sm-24 col-md-24 col-lg-24">
                        <p className="results">
                            <span id="disp_collectionSite"></span>
                            <span id="disp_category"></span>
                            <span id="disp_cancellationReason"></span>
                        </p>

                        <div className="reset-form pull-right">
                            <img src="/img/icons/searchdonaricons/reset.png" alt="reset"/>
                            <a
                                href="javascript:void(0)"
                                onClick={this
                                .clearfields
                                .bind(this)}>
                                Reset</a>
                            <div className="clear"></div>

                        </div>
                        <hr/>

                    </div>
                    <div
                        name="search-donar-form"
                        id="search-donar-form"
                        className="search-donar-form">
                        <ErrorArea errorList={this.state.errors}/>
                        <div className="col-xs-24 col-sm-24 col-md-24 col-lg-24 collapse-sm">
                            <InputControl
                                containerClass="col-xs-24 col-sm-6  form-group"
                                type="date"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["fromDate"] = input
                            }}
                                name="fromDate"
                                label="From Date"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="10"
                                validate={validations.date}
                                errorMessage="The &#39;From Date&#39; does not seem to be valid. Please check and enter again."
                                value={this.state.searchParams.fromDate}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-6  form-group"
                                type="date"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["toDate"] = input
                            }}
                                name="toDate"
                                label="To Date"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="10"
                                validate={validations.date}
                                errorMessage="The &#39;To Date&#39; does not seem to be valid. Please check and enter again."
                                value={this.state.searchParams.toDate}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                        </div>

                        <InputControl
                            containerClass="col-xs-24 col-sm-6 form-group"
                            ref
                            ={(input) => {
                            this.inputControls["donorId"] = input
                        }}
                            searchControl={true}
                            type="text"
                            name="donorId"
                            label="Donor Id"
                            labelClass="ucase"
                            inputClass="form-control"
                            maxLength="10"
                            placeholder="Donor Id"
                            validate={validations.number}
                            errorMessage="The &#39;Work Order&#39; should be number. Please check the Work Order and enter again."
                            value={this.state.searchParams.donorId}
                            onChange={this.onChange}
                            onInvalid={this.onInvalid}
                            onValid={this.onValid}/>

                        <InputControl
                            containerClass="col-xs-24  col-sm-6 form-group"
                            ref
                            ={(input) => {
                            this.inputControls["woNumber"] = input
                        }}
                            searchControl={true}
                            type="text"
                            name="woNumber"
                            label="Work order #"
                            labelClass="ucase"
                            inputClass="form-control"
                            maxLength="10"
                            placeholder="Work order #"
                            validate={validations.number}
                            errorMessage="The &#39;Work Order&#39; should be number. Please check the Work Order and enter again."
                            value={this.state.searchParams.woNumber}
                            onChange={this.onChange}
                            onInvalid={this.onInvalid}
                            onValid={this.onValid}/>

                        <InputControl
                            containerClass="col-xs-24 col-sm-6 form-group"
                            ref
                            ={(input) => {
                            this.inputControls["refNo"] = input
                        }}
                            searchControl={true}
                            type="text"
                            name="refNo"
                            label="Reference Number"
                            labelClass="ucase"
                            inputClass="form-control"
                            placeholder="Reference Number"
                            maxLength="20"
                            value={this.state.searchParams.refNo}
                            onChange={this.onChange}
                            onInvalid={this.onInvalid}
                            onValid={this.onValid}/>

                        <div className="clear"></div>

                        <div className="col-sm-24 form-group">

                            <button className="btn btn-default" onClick={this.onSearch}>Search</button>
                        </div>

                        {errText}

                        {this.state.data.length
                            ? (<hr/>)
                            : null}

                        <div className="clear"></div>

                        {this.renderSeachResult()}

                    </div>
                </div>
            </div>

        );
    }
}

export default PaymentReportForm;