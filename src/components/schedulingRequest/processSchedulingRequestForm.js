import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import * as validations from '../../validations/common'
import {API_STATUS, PATH_PREFIX} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import {hashHistory, Router} from 'react-router';
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import {localDateToUTCString, displayDate, displayDate2, displayTime, displayTime2} from '../../utils/date.js';

class ProcessSchedulingRequestForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: jQuery.extend(true, {}, this.props.schedulingRequest.data),
            errors: {},
            processing: false,
            allMedicalProcedures: [],
            processScheduleRequest: [],
            paramsEditted: false
        }

        this.onShowMore = this
            .onShowMore
            .bind(this);

        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);

        this.onChange = this
            .onChange
            .bind(this);

        this.onShowFilter = this
            .onShowFilter
            .bind(this);
        this.onSearch = this
            .onSearch
            .bind(this);

        this.inputControls = {};

        this.procedures = [];
        this.boneMarrowVolumms = [];
        this.leukoparkVolumns = [];
    }

    buildProcedureOptions(allMedicalProcedures) {
        this.procedures = [];
        this.boneMarrowVolumms = [];
        this.leukoparkVolumns = [];
        var self = this;
        var wholeBlood = false;
        var leukopark = false;
        var boneMarrow = false;
        allMedicalProcedures.forEach(function (element) {
            if (element.category == "Whole Blood" && !wholeBlood) {
                self
                    .procedures
                    .push([
                        "Whole Blood", "Whole Blood|" + element.id
                    ]);
                wholeBlood = true;
            } else if (element.category == "Bone Marrow") {
                if (!boneMarrow) {
                    self
                        .procedures
                        .push(["Bone Marrow", "Bone Marrow"]);
                    boneMarrow = true;
                    self
                        .boneMarrowVolumms
                        .push([
                            element.volumeUnit + " ml",
                            element.volumeUnit + "|" + element.id
                        ]);
                } else {
                    self
                        .boneMarrowVolumms
                        .push([
                            element.volumeUnit + " ml",
                            element.volumeUnit + "|" + element.id
                        ]);
                }

            } else if (element.category == "LeukoPak") {
                if (!leukopark) {
                    self
                        .procedures
                        .push(["LeukoPak", "LeukoPak"]);
                    leukopark = true;
                    self
                        .leukoparkVolumns
                        .push([
                            element.volumeUnit, element.volumeUnit + "|" + element.id
                        ]);
                } else {
                    self
                        .leukoparkVolumns
                        .push([
                            element.volumeUnit, element.volumeUnit + "|" + element.id
                        ]);
                }
            } else {
                self
                    .procedures
                    .push([element.name, element.id]);
            }
        });
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.processScheduleRequest.actionType == actionTypes.SAVE_OUT_REACH && nextProps.processScheduleRequest.status == API_STATUS.DONE && nextProps.processScheduleRequest.needRefresh) {
            this.onSearch();
            return;
        }

        if (!this.didMountCalling && this.props.needReloadAllProcedures) {
            this
                .props
                .loadAllMedicalProcedures();
        }
        this.didMountCalling = false;

        if (nextProps.allMedicalProcedures && nextProps.allMedicalProcedures.length) {
            this.setState({allMedicalProcedures: nextProps.allMedicalProcedures});
            this.buildProcedureOptions(nextProps.allMedicalProcedures);
        }

        if (nextProps.schedulingRequest.actionType == actionTypes.CHANG_ROUTE && nextProps.routeParams.id) { //need to check if current route is process request (not edit)

            var id = this.props.routeParams.id;
            if (id) {
                this
                    .props
                    .scheduleActions
                    .getSchedulingRequest(id);

            }
        }

        if (nextProps.schedulingRequest.actionType != actionTypes.GET_SCHEDULING_REQUEST && nextProps.schedulingRequest.actionType != actionTypes.CLEAR_SCHEDULING_REQUEST && nextProps.schedulingRequest.actionType != actionTypes.CHANG_ROUTE) {
            return;
        }

        if (nextProps.processScheduleRequest.actionType == actionTypes.CLEAR_MATCH_DONORS_PARAMS) {

            this.setState({
                data: {}, //reset all schedule request data
                errors: {},
                processing: false,
                processScheduleRequest: nextProps.processScheduleRequest
            });
            return;
        }

        if (nextProps.processScheduleRequest.hideFilterIfResultsFound || nextProps.processScheduleRequest.actionType == actionTypes.MATCH_DONORS || nextProps.processScheduleRequest.actionType == actionTypes.MATCH_DONORS_RELOAD) {
            if (nextProps.processScheduleRequest.hideFilterIfResultsFound) {
                $("#edit_search_filters").removeClass("dropdown-active");
            } else {
                $("#edit_search_filters").addClass("dropdown-active");
                $('html, body').animate({
                    scrollTop: $(document).height() - $(window).height()
                }, 700);
            }
            if (nextProps.processScheduleRequest.status == API_STATUS.DONE) {
                this.setState({errors: {}, processing: false, processScheduleRequest: nextProps.processScheduleRequest});
            }
        } else if (!this.state.paramsEditted) {
            if (nextProps.schedulingRequest.status == API_STATUS.DONE && nextProps.schedulingRequest.actionType == actionTypes.GET_SCHEDULING_REQUEST || nextProps.schedulingRequest.actionType == actionTypes.CHANG_ROUTE) {

                let collectionDateOnly = displayDate2(nextProps.schedulingRequest.data.internalCollectionTime, nextProps.schedulingRequest.data.collectionDate, nextProps.schedulingRequest.data.collectionByTime);

                let collectionTime = displayTime2(nextProps.schedulingRequest.data.internalCollectionTime, nextProps.schedulingRequest.data.collectionDate, nextProps.schedulingRequest.data.collectionByTime);

                this.state = {
                    data: jQuery.extend(true, {
                        collectionDateOnly: collectionDateOnly,
                        collectionTime: collectionTime
                    }, nextProps.schedulingRequest.data),
                    errors: {},
                    processing: false
                }

                if (this.state.data && this.state.data.includeAllergy) {
                    this.state.data.includeAllergy = this
                        .state
                        .data
                        .includeAllergy
                        .split("|");
                }

                if (this.state.data && this.state.data.includeAllergy && this.state.data.includeAllergy.length > 0) {
                    $(".allergy-note").show();
                } else {
                    $(".allergy-note").hide();
                }

                if (this.state.data.procedureInfo && this.state.data.procedureInfo.category) {
                    var procedure = this.state.data.procedureInfo;
                    if (procedure.category == "Whole Blood") {
                        this.state.data.procedureInfo = "Whole Blood|" + procedure.id;
                        this.state.data.volume = this.state.data.volume;
                    } else if (procedure.category == "Bone Marrow") {
                        this.state.data.procedureInfo = "Bone Marrow";
                        this.state.data.volume = procedure.volumeUnit + "|" + procedure.id;
                    } else if (procedure.category == "LeukoPak") {
                        this.state.data.procedureInfo = "LeukoPak";
                        this.state.data.volume = procedure.volumeUnit + "|" + procedure.id;
                    } else {
                        this.state.data.procedureInfo = procedure.id;
                        this.state.data.volume = '';
                    }
                    this.state.data.procedureName = this.state.data.procedureInfo.name;
                }

                if (this.state.data.race) {
                    this.state.data.race = this
                        .state
                        .data
                        .race
                        .split("|");
                }

                if (this.state.data.bloodType) {
                    this.state.data.bloodType = this
                        .state
                        .data
                        .bloodType
                        .split("|");
                }

                if (this.state.data.excludeMedication) {
                    this.state.data.excludeMedication = this
                        .state
                        .data
                        .excludeMedication
                        .split("|");
                }

                if (this.state.data && this.state.data.excludeMedication && this.state.data.excludeMedication.length > 0) {
                    $(".med-show").show();
                } else {
                    $(".med-show").hide();
                }

                if (this.state.data && this.state.data.additionalTesting) {
                    $(".type-add-test").show();
                } else {
                    $(".type-add-test").hide();
                }

                if (this.state.data && this.state.data.futureProcedureAvailability) {
                    if (this.state.data.futureProcedureAvailability == "Whole Blood") 
                        this.state.data.futureWbVolume = this.state.data.futureProcedureVolume;
                    else if (this.state.data.futureProcedureAvailability == "Bone Marrow") 
                        this.state.data.futureBoneMarrowVolume = this.state.data.futureProcedureVolume;
                    else if (this.state.data.futureProcedureAvailability == "LeukoPak") 
                        this.state.data.futureFullHalfPakVolume = this.state.data.futureProcedureVolume;
                    else if (this.state.data.futureProcedureAvailability == "Mobilized LeukoPak") 
                        this.state.data.futureFullPakVolume = this.state.data.futureProcedureVolume;
                    }
                
                if (this.state.data.hla) {
                    var hla = this.state.data.hla;
                    this.state.data.hlaAlleleOption = hla.alleleMatchOption;
                    this.state.data.hlaGeneTypes = [];
                    var self = this;
                    hla
                        .geneTypes
                        .forEach(function (gen) {
                            self
                                .state
                                .data
                                .hlaGeneTypes
                                .push(gen.geneType);
                        });
                }

                this.setState(this.state);

                if ((nextProps.schedulingRequest.actionType == actionTypes.GET_SCHEDULING_REQUEST && nextProps.schedulingRequest.status == API_STATUS.DONE)) {
                    $("#collectionTime")
                        .timepicker("setTime", displayTime2(nextProps.schedulingRequest.data.internalCollectionTime, nextProps.schedulingRequest.data.collectionDate, nextProps.schedulingRequest.data.collectionByTime))
                        .timepicker("update");
                }

            }
        } else if (nextProps.schedulingRequest.actionType == "CHANG_ROUTE" || nextProps.processScheduleRequest.actionType == "CHANG_ROUTE") {
            this.clearfields();
        }

    }

    clearfields() {
        setTimeout(function () {
            $("#collectionTime")
                .timepicker("setTime", '')
                .timepicker("update");
        }, 10);

        $('#disp_collectionSite, #disp_category,#disp_gender, #disp_status,#disp_procedureI' +
                'nfo,#disp_ethnicity,#disp_race,#disp_bloodType,#disp_smoker,#disp_clinicalGrade,' +
                '#disp_excludeMedication,#disp_includeAllergy,#disp_futureProcedureAvailability,#' +
                'disp_cmv,#disp_backupDonorRequired,#disp_additionalTesting,#additionalTestingTyp' +
                'e,#disp_hlaGeneTypes,#disp_hlaAlleleOption').html('');

        $('input').each(function () {
            $(this)
                .closest('label')
                .removeClass('checked');

        });

        $("#gene_type_alle1")
            .children()
            .remove();

        this
            .props
            .processScheduleActions
            .clearProcessRequestParams();

        // this.setState({searchParams: {}, errors: {}, sortName:
        // this.props.searchDonor.sortBy, sortOrder: this.props.searchDonor.sortOrder});

        $("#edit_search_filters").addClass("dropdown-active");
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {

        this.state.paramsEditted = true;
        const data = this.state.data;

        if (name == "excludeMedication" || name == "includeAllergy" || name == "hlaGeneTypes" || name == "race" || name == "bloodType") {
            if (name == "hlaGeneTypes") {
                var value1 = value.substring(1);
                if (value[0] == "+") {
                    this.fillHlaSpecific(value1);
                } else {
                    $("#hla" + value1).remove();
                }
            }

            var newArray = jQuery.extend(true, [], data[name] || []);
            if (value.startsWith("+")) 
                newArray.push(value.substring(1));
            else if (value.startsWith("-")) {
                var i = newArray.indexOf(value.substring(1));
                if (i >= 0) 
                    newArray.splice(i, 1);
                }
            value = newArray;

            if (name == "excludeMedication") {
                value.length > 0
                    ? $(".med-show").show()
                    : $(".med-show").hide();
            } else if (name == "includeAllergy") {
                value.length > 0
                    ? $(".allergy-note").show()
                    : $(".allergy-note").hide();
            }

            if (name == "hlaGeneTypes" && value.length > 0 && !$('[name=hlaAlleleOption]:checked').val()) {
                $('[name=hlaAlleleOption][value="either"]')
                    .closest('label')
                    .click()
                    .addClass('checked');
                if ($('[name=hlaAlleleOption][value="either"]').length) {
                    this.radioButtonClick(($('[name=hlaAlleleOption][value="either"]')[0]), this);
                }
            }

        } else if (name == "additionalTesting") {
            if ($("[name=additionalTesting]:checked").length > 0) {
                $(".type-add-test").show();
            } else {
                $(".type-add-test").hide();
            }
        } else if (name == "procedureInfo") {
            if (typeof(value) === "string" && value.startsWith("Whole Blood")) {
                $("#whole").show();
                $("#bonemarrow,#lukopack").hide();
                data.volume = null;
            } else if (typeof(value) === "string" && value.startsWith("Bone Marrow")) {
                $("#bonemarrow").show();
                $("#whole,#lukopack").hide();
                if ($("#bonemarrow input").length > 0 && $("#bonemarrow input:checked").length == 0) {
                    $($("#bonemarrow input")[0])
                        .closest("label")
                        .click()
                        .addClass("checked");
                }
            } else if (typeof(value) === "string" && value.startsWith("LeukoPak")) {
                $("#lukopack").show();
                $("#bonemarrow,#whole").hide();
                if ($("#lukopack input").length > 0 && $("#lukopack input:checked").length == 0) {
                    $($("#lukopack input")[0])
                        .closest("label")
                        .click()
                        .addClass("checked");
                }
            } else {
                $("#whole,#bonemarrow, #lukopack").hide();
                data.volume = null;
            }
        } else if (name == "futureProcedureAvailability") {
            if (value == "Whole Blood") {
                $("#fwhole").show();
                $("#other,#fbonemarrow,#flukopack").hide();
            } else if (value == "Bone Marrow") {
                $("#fbonemarrow").show();
                $("#other,#fwhole,#flukopack").hide();
            } else if (value == "LeukoPak") {
                $("#flukopack").show();
                $("#other,#fbonemarrow,#fwhole").hide();
            } else {
                $("#other").show();
                $("#fwhole,#fbonemarrow, #flukopack").hide();
            }
        } else if (name == "hlaAlleleOption") {
            if (value == "both") {
                $(".type_alle2").show();
                $("h4.alleTitle").each(function () {
                    var t = $(this).text();
                    $(this).text(t + " 1");
                });
            } else {
                $(".type_alle2").hide();
                $("h4.alleTitle").each(function () {
                    var t = $(this).text();
                    if (t.endsWith("1")) 
                        $(this).text(t.substring(0, t.length - 2));
                    }
                );
            }
        }

        if (value == 'y') 
            value = true;
        else if (value == 'n') 
            value = false;
        
        data[name] = value;
        this.setState({data: data});

    }

    fillHlaSpecific(hlaGeneValue, dontCreateSelect2) {
        if ($("#hla" + hlaGeneValue).length > 0 || this.fillingHla) 
            return;
        
        this.fillingHla = true;

        this.fillingHla = true;
        var v = $('[name=hlaAlleleOption]:checked').val();

        var alleNumber1 = "1";
        if (!v || v == "either" || v == "homozygous") {
            var alleNumber1 = "";
        }

        var options = '<div class="geneTypeContainer" id=hla' + hlaGeneValue + '><div class="col-sm-24"><h4 class="alleTitle">GENE TYPE ' + hlaGeneValue + ', ALLELE ' + alleNumber1 + '</h4></div><div id="gene_typea" class="alle1" data-gene=' + hlaGeneValue + '><div class="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm ' +
                'form-group"><select class="form-control cityselect" id="PredicateAA"><option val' +
                'ue="predicate">Predicate</option><option value="ignore">Ignore</option><option v' +
                'alue="equal">Equal</option><option value="not_equal">Not Equal</option></select>' +
                '</div><div class="col-sm-6 collapse-sm alle-type form-group"><input type="text" ' +
                'placeholder="AA" class="form-control"/></div></div><div class="col-sm-6 form-gro' +
                'up alle-select"><div class="col-sm-18 collapse-sm form-group"><select class="for' +
                'm-control cityselect" id="PredicateBB"><option value="predicate">Predicate</opti' +
                'on><option value="ignore">Ignore</option><option value="equal">Equal</option><op' +
                'tion value="not_equal">Not Equal</option></select></div><div class="col-sm-6 col' +
                'lapse-sm alle-type form-group"><input type="text" placeholder="BB" class="form-c' +
                'ontrol"/></div></div><div class="col-sm-6 form-group alle-select"><div class="co' +
                'l-sm-18 collapse-sm form-group"><select class="form-control cityselect" id="Pred' +
                'icateCC"><option value="predicate">Predicate</option><option value="ignore">Igno' +
                're</option><option value="equal">Equal</option><option value="not_equal">Not Equ' +
                'al</option></select></div><div class="col-sm-6 collapse-sm alle-type form-group"' +
                '><input type="text" placeholder="CC" class="form-control"/></div></div><div clas' +
                's="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm form-group' +
                '"><select class="form-control cityselect" id="PredicateDD"><option value="predic' +
                'ate">Predicate</option><option value="ignore">Ignore</option><option value="equa' +
                'l">Equal</option><option value="not_equal">Not Equal</option></select></div><div' +
                ' class="col-sm-6 collapse-sm alle-type form-group"><input type="text" placeholde' +
                'r="DD" class="form-control"/></div></div></div><div class="col-sm-24 collapse-sm' +
                ' type_alle2" id="gene_type_alle2"><div class="col-sm-24"><h4>GENE TYPE ' + hlaGeneValue + ', ALLELE 2</h4></div><div id="gene_typea" class="alle2" data-gene=' + hlaGeneValue + '><div class="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm ' +
                'form-group"><select class="form-control cityselect " id="PredicateAA"><option  v' +
                'alue="predicate">Predicate</option><option value="ignore">Ignore</option><option' +
                ' value="equal">Equal</option><option value="not_equal">Not Equal</option></selec' +
                't></div><div class="col-sm-6 collapse-sm alle-type form-group"><input type="text' +
                '" placeholder="AA" class="form-control"/></div></div><div class="col-sm-6 form-g' +
                'roup alle-select"><div class="col-sm-18 collapse-sm form-group"><select class="f' +
                'orm-control cityselect" id="PredicateBB"><option value="predicate">Predicate</op' +
                'tion><option value="ignore">Ignore</option><option value="equal">Equal</option><' +
                'option value="not_equal">Not Equal</option></select></div><div class="col-sm-6 c' +
                'ollapse-sm alle-type form-group"><input type="text" placeholder="BB" class="form' +
                '-control"/></div></div><div class="col-sm-6 form-group alle-select"><div class="' +
                'col-sm-18 form-group collapse-sm"><select class="form-control cityselect" id="Pr' +
                'edicateCC"><option value="predicate">Predicate</option><option value="ignore">Ig' +
                'nore</option><option value="equal">Equal</option><option value="not_equal">Not E' +
                'qual</option></select></div><div class="col-sm-6 collapse-sm alle-type form-grou' +
                'p"><input type="text" placeholder="CC" class="form-control"/></div></div><div cl' +
                'ass="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm form-gro' +
                'up"><select class="form-control cityselect" id="PredicateDD"><option value="pred' +
                'icate">Predicate</option><option value="ignore">Ignore</option><option value="eq' +
                'ual">Equal</option><option value="not_equal">Not Equal</option></select></div><d' +
                'iv class="col-sm-6 collapse-sm alle-type form-group"><input type="text" placehol' +
                'der="DD" class="form-control"/></div></div></div></div></div>';

        $('#gene_type_alle1').append(options);
        if (v == "both") {
            $(".type_alle2").show();
        } else {
            $(".type_alle2").hide();
        }

        if (!dontCreateSelect2) {
            $("#gene_type_alle1 .cityselect").select2({minimumResultsForSearch: Infinity});
        }

        this.fillingHla = false;

    }

    onShowFilter() {
        if ($('.advanced_search').css('display') == 'none') {
            $('.advanced_search').show();
            $('#filtertext').html('<div class="hide-text">Hide Advanced Filters</div>');
        } else {
            $('.advanced_search').hide();
            $('#filtertext').html('<div class="hide-text">Show Advanced Filters</div>');

        }
    }

    marshalDataBeforeSearch(data) {
        data.isMatchDonor = true;

        data.collectionDate = data.collectionDateOnly;
        if (data.procedureInfo) {
            if (typeof(data.procedureInfo) === "string" && data.procedureInfo.startsWith("Whole Blood")) {
                data.procedureInfo = {
                    id: data
                        .procedureInfo
                        .split("|")[1]
                };
            } else if (typeof(data.procedureInfo) === "string" && data.procedureInfo.startsWith("Bone Marrow")) {
                var vo = data.volume;
                if (vo) {
                    data.volume = vo.split("|")[0];
                    data.procedureInfo = {
                        id: vo.split("|")[1]
                    };
                }
            } else if (typeof(data.procedureInfo) === "string" && data.procedureInfo.startsWith("LeukoPak")) {
                var vo = data.volume;
                if (vo) {
                    data.volume = vo.split("|")[0];
                    data.procedureInfo = {
                        id: vo.split("|")[1]
                    };
                }
            } else {
                data.procedureInfo = {
                    id: data.procedureInfo
                };
            }
        }

        if (data.race && data.race.constructor === Array) {
            data.race = data
                .race
                .join("|");

        }

        if (data.bloodType && data.bloodType.constructor === Array) {
            data.bloodType = data
                .bloodType
                .join("|");
        }

        if (data.excludeMedication && data.excludeMedication.constructor === Array) {
            data.excludeMedication = data
                .excludeMedication
                .join("|");
        }

        if (data.ageStart || data.ageEnd) 
            data.ageRange = data.ageStart + "-" + data.ageEnd;
        else 
            data.ageRange = null;
        
        if (data.bmiStart || data.bmiEnd) 
            data.bmiRange = data.bmiStart + "-" + data.bmiEnd;
        else 
            data.bmiRange = null;
        
        if (data && data.includeAllergy && data.includeAllergy.constructor === Array) {
            data.includeAllergy = data
                .includeAllergy
                .join("|");
        }

        if (data.futureProcedureAvailability == "Whole Blood") 
            data.futureProcedureVolume = data.futureWbVolume;
        else if (data.futureProcedureAvailability == "Bone Marrow") 
            data.futureProcedureVolume = data.futureBoneMarrowVolume;
        else if (data.futureProcedureAvailability == "LeukoPak") 
            data.futureProcedureVolume = data.futureFullHalfPakVolume;
        else if (data.futureProcedureAvailability == "LeukoPak Mobilized") 
            data.futureProcedureVolume = data.futureFullPakVolume;
        
        this.buildHla(data);

        data.hlaAlleleOption = null;
        data.hlaGeneTypes = null;
        if (!data.procedureInfo) 
            data.procedureInfo = null;
        }
    
    onSearch() {
        //$("#ddAdvancedFilters").show();

        const data = jQuery.extend(true, {}, this.state.data);

        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (key) {
                controls[key].validateAllRules(key, data[key]);
            });

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })
        if (numError == 0) {
            this.marshalDataBeforeSearch(data);
            this
                .props
                .processScheduleActions
                .processSchedulingRequest(data, 1, this.props.processScheduleRequest.pageSize, this.props.processScheduleRequest.sortBy, this.props.processScheduleRequest.sortOrder, true);
        } else {
            $(".error-list").show();
        }
    }

    buildHla(returnData) {
        var hla = {};
        var reactComponent = this;

        hla.alleleMatchOption = this.state.data.hlaAlleleOption
            ? this.state.data.hlaAlleleOption
            : 'either';
        hla.geneTypes = [];

        $("div.alle1").each(function () {
            var result = {};
            result.geneType = $(this).attr("data-gene");
            result.allele1 = [];
            $(this)
                .find(".form-group.alle-select")
                .each(function () {
                    reactComponent.buildAlle($(this), result, true);
                });

            hla
                .geneTypes
                .push(result);
        });

        if (hla.alleleMatchOption == "both") {
            var index = 0;
            $("div.alle2").each(function () {
                var result = hla.geneTypes[index];
                result.geneType = $(this).attr("data-gene");
                result.allele2 = [];
                $(this)
                    .find(".form-group.alle-select")
                    .each(function () {
                        reactComponent.buildAlle($(this), result, false);
                    });
                index++;
            });
        }

        if (hla.geneTypes.length == 0) {
            hla = null;
        }

        if (!returnData) {
            const data = this.state.data;
            data.hla = hla;
            this.setState({data: data});
        } else {
            returnData.hla = hla;
        }
    }

    buildAlle(alleHtmlObject, result, addToAllele1)
    {
        if (alleHtmlObject.find("select#PredicateAA").length) {
            this.buildAlleGene(alleHtmlObject, "AA", result, addToAllele1);
        } else if (alleHtmlObject.find("select#PredicateBB").length) {
            this.buildAlleGene(alleHtmlObject, "BB", result, addToAllele1);
        } else if (alleHtmlObject.find("select#PredicateCC").length) {
            this.buildAlleGene(alleHtmlObject, "CC", result, addToAllele1);
        } else if (alleHtmlObject.find("select#PredicateDD").length) {
            this.buildAlleGene(alleHtmlObject, "DD", result, addToAllele1);
        }
    }

    buildAlleGene(alleHtmlObject, gene, result, addToAllele1)
    {
        gene = gene.replace("|", "");
        var alleleData = {
            alleleType: gene
        };
        alleleData.predicate = alleHtmlObject
            .find("select#Predicate" + gene + " option:checked")
            .val()
            .toLowerCase()
            .replace(" ", "_");
        if (alleleData.predicate != "predicate") {
            alleleData.value = alleHtmlObject
                .find("input")
                .val()
                .toLowerCase();

            if (addToAllele1) {
                result
                    .allele1
                    .push(alleleData);
            } else {
                result
                    .allele2
                    .push(alleleData);
            }
        }
    }

    onShowMore() {
        if (this.props.processScheduleRequest.items.length < this.props.processScheduleRequest.totalRecords) {

            const data = jQuery.extend(true, {}, this.state.data);
            this.marshalDataBeforeSearch(data);
            this
                .props
                .processScheduleActions
                .processSchedulingRequest(data, ++this.props.processScheduleRequest.page, this.props.processScheduleRequest.pageSize, this.props.processScheduleRequest.sortBy, this.props.processScheduleRequest.sortOrder, false);
        }
    }

    onSortChange(sortName, sortOrder) {
        const data = jQuery.extend(true, {}, this.state.data);
        this.marshalDataBeforeSearch(data);

        this
            .props
            .processScheduleActions
            .processSchedulingRequest(data, 1, this.props.processScheduleRequest.pageSize, this.props.processScheduleRequest.sortBy, this.props.processScheduleRequest.sortOrder, true);
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('process-schediuling-request-second');
        document
            .body
            .classList
            .add('process-scheduling-request');
        document
            .body
            .classList
            .add('inner');

        if (this.props.needReloadAllProcedures) {
            this
                .props
                .loadAllMedicalProcedures();
            this.didMountCalling = true;
        }

        var id = this.props.routeParams.id;
        if (id) {
            this
                .props
                .scheduleActions
                .getSchedulingRequest(id);

        }

        document.title = 'Process Scheduling Request';

    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('process-schediuling-request-second');
        document
            .body
            .classList
            .remove('process-scheduling-request');
        document
            .body
            .classList
            .remove('inner');
    }

    radioButtonClick(obj, form) {
        //// Get the radio button value
        var res = $(obj).val();
        var grp = $(obj).attr("data-group");
        // onclick below conternt display in bar addclass to selected criteria
        $('input[name="' + obj.name + '"]')
            .not(obj)
            .closest('label')
            .removeClass('checked');
        $(obj)
            .closest('label')
            .addClass('checked');

        var repid;
        // replace spl char form value
        if (!/^[0-9A-Za-z]+$/.test(res)) {
            repid = res.replace(/[_\W]+/g, "");
        } else {
            repid = res;
        }

        var content_before = '<span data-value="' + res + '" data-group="' + grp + '" id="rem_' + repid + '" class=" res-display">';
        var content_after = '</span>';
        var content = content_before + $(obj).attr("data-label") + '<a  href="javascript:void(0)" data-value="' + res + '"   id="remove_term_' + repid + '">X</a>' + content_after;

        var elemExists = $('#disp_' + grp).has('#rem_' + repid);

        if (elemExists.length) {
            $('#disp_' + grp)
                .find('#rem_' + repid)
                .remove();
            $('input[name=' + grp + ']')
                .prop("checked", false)
                .change(); // uncheck radio
            $("input[data-group='" + grp + "']")
                .closest('label')
                .removeClass('checked');

            form.onChange(grp, "");
        } else {
            $('#disp_' + grp).html('');
            $('#disp_' + grp).append(content);
        }

        function remove_terms(resObj, val, grp) { //type=radio
            $(resObj).remove(); // remove data in bar
            $('input[name=' + grp + '][value="' + val + '"]')
                .closest('label')
                .prop("checked", false)
                .removeClass('checked');

            form.onChange(grp, "");
        }

        $("#remove_term_" + repid + '[data-value="' + res + '"]')
            .click(function (e) {
                var rem = $(e.target).closest("span");
                remove_terms($(rem), $(rem).attr("data-value"), $(rem).attr("data-group"));
            });

    }

    componentDidUpdate()
    {

        $(".hidden-icons").hide();

        $("body").click(function () {
            $(".hidden-icons").hide();
        });

        var self = this;
        $("input:radio").off("click");
        $("input:checkbox").off("click");
        $("#ddAdvancedFilters").off("click");
        $("#ddAdvancedFilters").click(function (event) {
            $("#edit_search_filters").toggleClass("dropdown-active");
        });

        $("input:radio").click(function () {
            self.radioButtonClick(this, self);
        });

        $("input:checkbox").click(self.checkBoxClick);

        var radios = $("input[type=radio]:checked").each(function () {
            var $radio = $(this);
            if (!$radio.parent().hasClass("checked")) {
                self.radioButtonClick(this, self);
            }

        });

        var checkboxes = $("input[type=checkbox]:checked").each(function () {
            var $radio = $(this);
            if (!$radio.parent().hasClass("checked")) {
                self
                    .checkBoxClick
                    .apply(this);
            }

        });

        var value = this.state.data.procedureInfo;
        if (value) {
            if (typeof(value) === "string" && value.startsWith("Whole Blood")) {
                $("#whole").show();
                $("#bonemarrow,#lukopack").hide();
            } else if (typeof(value) === "string" && value.startsWith("Bone Marrow")) {
                $("#bonemarrow").show();
                $("#whole,#lukopack").hide();
            } else if (typeof(value) === "string" && value.startsWith("LeukoPak")) {
                $("#lukopack").show();
                $("#bonemarrow,#whole").hide();
            } else {
                $("#whole,#bonemarrow, #lukopack").hide();
            }
        } else {
            $("#whole,#bonemarrow, #lukopack").hide();
        }

        value = this.state.data.futureProcedureAvailability;
        if (value) {
            if (value == "Whole Blood") {
                $("#fwhole").show();
                $("#other,#fbonemarrow,#flukopack").hide();
            } else if (value == "Bone Marrow") {
                $("#fbonemarrow").show();
                $("#other,#fwhole,#flukopack").hide();
            } else if (value == "LeukoPak") {
                $("#flukopack").show();
                $("#other,#fbonemarrow,#fwhole").hide();
            } else {
                $("#other").show();
                $("#fwhole,#fbonemarrow, #flukopack").hide();
            }
        } else {
            $("#fwhole,#other,#fbonemarrow,#flukopack").hide();
        }

        if (this.state.data && this.state.data.hla && this.state.data.hla.geneTypes.length && !$('[name=hlaAlleleOption]:checked').val()) {
            $('[name=hlaAlleleOption][value="either"]')
                .closest('label')
                .click()
                .addClass('checked');
        }

        if (this.state.data && this.state.data.hla && this.state.data.hla.geneTypes.length && $("div.alle1").length == 0) {
            var self = this;
            this
                .state
                .data
                .hla
                .geneTypes
                .forEach(function (gene) {
                    self.fillHlaSpecific(gene.geneType, true);

                    gene
                        .allele1
                        .forEach(function (allele) {
                            $("#hla" + gene.geneType + " div.alle1 select#Predicate" + allele.alleleType)
                                .val(allele.predicate)
                                .change();
                            $("#hla" + gene.geneType + " div.alle1 input[placeholder=" + allele.alleleType + "]").val(allele.value);
                        });

                    gene
                        .allele2
                        .forEach(function (allele) {
                            $("#hla" + gene.geneType + " div.alle2 select#Predicate" + allele.alleleType)
                                .val(allele.predicate)
                                .change();
                            $("#hla" + gene.geneType + " div.alle2 input[placeholder=" + allele.alleleType + "]").val(allele.value);
                        });

                });

            $("#gene_type_alle1 select.cityselect").select2({minimumResultsForSearch: Infinity});
        }

        if (!this.state.data || !this.state.data.hlaGeneTypes || !this.state.data.hlaGeneTypes.length) {
            $("#gene_type_alle1")
                .children()
                .remove();
        }

    }

    checkBoxClick() {
        var res = $(this).val();
        var grp = $(this).attr("data-group")

        // get checkbox status as checked or not
        var chk_sts = $('input:checkbox[value="' + res + '"]').prop('checked');

        var repid;
        // replace spl char form value
        if (!/^[0-9A-Za-z]+$/.test(res)) {
            repid = res.replace(/[_\W]+/g, "");
        } else {
            repid = res;
        }

        // content formation for display in bar
        var content_before = '<span data-value="' + res + '" id="rem_' + repid + '" class="res-display">';
        var content_after = '</span>';
        var content = content_before + $(this).attr("data-label") + '<a  href="javascript:void(0)" data-value="' + res + '"  id="remove_term_' + repid + '">X</a>' + content_after;

        function remove_terms(resObj, val, grp) { //type=checkbox
            $(resObj).remove(); // remove data in bar
            $('input:checkbox[value="' + val + '"]')
                .closest('label')
                .click()
                .removeClass('checked');
        }

        if (chk_sts == true) { // by check chkbox add the content in bar
            $('#disp_' + grp).append(content);
            //addclass to selected criteria
            $(this)
                .closest('label')
                .addClass('checked');

            $("#remove_term_" + repid + '[data-value="' + res + '"]').click(function (e) {
                var rem = $(e.target).closest("span");
                remove_terms($(rem), $(rem).attr("data-value"), $(rem).attr("data-group"));
            });

        } else { // by uncheck chkbox remove the content in bar
            $("#rem_" + repid).remove();
            $(this)
                .closest('label')
                .removeClass('checked');

        }

    }

    renderCategory(cell, row) {
        var status = row && row.recruitment && row.recruitment.status
            ? row.recruitment.status
            : '';
        var className = status + "-clr";
        return (
            <div className={className}>
                <span></span>
                {status}
            </div>
        );
    }

    renderButtonAction(cell, row) {
        var hiddenIconId = "hidden-icons" + row.id;
        var fToogleMore = function (row) {
            setTimeout(function () {
                $("#hidden-icons" + row.id).toggle("slide");
            }, 20);

        };
        var scheduleId = this.state.data && this.state.data.id
            ? this.state.data.id
            : '';
        if (!scheduleId && location.hash.startsWith("#/schedule-request/process-schedule-request/")) {

            scheduleId = parseInt(location.hash.substr(location.hash.lastIndexOf("/") + 1));
        }
        var openAddOutReachDialog = this.props.layoutActions.openAddOutReachDialog;
        var fAddOutReachDialog = function (row) {
            openAddOutReachDialog(row.id, scheduleId);
        };

        var didOnly = (this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id == 3);
        var iconEditDonorSrc = didOnly
            ? "icon-view.svg"
            : "icon-edit.svg";
        iconEditDonorSrc = "/img/icons/searchdonaricons/" + iconEditDonorSrc

        var titleEditDonor = didOnly
            ? "View donor"
            : "Edit donor";

        var fEditDonor = function (row) {
            hashHistory.push(PATH_PREFIX + '/donor/edit-donor/' + row.id);
        };

        var self = this;
        var fScheduleDonor = function (row) {
            self
                .props
                .processScheduleActions
                .matchDonorWithRequest(row.id, scheduleId);
            hashHistory.push(PATH_PREFIX + '/donor/edit-donor/' + row.id);
        };

        return (
            <div >
                <a
                    href="javascript:void(0)"
                    onClick={function () {
                    fToogleMore(row);
                }}><img
                    src="/img/icons/processschedulingrequest/icon-more.svg"
                    alt="More"
                    width="20"/></a>
                <div
                    className="hidden-icons"
                    id={hiddenIconId}
                    style={{
                    display: "none"
                }}>
                    <a
                        href="javascript:void(0)"
                        title="Add Outreach Log"
                        onClick={function () {
                        fAddOutReachDialog(row);
                    }}>
                        <img
                            src="/img/icons/processschedulingrequest/icon-add-outreach-log.svg"
                            alt=""
                            width="20"/>
                    </a>
                    <a
                        href="javascript:void(0)"
                        title="Schedule Donor"
                        onClick={function () {
                        fScheduleDonor(row);
                    }}><img
                        src="/img/icons/processschedulingrequest/icon-directory.svg"
                        alt=""
                        width="20"/>
                    </a>
                    <a
                        href="javascript:void(0)"
                        title={titleEditDonor}
                        onClick={function () {
                        fEditDonor(row);
                    }}><img src={iconEditDonorSrc} alt="" width="20" data-pin-nopin="true"/>
                    </a>
                </div>

            </div>
        );
    }

    renderSeachResult() {

        let options = {
            onSortChange: this
                .onSortChange
                .bind(this)
        };

        if (this.state.processScheduleRequest && this.state.processScheduleRequest.items && this.state.processScheduleRequest.items.length) {
            let showMore = null;
            if (this.props.processScheduleRequest.items.length < this.props.processScheduleRequest.totalRecords) {
                showMore = <div className="text-right showmore">
                    <a
                        className="showmore_rows"
                        href="javascript:void(0)"
                        onClick={this.onShowMore}>Show More</a>
                </div>
            }
            return (
                <div className="searchResultsContainer">

                    <BootstrapTable
                        ref='table'
                        data={this.state.processScheduleRequest.items}
                        options={options}
                        search={false}
                        tableContainerClass="donortable"
                        bordered={false}>
                        <TableHeaderColumn dataField='id' isKey dataSort>Id</TableHeaderColumn>
                        <TableHeaderColumn dataField='email' dataSort>Email</TableHeaderColumn>
                        <TableHeaderColumn dataField='firstName' dataSort>First Name</TableHeaderColumn>
                        <TableHeaderColumn dataField='lastName' dataSort>Last Name</TableHeaderColumn>
                        <TableHeaderColumn dataField='primaryPhone' dataSort>Phone</TableHeaderColumn>

                        <TableHeaderColumn
                            dataField='category'
                            dataFormat={this
                            .renderCategory
                            .bind(this)}
                            dataSort>Category</TableHeaderColumn>
                        <TableHeaderColumn dataField='lastRequestId' dataSort>Last Request #</TableHeaderColumn>
                        <TableHeaderColumn
                            dataField='lastOutReachDate'
                            dataFormat={displayDate}
                            dataSort>Last Outreach Date</TableHeaderColumn>
                        <TableHeaderColumn dataField='lastOutReachReason' dataSort>Last Outreach Reason</TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="button"
                            dataFormat={this
                            .renderButtonAction
                            .bind(this)}
                            columnClassName="img-center actionBlock"
                            headerTitle={false}>&nbsp;</TableHeaderColumn>

                    </BootstrapTable>

                    <div className="clear"></div>
                    {showMore}
                </div>

            );
        }
    }

    render() {

        var errText = null;
        if (this.state.processScheduleRequest && this.state.processScheduleRequest.status == API_STATUS.ERROR) {

            $("#errorDil").modal('show');

        } else if (this.state.processScheduleRequest && this.state.processScheduleRequest.status == API_STATUS.DONE && this.state.processScheduleRequest.items.length == 0) {
            errText = (
                <span
                    id="errorSpan"
                    style={{
                    color: 'red',
                    position: 'relative',
                    fontSize: '20px',
                    top: '12px',
                    marginRight: '15px'
                }}>No result found.</span>
            );
        }

        let matchedDonor = null;
        if (this.state.processScheduleRequest && this.state.processScheduleRequest.items && this.state.processScheduleRequest.items.length) {
            matchedDonor = <div className="col-sm-24 collapse-sm">
                <h4>Matched Donors</h4>
            </div>
        }

        var vol = null;
        if (this.props.schedulingRequest.data && this.props.schedulingRequest.data.procedureInfo) {
            if (this.props.schedulingRequest.data.procedureInfo.category == "Whole Blood") 
                vol = this.props.schedulingRequest.data.volume;
            else 
                vol = this.props.schedulingRequest.data.procedureInfo.volumeUnit;
            }
        
        if (vol && !vol.endsWith("ak")) 
            vol += "ML";
        
        return (
            <div id="schedule-appointment" className="clearfix">

                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>
                            <a href="javascript:void(0)">
                                Manage Schedule Requests</a>
                        </li>
                        <li className="active">Process Scheduling Request</li>
                    </ol>
                </div>

                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Process Scheduling Request</h2>
                    </div>

                </div>

                <div className="search-wrapper">

                    <div className="col-xs-24 col-sm-24 col-md-24 col-lg-24">
                        <div className="results col-sm-19">

                            <span id="disp_collectionSite"></span>
                            <span id="disp_category"></span>
                            <span id="disp_status"></span>

                            <span id="disp_procedureInfo"></span>
                            <span id="disp_gender"></span>
                            <span id="disp_ethnicity"></span>
                            <span id="disp_race"></span>
                            <span id="disp_bloodType"></span>
                            <span id="disp_smoker"></span>
                            <span id="disp_clinicalGrade"></span>
                            <span id="disp_excludeMedication"></span>
                            <span id="disp_includeAllergy"></span>

                            <span id="disp_futureProcedureAvailability"></span>

                            <span id="disp_cmv"></span>

                            <span id="disp_backupDonorRequired"></span>
                            <span id="disp_additionalTesting"></span>
                            <span id="additionalTestingType"></span>

                            <span id="disp_hlaGeneTypes"></span>
                            <span id="disp_hlaAlleleOption"></span>

                        </div>
                        <div className="reset-form col-sm-5">
                            <div className="btn-group" role="group" aria-label="actions">
                                <a
                                    type="button"
                                    className="btn btn-default"
                                    href="javascript:void(0)"
                                    onClick={this
                                    .clearfields
                                    .bind(this)}>
                                    <img src="/img/icons/searchdonaricons/reset.png" alt="reset"/>Reset</a>
                                <a
                                    type="button"
                                    className="btn btn-default btn-sm"
                                    href="javascript:void(0)"
                                    id="ddAdvancedFilters"
                                    aria-haspopup="true"
                                    aria-expanded="true">
                                    <img src="/img/icons/searchdonaricons/icon-filters.png" alt="reset"/>Filters
                                </a>
                            </div>
                        </div>
                        <div className="clear"></div>
                        <hr/>

                    </div>

                    <div className="clear"></div>

                    <div className="col-sm-24 info">
                        <div className="col-sm-3">
                            <label className="ucase">Request:</label>
                            <label>{this.props.schedulingRequest.data && this.props.schedulingRequest.data.id
                                    ? this.props.schedulingRequest.data.id
                                    : ''}</label>
                        </div>
                        <div className="col-sm-4">
                            <label className="ucase">Request Date:</label>
                            <label>{this.props.schedulingRequest.data && this.props.schedulingRequest.data.internalCollectionTime
                                    ? this.props.schedulingRequest.data.collectionDateOnly
                                    : ''}</label>
                        </div>
                        <div className="col-sm-4">
                            <label className="ucase">Donor Id:</label>
                            <label>{this.props.schedulingRequest.data && this.props.schedulingRequest.data.donorId
                                    ? this.props.schedulingRequest.data.donorId
                                    : 'N/A'}</label>
                        </div>
                        <div className="col-sm-6">
                            <label className="ucase">Procedure:</label>
                            <label>{this.props.schedulingRequest.data && this.props.schedulingRequest.data.procedureInfo
                                    ? this.props.schedulingRequest.data.procedureInfo.name
                                    : ''}</label>
                        </div>
                        <div className="col-sm-3">
                            <label className="ucase">Volume:</label>
                            <label>{vol}</label>
                        </div>
                        <div className="col-sm-4">
                            <label className="ucase">Work Order:</label>
                            <label>{this.props.schedulingRequest.data && this.props.schedulingRequest.data.woNumber
                                    ? this.props.schedulingRequest.data.woNumber
                                    : ''}</label>
                        </div>

                    </div>
                    <div className="clear"></div>

                    <div
                        id="edit_search_filters"
                        className="dropdown-active clearfix dropdown-content">
                        <div
                            name="search-donor-form"
                            id="search-donor-form"
                            className="search-donor-form">
                            <div className="clear"></div>
                            <ErrorArea errorList={this.state.errors}/>
                            <InputControl
                                containerClass="col-sm-24 form-group"
                                type="textarea"
                                ref
                                ={(input) => {
                                this.inputControls["additionalNotes"] = input
                            }}
                                useH4AsLabel={true}
                                name="additionalNotes"
                                label="Additional Notes"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="100"
                                rows="4"
                                value={this.state.data && this.state.data.additionalNotes
                                ? this.state.data.additionalNotes
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>
                            <div className="col-sm-24">
                                <h3>Criteria Section 1</h3>
                            </div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                type="date"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["collectionDateOnly"] = input
                            }}
                                name="collectionDateOnly"
                                label="Collection Date"
                                useH4AsLabel
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="10"
                                validate={validations.date}
                                errorMessage="The &#39;Collection Date&#39; does not seem to be valid. Please check and enter again."
                                emptyMessage="The &#39;Collection Date&#39; field cannot be left empty. Please enter a value to continue."
                                value={this.state.data && this.state.data.collectionDate
                                ? this.state.data.collectionDateOnly
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-7 col-lg-7 form-group"
                                type="searchRadio"
                                name="collectionSite"
                                label="Location"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.data && this.state.data.collectionSite
                                ? this.state.data.collectionSite
                                : ''}
                                optionValues={[
                                [
                                    "California", "CA"
                                ],
                                ["Massachusetts", "MA"]
                            ]}
                                ref
                                ={(input) => {
                                this.inputControls["collectionSite"] = input
                            }}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-5 form-group"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["woNumber"] = input
                            }}
                                name="woNumber"
                                label="Work Order #"
                                errorMessage="The &#39;Work Order Number&#39; does not seem to be valid. Please check and enter again."
                                emptyMessage="The &#39;Work Order Number&#39; field cannot be left empty. Please enter a value to continue."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="35"
                                value={this.state.data && this.state.data.woNumber
                                ? this.state.data.woNumber
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                ref
                                ={(input) => {
                                this.inputControls["collectionTime"] = input
                            }}
                                type="time"
                                name="collectionTime"
                                useH4AsLabel
                                label="COLLECTION MUST COMPLETE BY"
                                errorMessage="The &#39;Collection End  Date&#39; does not seem to be valid. Please check and enter again."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="10"
                                value={this.state.data && this.state.data.collectionTime
                                ? this.state.data.collectionTime
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}
                                onlyFutureDate={true}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-24 col-lg-24 form-group"
                                type="searchRadio"
                                name="procedureInfo"
                                label="Procedure"
                                labelClass="ucase"
                                inputClass="form-control"
                                value={this.state.data && this.state.data.procedureInfo
                                ? this.state.data.procedureInfo
                                : ''}
                                optionValues={this.procedures}
                                ref
                                ={(input) => {
                                this.inputControls["procedureInfo"] = input
                            }}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerId="whole"
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                searchControl={true}
                                ref
                                ={(input) => {
                                this.inputControls["volume"] = input
                            }}
                                name="volume"
                                label="Volume"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="3"
                                value={this.state.data && this.state.data.volume
                                ? this.state.data.volume
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerId="bonemarrow"
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                type="searchRadio"
                                ref
                                ={(input) => {
                                this.inputControls["volume"] = input
                            }}
                                name="volume"
                                label="Volume"
                                labelClass="ucase"
                                inputClass="form-control"
                                optionValues={this.boneMarrowVolumms}
                                value={this.state.data && this.state.data.volume
                                ? this.state.data.volume
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerId="lukopack"
                                containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                type="searchRadio"
                                ref
                                ={(input) => {
                                this.inputControls["volume"] = input
                            }}
                                name="volume"
                                label="Volume"
                                labelClass="ucase"
                                inputClass="form-control"
                                optionValues={this.leukoparkVolumns}
                                value={this.state.data && this.state.data.volume
                                ? this.state.data.volume
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>

                            <div className="col-sm-24">
                                <h3>Criteria Section 2</h3>
                            </div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                type="searchRadio"
                                name="gender"
                                ref
                                ={(input) => {
                                this.inputControls["gender"] = input
                            }}
                                value={this.state.data && this.state.data.gender
                                ? this.state.data.gender
                                : ''}
                                label="Gender"
                                labelClass="ucase"
                                inputClass="form-control cityselect"
                                optionValues={[
                                [
                                    "Male", "m"
                                ],
                                ["Female", "f"]
                            ]}
                                defaultValue="m"
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-18 col-lg-18 form-group"
                                type="searchRadio"
                                name="ethnicity"
                                ref
                                ={(input) => {
                                this.inputControls["ethnicity"] = input
                            }}
                                value={this.state.data && this.state.data.ethnicity
                                ? this.state.data.ethnicity
                                : ''}
                                label="Ethnicity"
                                labelClass="ucase"
                                inputClass="form-control cityselect"
                                optionValues={[
                                [
                                    "Hispanic or Latino ", "hispanic"
                                ],
                                [
                                    "Not Hispanic or Latino", "non_hispanic"
                                ],
                                ["Not Specified", "not_specified"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-24 col-lg-24 form-group"
                                type="searchCheckbox"
                                multiple
                                name="race"
                                ref
                                ={(input) => {
                                this.inputControls["race"] = input
                            }}
                                selectedValues={this.state.data && this.state.data.race
                                ? this.state.data.race
                                : ''}
                                label="Race"
                                labelClass="ucase"
                                inputClass="form-control"
                                optionValues={[
                                [
                                    "American Indian/Alaskan Native", "american_indian"
                                ],
                                [
                                    "Asian", "asian"
                                ],
                                [
                                    "Black or African American", "black"
                                ],
                                [
                                    "Native Hawaiian or Other Pacific Islander", "hawaiian"
                                ],
                                [
                                    "White", "white"
                                ],
                                ["Other Race", "other"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-14 col-lg-14 form-group"
                                type="searchCheckbox"
                                name="bloodType"
                                multiple={true}
                                ref
                                ={(input) => {
                                this.inputControls["bloodType"] = input
                            }}
                                selectedValues={this.state.data && this.state.data.bloodType
                                ? this.state.data.bloodType
                                : ''}
                                label="Blood Type"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    'A\uFF0B', "A+"
                                ],
                                [
                                    'A \u2013', "A-"
                                ],
                                [
                                    'B\uFF0B', "B+"
                                ],
                                [
                                    'B \u2013', "B-"
                                ],
                                [
                                    'O\uFF0B', "O+"
                                ],
                                [
                                    'O \u2013', "O-"
                                ],
                                [
                                    'AB\uFF0B', "AB+"
                                ],
                                ['AB \u2013', "AB-"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-5 col-lg-5 form-group"
                                type="searchRadio"
                                name="smoker"
                                ref
                                ={(input) => {
                                this.inputControls["smoker"] = input
                            }}
                                value={this.state.data && this.state.data.smoker === true
                                ? 'y'
                                : (this.state.data && this.state.data.smoker === false)
                                    ? 'n'
                                    : ''}
                                label="Smoker"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "Smoker"
                                ],
                                ["No", "n", "Not Smoker"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-5 col-lg-5 form-group"
                                type="searchRadio"
                                name="clinicalGrade"
                                ref
                                ={(input) => {
                                this.inputControls["clinicalGrade"] = input
                            }}
                                value={this.state.data && this.state.data.clinicalGrade === true
                                ? 'y'
                                : (this.state.data && this.state.data.clinicalGrade === false)
                                    ? 'n'
                                    : ''}
                                label="Clinical Grade"
                                labelClass="ucase"
                                optionValues={[
                                [
                                    "Yes", "y", "Clinical Grade Donor"
                                ],
                                ["No", "n", "Not Clinical Grade Donor"]
                            ]}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                ref={(input) => {
                                this.inputControls["did"] = input
                            }}
                                name="did"
                                useH4AsLabel
                                label="Specific Donor Id"
                                validate={validations.did}
                                errorMessage="The &#39;Specific Donor Id&#39; should be number. Please check the Specific Donor Id and enter again."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="8"
                                value={this.state.data && this.state.data.did
                                ? this.state.data.did
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                searchControl={true}
                                ref={(input) => {
                                this.inputControls["didExclusion"] = input
                            }}
                                name="didExclusion"
                                label="Exclude Donor Id's (4 Max)"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="64"
                                value={this.state.data && this.state.data.didExclusion
                                ? this.state.data.didExclusion
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                ref={(input) => {
                                this.inputControls["ageStart"] = input
                            }}
                                name="ageStart"
                                useH4AsLabel
                                label="From Age"
                                validate={validations.age}
                                errorMessage="The &#39;From Age&#39; should be number. Please check the From Age and enter again."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="3"
                                value={this.state.data && this.state.data.ageStart
                                ? this.state.data.ageStart
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                ref
                                ={(input) => {
                                this.inputControls["ageEnd"] = input
                            }}
                                name="ageEnd"
                                useH4AsLabel
                                label="To Age"
                                validate={validations.age}
                                errorMessage="The &#39;To Age&#39; should be number. Please check the To Age and enter again."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="3"
                                value={this.state.data.ageEnd && this.state.data.ageEnd
                                ? this.state.data.ageEnd
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                ref={(input) => {
                                this.inputControls["bmiStart"] = input
                            }}
                                name="bmiStart"
                                useH4AsLabel
                                label="From Bmi"
                                validate={validations.bmi}
                                errorMessage="The &#39;From Bmi&#39; should be number and maximum 99. Please check the From  Bmi and enter again."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="5"
                                value={this.state.data && this.state.data.bmiStart
                                ? this.state.data.bmiStart
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                ref={(input) => {
                                this.inputControls["bmiEnd"] = input
                            }}
                                name="bmiEnd"
                                useH4AsLabel
                                label="To Bmi"
                                validate={validations.bmi}
                                errorMessage="The &#39;To Bmi&#39; should be number and maximum 99. Please check the To Bmi and enter again."
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="5"
                                value={this.state.data && this.state.data.bmiEnd
                                ? this.state.data.bmiEnd
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                validate={validations.number}
                                errorMessage="The &#39;# Days Alcohol restrictions&#39; should be number. Please check the # Days Alcohol restrictions and enter again."
                                ref
                                ={(input) => {
                                this.inputControls["priorDaysAlllowedAlcohol"] = input
                            }}
                                name="priorDaysAlllowedAlcohol"
                                label="# Days Alcohol restrictions"
                                useH4AsLabel
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="8"
                                value={this.state.data && this.state.data.priorDaysAlllowedAlcohol
                                ? this.state.data.priorDaysAlllowedAlcohol
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-18 col-lg-18 form-group"
                                showSelectOption={true}
                                name="excludeMedication"
                                multiple={true}
                                ref
                                ={(input) => {
                                this.inputControls["excludeMedication"] = input
                            }}
                                label="Medication use Restrictions"
                                type="searchCheckbox"
                                labelClass="ucase"
                                inputClass="form-control medicalselectpicker"
                                optionValues={[
                                [
                                    "Birth Control", "birth_control"
                                ],
                                [
                                    "Vitamin/Supplement", "supplement"
                                ],
                                [
                                    "Over-Counter", "over_counter"
                                ],
                                [
                                    "Prescription", "prescription"
                                ],
                                ["Steroid", "steroid"]
                            ]}
                                selectedValues={this.state.data && this.state.data.excludeMedication
                                ? this.state.data.excludeMedication
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group med-show"
                                validate={validations.number}
                                errorMessage="The &#39;Number Of Days&#39; should be number. Please check the Number Of Days and enter again."
                                ref
                                ={(input) => {
                                this.inputControls["priorDaysAllowedMedUse"] = input
                            }}
                                name="priorDaysAllowedMedUse"
                                label="Number Of Days"
                                useH4AsLabel
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="8"
                                value={this.state.data && this.state.data.priorDaysAllowedMedUse
                                ? this.state.data.priorDaysAllowedMedUse
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <InputControl
                                containerClass="col-xs-24 col-sm-24 col-md-12 col-lg-12 form-group med-show"
                                ref={(input) => {
                                this.inputControls["allowedMedication"] = input
                            }}
                                name="allowedMedication"
                                useH4AsLabel
                                label="Medication acceptable, If Any"
                                labelClass="ucase"
                                inputClass="form-control"
                                maxLength="100"
                                value={this.state.data && this.state.data.allowedMedication
                                ? this.state.data.allowedMedication
                                : ''}
                                onChange={this.onChange}
                                onInvalid={this.onInvalid}
                                onValid={this.onValid}/>

                            <div className="clear"></div>

                            <div
                                className="advanced_search"
                                style={{
                                display: "none"
                            }}>

                                <div className="col-sm-24">
                                    <h3>Criteria Section 3</h3>
                                </div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    ref={(input) => {
                                    this.inputControls["donationCountLimit"] = input
                                }}
                                    name="donationCountLimit"
                                    useH4AsLabel
                                    label="Lifetime Donation limit"
                                    validate={validations.age}
                                    errorMessage="The &#39;Lifetime Donation limit&#39; should be number. Please check the Lifetime Donation limit and enter again."
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="2"
                                    value={this.state.data && this.state.data.donationCountLimit
                                    ? this.state.data.donationCountLimit
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    ref={(input) => {
                                    this.inputControls["fastingDays"] = input
                                }}
                                    name="fastingDays"
                                    useH4AsLabel
                                    label="Fasting day required"
                                    validate={validations.number}
                                    errorMessage="The &#39;Fasting day required&#39; should be number. Please check the Fasting day required and enter again."
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="3"
                                    value={this.state.data && this.state.data.fastingDays
                                    ? this.state.data.fastingDays
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-12 col-lg-12 form-group"
                                    type="searchRadio"
                                    showSelectOption={true}
                                    name="cmv"
                                    ref
                                    ={(input) => {
                                    this.inputControls["cmv"] = input
                                }}
                                    value={this.state.data && this.state.data.cmv
                                    ? this.state.data.cmv
                                    : ''}
                                    label="CMV Status"
                                    labelClass="ucase"
                                    inputClass="form-control cityselect"
                                    optionValues={[
                                    [
                                        "Positive", "positive"
                                    ],
                                    [
                                        "Negative", "negative"
                                    ],
                                    ["Equivalent", "equivalent"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group"
                                    type="searchRadio"
                                    name="backupDonorRequired"
                                    ref
                                    ={(input) => {
                                    this.inputControls["backupDonorRequired"] = input
                                }}
                                    value={this.state.data && this.state.data.backupDonorRequired === true
                                    ? 'y'
                                    : (this.state.data && this.state.data.backupDonorRequired === false)
                                        ? 'n'
                                        : ''}
                                    label="Backup Donor Needed"
                                    labelClass="ucase"
                                    inputClass="form-control cityselect showhidedrop"
                                    optionValues={[
                                    [
                                        "Yes", "y", "Backup Donor Needed"
                                    ],
                                    ["No", "n", "Backup Donor Not Needed"]
                                ]}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-12 col-lg-12 form-group"
                                    type="searchRadio"
                                    name="additionalTesting"
                                    showSelectOption={true}
                                    ref
                                    ={(input) => {
                                    this.inputControls["additionalTesting"] = input
                                }}
                                    label="Additional Testing Needed"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "Prior to collection", "prior_to_collection"
                                    ],
                                    [
                                        "During collection", "during_collection"
                                    ],
                                    ["Post collection", "post_collection"]
                                ]}
                                    value={this.state.data && this.state.data.additionalTesting
                                    ? this.state.data.additionalTesting
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-10 col-lg-10 form-group type-add-test"
                                    type="searchRadio"
                                    showSelectOption={true}
                                    name="additionalTestingType"
                                    ref
                                    ={(input) => {
                                    this.inputControls["additionalTestingType"] = input
                                }}
                                    label="additional Test Type"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "Expanded Virals", "expanded_virals"
                                    ],
                                    [
                                        "Clinical Grade Virals", "clinical_grade_virals"
                                    ],
                                    ["Other", "other"]
                                ]}
                                    value={this.state.data && this.state.data.additionalTestingType
                                    ? this.state.data.additionalTestingType
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-14 col-lg-14 form-group"
                                    type="searchRadio"
                                    showSelectOption={true}
                                    containerId="futuretest"
                                    name="futureProcedureAvailability"
                                    ref
                                    ={(input) => {
                                    this.inputControls["futureProcedureAvailability"] = input
                                }}
                                    label="Future Procedure Availability"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "Whole Blood", "Whole Blood"
                                    ],
                                    [
                                        "Bone Marrow", "Bone Marrow"
                                    ],
                                    [
                                        "LeukoPak", "LeukoPak"
                                    ],
                                    ["Mobilized LeukoPak", "Mobilized LeukoPak"]
                                ]}
                                    value={this.state.data && this.state.data.futureProcedureAvailability
                                    ? this.state.data.futureProcedureAvailability
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>
                                <InputControl
                                    containerId="fwhole"
                                    useH4AsLabel
                                    containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                    ref
                                    ={(input) => {
                                    this.inputControls["futureWbVolume"] = input
                                }}
                                    name="futureWbVolume"
                                    label="Future Procedure Volume"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="3"
                                    value={this.state.data && this.state.data.futureWbVolume
                                    ? this.state.data.futureWbVolume
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerId="fbonemarrow"
                                    containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                    type="searchRadio"
                                    ref
                                    ={(input) => {
                                    this.inputControls["futureBoneMarrowVolume"] = input
                                }}
                                    name="futureBoneMarrowVolume"
                                    label="Future Procedure Volume"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "50 ML", "50"
                                    ],
                                    ["100 ML", "100"]
                                ]}
                                    value={this.state.data && this.state.data.futureBoneMarrowVolume
                                    ? this.state.data.futureBoneMarrowVolume
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerId="flukopack"
                                    containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                    type="searchRadio"
                                    ref
                                    ={(input) => {
                                    this.inputControls["futureFullHalfPakVolume"] = input
                                }}
                                    name="futureFullHalfPakVolume"
                                    label="Future Procedure Volume"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "Half", "Half Pak"
                                    ],
                                    ["Full", "Full Pak"]
                                ]}
                                    value={this.state.data && this.state.data.futureFullHalfPakVolume
                                    ? this.state.data.futureFullHalfPakVolume
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerId="other"
                                    containerClass="col-xs-24 col-sm-24 col-md-4 col-lg-6 form-group"
                                    type="searchRadio"
                                    showSelectOption={true}
                                    ref
                                    ={(input) => {
                                    this.inputControls["futureFullPakVolume"] = input
                                }}
                                    name="futureFullPakVolume"
                                    label="Future Procedure Volume"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    ["Full", "Full Pak"]
                                ]}
                                    value={this.state.data && this.state.data.futureFullPakVolume
                                    ? this.state.data.futureFullPakVolume
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group feture-hide"
                                    ref
                                    ={(input) => {
                                    this.inputControls["futureProcedureWoNumber"] = input
                                }}
                                    name="futureProcedureWoNumber"
                                    useH4AsLabel
                                    label="Future Work Order #"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="10"
                                    validate={validations.number}
                                    errorMessage="The &#39;Future Work Order #&#39; should be number. Please check the Future Work Order # and enter again."
                                    value={this.state.data && this.state.data.futureProcedureWoNumber
                                    ? this.state.data.futureProcedureWoNumber
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group feture-hide"
                                    ref
                                    ={(input) => {
                                    this.inputControls["futureProcedureDate"] = input
                                }}
                                    type="date"
                                    name="futureProcedureDate"
                                    useH4AsLabel
                                    label="future Procedure Date"
                                    errorMessage="The &#39;Future Procedure Date&#39; does not seem to be valid. Please check and enter again."
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="10"
                                    value={this.state.data && this.state.data.futureProcedureDate
                                    ? this.state.data.futureProcedureDate
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-8 col-lg-8 form-group"
                                    type="searchCheckbox"
                                    name="includeAllergy"
                                    ref
                                    ={(input) => {
                                    this.inputControls["includeAllergy"] = input
                                }}
                                    label="Allergy Restrictions"
                                    labelClass="ucase"
                                    inputClass="form-control allergyselectpicker"
                                    optionValues={[
                                    [
                                        "Food", "food"
                                    ],
                                    [
                                        "Environment", "environmental"
                                    ],
                                    ["Medication", "medication"]
                                ]}
                                    selectedValues={this.state.data && this.state.data.includeAllergy
                                    ? this.state.data.includeAllergy
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-6 col-lg-6 form-group allergy-note"
                                    ref
                                    ={(input) => {
                                    this.inputControls["allergyNote"] = input
                                }}
                                    name="allergyNote"
                                    label="Allergy Restrictions Note"
                                    useH4AsLabel
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="100"
                                    value={this.state.data && this.state.data.allergyNote
                                    ? this.state.data.allergyNote
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>
                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-24 col-lg-24 form-group gene_typea_select"
                                    type="searchCheckbox"
                                    ref
                                    ={(input) => {
                                    this.inputControls["hlaGeneTypes"] = input
                                }}
                                    name="hlaGeneTypes"
                                    label="Hla Typing-Gene Type"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "A", "A"
                                    ],
                                    [
                                        "B", "B"
                                    ],
                                    [
                                        "C", "C"
                                    ],
                                    [
                                        "DPA1", "DPA1"
                                    ],
                                    [
                                        "DPB1", "DPB1"
                                    ],
                                    [
                                        "DQA1", "DQA1"
                                    ],
                                    [
                                        "DQB1", "DQB1"
                                    ],
                                    [
                                        "DRB1", "DRB1"
                                    ],
                                    [
                                        "DRB3", "DRB3"
                                    ],
                                    [
                                        "DRB4", "DRB4"
                                    ],
                                    ["DRB5", "DRB5"]
                                ]}
                                    selectedValues={this.state.data && this.state.data.hlaGeneTypes
                                    ? this.state.data.hlaGeneTypes
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>

                                <InputControl
                                    containerClass="col-xs-24 col-sm-24 col-md-24 col-lg-24 form-group"
                                    type="searchRadio"
                                    name="hlaAlleleOption"
                                    ref
                                    ={(input) => {
                                    this.inputControls["hlaAlleleOption"] = input
                                }}
                                    label="Allele Option"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    optionValues={[
                                    [
                                        "Either Allele", "either"
                                    ],
                                    [
                                        "Homozygous Alleles", "homozygous"
                                    ],
                                    ["Both Alleles", "both"]
                                ]}
                                    value={this.state.data && this.state.data.hlaAlleleOption
                                    ? this.state.data.hlaAlleleOption
                                    : ''}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="clear"></div>
                                <div className="col-sm-24 collapse-sm" id="gene_type_alle1"></div>

                            </div>

                            <div className="clear"></div>

                            <div className="clear"></div>

                            <div className="col-xs-24 col-sm-24 col-md-24 col-lg-24">
                                <div className="pull-left showhide">
                                    <a onClick={this.onShowFilter} id="filtertext">Show Advanced Filters</a>
                                </div>
                                <div className="pull-right">
                                    {errText}
                                    <button id="searchButton" className="btn btn-default" onClick={this.onSearch}>Search</button>

                                </div>
                            </div>
                        </div>

                        {this.state.processScheduleRequest && this.state.processScheduleRequest.items && this.state.processScheduleRequest.items.length
                            ? (<hr/>)
                            : null}

                        <div className="clear"></div>
                    </div>

                    <div className="clear"></div>
                    {matchedDonor}
                    <div className="clear"></div>
                    {this.renderSeachResult()}
                </div>

            </div>

        );
    }
}

export default ProcessSchedulingRequestForm;