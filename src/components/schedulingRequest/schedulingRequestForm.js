import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import * as validations from '../../validations/common'
import {API_STATUS} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import SchedulingRequestTab from './schedulingRequestFormTab'
import CriteriaFirstSection from './schedulingRequestFormSection1'
import CriteriaSecondSection from './schedulingRequestFormSection2'
import CriteriaThirdSection from './schedulingRequestFormSection3'
import CriteriaFourSection from './schedulingRequestFormSection4'
import {displayDate} from '../../utils/date.js';

class SchedulingRequestForm extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            schedulingRequest: jQuery.extend(true, {}, this.props.schedulingRequest),
            allMedicalProcedures: this.props.allMedicalProcedures
        });

        this.onCloneRequest = this
            .onCloneRequest
            .bind(this);

        this.onCancelRequest = this
            .onCancelRequest
            .bind(this);

        this.onSave = this
            .onSave
            .bind(this);

        this.hasUnsavedChanges = this
            .hasUnsavedChanges
            .bind(this);
        this.autoSave = this
            .autoSave
            .bind(this);

        this.doCloneRequest = this
            .doCloneRequest
            .bind(this);

        this.cloneRequest = false;
        this.isCreateForm = true;
    }

    hasUnsavedChanges() {
        var changed = (this.tab1 && this.tab1.hasUnsavedChanges()) || this
            .tab2
            .hasUnsavedChanges() || this
            .tab3
            .hasUnsavedChanges() || this
            .tab4
            .hasUnsavedChanges();

        return changed;
    }

    autoSave(nextTarget) {
        this.setState({autoSave: true, nextTarget: nextTarget});
        this
            .tab1
            .onSave(null, null, false, true, nextTarget);
    }

    initCreateForm() {
        if (this.hasInitCreateForm) 
            return;
        
        this.hasInitCreateForm = true;

        this.isCreateForm = true;
        this.hasInitEditForm = false;
        document
            .body
            .classList
            .add('create-scheduling-request');
        document.title = 'Create Scheduling Request';

        this.cloneRequest = false;
        this.tab1.cloneRequest = false;
        this
            .tab1
            .clearData();
        this
            .tab2
            .clearData();
        this
            .tab3
            .clearData();
        this
            .tab4
            .clearData();

        let self = this;
        setTimeout(function () {
            if (self.tabs) 
                self.tabs.showTab("step1");
            
            $("#listAssignDonor").hide();
        }, 50);
    }

    initEditForm(schedulingRequestId) {
        this.isCreateForm = false;

        if (this.hasInitEditForm) 
            return;
        this.hasInitEditForm = true;
        document
            .body
            .classList
            .add('edit-scheduling-request');
        document.title = 'Edit Scheduling Request';

        this.cloneRequest = false;
        this.tab1.cloneRequest = false;

        this
            .props
            .actions
            .getSchedulingRequest(schedulingRequestId);
    }

    componentWillReceiveProps(nextProps) {
        if (!this.didMountCalling && this.props.needReloadAllProcedures) {
            this
                .props
                .loadAllMedicalProcedures();
            this.didMountCalling = false;
        } else {
            this.setState({allMedicalProcedures: nextProps.allMedicalProcedures});
        }

        var self = this;
        if (nextProps.schedulingRequest.actionType == actionTypes.CHANG_ROUTE) {
            if (location.hash.indexOf("#/schedule-request/edit-schedule-request/") < 0) {
                // create new request
                if (this.hasUnsavedChanges()) {
                    if (!$(".jconfirm").hasClass("jconfirm-open") && !this.showConfirm) {
                        this.showConfirm = true;
                        $.confirm({
                            title: 'Confirm leave',
                            content: 'Changes were made. Would you like to leave and discard them or stay to save?',
                            buttons: {
                                /*  Yes: {
                                text: 'Yes',
                                btnClass: 'btn-blue',
                                action: function () {
                                    self.autoSave("newRequest");
                                    self.showConfirm = false;
                                }
                            },*/
                                No: {
                                    text: 'Leave',
                                    btnClass: 'btn-danger',
                                    action: function () {
                                        self.showConfirm = false;
                                        self.hasInitCreateForm = false;
                                        self.initCreateForm();
                                    }
                                },
                                cancel: {
                                    text: 'Stay',
                                    btnClass: 'btn-blue',
                                    action: function () {
                                        self.showConfirm = false;
                                    }
                                }

                            }
                        });
                    }
                } else {
                    this.initCreateForm();
                }
            } else if (location.hash.indexOf("#/schedule-request/edit-schedule-request/") >= 0) {
                let schedulingRequestId = location
                    .hash
                    .replace("#/schedule-request/edit-schedule-request/", "");
                this.initEditForm(schedulingRequestId);

            }
        } else if ((nextProps.schedulingRequest.actionType == actionTypes.GET_SCHEDULING_REQUEST && nextProps.schedulingRequest.status == API_STATUS.DONE) || (nextProps.schedulingRequest.actionType == actionTypes.UNASSIGN_DONOR || nextProps.schedulingRequest.actionType == actionTypes.SAVE_SCHEDULING_REQUEST)) {
            this.setState({
                schedulingRequest: jQuery.extend(true, {}, nextProps.schedulingRequest)
            });

            if (nextProps.schedulingRequest.actionType == actionTypes.SAVE_SCHEDULING_REQUEST && nextProps.schedulingRequest.status == API_STATUS.DONE) {
                this.hasInitCreateForm = false;
            }

        } else if (nextProps.schedulingRequest.status == API_STATUS.DONE && nextProps.schedulingRequest.actionType == actionTypes.CANCEL_SCHEDULING_REQUEST && this.state.processing) {
            this.setState({processing: false});
            $("#successMessage").html("Cancel request successfully.");
            this
                .props
                .layoutActions
                .openSuccessDialog();
        } else if (nextProps.schedulingRequest.status == API_STATUS.ERROR && nextProps.schedulingRequest.actionType == actionTypes.CANCEL_SCHEDULING_REQUEST && this.state.processing) {
            $("#errorDil").modal('show');
            this.setState({processing: false});
        }
    }

    onCloneRequest() {
        const self = this;
        if (!$(".jconfirm").hasClass("jconfirm-open") && this.hasUnsavedChanges()) {
            $.confirm({
                title: 'Confirm leave',
                content: 'Changes were made. Would you like to leave and discard them or stay to save?',
                buttons: {
                    /*  Yes: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function () {
                            self.autoSave("cloneRequest");
                        }
                    },*/
                    No: {
                        text: 'Leave',
                        btnClass: 'btn-danger',
                        action: function () {
                            self.doCloneRequest();
                        }
                    },
                    cancel: {
                        text: 'Stay',
                        btnClass: 'btn-blue',
                        action: function () {}
                    }
                }
            });
        } else {
            this.doCloneRequest();
        }
    }

    doCloneRequest() {
        window
            .history
            .replaceState(null, null, "#/schedule-request/create-scheduling-request/");

        this.cloneRequest = true;
        this.isCreateForm = true;
        if (this.tab1) {
            this.tab1.cloneRequest = true;
            this
                .tab1
                .cloneData();
        }
        if (this.tab2) {
            this
                .tab2
                .cloneData();
        }
        if (this.tab3) {
            this
                .tab3
                .cloneData();
        }
        if (this.tab4) {
            this
                .tab4
                .cloneData();
        }

        $("#cloneCancel").hide();
        $(".section-heading").text("Clone Scheduling Request");
        $("#readcrumbMainEle").text("Clone Scheduling Request");

        var schedulingRequest = this.state.schedulingRequest;
        schedulingRequest.data.donorToScheduleRequests = [];
        this.setState({
            schedulingRequest: jQuery.extend(true, {}, schedulingRequest)
        });
    }

    onCancelRequest() {
        var self = this;
        $.confirm({
            title: 'Cancel schedule request',
            content: 'This will permanently cancel the request. Are you sure?',
            buttons: {
                confirm: function () {
                    self.setState({processing: true});
                    self
                        .props
                        .actions
                        .cancelSchedulingRequest(self.props.routeParams.id);
                },
                cancel: function () {}
            }
        });
    }

    onSave(tabName, data) {
        var request = jQuery.extend(true, {}, this.state.schedulingRequest.data); //deep clone

        var error = false;
        if (tabName != "Tab1") {
            var t1 = this
                .tab1
                .onSave(null, null, true);
            if (t1) {
                request = Object.assign(request, t1.data); //shallow assign
            } else {
                error = true;
                this
                    .tabs
                    .showTab("step1");
                this
                    .tab1
                    .showErrors();
            }
        } else {
            request = Object.assign(request, data); //shallow assign
        }

        if (!error && tabName != "Tab2") {
            var t2 = this
                .tab2
                .onSave(null, null, true);
            if (t2) {
                request = Object.assign(request, t2.data); //shallow assign
            } else {
                error = true;
                this
                    .tabs
                    .showTab("step2");
                this
                    .tab2
                    .showErrors();
            }
        } else {
            request = Object.assign(request, data); //shallow assign
        }

        if (!error && tabName != "Tab3") {
            var t3 = this
                .tab3
                .onSave(null, null, true);
            if (t3) {
                request = Object.assign(request, t3.data); //shallow assign
            } else {
                error = true;
                this
                    .tabs
                    .showTab("step3");
                this
                    .tab3
                    .showErrors();
            }
        } else {
            request = Object.assign(request, data); //shallow assign
        }

        if (!error && tabName != "Tab4") {
            var t4 = this
                .tab4
                .onSave(null, null, true);
            if (t4) {
                request = Object.assign(request, t4.data); //shallow assign
            } else {
                error = true;
                this
                    .tabs
                    .showTab("step4");
                this
                    .tab4
                    .showErrors();
            }
        } else {
            request = Object.assign(request, data); //shallow assign
        }

        if (this.props.user) 
            request.requestedBy = this.props.user.email;
        else 
            request.requestedBy = "test@dms.com";
        if (!error) {
            if (this.cloneRequest || this.isCreateForm) {
                request.id = null;
                request.status = "OPEN";
            }
            this
                .props
                .actions
                .saveSchedulingRequest(tabName, request);
        }
    }

    onCancel() {
        if (this.tab1) {
            this
                .tab1
                .onCancel();
        }
        if (this.tab2) {
            this
                .tab2
                .onCancel();
        }
        if (this.tab3) {
            this
                .tab3
                .onCancel();
        }
        if (this.tab4) {
            this
                .tab4
                .onCancel();
        }
    }

    componentDidMount() {

        if (this.props.needReloadAllProcedures) {
            this
                .props
                .loadAllMedicalProcedures();
            this.didMountCalling = true;
        }

        document
            .body
            .classList
            .add('inner');

        this.hasInitCreateForm = false;
        this.hasInitEditForm = false;

        var id = this.props.routeParams.id;
        if (id) {
            this.hasInitEditForm = false;
            this.initEditForm(id);

        } else {
            document
                .body
                .classList
                .add('create-scheduling-request');
            document.title = 'Create Scheduling Request';
        }

        $(".tab-pil")
            .click(function () {
                $(".tab-pil").removeClass("active");
                $(this).addClass("active");
            });
    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('create-scheduling-request');
        document
            .body
            .classList
            .remove('edit-scheduling-request');
        document
            .body
            .classList
            .remove('inner');
    }

    renderBreadcrumbMainEle() {
        if (this.cloneRequest) {
            return (
                <li className="active">
                    Clone Scheduling Request</li>
            );
        } else {
            return (
                <li id="readcrumbMainEle" className="active">{this.props.routeParams.id
                        ? 'Edit Scheduling Request'
                        : 'Create Scheduling Request'}</li>
            );
        }
    }

    renderHeader() {
        if (this.cloneRequest) {
            return (
                <h2 className="section-heading">
                    Clone Scheduling Request</h2>
            );
        } else {
            return (
                <h2 className="section-heading">{this.props.routeParams.id
                        ? 'Edit Scheduling Request'
                        : 'Create Scheduling Request'}</h2>
            );
        }
    }

    render() {
        var canEditSchedule = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 3;
        var cssHideEditSchedule = canEditSchedule === false
            ? " hide"
            : "";
        return (
            <div>
                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>Manage Schedule Requests</li>
                        {this.renderBreadcrumbMainEle()}
                    </ol>
                </div>
                <div id="section-title" className="clearfix">
                    <div className="pull-left-lg txt-center-xs heading">
                        {this.renderHeader()}
                    </div>

                    {this.props.routeParams.id
                        ? <div id="cloneCancel" className="pull-right-lg txt-center-xs filters">
                                <button
                                    id="cloneRequest"
                                    onClick={this.onCloneRequest}
                                    disabled={!this.state.schedulingRequest.data}
                                    className={"btn btn-print btn-hollow" + cssHideEditSchedule}>Clone Request</button>
                                <button
                                    onClick={this.onCancelRequest}
                                    disabled={!this.state.schedulingRequest.data || (this.state.schedulingRequest.data.status && (this.state.schedulingRequest.data.status.toLowerCase() == "completed" || this.state.schedulingRequest.data.status.toLowerCase() == "cancelled"))}
                                    className={"btn btn-print btn-hollow" + cssHideEditSchedule}>Cancel Request</button>
                            </div>
                        : null
}
                </div>

                <div className="panel">
                    <div className="panel-body">
                        {this.state.schedulingRequest.data.id && !this.cloneRequest && !this.isCreateForm
                            ? (
                                <div
                                    className="col-sm-24 collapse-sm info clearfix"
                                    style={{
                                    height: '40px'
                                }}>
                                    <div className="col-sm-6 form-group">
                                        <label className="ucase">Schedule Request #:&nbsp;</label>
                                        <label>{this.state.schedulingRequest.data.id}</label>
                                    </div>
                                    <div className="col-sm-6 form-group">
                                        <label className="ucase">Status:&nbsp;</label>
                                        <label>{this.state.schedulingRequest.data.status}</label>
                                    </div>
                                    <div className="col-sm-6 form-group">
                                        <label className="ucase">Created On:&nbsp;</label>
                                        <label>{displayDate(this.state.schedulingRequest.data.createdAt)}</label>
                                    </div>
                                    <div className="col-sm-6 form-group">
                                        <label className="ucase">Created By:&nbsp;</label>
                                        <label>{this.state.schedulingRequest.data.createdBy}</label>
                                    </div>
                                </div>
                            )
                            : null
}
                        <SchedulingRequestTab
                            ref={(c) => {
                            this.tabs = c
                        }}/>
                        <div className="tab-content col-sm-24">
                            <CriteriaFirstSection
                                doCloneRequest={this.doCloneRequest}
                                onSave={this.onSave}
                                user={this.props.user}
                                schedulingRequest={this.state.schedulingRequest}
                                allMedicalProcedures={this.state.allMedicalProcedures}
                                layoutActions={this.props.layoutActions}
                                ref={(c) => {
                                this.tab1 = c
                            }}
                                canEditSchedule={canEditSchedule}
                                unAssignedDonor={this.props.actions.unAssignedDonor}/>

                            <CriteriaSecondSection
                                onSave={this.onSave}
                                schedulingRequest={this.state.schedulingRequest}
                                layoutActions={this.props.layoutActions}
                                ref={(c) => {
                                this.tab2 = c
                            }}
                                canEditSchedule={canEditSchedule}/>

                            <CriteriaThirdSection
                                onSave={this.onSave}
                                schedulingRequest={this.state.schedulingRequest}
                                layoutActions={this.props.layoutActions}
                                ref={(c) => {
                                this.tab3 = c
                            }}
                                canEditSchedule={canEditSchedule}/>

                            <CriteriaFourSection
                                onSave={this.onSave}
                                schedulingRequest={this.state.schedulingRequest}
                                layoutActions={this.props.layoutActions}
                                ref={(c) => {
                                this.tab4 = c
                            }}
                                canEditSchedule={canEditSchedule}/>
                        </div>
                    </div>
                </div>

            </div>

        )
    }
}

export default SchedulingRequestForm