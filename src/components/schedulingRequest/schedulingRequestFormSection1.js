import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import * as validations from '../../validations/common'
import {API_STATUS, PATH_PREFIX} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import isEqual from 'lodash';
import {hashHistory} from 'react-router';
import {localDateToUTCString, displayDate, displayTime, displayDate2, displayTime2} from '../../utils/date.js';

class CriteriaFirstSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: jQuery.extend(true, {
        collectionTime: null,
        collectionDateOnly: null
      }, this.props.schedulingRequest.data),
      initData: jQuery.extend(true, {
        collectionTime: null,
        collectionDateOnly: null
      }, this.props.schedulingRequest.data),
      orgData: jQuery.extend(true, {
        collectionTime: null,
        collectionDateOnly: null
      }, this.props.schedulingRequest.data),
      errors: {},
      processing: false,
      allMedicalProcedures: []
    }

    this.onInvalid = this
      .onInvalid
      .bind(this);
    this.onValid = this
      .onValid
      .bind(this);
    this.onChange = this
      .onChange
      .bind(this);

    this.onSave = this
      .onSave
      .bind(this);
    this.onCancel = this
      .onCancel
      .bind(this);

    this.inputControls = {};

    this.procedures = [];
    this.boneMarrowVolumms = [];
    this.leukoparkVolumns = [];

  }

  componentDidMount() {

    if ((!this.state.allMedicalProcedures || this.state.allMedicalProcedures.length == 0) && this.props.allMedicalProcedures && this.props.allMedicalProcedures.length) {
      this.setState({allMedicalProcedures: this.props.allMedicalProcedures});
      this.buildProcedureOptions(this.props.allMedicalProcedures);
    }
  }

  hasUnsavedChanges() {
    var changed = !_.isEqual(this.state.data, this.state.orgData);
    return changed;
  }

  cloneData() {
    this.setState({orgData: this.state.initData});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.allMedicalProcedures && nextProps.allMedicalProcedures.length) {
      this.setState({allMedicalProcedures: nextProps.allMedicalProcedures});
      this.buildProcedureOptions(nextProps.allMedicalProcedures);
    }

    if (nextProps.schedulingRequest.status == API_STATUS.DONE && !this.loadRequestComplete && nextProps.schedulingRequest.actionType == actionTypes.GET_SCHEDULING_REQUEST) {
      this.loadRequestComplete = true;
      var schedulingRequest = jQuery.extend(true, {}, nextProps.schedulingRequest.data)
      if (schedulingRequest.procedureInfo && schedulingRequest.procedureInfo.category) {
        var procedure = schedulingRequest.procedureInfo;
        if (procedure.category == "Whole Blood") {
          schedulingRequest.procedureInfo = "Whole Blood|" + procedure.id;
          schedulingRequest.volume = schedulingRequest.volume;
        } else if (procedure.category == "Bone Marrow") {
          schedulingRequest.procedureInfo = "Bone Marrow";
          schedulingRequest.volume = procedure.volumeUnit + "|" + procedure.id;
        } else if (procedure.category == "LeukoPak") {
          schedulingRequest.procedureInfo = "LeukoPak";
          schedulingRequest.volume = procedure.volumeUnit + "|" + procedure.id;
        } else {
          schedulingRequest.procedureInfo = procedure.id;
          schedulingRequest.volume = '';
        }
      }

      let collectionDateOnly = displayDate2(nextProps.schedulingRequest.data.internalCollectionTime, nextProps.schedulingRequest.data.collectionDate, nextProps.schedulingRequest.data.collectionByTime);

      let collectionTime = displayTime2(nextProps.schedulingRequest.data.internalCollectionTime, nextProps.schedulingRequest.data.collectionDate, nextProps.schedulingRequest.data.collectionByTime);

      this.setState({
        data: jQuery.extend(true,  {
          collectionDateOnly: collectionDateOnly,
          collectionTime: collectionTime
        },schedulingRequest),
        orgData: jQuery.extend(true, {
          collectionDateOnly: collectionDateOnly,
          collectionTime: collectionTime
        },schedulingRequest)
      });
      setTimeout(function () {
        $("#collectionTime")
          .timepicker("setTime", collectionTime)
          .timepicker("update");
      }, 10);

    } else if (nextProps.schedulingRequest.status == API_STATUS.DONE && nextProps.schedulingRequest.actionType == actionTypes.SAVE_SCHEDULING_REQUEST && this.state.processing) {
      this.setState({processing: false, orgData: this.state.data});
      if (this.state.autoSave) {
        if (this.state.nextTarget) {
          if (this.state.nextTarget == "newRequest") {
            $("#step1").click();
          } else if (this.state.nextTarget == "cloneRequest") {
            this.cloneData();
            this.cloneRequest = true;
            this
              .props
              .doCloneRequest();
          } else {
            hashHistory.push(this.state.nextTarget);
          }
          this.setState({nextTarget: null});
        }
      } else {
        $("#successMessage").html("Save scheduling request successfully.");
        $("#cloneCancel").show();

        this
          .props
          .layoutActions
          .openSuccessDialog();
      }
    } else if (nextProps.schedulingRequest.status == API_STATUS.ERROR && nextProps.schedulingRequest.actionType == actionTypes.SAVE_SCHEDULING_REQUEST && this.state.processing) {
      this.setState({processing: false});
      var message = "";
      if (nextProps.schedulingRequest.cause) {
        message = nextProps.schedulingRequest.cause;
        if (message.message) 
          message = message.message;
        }
      else if (nextProps.schedulingRequest.message) {
        message = nextProps.schedulingRequest.message;
      }
      message = message.replace("IllegalArgumentException: ", "");
      if (message.indexOf("Work Order") >= 0) {
        var controls = this.inputControls;
        controls["woNumber"].setError(message);
        this.showErr = true;
      } else if (message.indexOf("Volume") >= 0) {
        var controls = this.inputControls;
        controls["volume"].setError(message);
        this.showErr = true;
      } else {
        if (message) {
          $("#errorDil #errorMessage").html(message);
        }
        $("#errorDil").modal('show');
      }

    } else if (nextProps.schedulingRequest.actionType == actionTypes.UNASSIGN_DONOR && this.state.processing) {
      this.setState({processing: false});
      if (nextProps.schedulingRequest.status == API_STATUS.ERROR) {
        $("#errorDil").modal('show');
      }
    }
  }

  componentDidUpdate() {

    if (this.showErr) {
      $(".error-list").show();
      this.showErr = false;
      $("#step1").click();
    }
    var value = this.state.data.procedureInfo;
    if (typeof(value) === "string" && value.startsWith("Whole Blood")) {
      $("#whole").show();
      $("#bonemarrow,#lukopack").hide();
    } else if (typeof(value) === "string" && value.startsWith("Bone Marrow")) {
      $("#bonemarrow").show();
      $("#whole,#lukopack").hide();
    } else if (typeof(value) === "string" && value.startsWith("LeukoPak")) {
      $("#lukopack").show();
      $("#bonemarrow,#whole").hide();
    } else {
      $("#whole,#bonemarrow, #lukopack").hide();
    }

  }

  onInvalid(name, message) {
    const errors = this.state.errors;
    errors[name] = message;
    this.setState({errors: errors})
  }

  onValid(name) {
    const errors = this.state.errors;
    errors[name] = "";
    this.setState({errors: errors})
  }

  onChange(name, value) {
    const data = this.state.data;

    if (name.startsWith('volume')) {
      data["volume"] = value;
    } else if (value == '') {
      data[name] = null;
    } else {
      data[name] = value;
    }

    if (name == "procedureInfo") {
      const errors = this.state.errors;
      errors["volume"] = "";
      errors["volumelukopack"] = "";
      errors["volumebonemarrow"] = "";
      this.setState({errors: errors})
      if (typeof(value) === "string" && value.startsWith("Whole Blood")) {
        this
          .inputControls["volumelukopack"]
          .clearData();
        this
          .inputControls["volumebonemarrow"]
          .clearData();
        $("#whole").show();
        $("#bonemarrow,#lukopack").hide();
      } else if (typeof(value) === "string" && value.startsWith("Bone Marrow")) {
        this
          .inputControls["volume"]
          .clearData();
        this
          .inputControls["volumelukopack"]
          .clearData();
        $("#bonemarrow").show();
        $("#whole,#lukopack").hide();
      } else if (typeof(value) === "string" && value.startsWith("LeukoPak")) {
        this
          .inputControls["volume"]
          .clearData();
        this
          .inputControls["volumebonemarrow"]
          .clearData();
        $("#lukopack").show();
        $("#bonemarrow,#whole").hide();
      } else {
        this
          .inputControls["volume"]
          .clearData();
        this
          .inputControls["volumelukopack"]
          .clearData();
        this
          .inputControls["volumebonemarrow"]
          .clearData();
        $("#whole,#bonemarrow, #lukopack").hide();
      }
    }
    this.setState({data: data});
  }

  showErrors()
  {
    $(".error-list").show();
    //scroll top
    $('html, body').animate({
      scrollTop: 200
    }, 700);
  }

  onSave(proxy, event, fromParent, autoSave, nextTarget) {
    const data = jQuery.extend(true, {}, this.state.data);
    this.setState({autoSave: autoSave, nextTarget: nextTarget});
    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        var value;
        if (name.startsWith('volume')) {
          value = data['volume'];
        } else if (data && data[name]) {
          value = data[name];
        } else {
          value = '';
        }

        if ((name != "volumelukopack" || (typeof(data.procedureInfo) === "string" && data.procedureInfo.startsWith("LeukoPak"))) && (name != "volumebonemarrow" || (typeof(data.procedureInfo) === "string" && data.procedureInfo.startsWith("Bone Marrow"))) && (name != "volume" || (typeof(data.procedureInfo) === "string" && data.procedureInfo.startsWith("Whole Blood")))) {
          controls[name].validateAllRules(name, value, fromParent);
        }

      });

    const errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })

    if (numError == 0) {

      if (data.procedureInfo) {
        if (typeof(data.procedureInfo) === "string" && data.procedureInfo.startsWith("Whole Blood")) {
          data.procedureInfo = {
            id: data
              .procedureInfo
              .split("|")[1]
          };
        } else if (typeof(data.procedureInfo) === "string" && data.procedureInfo.startsWith("Bone Marrow")) {
          var vo = data.volume;
          data.volume = vo
            ? vo.split("|")[0]
            : null;
          data.procedureInfo = {
            id: vo.split("|")[1]
          };
        } else if (typeof(data.procedureInfo) === "string" && data.procedureInfo.startsWith("LeukoPak")) {
          var vo = data.volume;
          data.volume = vo
            ? vo.split("|")[0]
            : null;
          data.procedureInfo = {
            id: vo.split("|")[1]
          };
        } else {
          data.procedureInfo = {
            id: data.procedureInfo
          };
        }
      }

      //send only data of this tab for parent to combine
      var newData = {};
      newData.procedureInfo = data.procedureInfo;
      var collectionDateTime = localDateToUTCString(moment(data.collectionDateOnly + (data.collectionTime != null && data.collectionTime != ''
        ? (' ' + data.collectionTime)
        : ' 23:59')));
      newData.internalCollectionTime = collectionDateTime;
      newData.collectionSite = data.collectionSite;
      //  newData.collectionTime = data.collectionTime;
      newData.volume = data.volume;
      newData.woNumber = data.woNumber;

      this.setState({processing: true});
      if (!fromParent) {
        this
          .props
          .onSave("Tab1", newData);
      } else {
        return {tab: "Tab1", data: newData};
      }
    } else {
      this.showErrors();
    }
  }

  clearData() {
    this.setState({
      data: jQuery.extend(true, {}, this.state.initData),
      orgData: jQuery.extend(true, {}, this.state.initData),
      errors: {},
      processing: false
    });

    setTimeout(function () {
      $("#collectionTime")
        .timepicker("setTime", null)
        .timepicker("update");
      $("#collectionDateOnly").data('date', null);
      $("#collectionDateOnly").datetimepicker('update');
    }, 10);

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].clearError();
      });
  }

  onCancel() {
    const self = this;
    self.setState({
      data: jQuery.extend(true, {}, self.state.orgData),
      errors: {},
      processing: false
    });

    var controls = self.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].clearError();
      });
  }

  buildProcedureOptions(allMedicalProcedures) {

    this.procedures = [];
    this.boneMarrowVolumms = [];
    this.leukoparkVolumns = [];
    var self = this;
    var wholeBlood = false;
    var leukopark = false;
    var boneMarrow = false;
    allMedicalProcedures.forEach(function (element) {
      if (element.category == "Whole Blood" && !wholeBlood) {
        self
          .procedures
          .push([
            "Whole Blood", "Whole Blood|" + element.id
          ]);
        wholeBlood = true;
      } else if (element.category == "Bone Marrow") {
        if (!boneMarrow) {
          self
            .procedures
            .push(["Bone Marrow", "Bone Marrow"]);
          boneMarrow = true;
          self
            .boneMarrowVolumms
            .push([
              element.volumeUnit + " ml",
              element.volumeUnit + "|" + element.id
            ]);
        } else {
          self
            .boneMarrowVolumms
            .push([
              element.volumeUnit + " ml",
              element.volumeUnit + "|" + element.id
            ]);
        }

      } else if (element.category == "LeukoPak") {
        if (!leukopark) {
          self
            .procedures
            .push(["LeukoPak", "LeukoPak"]);
          leukopark = true;
          self
            .leukoparkVolumns
            .push([
              element.volumeUnit, element.volumeUnit + "|" + element.id
            ]);
        } else {
          self
            .leukoparkVolumns
            .push([
              element.volumeUnit, element.volumeUnit + "|" + element.id
            ]);
        }
      } else {
        self
          .procedures
          .push([element.name, element.id]);
      }
    });

  }

  unAssignedDonor(scheduleId, donorId) {
    this.setState({processing: true});
    this
      .props
      .unAssignedDonor(scheduleId, donorId);
  }

  render() {
    var cssHideSave = this.props.canEditSchedule === false
      ? " hide"
      : "";

    var assignedDonorSection = null;
    var canSeeAssignedDonor = this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id <= 2;

    if (canSeeAssignedDonor && this.props.schedulingRequest && this.props.schedulingRequest.data && this.props.schedulingRequest.data.donorToScheduleRequests && this.props.schedulingRequest.data.donorToScheduleRequests.length > 0 && this.props.schedulingRequest.data.donorToScheduleRequests[0].status == "SCHEDULED") {

      var donor = this.props.schedulingRequest.data.donorToScheduleRequests[0].donor;
      var self = this;
      if (!this.cloneRequest) {
        assignedDonorSection = (
          <div id="listAssignDonor">
            <div className="clear"></div>

            <div className="spacer"></div>

            <h4>Assigned donor</h4>

            <div className="table-responsive allcells-responsive-tables">
              <table
                id="search_donor_dtab2"
                className="table allcells-tables"
                cellSpacing="0"
                width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Category</th>
                    <th>Scheduled</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <a
                        style={{
                        textDecoration: "underline"
                      }}
                        href="javascript:void(0)"
                        onClick={function () {
                        hashHistory.push(PATH_PREFIX + '/donor/edit-donor/' + donor.id);
                      }}>{donor.id}</a>
                    </td>
                    <td>{donor.firstName}</td>
                    <td>{donor.lastName}</td>
                    <td>{donor.email}</td>
                    <td>{donor.phone}</td>
                    <td>{donor.recruitment.category}</td>
                    <td>Yes</td>
                    <td>
                      <a
                        disabled={this.state.processing}
                        className={this.state.processing
                        ? " disabled"
                        : ""}
                        href="javascript:void(0)"
                        title="Remove assigned donor"
                        onClick={function () {
                        $.confirm({
                          title: 'Unassign donor',
                          content: 'Are you sure to unassign this donor ?',
                          buttons: {
                            confirm: function () {
                              self.unAssignedDonor(self.props.schedulingRequest.data.id, donor.id);
                            },
                            cancel: function () {}
                          }
                        });
                      }}><img
                        className="icon-svg"
                        src="/img/icons/editdonor/icon-delete.svg"
                        alt="edit"
                        width="18"/></a>
                    </td>
                  </tr>
                </tbody>
              </table>

            </div>

            <div className="clear"></div>
          </div>
        );
      }
    }

    return (

      <div id="criteria0" className="tab-pane fade in active">
        <ErrorArea errorList={this.state.errors}/>
        <div className="donor-form">
          <div className="col-sm-24">
            <h4>Collection Details</h4>
          </div>

          <InputControl
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["collectionDateOnly"] = input
          }}
            type="date"
            name="collectionDateOnly"
            readOnly={!this.props.canEditSchedule}
            label="Collection Date"
            required={true}
            validate={validations.date}
            errorMessage="The &#39;Collection Date&#39; does not seem to be valid. Please check and enter again."
            emptyMessage="The &#39;Collection Date&#39; field cannot be left empty. Please enter a value to continue."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="10"
            value={this.state.data && this.state.data.collectionDateOnly
            ? this.state.data.collectionDateOnly
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}
            onlyFutureDate={true}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            type="select"
            name="collectionSite"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["collectionSite"] = input
          }}
            label="Collection Site"
            required={true}
            emptyMessage="The &#39;Collection Site&#39; field cannot be left blank. Please select a Collection Site from the dropdown to continue."
            labelClass="ucase"
            inputClass="form-control"
            showSelectOption={true}
            optionValues={[
            [
              "California", "CA"
            ],
            ["Massachusetts", "MA"]
          ]}
            value={this.state.data && this.state.data.collectionSite
            ? this.state.data.collectionSite
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["collectionTime"] = input
          }}
            type="time"
            name="collectionTime"
            readOnly={!this.props.canEditSchedule}
            label="COLLECTION MUST COMPLETE BY"
            errorMessage="The &#39;Collection End  Time&#39; does not seem to be valid. Please check and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="10"
            value={this.state.data && this.state.data.collectionTime}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}
            onlyFutureDate={true}/>

          <div className="col-sm-24">
            <h4>Procedure Details</h4>
          </div>

          <InputControl
            containerClass="col-sm-6 form-group"
            containerId="procedure"
            showSelectOption={true}
            type="select"
            name="procedureInfo"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["procedureInfo"] = input
          }}
            label="Procedure"
            required={true}
            emptyMessage="The &#39;Procedure&#39; field cannot be left blank. Please select a Procedure from the dropdown to continue."
            labelClass="ucase"
            inputClass="form-control"
            optionValues={this.procedures}
            value={this.state.data && this.state.data.procedureInfo
            ? this.state.data.procedureInfo
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerId="whole"
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["volume"] = input
          }}
            name="volume"
            readOnly={!this.props.canEditSchedule}
            required={true}
            errorMessage="The &#39;Whole Blood Volume&#39; does not seem to be valid. Please check and enter again."
            emptyMessage="The &#39;Volume&#39; field cannot be left empty. Please enter a value to continue."
            label="Volume"
            labelClass="ucase"
            inputClass="form-control"
            validate={validations.number}
            maxLength="10"
            value={this.state.data && this.state.data.volume
            ? this.state.data.volume
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerId="bonemarrow"
            containerClass="col-sm-6 form-group"
            type="select"
            required={true}
            emptyMessage="The &#39;Volume&#39; field cannot be left empty. Please enter a value to continue."
            showSelectOption={true}
            ref
            ={(input) => {
            this.inputControls["volumebonemarrow"] = input
          }}
            name="volumebonemarrow"
            readOnly={!this.props.canEditSchedule}
            label="Volume"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={this.boneMarrowVolumms}
            value={this.state.data && this.state.data.volume
            ? this.state.data.volume
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerId="lukopack"
            containerClass="col-sm-6 form-group"
            type="select"
            showSelectOption={true}
            ref
            ={(input) => {
            this.inputControls["volumelukopack"] = input
          }}
            required={true}
            emptyMessage="The &#39;Volume&#39; field cannot be left empty. Please enter a value to continue."
            name="volumelukopack"
            readOnly={!this.props.canEditSchedule}
            label=" Volume"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={this.leukoparkVolumns}
            value={this.state.data && this.state.data.volume
            ? this.state.data.volume
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["woNumber"] = input
          }}
            name="woNumber"
            readOnly={!this.props.canEditSchedule}
            label="Work Order Number"
            required={true}
            errorMessage="The &#39;Work Order Number&#39; does not seem to be valid. Please check and enter again."
            emptyMessage="The &#39;Work Order Number&#39; field cannot be left empty. Please enter a value to continue."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="10"
            validate={validations.number}
            value={this.state.data && this.state.data.woNumber
            ? this.state.data.woNumber
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <div className="col-sm-24">
            <button
              className={"btn btn-default" + cssHideSave}
              disabled={this.state.processing}
              onClick={this.onSave}>Save</button>
            <button
              onClick={this.onCancel}
              disabled={this.state.processing}
              className={"btn btn-default btn-hollow" + cssHideSave}>Cancel</button>
          </div>

          {assignedDonorSection}

        </div>
      </div>
    )
  }
}

export default CriteriaFirstSection