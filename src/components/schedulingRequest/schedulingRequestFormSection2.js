import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import * as validations from '../../validations/common'
import {API_STATUS} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import isEqual from 'lodash';

class CriteriaSecondSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: jQuery.extend(true, {}, this.props.schedulingRequest.data),
      initData: jQuery.extend(true, {}, this.props.schedulingRequest.data),
      orgData: jQuery.extend(true, {}, this.props.schedulingRequest.data),
      errors: {},
      processing: false
    }

    this.onInvalid = this
      .onInvalid
      .bind(this);
    this.onValid = this
      .onValid
      .bind(this);
    this.onChange = this
      .onChange
      .bind(this);

    this.onSave = this
      .onSave
      .bind(this);
    this.onCancel = this
      .onCancel
      .bind(this);

    this.inputControls = {};

  }

  hasUnsavedChanges() {
    var changed = !_.isEqual(this.state.data, this.state.orgData);
    return changed;
  }

  cloneData() {
    this.setState({orgData: this.state.initData});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.schedulingRequest.status == API_STATUS.DONE && !this.loadRequestComplete && nextProps.schedulingRequest.actionType == actionTypes.GET_SCHEDULING_REQUEST) {
      this.loadRequestComplete = true;
      var schedulingRequest = jQuery.extend(true, {}, nextProps.schedulingRequest.data);
      if (schedulingRequest.race) {
        schedulingRequest.race = schedulingRequest
          .race
          .split("|");
      }

      if (schedulingRequest.bloodType) {
        schedulingRequest.bloodType = schedulingRequest
          .bloodType
          .split("|");
      }

      if (schedulingRequest.excludeMedication) {
        schedulingRequest.excludeMedication = schedulingRequest
          .excludeMedication
          .split("|");
      }
      this.setState({
        processing: false,
        data: jQuery.extend(true, {}, schedulingRequest),
        orgData: jQuery.extend(true, {}, schedulingRequest)
      });
    } else if (nextProps.schedulingRequest.status == API_STATUS.DONE && nextProps.schedulingRequest.actionType == actionTypes.SAVE_SCHEDULING_REQUEST && this.state.processing) {
      this.setState({processing: false, orgData: this.state.data});
    } else if (nextProps.schedulingRequest.status == API_STATUS.ERROR && this.state.processing) {
      this.setState({processing: false});
    }
  }

  onInvalid(name, message) {
    const errors = this.state.errors;
    errors[name] = message;
    this.setState({errors: errors})
  }

  onValid(name) {
    const errors = this.state.errors;
    errors[name] = "";
    this.setState({errors: errors})
  }

  onChange(name, value) {
    const data = this.state.data;

    if (name == "excludeMedication") {
      if (value) {
        $(".med-show").show()
      } else {
        $(".med-show").hide();
      }
    }

    if (name == "smoker" || name == "clinicalGrade") {
      if (value == "y") {
        data[name] = true;
      } else if (value == "n") {
        data[name] = false;
      } else if (value == '') {
        data[name] = null;
      }
    } else {
      data[name] = value;
    }
    this.setState({data: data});
  }

  componentDidUpdate()
  {
    if (this.state.data.excludeMedication && this.state.data.excludeMedication.length) {
      $(".med-show").show();
    } else {
      $(".med-show").hide();
    }
  }

  showErrors()
  {
    $(".error-list").show();
    //scroll top
    $('html, body').animate({
      scrollTop: 200
    }, 700);
  }

  onSave(proxy, event, fromParent) {
    const data = this.state.data;

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (data && data[name]) {
          controls[name].validateAllRules(name, data[name], fromParent);
        } else {
          controls[name].validateAllRules(name, '', fromParent);
        }

      });

    const errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })

    var d = jQuery.extend(true, {}, this.state.data);

    if (d.race && d.race.constructor === Array) {
      d.race = d
        .race
        .join("|");

    }

    if (d.bloodType && d.bloodType.constructor === Array) {
      d.bloodType = d
        .bloodType
        .join("|");
    }

    if (d.excludeMedication && d.excludeMedication.constructor === Array) {
      d.excludeMedication = d
        .excludeMedication
        .join("|");
    }

    //send only data of this tab for parent to combine
    var newData = {};
    newData.ethnicity = d.ethnicity;
    newData.race = d.race;
    newData.ageStart = d.ageStart;
    newData.ageEnd = d.ageEnd;
    newData.gender = d.gender;
    newData.bmiStart = d.bmiStart;
    newData.bmiEnd = d.bmiEnd;
    newData.bloodType = d.bloodType;
    newData.smoker = d.smoker;
    newData.clinicalGrade = d.clinicalGrade;
    newData.cmv = d.cmv;
    newData.did = d.did;
    newData.didExclusion = d.didExclusion;
    newData.priorDaysAlllowedAlcohol = d.priorDaysAlllowedAlcohol;
    newData.excludeMedication = d.excludeMedication;
    newData.priorDaysAllowedMedUse = d.priorDaysAllowedMedUse;
    newData.allowedMedication = d.allowedMedication;

    if (!fromParent) {
      if (numError == 0) {
        this.setState({processing: true});
        this
          .props
          .onSave("Tab2", newData);
      } else {
        this.showErrors();
      }
    } else {
      if (numError == 0) {
        this.setState({processing: true});
        return {tab: "Tab2", data: newData};
      } else {
        return false;
      }
    }

  }

  clearData() {
    this.setState({
      data: jQuery.extend(true, {}, this.state.initData),
      orgData: jQuery.extend(true, {}, this.state.initData),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].clearError();
      });
  }

  onCancel() {
    this.setState({
      data: jQuery.extend(true, {}, this.state.orgData),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].clearError();
      });
  }

  render() {
    var cssHideSave = this.props.canEditSchedule === false
      ? " hide"
      : "";
    return (

      <div id="criteria1" className="tab-pane fade in">
        <ErrorArea errorList={this.state.errors}/>
        <div className="donor-form">
          <div className="col-sm-24">
            <h4>Ethnic Profile</h4>
          </div>
          <InputControl
            containerClass="col-sm-6 form-group"
            type="select"
            showSelectOption={true}
            name="ethnicity"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["ethnicity"] = input
          }}
            value={this.state.data && this.state.data.ethnicity
            ? this.state.data.ethnicity
            : ''}
            label="EthniCity"
            emptyMessage="The &#39;EthniCity&#39; field cannot be left blank. Please select a EthniCity from the dropdown to continue."
            labelClass="ucase"
            inputClass="form-control cityselect"
            optionValues={[
            [
              "Hispanic or Latino ", "hispanic"
            ],
            [
              "Not Hispanic or Latino", "non_hispanic"
            ],
            ["Not Specified", "not_specified"]
          ]}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            type="select"
            multiple
            name="race"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["race"] = input
          }}
            selectedValues={this.state.data && this.state.data.race
            ? this.state.data.race
            : ''}
            label="Race"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={[
            [
              "American Indian/Alaskan Native", "american_indian"
            ],
            [
              "Asian", "asian"
            ],
            [
              "Black or African American", "black"
            ],
            [
              "Native Hawaiian or Other Pacific Islander", "hawaiian"
            ],
            [
              "White", "white"
            ],
            ["Other Race", "other"]
          ]}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>
          <div className="col-sm-24">
            <h4>Vitals</h4>
          </div>

          <InputControl
            containerClass="col-sm-3 form-group"
            ref
            ={(input) => {
            this.inputControls["ageStart"] = input
          }}
            name="ageStart"
            readOnly={!this.props.canEditSchedule}
            label="From Age"
            validate={validations.age}
            errorMessage="The &#39;From Age&#39; should be number. Please check the From Age and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="3"
            value={this.state.data && this.state.data.ageStart
            ? this.state.data.ageStart
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-3 form-group"
            ref
            ={(input) => {
            this.inputControls["ageEnd"] = input
          }}
            name="ageEnd"
            readOnly={!this.props.canEditSchedule}
            label="To Age"
            validate={validations.age}
            errorMessage="The &#39;To Age&#39; should be number. Please check the To Age and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="3"
            value={this.state.data.ageEnd && this.state.data.ageEnd
            ? this.state.data.ageEnd
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            type="select"
            showSelectOption={true}
            name="gender"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["gender"] = input
          }}
            value={this.state.data && this.state.data.gender
            ? this.state.data.gender
            : ''}
            label="Gender"
            emptyMessage="The &#39;Gender&#39; field cannot be left blank. Please select a Gender from the dropdown to continue."
            labelClass="ucase"
            inputClass="form-control cityselect"
            optionValues={[
            [
              "Male", "m"
            ],
            ["Female", "f"]
          ]}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-3 form-group"
            ref
            ={(input) => {
            this.inputControls["bmiStart"] = input
          }}
            name="bmiStart"
            readOnly={!this.props.canEditSchedule}
            label="From Bmi"
            validate={validations.bmi}
            errorMessage="The &#39;From Bmi&#39; should be number and maximum 99. Please check the From  Bmi and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="5"
            value={this.state.data && this.state.data.bmiStart
            ? this.state.data.bmiStart
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-3 form-group"
            ref={(input) => {
            this.inputControls["bmiEnd"] = input
          }}
            name="bmiEnd"
            readOnly={!this.props.canEditSchedule}
            label="To Bmi"
            validate={validations.bmi}
            errorMessage="The &#39;To Bmi&#39; should be number and maximum 99. Please check the To Bmi and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="5"
            value={this.state.data && this.state.data.bmiEnd
            ? this.state.data.bmiEnd
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <div className="col-sm-24">
            <h4>Stats</h4>
          </div>
          <InputControl
            showSelectOption={true}
            containerClass="col-sm-6 form-group"
            type="select"
            name="bloodType"
            readOnly={!this.props.canEditSchedule}
            multiple={true}
            ref
            ={(input) => {
            this.inputControls["bloodType"] = input
          }}
            selectedValues={this.state.data && this.state.data.bloodType
            ? this.state.data.bloodType
            : ''}
            label="Blood Type"
            labelClass="ucase"
            inputClass="form-control cityselect"
            optionValues={[
            [
              'A\uFF0B', "A+"
            ],
            [
              'A \u2013', "A-"
            ],
            [
              'B\uFF0B', "B+"
            ],
            [
              'B \u2013', "B-"
            ],
            [
              'O\uFF0B', "O+"
            ],
            [
              'O \u2013', "O-"
            ],
            [
              'AB \u2013', "AB-"
            ],
            ['AB\uFF0B', "AB+"]
          ]}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            showSelectOption={true}
            type="select"
            name="smoker"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["smoker"] = input
          }}
            value={this.state.data && this.state.data.smoker
            ? 'y'
            : (!this.state.data || this.state.data.smoker == null)
              ? ''
              : 'n'}
            label="Smoker"
            labelClass="ucase"
            inputClass="form-control cityselect"
            optionValues={[
            [
              "Yes", 'y'
            ],
            ["No", 'n']
          ]}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            showSelectOption={true}
            containerClass="col-sm-5 form-group"
            type="select"
            name="cmv"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["cmv"] = input
          }}
            value={this.state.data && this.state.data.cmv
            ? this.state.data.cmv
            : ''}
            label="CMV Status"
            labelClass="ucase"
            inputClass="form-control cityselect"
            optionValues={[
            [
              "Positive", "positive"
            ],
            [
              "Negative", "negative"
            ],
            ["Equivalent", "equivalent"]
          ]}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            showSelectOption={true}
            type="select"
            name="clinicalGrade"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["clinicalGrade"] = input
          }}
            value={this.state.data && this.state.data.clinicalGrade
            ? 'y'
            : (!this.state.data || this.state.data.clinicalGrade == null)
              ? ''
              : 'n'}
            label="Clinical Grade"
            labelClass="ucase"
            inputClass="form-control cityselect"
            optionValues={[
            [
              "Yes", 'y'
            ],
            ["No", 'n']
          ]}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <div className="col-sm-24">
            <h4>Specific Donor</h4>
          </div>

          <InputControl
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["did"] = input
          }}
            name="did"
            readOnly={!this.props.canEditSchedule}
            label="Specific Donor Id"
            validate={validations.did}
            errorMessage="The &#39;Specific Donor Id&#39; should be number. Please check the Specific Donor Id and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="8"
            value={this.state.data && this.state.data.did
            ? this.state.data.did
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-12 form-group"
            ref
            ={(input) => {
            this.inputControls["didExclusion"] = input
          }}
            name="didExclusion"
            readOnly={!this.props.canEditSchedule}
            label="Exclude Donor Id's (4 Max)"
            labelClass="ucase"
            inputClass="form-control"
            maxLength="64"
            value={this.state.data && this.state.data.didExclusion
            ? this.state.data.didExclusion
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <div className="col-sm-24">
            <h4>Restictions</h4>
          </div>

          <InputControl
            containerClass="col-sm-6 form-group"
            validate={validations.number}
            errorMessage="The &#39;# Days Alcohol restrictions&#39; should be number. Please check the # Days Alcohol restrictions and enter again."
            ref
            ={(input) => {
            this.inputControls["priorDaysAlllowedAlcohol"] = input
          }}
            name="priorDaysAlllowedAlcohol"
            readOnly={!this.props.canEditSchedule}
            label="# Days Alcohol restrictions"
            labelClass="ucase"
            inputClass="form-control"
            maxLength="8"
            value={this.state.data && this.state.data.priorDaysAlllowedAlcohol
            ? this.state.data.priorDaysAlllowedAlcohol
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            showSelectOption={true}
            multiple={true}
            readOnly={!this.props.canEditSchedule}
            name="excludeMedication"
            ref
            ={(input) => {
            this.inputControls["excludeMedication"] = input
          }}
            label="Medication use Restrictions"
            type="select"
            labelClass="ucase"
            inputClass="form-control medicalselectpicker"
            optionValues={[
            [
              "Birth Control", "birth_control"
            ],
            [
              "Vitamin/Supplement", "supplement"
            ],
            [
              "Over-Counter", "over_counter"
            ],
            [
              "Prescription", "prescription"
            ],
            ["Steroid", "steroid"]
          ]}
            selectedValues={this.state.data && this.state.data.excludeMedication
            ? this.state.data.excludeMedication
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-4 form-group med-show"
            validate={validations.number}
            errorMessage="The &#39;Number Of Days&#39; should be number. Please check the Number Of Days and enter again."
            ref
            ={(input) => {
            this.inputControls["priorDaysAllowedMedUse"] = input
          }}
            name="priorDaysAllowedMedUse"
            readOnly={!this.props.canEditSchedule}
            label="Number Of Days"
            labelClass="ucase"
            inputClass="form-control"
            maxLength="8"
            value={this.state.data && this.state.data.priorDaysAllowedMedUse
            ? this.state.data.priorDaysAllowedMedUse
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-8 form-group med-show"
            ref
            ={(input) => {
            this.inputControls["allowedMedication"] = input
          }}
            name="allowedMedication"
            readOnly={!this.props.canEditSchedule}
            label="Medication acceptable, If Any"
            labelClass="ucase"
            inputClass="form-control"
            maxLength="100"
            value={this.state.data && this.state.data.allowedMedication
            ? this.state.data.allowedMedication
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

        </div>

        <div className="col-sm-24">
          <button
            className={"btn btn-default" + cssHideSave}
            disabled={this.state.processing}
            onClick={this.onSave}>Save</button>
          <button
            onClick={this.onCancel}
            disabled={this.state.processing}
            className={"btn btn-default btn-hollow" + cssHideSave}>Cancel</button>
        </div>

      </div>
    )
  }
}

export default CriteriaSecondSection