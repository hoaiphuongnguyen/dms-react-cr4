import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import * as validations from '../../validations/common'
import {API_STATUS} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import isEqual from 'lodash';

class CriteriaThirdSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: jQuery.extend(true, {}, this.props.schedulingRequest.data),
      initData: jQuery.extend(true, {}, this.props.schedulingRequest.data),
      orgData: jQuery.extend(true, {}, this.props.schedulingRequest.data),
      errors: {},
      processing: false
    }

    this.state.data.hlaAlleleOption = "either";
    this.state.orgData.hlaAlleleOption = "either";

    this.onInvalid = this
      .onInvalid
      .bind(this);
    this.onValid = this
      .onValid
      .bind(this);
    this.onChange = this
      .onChange
      .bind(this);

    this.onSave = this
      .onSave
      .bind(this);
    this.onCancel = this
      .onCancel
      .bind(this);

    this.inputControls = {};

  }

  componentDidUpdate()
  {
    if (this.state.data && this.state.data.hla && this.state.data.hla.geneTypes.length && $("div.alle1").length == 0) {
      var self = this;
      this
        .state
        .data
        .hla
        .geneTypes
        .forEach(function (gene) {
          self.fillHlaSpecific(gene.geneType, true);

          gene
            .allele1
            .forEach(function (allele) {
              $("#hla" + gene.geneType + " div.alle1 select#Predicate" + allele.alleleType)
                .val(allele.predicate)
                .change();
              $("#hla" + gene.geneType + " div.alle1 input[placeholder=" + allele.alleleType + "]").val(allele.value);
            });

          gene
            .allele2
            .forEach(function (allele) {
              $("#hla" + gene.geneType + " div.alle2 select#Predicate" + allele.alleleType)
                .val(allele.predicate)
                .change();
              $("#hla" + gene.geneType + " div.alle2 input[placeholder=" + allele.alleleType + "]").val(allele.value);
            });

        });

      $('#hla select.cityselect').prop('disabled', !this.props.canEditSchedule);
      $("#hla select.cityselect").select2({minimumResultsForSearch: Infinity});
      $("#hla input[type='text'").attr("readOnly", !this.props.canEditSchedule);

    }

    if (!this.state.data || !this.state.data.hlaGeneTypes || !this.state.data.hlaGeneTypes.length) {
      $("#hla")
        .children()
        .remove();
    }

    if (this.state.data.includeAllergy && this.state.data.includeAllergy.length) {
      $(".allergy-note").show();
    } else {
      $(".allergy-note").hide();
    }

    if (this.state.data && this.state.data.additionalTesting) {
      $(".type-add-test").show();
    } else {
      $(".type-add-test").hide();
    }

    var futureProcedureAvailability = this.state.data && this.state.data.futureProcedureAvailability;

    if (futureProcedureAvailability == "Whole Blood") {
      $("#fwhole").show();
      $("#other,#fbonemarrow,#flukopack").hide();
    } else if (futureProcedureAvailability == "Bone Marrow") {
      $("#fbonemarrow").show();
      $("#other,#fwhole,#flukopack").hide();
    } else if (futureProcedureAvailability == "LeukoPak") {
      $("#flukopack").show();
      $("#other,#fbonemarrow,#fwhole").hide();
    } else {
      $("#other").show();
      $("#fwhole,#fbonemarrow, #flukopack").hide();
    }

  }

  hasUnsavedChanges() {
    var changed = !_.isEqual(this.state.data, this.state.orgData);
    return changed;
  }

  cloneData() {
    this.setState({orgData: this.state.initData});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.schedulingRequest.status == API_STATUS.DONE && !this.loadRequestComplete && nextProps.schedulingRequest.actionType == actionTypes.GET_SCHEDULING_REQUEST) {
      this.loadRequestComplete = true;
      var schedulingRequest = jQuery.extend(true, {}, nextProps.schedulingRequest.data);

      if (schedulingRequest && schedulingRequest.includeAllergy) {
        schedulingRequest.includeAllergy = schedulingRequest
          .includeAllergy
          .split("|");
      }

      if (schedulingRequest && schedulingRequest.futureProcedureAvailability) {
        if (schedulingRequest.futureProcedureAvailability == "Whole Blood") 
          schedulingRequest.futureWbVolume = schedulingRequest.futureProcedureVolume;
        else if (schedulingRequest.futureProcedureAvailability == "Bone Marrow") 
          schedulingRequest.futureBoneMarrowVolume = schedulingRequest.futureProcedureVolume;
        else if (schedulingRequest.futureProcedureAvailability == "LeukoPak") 
          schedulingRequest.futureFullHalfPakVolume = schedulingRequest.futureProcedureVolume;
        else if (schedulingRequest.futureProcedureAvailability == "LeukoPak Mobilized") 
          schedulingRequest.futureFullPakVolume = schedulingRequest.futureProcedureVolume;
        }
      
      if (schedulingRequest.hla) {
        var hla = schedulingRequest.hla;
        schedulingRequest.hlaAlleleOption = hla.alleleMatchOption;
        schedulingRequest.hlaGeneTypes = [];
        var self = this;
        hla
          .geneTypes
          .forEach(function (gen) {
            schedulingRequest
              .hlaGeneTypes
              .push(gen.geneType);
          });
      }

      if (!schedulingRequest.hlaAlleleOption) 
        schedulingRequest.hlaAlleleOption = "either";
      
      this.setState({
        data: jQuery.extend(true, {}, schedulingRequest),
        orgData: jQuery.extend(true, {}, schedulingRequest)
      });
    } else if (nextProps.schedulingRequest.status == API_STATUS.DONE && nextProps.schedulingRequest.actionType == actionTypes.SAVE_SCHEDULING_REQUEST && this.state.processing) {
      this.setState({processing: false, orgData: this.state.data});
    } else if (nextProps.schedulingRequest.status == API_STATUS.ERROR && this.state.processing) {
      this.setState({processing: false});
    }
  }

  onInvalid(name, message) {
    const errors = this.state.errors;
    errors[name] = message;
    this.setState({errors: errors})
  }

  onValid(name) {
    const errors = this.state.errors;
    errors[name] = "";
    this.setState({errors: errors})
  }

  onChange(name, value) {
    const data = this.state.data
      ? this.state.data
      : {};

    if (name == "includeAllergy") {
      if (typeof(value) == "object") 
        value = value.join("|");
      
      value.length > 0
        ? $(".allergy-note").show()
        : $(".allergy-note").hide();

    } else if (name == "additionalTesting") {
      value != ''
        ? $(".type-add-test").show()
        : $(".type-add-test").hide();
    }

    if (name == "futureProcedureAvailability") {
      if (value == "Whole Blood") {
        $("#fwhole").show();
        $("#other,#fbonemarrow,#flukopack").hide();
      } else if (value == "Bone Marrow") {
        $("#fbonemarrow").show();
        $("#other,#fwhole,#flukopack").hide();
      } else if (value == "LeukoPak") {
        $("#flukopack").show();
        $("#other,#fbonemarrow,#fwhole").hide();
      } else {
        $("#other").show();
        $("#fwhole,#fbonemarrow, #flukopack").hide();
      }
    } else if (name == "hlaGeneTypes") {
      var preValue = data[name];
      if (!preValue) 
        preValue = [];
      var curValue = value;
      var itemsToDelete = [];
      var itemsToAdd = [];

      if (typeof(curValue) == "object") {
        curValue
          .forEach(function (ele) {
            if (!preValue.find(v => v == ele)) {
              itemsToAdd.push(ele);
            }
          });
      }

      if (typeof(preValue) == "object") {
        preValue
          .forEach(function (ele) {
            if (!curValue.find(v => v == ele)) {
              itemsToDelete.push(ele);
            }
          });
      }

      var self = this;
      itemsToAdd.forEach(function (ele) {
        self.fillHlaSpecific(ele);
      });

      itemsToDelete.forEach(function (ele) {
        $("#hla" + ele).remove();
      });
    } else if (name == "hlaAlleleOption") {
      if (value == "both") {
        $(".type_alle2").show();
        $("h4.alleTitle").each(function () {
          var t = $(this).text();
          $(this).text(t + " 1");
        });
      } else {
        $(".type_alle2").hide();
        $("h4.alleTitle").each(function () {
          var t = $(this).text();
          if (t.endsWith("1")) 
            $(this).text(t.substring(0, t.length - 2));
          }
        );
      }
    }

    if (name == "backupDonorRequired") {
      if (value == "y") {
        data[name] = true;
      } else if (value == "n") {
        data[name] = false;
      } else if (value == '') {
        data[name] = null;
      }
    } else {
      data[name] = value;
    }

    this.setState({data: data});

  }

  fillHlaSpecific(hlaGeneValue, dontCreateSelect2) {
    if ($("#hla" + hlaGeneValue).length > 0 || this.fillingHla) 
      return;
    
    this.fillingHla = true;
    var v = $("#hlaAlleleOption").val();

    var alleNumber1 = "1";
    if (!v || v == "either" || v == "homozygous") {
      var alleNumber1 = "";
    }

    var options = '<div class="geneTypeContainer" id=hla' + hlaGeneValue + '><div class="alle1" data-gene=' + hlaGeneValue + '> <div class="col-sm-24"><h4 class="alleTitle">GENE TYPE ' + hlaGeneValue + ', ALLELE ' + alleNumber1 + '</h4></div><div class="col-sm-6 form-group alle-select"><div class="col-sm-18 co' +
        'llapse-sm form-group"><select class="form-control cityselect" id="PredicateAA"><' +
        'option value="predicate">Predicate</option><option value="ignore">Ignore</option' +
        '><option value="equal">Equal</option><option value="not_equal">Not Equal</option' +
        '></select></div><div class="col-sm-6 collapse-sm alle-type form-group"><input ty' +
        'pe="text" placeholder="AA" class="form-control"/></div></div><div class="col-sm-' +
        '6 form-group alle-select"><div class="col-sm-18 collapse-sm form-group"><select ' +
        'class="form-control cityselect" id="PredicateBB"><option value="predicate">Predi' +
        'cate</option><option value="ignore">Ignore</option><option value="equal">Equal</' +
        'option><option value="not_equal">Not Equal</option></select></div><div class="co' +
        'l-sm-6 collapse-sm alle-type form-group"><input type="text" placeholder="BB" cla' +
        'ss="form-control"/></div></div><div class="col-sm-6 form-group alle-select"><div' +
        ' class="col-sm-18 collapse-sm form-group"><select class="form-control cityselect' +
        '" id="PredicateCC"><option value="predicate">Predicate</option><option value="ig' +
        'nore">Ignore</option><option value="equal">Equal</option><option value="not_equa' +
        'l">Not Equal</option></select></div><div class="col-sm-6 collapse-sm alle-type f' +
        'orm-group"><input type="text" placeholder="CC" class="form-control"/></div></div' +
        '><div class="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm ' +
        'form-group"><select class="form-control cityselect" id="PredicateDD"><option val' +
        'ue="predicate">Predicate</option><option value="ignore">Ignore</option><option v' +
        'alue="equal">Equal</option><option value="not_equal">Not Equal</option></select>' +
        '</div><div class="col-sm-6 collapse-sm alle-type form-group"><input type="text" ' +
        'placeholder="DD" class="form-control"/></div></div></div><div class="alle2  type' +
        '_alle2" data-gene=' + hlaGeneValue + '><div class="col-sm-24"><h4>GENE TYPE ' + hlaGeneValue + ', ALLELE 2</h4></div><div class="col-sm-6 form-group alle-select"><div class="co' +
        'l-sm-18 collapse-sm form-group"><select class="form-control cityselect " id="Pre' +
        'dicateAA"><option  value="predicate">Predicate</option><option value="ignore">Ig' +
        'nore</option><option value="equal">Equal</option><option value="not_equal">Not E' +
        'qual</option></select></div><div class="col-sm-6 collapse-sm alle-type form-grou' +
        'p"><input type="text" placeholder="AA" class="form-control"/></div></div><div cl' +
        'ass="col-sm-6 form-group alle-select"><div class="col-sm-18 collapse-sm form-gro' +
        'up"><select class="form-control cityselect" id="PredicateBB"><option value="pred' +
        'icate">Predicate</option><option value="ignore">Ignore</option><option value="eq' +
        'ual">Equal</option><option value="not_equal">Not Equal</option></select></div><d' +
        'iv class="col-sm-6 collapse-sm alle-type form-group"><input type="text" placehol' +
        'der="BB" class="form-control"/></div></div><div class="col-sm-6 form-group alle-' +
        'select"><div class="col-sm-18 form-group collapse-sm"><select class="form-contro' +
        'l cityselect" id="PredicateCC"><option value="predicate">Predicate</option><opti' +
        'on value="ignore">Ignore</option><option value="equal">Equal</option><option val' +
        'ue="not_equal">Not Equal</option></select></div><div class="col-sm-6 collapse-sm' +
        ' alle-type form-group"><input type="text" placeholder="CC" class="form-control"/' +
        '></div></div><div class="col-sm-6 form-group alle-select"><div class="col-sm-18 ' +
        'collapse-sm form-group"><select class="form-control cityselect" id="PredicateDD"' +
        '><option value="predicate">Predicate</option><option value="ignore">Ignore</opti' +
        'on><option value="equal">Equal</option><option value="not_equal">Not Equal</opti' +
        'on></select></div><div class="col-sm-6 collapse-sm alle-type form-group"><input ' +
        'type="text" placeholder="DD" class="form-control"/></div></div></div></div>';

    $('#hla').append(options);

    if ($("#hlaAlleleOption").val() == "both") {
      $(".type_alle2").show();
    } else {
      $(".type_alle2").hide();
    }

    if (!dontCreateSelect2) {
      $("#hla .cityselect").select2({minimumResultsForSearch: Infinity});
    }

    this.fillingHla = false;
  }

  showErrors()
  {
    $(".error-list").show();
    //scroll top
    $('html, body').animate({
      scrollTop: 200
    }, 700);
  }

  onSave(proxy, event, fromParent) {
    const data = this.state.data;

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (data && data[name]) {
          controls[name].validateAllRules(name, data[name], fromParent);
        } else {
          controls[name].validateAllRules(name, '', fromParent);
        }

      });

    const errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })

    var d = jQuery.extend(true, {}, this.state.data);
    if (d && d.includeAllergy && d.includeAllergy.constructor === Array) {
      d.includeAllergy = d
        .includeAllergy
        .join("|");
    }

    //send only data of this tab for parent to combine
    var newData = {};
    newData.donationCountLimit = d.donationCountLimit;
    newData.fastingDays = d.fastingDays;
    newData.backupDonorRequired = d.backupDonorRequired;
    newData.includeAllergy = d.includeAllergy;
    newData.allergyNote = d.allergyNote;
    newData.additionalTesting = d.additionalTesting;
    newData.additionalTestingType = d.additionalTestingType;
    newData.futureProcedureAvailability = d.futureProcedureAvailability;

    if (newData.futureProcedureAvailability == "Whole Blood") 
      newData.futureProcedureVolume = d.futureWbVolume;
    else if (newData.futureProcedureAvailability == "Bone Marrow") 
      newData.futureProcedureVolume = d.futureBoneMarrowVolume;
    else if (newData.futureProcedureAvailability == "LeukoPak") 
      newData.futureProcedureVolume = d.futureFullHalfPakVolume;
    else if (newData.futureProcedureAvailability == "LeukoPak Mobilized") 
      newData.futureProcedureVolume = d.futureFullPakVolume;
    
    newData.futureProcedureWoNumber = d.futureProcedureWoNumber;
    newData.futureProcedureDate = d.futureProcedureDate;
    newData.hlaGeneTypes = null; // d.hlaGeneTypes;
    newData.hlaAlleleOption = null; // d.hlaAlleleOption;

    if (!fromParent) {
      if (numError == 0) {
        this.setState({processing: true});
        this.buildHla(newData);
        this
          .props
          .onSave("Tab3", newData);
      } else {
        this.showErrors();
      }
    } else {
      if (numError == 0) {
        this.setState({processing: true});
        this.buildHla(newData);
        return {tab: "Tab3", data: newData};
      } else {
        return false;
      }
    }

  }

  buildHla(data) {
    var hla = {};
    var reactComponent = this;
    var v = $("#hlaAlleleOption").val();
    if (!v || !$("div.alle1").length) {
      hla = null;
    } else {
      hla.alleleMatchOption = v;
      hla.geneTypes = [];

      $("div.alle1").each(function () {
        var result = {};
        result.geneType = $(this).attr("data-gene");
        result.allele1 = [];
        $(this)
          .find(".form-group.alle-select")
          .each(function () {
            reactComponent.buildAlle($(this), result, true);
          });

        hla
          .geneTypes
          .push(result);
      });

      if (hla.alleleMatchOption == "both") {
        var index = 0;
        $("div.alle2").each(function () {
          var result = hla.geneTypes[index];
          result.geneType = $(this).attr("data-gene");
          result.allele2 = [];
          $(this)
            .find(".form-group.alle-select")
            .each(function () {
              reactComponent.buildAlle($(this), result, false);
            });
          index++;
        });
      }

      if (hla.geneTypes.length == 0) {
        hla = null;
      }

    }
    data.hla = hla;
  }

  buildAlle(alleHtmlObject, result, addToAllele1)
  {
    if (alleHtmlObject.find("select#PredicateAA").length) {
      this.buildAlleGene(alleHtmlObject, "AA", result, addToAllele1);
    } else if (alleHtmlObject.find("select#PredicateBB").length) {
      this.buildAlleGene(alleHtmlObject, "BB", result, addToAllele1);
    } else if (alleHtmlObject.find("select#PredicateCC").length) {
      this.buildAlleGene(alleHtmlObject, "CC", result, addToAllele1);
    } else if (alleHtmlObject.find("select#PredicateDD").length) {
      this.buildAlleGene(alleHtmlObject, "DD", result, addToAllele1);
    }
  }

  buildAlleGene(alleHtmlObject, gene, result, addToAllele1)
  {
    gene = gene.replace("|", "");
    var alleleData = {
      alleleType: gene
    };
    alleleData.predicate = alleHtmlObject
      .find("select#Predicate" + gene + " option:checked")
      .val()
      .toLowerCase()
      .replace(" ", "_");
    if (alleleData.predicate != "predicate") {
      alleleData.value = alleHtmlObject
        .find("input")
        .val()
        .toLowerCase();

      if (addToAllele1) {
        result
          .allele1
          .push(alleleData);
      } else {
        result
          .allele2
          .push(alleleData);
      }
    }
  }

  clearData() {
    this.setState({
      data: jQuery.extend(true, {}, this.state.initData),
      orgData: jQuery.extend(true, {}, this.state.initData),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].clearError();
      });
  }

  onCancel() {
    this.setState({
      data: jQuery.extend(true, {}, this.state.orgData),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].clearError();
      });
  }

  render() {
    var cssHideSave = this.props.canEditSchedule === false
      ? " hide"
      : "";
    return (

      <div id="criteria2" className="tab-pane fade in">
        <ErrorArea errorList={this.state.errors}/>
        <div className="donor-form">
          <div className="col-sm-24">
            <h4>Limitations/Specifications</h4>
          </div>

          <InputControl
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["donationCountLimit"] = input
          }}
            name="donationCountLimit"
            readOnly={!this.props.canEditSchedule}
            label="Lifetime Donation limit"
            validate={validations.age}
            errorMessage="The &#39;Lifetime Donation limit&#39; should be number. Please check the Lifetime Donation limit and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="2"
            value={this.state.data && this.state.data.donationCountLimit
            ? this.state.data.donationCountLimit
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["fastingDays"] = input
          }}
            name="fastingDays"
            readOnly={!this.props.canEditSchedule}
            label="Fasting day required"
            validate={validations.number}
            errorMessage="The &#39;Fasting day required&#39; should be number. Please check the Fasting day required and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="3"
            value={this.state.data && this.state.data.fastingDays
            ? this.state.data.fastingDays
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            type="select"
            showSelectOption={true}
            name="backupDonorRequired"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["backupDonorRequired"] = input
          }}
            value={this.state.data && this.state.data.backupDonorRequired
            ? 'y'
            : (!this.state.data || this.state.data.backupDonorRequired == null)
              ? ''
              : 'n'}
            label="Backup Donor Needed"
            labelClass="ucase"
            inputClass="form-control cityselect showhidedrop"
            optionValues={[
            [
              "Yes", "y"
            ],
            ["No", "n"]
          ]}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>
          <div className="col-sm-24 collapse-sm">
            <InputControl
              containerClass="col-sm-6 form-group"
              type="select"
              multiple
              name="includeAllergy"
              readOnly={!this.props.canEditSchedule}
              ref
              ={(input) => {
              this.inputControls["includeAllergy"] = input
            }}
              label="Allergy Restrictions"
              labelClass="ucase"
              inputClass="form-control allergyselectpicker"
              optionValues={[
              [
                "Food", "food"
              ],
              [
                "Environment", "environmental"
              ],
              ["Medication", "medication"]
            ]}
              selectedValues={this.state.data && this.state.data.includeAllergy
              ? this.state.data.includeAllergy
              : ''}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>

            <InputControl
              containerClass="col-sm-12 form-group allergy-note"
              ref
              ={(input) => {
              this.inputControls["allergyNote"] = input
            }}
              name="allergyNote"
              label="Allergy Restrictions Note"
              labelClass="ucase"
              inputClass="form-control"
              maxLength="100"
              value={this.state.data && this.state.data.allergyNote
              ? this.state.data.allergyNote
              : ''}
              onChange={this.onChange}
              onInvalid={this.onInvalid}
              onValid={this.onValid}/>
          </div>

          <div className="col-sm-24">
            <h4>Additional Testing Procedure</h4>
          </div>

          <InputControl
            containerClass="col-sm-6 form-group"
            type="select"
            name="additionalTesting"
            readOnly={!this.props.canEditSchedule}
            showSelectOption={true}
            ref
            ={(input) => {
            this.inputControls["additionalTesting"] = input
          }}
            label="Additional Testing Needed"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={[
            [
              "Prior to collection", "prior_to_collection"
            ],
            [
              "During collection", "during_collection"
            ],
            ["Post collection", "post_collection"]
          ]}
            value={this.state.data && this.state.data.additionalTesting
            ? this.state.data.additionalTesting
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group type-add-test "
            type="select"
            showSelectOption={true}
            name="additionalTestingType"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["additionalTestingType"] = input
          }}
            label="additional Test Type"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={[
            [
              "Expanded Virals", "expanded_virals"
            ],
            [
              "Clinical Grade Virals", "clinical_grade_virals"
            ],
            ["Other", "other"]
          ]}
            value={this.state.data && this.state.data.additionalTestingType
            ? this.state.data.additionalTestingType
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            type="select"
            showSelectOption={true}
            containerId="futuretest"
            name="futureProcedureAvailability"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["futureProcedureAvailability"] = input
          }}
            label="Future Procedure Availability"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={[
            [
              "Whole Blood", "Whole Blood"
            ],
            [
              "Bone Marrow", "Bone Marrow"
            ],
            [
              "LeukoPak", "LeukoPak"
            ],
            ["Mobilized LeukoPak", "LeukoPak Mobilized"]
          ]}
            value={this.state.data && this.state.data.futureProcedureAvailability
            ? this.state.data.futureProcedureAvailability
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerId="fwhole"
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["futureWbVolume"] = input
          }}
            name="futureWbVolume"
            readOnly={!this.props.canEditSchedule}
            label="Future Procedure Volume"
            labelClass="ucase"
            inputClass="form-control"
            maxLength="3"
            value={this.state.data && this.state.data.futureWbVolume
            ? this.state.data.futureWbVolume
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerId="fbonemarrow"
            containerClass="col-sm-6 form-group"
            type="select"
            showSelectOption={true}
            ref
            ={(input) => {
            this.inputControls["futureBoneMarrowVolume"] = input
          }}
            name="futureBoneMarrowVolume"
            label="Future Procedure Volume"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={[
            [
              "50 ML", "50"
            ],
            ["100 ML", "100"]
          ]}
            value={this.state.data && this.state.data.futureBoneMarrowVolume
            ? this.state.data.futureBoneMarrowVolume
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerId="flukopack"
            containerClass="col-sm-6 form-group"
            type="select"
            showSelectOption={true}
            ref
            ={(input) => {
            this.inputControls["futureFullHalfPakVolume"] = input
          }}
            name="futureFullHalfPakVolume"
            readOnly={!this.props.canEditSchedule}
            label="Future Procedure Volume"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={[
            [
              "Half", "Half Pak"
            ],
            ["Full", "Full Pak"]
          ]}
            value={this.state.data && this.state.data.futureFullHalfPakVolume
            ? this.state.data.futureFullHalfPakVolume
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerId="other"
            containerClass="col-sm-6 form-group"
            type="select"
            showSelectOption={true}
            ref
            ={(input) => {
            this.inputControls["futureFullPakVolume"] = input
          }}
            name="futureFullPakVolume"
            readOnly={!this.props.canEditSchedule}
            label="Future Procedure Volume"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={[
            ["Full", "Full Pak"]
          ]}
            value={this.state.data && this.state.data.futureFullPakVolume
            ? this.state.data.futureFullPakVolume
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["futureProcedureWoNumber"] = input
          }}
            name="futureProcedureWoNumber"
            readOnly={!this.props.canEditSchedule}
            label="Future Work Order #"
            labelClass="ucase"
            inputClass="form-control"
            maxLength="10"
            validate={validations.number}
            errorMessage="The &#39;Future Work Order #&#39; should be number. Please check the Future Work Order # and enter again."
            value={this.state.data && this.state.data.futureProcedureWoNumber
            ? this.state.data.futureProcedureWoNumber
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            ref
            ={(input) => {
            this.inputControls["futureProcedureDate"] = input
          }}
            type="date"
            name="futureProcedureDate"
            readOnly={!this.props.canEditSchedule}
            label="future Procedure Date"
            errorMessage="The &#39;Future Procedure Date&#39; does not seem to be valid. Please check and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="10"
            value={this.state.data && this.state.data.futureProcedureDate
            ? this.state.data.futureProcedureDate
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <div className="col-sm-24">
            <h4>HLA</h4>
          </div>

          <InputControl
            containerClass="col-sm-6 form-group"
            type="select"
            multiple
            name="hlaGeneTypes"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["hlaGeneTypes"] = input
          }}
            label="HLA Gene Type"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={[
            [
              "A", "A"
            ],
            [
              "B", "B"
            ],
            [
              "C", "C"
            ],
            [
              "DPA1", "DPA1"
            ],
            [
              "DPB1", "DPB1"
            ],
            [
              "DQA1", "DQA1"
            ],
            [
              "DQB1", "DQB1"
            ],
            [
              "DRB1", "DRB1"
            ],
            [
              "DRB3", "DRB3"
            ],
            [
              "DRB4", "DRB4"
            ],
            ["DRB5", "DRB5"]
          ]}
            selectedValues={this.state.data && this.state.data.hlaGeneTypes
            ? this.state.data.hlaGeneTypes
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <InputControl
            containerClass="col-sm-6 form-group"
            type="select"
            name="hlaAlleleOption"
            readOnly={!this.props.canEditSchedule}
            ref
            ={(input) => {
            this.inputControls["hlaAlleleOption"] = input
          }}
            label="Allele Option"
            labelClass="ucase"
            inputClass="form-control"
            optionValues={[
            [
              "Either Allele", "either"
            ],
            [
              "Homozygous Alleles", "homozygous"
            ],
            ["Both Alleles", "both"]
          ]}
            value={this.state.data && this.state.data.hlaAlleleOption
            ? this.state.data.hlaAlleleOption
            : 'either'}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <div className="col-sm-24 collapse-sm" id="hla"></div>

          <div className="col-sm-24">
            <button
              className={"btn btn-default" + cssHideSave}
              disabled={this.state.processing}
              onClick={this.onSave}>Save</button>
            <button
              onClick={this.onCancel}
              disabled={this.state.processing}
              className={"btn btn-default btn-hollow" + cssHideSave}>Cancel</button>
          </div>

        </div>
      </div>
    )
  }
}

export default CriteriaThirdSection