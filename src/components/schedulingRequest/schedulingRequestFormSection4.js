import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import * as validations from '../../validations/common'
import {API_STATUS} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import isEqual from 'lodash';

class CriteriaFourSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: jQuery.extend(true, {}, this.props.schedulingRequest.data),
      initData: jQuery.extend(true, {}, this.props.schedulingRequest.data),
      orgData: jQuery.extend(true, {}, this.props.schedulingRequest.data),
      errors: {},
      processing: false
    }

    this.onInvalid = this
      .onInvalid
      .bind(this);
    this.onValid = this
      .onValid
      .bind(this);
    this.onChange = this
      .onChange
      .bind(this);

    this.onSave = this
      .onSave
      .bind(this);
    this.onCancel = this
      .onCancel
      .bind(this);

    this.inputControls = {};

  }

  hasUnsavedChanges() {
    var changed = !_.isEqual(this.state.data, this.state.orgData);
    return changed;
  }

  cloneData() {
    this.setState({orgData: this.state.initData});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.schedulingRequest.status == API_STATUS.DONE && !this.loadRequestComplete && nextProps.schedulingRequest.actionType == actionTypes.GET_SCHEDULING_REQUEST) {
      this.loadRequestComplete = true;
      this.setState({
        data: jQuery.extend(true, {}, nextProps.schedulingRequest.data),
        orgData: jQuery.extend(true, {}, nextProps.schedulingRequest.data)
      })
    } else if (nextProps.schedulingRequest.status == API_STATUS.DONE && nextProps.schedulingRequest.actionType == actionTypes.SAVE_SCHEDULING_REQUEST && this.state.processing) {
      this.setState({processing: false, orgData: this.state.data});
    } else if (nextProps.schedulingRequest.status == API_STATUS.ERROR && this.state.processing) {
      this.setState({processing: false});
    }
  }

  onInvalid(name, message) {
    const errors = this.state.errors;
    errors[name] = message;
    this.setState({errors: errors})
  }

  onValid(name) {
    const errors = this.state.errors;
    errors[name] = "";
    this.setState({errors: errors})
  }

  onChange(name, value) {
    const data = this.state.data;
    data[name] = value;
    this.setState({donor: data});
  }

  onSave(proxy, event, fromParent) {
    const data = this.state.data;

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        if (data && data[name]) {
          controls[name].validateAllRules(name, data[name], fromParent);
        } else {
          controls[name].validateAllRules(name, '', fromParent);
        }

      });

    const errorList = this.state.errors;
    var errorArray = Object.keys(errorList);
    var numError = 0;
    errorArray.map(function (key) {
      if (errorList[key] != "") {
        numError++;
      }
    })

    //send only data of this tab for parent to combine
    var newData = {};
    newData.additionalNotes = this.state.data && this.state.data.additionalNotes
      ? this.state.data.additionalNotes
      : '';

    if (!fromParent) {
      if (numError == 0) {
        this.setState({processing: true});
        this
          .props
          .onSave("Tab4", newData);
      } else {
        $(".error-list").show();
        //scroll top
        $('html, body').animate({
          scrollTop: 200
        }, 700);
      }
    } else {
      if (numError == 0) {
        this.setState({processing: true});
        return {tab: "Tab4", data: newData};
      } else {
        return false;
      }
    }

  }

  clearData() {
    this.setState({
      data: jQuery.extend(true, {}, this.state.initData),
      orgData: jQuery.extend(true, {}, this.state.initData),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].clearError();
      });
  }

  onCancel() {
    this.setState({
      data: jQuery.extend(true, {}, this.state.orgData),
      errors: {},
      processing: false
    });

    var controls = this.inputControls;
    Object
      .keys(controls)
      .forEach(function (name) {
        controls[name].clearError();
      });
  }

  render() {
    var cssHideSave = this.props.canEditSchedule === false
      ? " hide"
      : "";
    return (

      <div id="criteria3" className="tab-pane fade in">
        <ErrorArea errorList={this.state.errors}/>
        <div className="donor-form">

          <InputControl
            containerClass="col-sm-24 form-group"
            type="textarea"
            ref
            ={(input) => {
            this.inputControls["additionalNotes"] = input
          }}
            name="additionalNotes"
            readOnly={!this.props.canEditSchedule}
            label="Additional Notes"
            errorMessage="The &#39;Age&#39; should be number. Please check the Age and enter again."
            labelClass="ucase"
            inputClass="form-control"
            maxLength="100"
            value={this.state.data && this.state.data.additionalNotes
            ? this.state.data.additionalNotes
            : ''}
            onChange={this.onChange}
            onInvalid={this.onInvalid}
            onValid={this.onValid}/>

          <div className="col-sm-24">
            <button
              className={"btn btn-default" + cssHideSave}
              disabled={this.state.processing}
              onClick={this.onSave}>Save</button>
            <button
              onClick={this.onCancel}
              disabled={this.state.processing}
              className={"btn btn-default btn-hollow" + cssHideSave}>Cancel</button>
          </div>

        </div>
      </div>
    )
  }
}

export default CriteriaFourSection