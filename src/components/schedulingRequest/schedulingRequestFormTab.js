import React, {Component} from 'react';

class SchedulingRequestTab extends Component {
    constructor(props) {
        super(props);

    }

    showTab(id)
    {
        $("#schedulingRequestTabs .tab-pil").removeClass("active");
        $("#schedulingRequestTabs #" + id).tab('show');
        $("#schedulingRequestTabs #" + id).addClass("active");
    }

    render() {
        return (
            <div id="schedulingRequestTabs">
                <div className="col-sm-6">
                    <div id="step1" className="tab-pil active" data-toggle="tab" href="#criteria0">
                        <div className="step">1</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createschedulingrequest/icon-cs-1.svg"
                                alt="Criteria Section Icon"/>
                        </div>
                        <p className="ucase">Criteria Section</p>
                    </div>
                </div>

                <div className="col-sm-6">
                    <div  id ="step2" className="tab-pil " data-toggle="tab" href="#criteria1">
                        <div className="step">2</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createschedulingrequest/icon-cs-2.svg"
                                alt="Criteria Section Icon"/>
                        </div>
                        <p className="ucase">Criteria Section</p>
                    </div>
                </div>

                <div className="col-sm-6">
                    <div  id ="step3"  className="tab-pil " data-toggle="tab" href="#criteria2">
                        <div className="step">3</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createschedulingrequest/icon-cs-3.svg"
                                alt="Criteria Section Icon"/>
                        </div>
                        <p className="ucase">Criteria Section</p>
                    </div>
                </div>

                <div className="col-sm-6">
                    <div  id ="step4" className="tab-pil " data-toggle="tab" href="#criteria3">
                        <div className="step">4</div>
                        <div className="icon">
                            <img
                                src="/img/icons/createschedulingrequest/icon-cs-4.svg"
                                alt="Criteria Section Icon"/>
                        </div>
                        <p className="ucase">Criteria Section</p>
                    </div>
                </div>

            </div>

        );
    }
}

export default SchedulingRequestTab;
