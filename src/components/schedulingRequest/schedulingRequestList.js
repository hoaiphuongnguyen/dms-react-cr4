import React, {Component} from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import {hashHistory} from 'react-router';
import {API_STATUS, PATH_PREFIX} from '../../common/constants';
import {displayDate3} from '../../utils/date.js';

class SchedulingRequestList extends Component {

    constructor(props) {
        super(props);

        this.items = this.props.list.items;

        this.state = {
            data: this.items,
            showOnlyUnProcessRequest: false,
            keyword: '',
            sortName: this.props.list.sortBy,
            sortOrder: this.props.list.sortOrder
        };

        this.createShowOnlyUnProcessRequestCheckBox = this
            .createShowOnlyUnProcessRequestCheckBox
            .bind(this);

        this.onToogleShowOnlyUnProcessRequest = this
            .onToogleShowOnlyUnProcessRequest
            .bind(this);

        this.onShowMore = this
            .onShowMore
            .bind(this);

        this
            .props
            .actions
            .loadSchedulingRequestList(this.state.showOnlyUnProcessRequest, 1, this.props.list.pageSize, this.props.list.sortBy, this.props.list.sortOrder, true);
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('process-scheduling-request-first');
        document
            .body
            .classList
            .add('inner');

        document.title = 'Process Scheduling Request';
    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('process-scheduling-request-first');
        document
            .body
            .classList
            .remove('inner');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.list.needRefresh) {
            this
                .props
                .actions
                .loadSchedulingRequestList(this.state.showOnlyUnProcessRequest, 1, nextProps.list.pageSize, nextProps.list.sortBy, nextProps.list.sortOrder, true);
        } else {
            this.items = nextProps.list.items;

            this.setState({data: this.items, sortName: this.props.list.sortBy, sortOrder: this.props.list.sortOrder});
        }

    }

    onShowMore() {
        if (this.props.list.items.length < this.props.list.totalRecords) {
            this
                .props
                .actions
                .loadSchedulingRequestList(this.state.showOnlyUnProcessRequest, ++this.props.list.page, this.props.list.pageSize, this.props.list.sortBy, this.props.list.sortOrder, false);
        }
    }

    onSortChange(sortName, sortOrder) {
        this
            .props
            .actions
            .loadSchedulingRequestList(this.state.showOnlyUnProcessRequest, 1, this.props.list.pageSize, sortName, sortOrder, true);
    }

    buttonProcessRequest(cell, row) {
        if (row.donorId) {
            return null;
        }
        var f = function (row) {
            hashHistory.push(PATH_PREFIX + '/schedule-request/process-schedule-request/' + row.id);
        };
        return (
            <a
                href="javascript:void(0)"
                data-toggle="tooltip"
                title="Process Request"
                onClick={function () {
                f(row);
            }}>
                <img
                    className="icon-svg"
                    src="/img/icons/processschedulingrequest/icon-process-request.svg"
                    alt="edit"
                    width="18"/></a>
        );
    }

    buttonEdit(cell, row) {
        var f = function (row) {
            hashHistory.push(PATH_PREFIX + '/schedule-request/edit-schedule-request/' + row.id);
        };
        return (
            <a
                href="javascript:void(0)"
                data-toggle="tooltip"
                title="Edit Request"
                onClick={function () {
                f(row);
            }}>
                <img
                    className="icon-svg"
                    src="/img/icons/processschedulingrequest/icon-edit-scheduling-request.svg"
                    alt="edit"
                    width="18"/></a>
        );
    }

    displayCollectionSite(collestionSite) {
        return collestionSite == 'CA'
            ? 'California'
            : 'Massachusetts';
    }

    onToogleShowOnlyUnProcessRequest() {

        this.setState({
            showOnlyUnProcessRequest: $("#onlyUnProcessRequest").is(':checked')
        })

        this
            .props
            .actions
            .loadSchedulingRequestList($("#onlyUnProcessRequest").is(':checked'), 1, this.props.list.pageSize, this.props.list.sortBy, this.props.list.sortOrder, true);

    }
    createShowOnlyUnProcessRequestCheckBox() {
        var self = this;
        var f = function () {
            self.onToogleShowOnlyUnProcessRequest();
        };

        return (
            <label className="showUnProcessRequest">
                <input
                    type="checkbox"
                    id="onlyUnProcessRequest"
                    name="onlyUnProcessRequest"
                    onChange={this.onToogleShowOnlyUnProcessRequest}
                    data-group="onlyUnProcessRequest"/>
                <span>Exclude Processed Requests</span>
            </label>
        );
    }

    render() {

        let options = {
            exportCSVBtn: this.createShowOnlyUnProcessRequestCheckBox,
            onSortChange: this
                .onSortChange
                .bind(this)
        };

        let table = null;
        let showMore = null;
        if (this.props.list.items) {
            table = <BootstrapTable
                ref='table'
                data={this.state.data}
                options={options}
                exportCSV
                search
                tableContainerClass="donortable"
                bordered={false}>
                <TableHeaderColumn
                    dataField='internalCollectionTime'
                    dataFormat={displayDate3}
                    isKey
                    dataSort>Collection Date</TableHeaderColumn>
                <TableHeaderColumn dataField='id' dataSort>Request</TableHeaderColumn>
                <TableHeaderColumn dataField='donorId' dataSort>Donor Id</TableHeaderColumn>
                <TableHeaderColumn dataField='procedureInfoName' dataSort>Procedure</TableHeaderColumn>
                <TableHeaderColumn dataField='woNumber' dataSort>Work Order</TableHeaderColumn>

                <TableHeaderColumn
                    width='140px'
                    dataField='collectionSite'
                    dataFormat={this.displayCollectionSite}
                    dataSort>COLLECTION SITE</TableHeaderColumn>

                <TableHeaderColumn
                    dataField="button"
                    width='5%'
                    dataFormat={this
                    .buttonProcessRequest
                    .bind(this)}
                    dataAlign="center"
                    headerTitle={false}>&nbsp;</TableHeaderColumn>
                <TableHeaderColumn
                    dataField="button"
                    width='5%'
                    dataFormat={this
                    .buttonEdit
                    .bind(this)}
                    dataAlign="center"
                    headerTitle={false}>&nbsp;</TableHeaderColumn>
            </BootstrapTable>

            if (this.props.list.items.length < this.props.list.totalRecords) {
                showMore = <div className="col-sm-24 text-right showmore hide-table">
                    <a href="javascript:void(0)" onClick={this.onShowMore}>Show More</a>
                </div>

            }

        }

        return (
            <div id="schedule-appointment" className="clearfix">

                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>
                            <a href="javascript:void(0)">Manage Schedule Requests</a>
                        </li>
                        <li className="active">Process Scheduling Request</li>
                    </ol>
                </div>

                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Process Scheduling Request</h2>
                    </div>
                </div>

                <div className="search-wrapper">

                    <div
                        className="col-xs-24 col-sm-24 col-md-24 col-lg-24 collapse-sm table-responsive">
                        {table}
                    </div>

                    {showMore}
                    <div className="clear"></div>

                </div>

            </div>

        );
    }

}

export default SchedulingRequestList;