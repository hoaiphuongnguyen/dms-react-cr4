import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import * as validations from '../../validations/common'
import {API_STATUS, PATH_PREFIX} from '../../common/constants';
import * as actionTypes from '../../constants/actionTypes';
import {hashHistory, Router} from 'react-router';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import ErrorArea from '../common/errorArea'
import {displayDate,displayDate3} from '../../utils/date.js'

class SearchSchedulingRequestForm extends Component {
    constructor(props) {
        super(props);

        this.items = this.props.searchSchedulingRequest.items;

        this.state = {
            searchParams: {},
            errors: {},
            sortName: this.props.searchSchedulingRequest.sortBy,
            sortOrder: this.props.searchSchedulingRequest.sortOrder
        };

        this.onShowMore = this
            .onShowMore
            .bind(this);

        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);

        this.onChange = this
            .onChange
            .bind(this);

        this.onSearch = this
            .onSearch
            .bind(this);

        this.inputControls = {};
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.searchSchedulingRequest.actionType == "CHANG_ROUTE") {
            this.clearfields();
        } else {
            this.items = nextProps.searchSchedulingRequest.items;
            this.setState({data: this.items, errors: {}, sortName: this.props.searchSchedulingRequest.sortBy, sortOrder: this.props.searchSchedulingRequest.sortOrder});
        }
    }

    clearfields() {
        this.setState({searchParams: {}, errors: {}, sortName: this.props.searchSchedulingRequest.sortBy, sortOrder: this.props.searchSchedulingRequest.sortOrder});

        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (name) {
                controls[name].clearError();
            });

        this
            .props
            .actions
            .clearSearchParams();
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {
        const searchParams = this.state.searchParams;
        searchParams[name] = value
            ? value.trim()
            : '';
        this.setState({searchParams: searchParams});
    }

    onSearch() {

        const searchParams = this.state.searchParams;
        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (key) {
                controls[key].validateAllRules(key, searchParams[key]);
            });

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })

        if (numError == 0) {
            this
                .props
                .actions
                .searchSchedulingRequest(this.state.searchParams, 1, this.props.searchSchedulingRequest.pageSize, this.props.searchSchedulingRequest.sortBy, this.props.searchSchedulingRequest.sortOrder, true);

        } else {
            $(".error-list").show();
        }
    }

    onShowMore() {
        if (this.props.searchSchedulingRequest.items.length < this.props.searchSchedulingRequest.totalRecords) {
            this
                .props
                .actions
                .searchSchedulingRequest(this.state.searchParams, ++this.props.searchSchedulingRequest.page, this.props.searchSchedulingRequest.pageSize, this.props.searchSchedulingRequest.sortBy, this.props.searchSchedulingRequest.sortOrder, false);
        }
    }

    onSortChange(sortName, sortOrder) {
        if (sortName == "procedure") 
            sortName = "procedureInfo.Name";
        this
            .props
            .actions
            .searchSchedulingRequest(this.state.searchParams, 1, this.props.searchSchedulingRequest.pageSize, sortName, sortOrder, true);
    }

    buttonProcessRequest(cell, row) {
        if (row.status != "open") {
            return null;
        }
        //disable processing button for DID Only
        if(this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id > 2) {
            return null;
        }


        var f = function (row) {
            hashHistory.push(PATH_PREFIX + '/schedule-request/process-schedule-request/' + row.id);
        };
        return (
            <a
                href="javascript:void(0)"
                data-toggle="tooltip"
                title="Process Request"
                onClick={function () {
                f(row);
            }}>
                <img
                    className="icon-svg"
                    src="/img/icons/processschedulingrequest/icon-process-request.svg"
                    alt="edit"
                    width="18"/></a>
        );
    }

    buttonEdit(cell, row) {

        var readOnly = (this.props.user == null || this.props.user.role == null || !this.props.user.role.id || this.props.user.role.id > 3);

        var iconSrc = readOnly
            ? "icon-view.svg"
            : "icon-edit.svg";
        iconSrc = "/img/icons/searchdonaricons/" + iconSrc

        var titleEditRequest = readOnly
            ? "View Request"
            : "Edit Request";

        var f = function (row) {
            hashHistory.push(PATH_PREFIX + '/schedule-request/edit-schedule-request/' + row.id);
        };
        return (
            <a
                href="javascript:void(0)"
                data-toggle="tooltip"
                title={titleEditRequest}
                onClick={function () {
                f(row);
            }}>
                <img className="icon-svg" src={iconSrc} alt="edit" width="18"/></a>
        );
    }

    displayCollectionSite(collestionSite) {
        return collestionSite == 'CA'
            ? 'California'
            : 'Massachusetts';
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('search-scheduling-request');
        document
            .body
            .classList
            .add('inner');

        document.title = 'Search Scheduling Requests';
        var self = this;
        $(".search-wrapper.search-scheduling-request input").keypress(function (e) {
            if (e.keyCode == 13) {
                self.onSearch();
            }
        })

    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('search-scheduling-request');
        document
            .body
            .classList
            .remove('inner');
    }

    renderSeachResult() {
        let options = {
            onSortChange: this
                .onSortChange
                .bind(this)
        };

        if (this.props.searchSchedulingRequest.items && this.props.searchSchedulingRequest.items.length) {
            let showMore = null;
            if (this.props.searchSchedulingRequest.items.length < this.props.searchSchedulingRequest.totalRecords) {
                showMore = <div className="text-right showmore">
                    <a
                        className="showmore_rows"
                        href="javascript:void(0)"
                        onClick={this.onShowMore}>Show More</a>
                </div>
            }
            return (
                <div className="clearfix" data-dropdown-for="search-btn">
                    <div className="col-sm-24">
                        <h4>Matched Schedule Requests</h4>
                    </div>

                    <div className="clear"></div>
                    <div className="searchResultsContainer">
                        <BootstrapTable
                            ref='table'
                            data={this.items}
                            options={options}
                            search={false}
                            tableContainerClass="donortable"
                            bordered={false}>

                            <TableHeaderColumn dataField='id' isKey dataSort>SCHEDULE Request#</TableHeaderColumn>
                            <TableHeaderColumn dataField='woNumber' dataSort>WORK ORDER</TableHeaderColumn>
                            <TableHeaderColumn dataField='procedure' dataSort>Procedure</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='internalCollectionTime'
                                dataFormat={displayDate3}
                                dataSort>REQUEST COLLECTION DATE</TableHeaderColumn>
                            <TableHeaderColumn dataField='status' dataSort>Status</TableHeaderColumn>

                            <TableHeaderColumn dataField='createdAt' dataFormat={displayDate} dataSort>Created On</TableHeaderColumn>
                            <TableHeaderColumn
                                width='140px'
                                dataField='collectionSite'
                                dataFormat={this.displayCollectionSite}
                                dataSort>COLLECTION SITE</TableHeaderColumn>

                            <TableHeaderColumn
                                dataField="button"
                                width='5%'
                                dataFormat={this
                                .buttonProcessRequest
                                .bind(this)}
                                dataAlign="center"
                                headerTitle={false}>&nbsp;</TableHeaderColumn>

                            <TableHeaderColumn
                                dataField="button"
                                width='5%'
                                dataFormat={this
                                .buttonEdit
                                .bind(this)}
                                dataAlign="center"
                                headerTitle={false}>&nbsp;</TableHeaderColumn>

                        </BootstrapTable>
                        <div className="clear"></div>

                        {showMore}
                    </div>
                </div>

            );

        }
        return null;
    }

    render() {

        var errText = null;
        if (this.props.searchSchedulingRequest.status == API_STATUS.ERROR) {
            errText = (
                <span
                    id="errorSpan"
                    style={{
                    color: 'red',
                    position: 'relative',
                    top: '15px',
                    marginRight: '15px'
                }}>Error while performing search.Please try again.</span>
            );
        } else if (this.props.searchSchedulingRequest.status == API_STATUS.DONE && this.props.searchSchedulingRequest.items.length == 0) {
            errText = (
                <span
                    id="errorSpan"
                    style={{
                    color: 'red',
                    position: 'relative',
                    top: '15px',
                    marginRight: '15px'
                }}>No result found.</span>
            );
        }

        return (
            <div id="schedule-appointment" className="clearfix">

                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>
                            <a href="javascript:void(0)">
                                Manage Schedule Requests</a>
                        </li>
                        <li className="active">Search Scheduling Requests</li>
                    </ol>
                </div>

                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Search Scheduling Requests</h2>
                    </div>

                </div>

                <div className="search-wrapper search-scheduling-request">
                    <ErrorArea errorList={this.state.errors}/>

                    <InputControl
                        containerClass="col-sm-8 form-group"
                        ref
                        ={(input) => {
                        this.inputControls["id"] = input
                    }}
                        type="search"
                        name="id"
                        label="Schedule request Number"
                        validate={validations.number}
                        errorMessage="The &#39;Schedule request Number&#39; should be number. Please check the Schedule request Number and enter again."
                        labelClass="ucase"
                        inputClass="form-control search"
                        maxLength="9"
                        value={this.state.searchParams.id}
                        onChange={this.onChange}
                        onInvalid={this.onInvalid}
                        onValid={this.onValid}/>

                    <InputControl
                        containerClass="col-sm-8 form-group"
                        ref
                        ={(input) => {
                        this.inputControls["wo"] = input
                    }}
                        type="search"
                        name="wo"
                        label="Work Order"
                        labelClass="ucase"
                        inputClass="form-control search"
                        maxLength="9"
                        validate={validations.number}
                        errorMessage="The &#39;Work Order&#39; should be number. Please check the Work Order and enter again."
                        value={this.state.searchParams.wo}
                        onChange={this.onChange}
                        onInvalid={this.onInvalid}
                        onValid={this.onValid}/>

                    <div className="col-sm-4 form-group open">
                        <a
                            href="javascript:void(0)"
                            className="dropdown-toggle dropdown-extended"
                            id="search-btn"
                            onClick={this.onSearch}
                            title="Search"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="true">
                            <img src="/img/icons/header/icon_search.svg" alt="Search" width="16"/>
                        </a>
                    </div>

                    <div className="clear"></div>
                    {errText}
                    {this.renderSeachResult()}

                </div>

            </div>
        );
    }
}

export default SearchSchedulingRequestForm;