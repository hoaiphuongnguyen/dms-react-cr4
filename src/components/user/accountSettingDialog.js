import React, {Component} from 'react';
import {API_STATUS} from '../../common/constants'
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import InputControl from '../common/inputControl'
import * as validations from '../../validations/common'
import * as actionTypes from '../../constants/actionTypes'
import isEqual from 'lodash';

class AccountSettingDialog extends Component {

    constructor(props) {
        super(props);
        this.onSave = this
            .onSave
            .bind(this);

        this.state = {
            errors: {},
            data: {},
            orgData: {}
        }

        this.inputControls = {};

        this.onInvalid = this
            .onInvalid
            .bind(this);

        this.onValid = this
            .onValid
            .bind(this);

        this.onChange = this
            .onChange
            .bind(this);

        this.onSave = this
            .onSave
            .bind(this);
        this.onCancel = this
            .onCancel
            .bind(this);
    }

    hasUnsavedChanges() {
        return !_.isEqual(this.state.data, this.state.orgData);
    }

    componentDidMount() {
        setTimeout(function () {
            $(".hide-pass").hide();
            $("#show_cng_pass").click(function () {
                $(".hide-pass").toggle("fast");
            });
        }, 100);
    }

    componentDidUpdate()
    {
        if (this.showErr) {
            $(".error-list").show();
            this.showErr = false;
        }
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {
        const userData = this.state.data;
        userData[name] = value;
        this.setState({data: userData});
    }

    saveData(autoSave) {
        if (this.state.processing) 
            return;
        
        this.setState({autoSave: autoSave});
        //  this.setState({errors: {}});
        const userData = this.state.data;
        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (key) {
                if (key == "firstName" || key == "lastName" || key == "email") {
                    //validate these field without check hidden
                    controls[key].validateAllRules(key, userData[key], true);
                } else {
                    controls[key].validateAllRules(key, userData[key]);
                }
            });

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })

        if (numError == 0) {
            this.setState({clickSave: true, processing: true, errorMessage: "", successMessage: ""});
            var changPassword = true;
            if ($($(".hide-pass")[0]).is(":hidden")) {
                changPassword = false;
            }
            this
                .props
                .actions
                .accountSetting(this.state.data, changPassword);
        } else {
            $(".error-list").show();
        }
    }

    autoSave() {
        this.saveData(true);
    }

    onSave()
    {
        this.saveData(false);
    }

    onCancel() {
        this.setState({clickCancel: true});

        setTimeout(function () {
            $("#close-modal-account-setting").click();
        }, 10);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dialogShowing) {
            this.closing = false;
            $('#myAccountSetting').modal('show');
            if (nextProps.status == API_STATUS.IN_PROGRESS) {
                this.setState({processing: true});
            } else if (nextProps.status == API_STATUS.ERROR) {
                var message = "";
                if (nextProps.message) {
                    message = nextProps
                        .message
                        .replace("AlreadyExistsException: ", "")
                        .replace("IllegalArgumentException: ", "")
                        .replace("IncorrectPasswordException: ", "");
                }
                if (message.indexOf("email") > 0) {
                    var controls = this.inputControls;
                    controls["email"].setError(message);
                    this.setState({processing: false});
                } else if (message.indexOf("current password") >= 0) {
                    var controls = this.inputControls;
                    controls["password"].setError(message);
                    this.setState({processing: false});
                } else if (message.indexOf("New password") >= 0) {
                    var controls = this.inputControls;
                    controls["newPassword"].setError(message);
                    this.setState({processing: false});
                } else {
                    this.setState({errorMessage: message, processing: false});
                }

                this.showErr = true;

            } else if (nextProps.actionType == actionTypes.UPDATE_PROFILE && nextProps.status == API_STATUS.DONE) {
                localStorage.setItem("userInfo", JSON.stringify(this.state.data));
                var closeDialog = this.props.layoutActions.closeDialogAccountSetting;
                this.setState({successMessage: "Update person profile successfully.", processing: true});
                setTimeout(function () {
                    closeDialog(true);
                }, 3000);

            } else if (nextProps.layout.status == undefined || nextProps.layout.status == '') {
                //new dialog?
                $(".hide-pass").hide();

                var controls = this.inputControls;
                Object
                    .keys(controls)
                    .forEach(function (key) {
                        controls[key].clearError();
                    });

                var localUser = localStorage.getItem("userInfo");
                this.setState({
                    data: localUser
                        ? jQuery.extend(true, {}, JSON.parse(localUser))
                        : {},
                    orgData: localUser
                        ? jQuery.extend(true, {}, JSON.parse(localUser))
                        : {},
                    errors: {},
                    processing: false,
                    successMessage: '',
                    errorMessage: '',
                    clickSave: false,
                    clickCancel: false
                });
            }

        } else {
            this.closing = true;
            $('#myAccountSetting').modal('hide');
            this.setState({status: "", successMessage: "", errorMessage: ""});
        }

    }

    render() {
        return (
            <div
                className="modal fade"
                id="myAccountSetting"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="myAccountSettingLabel">
                <div
                    className="modal-dialog popup-modal"
                    id={this.props.dialogId}
                    role="document">
                    <div className="modal-content">

                        <div className="modal-header">

                            <div className="row">
                                <div className="col-sm-20 alpha-lg">
                                    <h4>Edit Personal Profile</h4>
                                </div>

                                <div className="col-sm-4">

                                    <a
                                        href="javascript:void(0)"
                                        data-dismiss="modal"
                                        id="close-modal-account-setting"
                                        className="close-modal">
                                        <span aria-hidden="true">×</span>
                                    </a>
                                </div>

                            </div>

                        </div>
                        <div className="modal-body">
                            <ErrorArea errorList={this.state.errors}/>
                            <ComonInfoArea
                                successMessage={this.state.successMessage}
                                errorMessage={this.state.errorMessage}/>

                            <div className="row">
                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    name="firstName"
                                    label="First Name"
                                    required={true}
                                    validate={validations.aplha}
                                    errorMessage="First name is invalid"
                                    emptyMessage="First name is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="35"
                                    value={this.state.data && this.state.data.firstName
                                    ? this.state.data.firstName
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["firstName"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    name="lastName"
                                    label="Last Name"
                                    required={true}
                                    validate={validations.aplha}
                                    errorMessage="Last name is invalid"
                                    emptyMessage="Last name is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="35"
                                    value={this.state.data && this.state.data.lastName
                                    ? this.state.data.lastName
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["lastName"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>
                                <InputControl
                                    containerClass="form-group col-sm-24"
                                    name="email"
                                    label="Email"
                                    required={true}
                                    validate={validations.email}
                                    errorMessage="Email is invalid"
                                    emptyMessage="Email is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="32"
                                    value={this.state.data && this.state.data.email
                                    ? this.state.data.email
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["email"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <div className="form-group">
                                    <a href="javascript:void(0)" id="show_cng_pass">Change Password</a>
                                </div>

                                <InputControl
                                    type="changepassword"
                                    containerClass="form-group col-sm-24 hide-pass"
                                    name="password"
                                    label="Current Password"
                                    required={true}
                                    emptyMessage="Current Password is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="32"
                                    value={this.state.data && this.state.data.password
                                    ? this.state.data.password
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["password"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    type="changepassword"
                                    containerClass="form-group col-sm-24 hide-pass"
                                    name="newPassword"
                                    label="New Password"
                                    required={true}
                                    emptyMessage="New Password is required"
                                    errorMessage="Password must includes of alphabets, numbers and special characters"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="30"
                                    value={this.state.data && this.state.data.newPassword
                                    ? this.state.data.newPassword
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["newPassword"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    type="changepassword"
                                    containerClass="form-group col-sm-24 hide-pass"
                                    name="confirmPassword"
                                    label="Confirm Password"
                                    required={true}
                                    emptyMessage="Confirm Password is required"
                                    errorMessage="Password must includes of alphabets, numbers and special characters"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="30"
                                    value={this.state.data && this.state.data.confirmPassword
                                    ? this.state.data.confirmPassword
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["confirmPassword"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                            </div>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-group">
                                <a onClick={this.onCancel} className="btn btn-white">
                                    Cancel</a>
                                <a
                                    disabled={this.state.processing}
                                    onClick={this.onSave}
                                    className="btn btn-white">
                                    Save Changes</a>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

export default AccountSettingDialog