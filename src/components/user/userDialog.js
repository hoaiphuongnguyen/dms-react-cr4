import React, {Component} from 'react';
import InputControl from '../common/inputControl'
import UserStatus from './userStatus'
import UserRole from './userRole'
import ErrorArea from '../common/errorArea'
import ComonInfoArea from '../common/comonInfoArea'
import {API_STATUS, DIALOG_TYPE} from '../../common/constants';
import * as validations from '../../validations/common'
import * as actionTypes from '../../constants/actionTypes';
import isEqual from 'lodash';

class UserDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            data: jQuery.extend(true, {}, this.props.userData
                ? this.props.user
                : {
                    role: {}
                }),
            orgData: jQuery.extend(true, {}, this.props.userData
                ? this.props.user
                : {
                    role: {}
                }),
            errors: {}
        }

        this.onInvalid = this
            .onInvalid
            .bind(this);
        this.onValid = this
            .onValid
            .bind(this);
        this.onChange = this
            .onChange
            .bind(this);
        this.onChangeRole = this
            .onChangeRole
            .bind(this);
        this.onSave = this
            .onSave
            .bind(this);
        this.onResetPassword = this
            .onResetPassword
            .bind(this);
        this.onCancel = this
            .onCancel
            .bind(this);

        this.inputControls = {};
    }

    componentDidUpdate()
    {
        if (this.showErr) {
            $(".error-list").show();
            this.showErr = false;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dialogShowing) {
            this.closing = false;
            $('#myModal').modal('show');
            if (nextProps.status == API_STATUS.IN_PROGRESS) {
                this.setState({processing: true});
            } else if (nextProps.status == API_STATUS.DONE && (nextProps.actionType == actionTypes.RESET_PASSWORD || nextProps.actionType == actionTypes.SAVE_USER)) {
                if (nextProps.actionType == actionTypes.RESET_PASSWORD) {
                    this.setState({resetPassword: true, successMessage: "New password has been generated and mailed to user."});
                    var closeDialog = this.props.layoutActions.closeDialog;
                    setTimeout(function () {
                        $("#reset_pass").toggleClass("pointer-cursor");
                        closeDialog(false);
                    }, 3000);
                } else if (nextProps.actionType == actionTypes.SAVE_USER) {
                    this.setState({successMessage: "Save user successfully."});
                    var closeDialog = this.props.layoutActions.closeDialog;
                    setTimeout(function () {
                        closeDialog(true);
                    }, 3000);
                }
            } else if (nextProps.status == API_STATUS.ERROR) {
                var message = "";
                if (nextProps.message) {
                    message = nextProps
                        .message
                        .replace("AlreadyExistsException: ", "")
                        .replace("IllegalArgumentException: ", "")
                        .replace("IncorrectPasswordException: ", "");
                }
                if (message.indexOf("email") > 0) {
                    var controls = this.inputControls;
                    controls["email"].setError(message);
                    this.showErr = true;
                    this.setState({processing: false});
                } else {
                    this.setState({errorMessage: message, processing: false});
                }
            } else if (nextProps.layout && nextProps.layout.status == "") {
                //new dialog
                setTimeout(function () {
                    $('#reset_pass').tooltip();
                }, 200);

                this.setState({
                    id: nextProps.user && nextProps.user.id
                        ? nextProps.user.id
                        : '',
                    data: jQuery.extend(true, {}, nextProps.user
                        ? nextProps.user
                        : {
                            role: {}
                        }),
                    orgData: jQuery.extend(true, {}, nextProps.user
                        ? nextProps.user
                        : {
                            role: {}
                        }),

                    errors: {},
                    errorMessage: '',
                    successMessage: '',
                    processing: false,
                    clickSave: false,
                    clickCancel: false
                });

                var controls = this.inputControls;
                Object
                    .keys(controls)
                    .forEach(function (key) {
                        controls[key].clearError();
                    });

                this
                    .statusControl
                    .clearError();

                this
                    .roleControl
                    .clearError();

            }
        } else {
            this.closing = true;
            $('#myModal').modal('hide');
            this.setState({status: "", errorMessage: "", successMessage: ""});
        }
    }

    onInvalid(name, message) {
        const errors = this.state.errors;
        errors[name] = message;
        this.setState({errors: errors})
    }

    onValid(name) {
        const errors = this.state.errors;
        errors[name] = "";
        this.setState({errors: errors})
    }

    onChange(name, value) {
        const userData = this.state.data;
        userData[name] = value;
        this.setState({data: userData});
    }

    onChangeRole(name, value) {
        const userData = this.state.data;
        userData.role["id"] = value;
        this.setState({data: userData});
    }

    onResetPassword() {
        if (this.state.processing) 
            return;
        
        $("#reset_pass").toggleClass("pointer-cursor");
        this.setState({processing: true, errorMessage: "", successMessage: ""});
        this
            .props
            .actions
            .resetPassword(this.state.data.id);
    }

    hasUnsavedChanges() {
        var changed = !_.isEqual(this.state.data, this.state.orgData);
        return changed;
    }

    saveData(autoSave) {
        if (this.state.processing) 
            return;
        
        this.setState({autoSave: autoSave});

        const userData = this.state.data;
        var controls = this.inputControls;
        Object
            .keys(controls)
            .forEach(function (key) {
                controls[key].validateAllRules(key, userData[key]);
            });

        this
            .statusControl
            .validation(userData.status);

        this
            .roleControl
            .validation(userData.role.id);

        const errorList = this.state.errors;
        var errorArray = Object.keys(errorList);
        var numError = 0;
        errorArray.map(function (key) {
            if (errorList[key] != "") {
                numError++;
            }
        })
        if (numError == 0) {
            this.setState({clickSave: true, processing: true, errorMessage: "", successMessage: ""});

            this
                .props
                .actions
                .saveUser(userData);
        } else {
            this.setState({errorMessage: "", successMessage: ""});
            $(".error-list").show();
        }
    }

    autoSave() {
        this.saveData(true);
    }

    onSave()
    {
        this.saveData(false);
    }

    onCancel() {
        this.setState({clickCancel: true});

        setTimeout(function () {
            $(".close-modal").click();
        }, 10);
    }

    render() {
        var reset = this.onResetPassword;
        return (
            <div
                className="modal fade"
                id="myModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="myModalLabel">
                <div
                    className="modal-dialog popup-modal"
                    id={this.props.dialogId}
                    role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <div className="row">
                                <div className="col-sm-18 alpha-lg">
                                    <h4>{this.props.dialogType == DIALOG_TYPE.ADD_DATA
                                            ? 'Add User'
                                            : 'Edit User'}</h4>
                                </div>

                                <div className="col-sm-6">
                                    {this.state.data.id
                                        ? <a
                                                disabled={this.state.processing}
                                                onClick={function () {
                                                reset();
                                            }}
                                                id="reset_pass"
                                                data-toggle="tooltip"
                                                title=""
                                                className="reset-pass pointer-cursor"
                                                data-original-title="Reset Password">
                                                <span aria-hidden="true">
                                                    <img src="/img/icons/arrow/icon-reset-pass.svg" alt="" data-pin-nopin="true"/></span>
                                            </a>
                                        : ''
}

                                    <a href="javascript:void(0)" data-dismiss="modal" className="close-modal">
                                        <span aria-hidden="true">×</span>
                                    </a>
                                </div>

                            </div>

                        </div>
                        <div className="modal-body">
                            <ErrorArea errorList={this.state.errors}/>

                            <ComonInfoArea
                                resetPassword={this.state.resetPassword}
                                successMessage={this.state.successMessage}
                                errorMessage={this.state.errorMessage}/>
                            <div className="row">
                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    name="firstName"
                                    label="First Name"
                                    required={true}
                                    validate={validations.aplha}
                                    errorMessage="First name is invalid"
                                    emptyMessage="First name is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="35"
                                    value={this.state.data && this.state.data.firstName
                                    ? this.state.data.firstName
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["firstName"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <InputControl
                                    containerClass="form-group col-sm-12"
                                    name="lastName"
                                    label="Last Name"
                                    required={true}
                                    validate={validations.aplha}
                                    errorMessage="Last name is invalid"
                                    emptyMessage="Last name is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="35"
                                    value={this.state.data && this.state.data.lastName
                                    ? this.state.data.lastName
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["lastName"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>
                                <InputControl
                                    containerClass="form-group col-sm-24"
                                    name="email"
                                    label="Email"
                                    required={true}
                                    validate={validations.email}
                                    errorMessage="Email is invalid"
                                    emptyMessage="Email is required"
                                    labelClass="ucase"
                                    inputClass="form-control"
                                    maxLength="32"
                                    value={this.state.data && this.state.data.email
                                    ? this.state.data.email
                                    : ''}
                                    ref
                                    ={(input) => {
                                    this.inputControls["email"] = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}/>

                                <UserStatus
                                    required={true}
                                    emptyMessage="Status is required"
                                    name="status"
                                    ref
                                    ={(input) => {
                                    this.statusControl = input
                                }}
                                    onChange={this.onChange}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}
                                    value={this.state.data.status}/>

                                <UserRole
                                    required={true}
                                    emptyMessage="Role is required"
                                    name="role"
                                    ref
                                    ={(input) => {
                                    this.roleControl = input
                                }}
                                    onChange={this.onChangeRole}
                                    onInvalid={this.onInvalid}
                                    onValid={this.onValid}
                                    value={this.state.data.role.id}/>

                            </div>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-group">
                                <a className="btn btn-white" onClick={this.onCancel}>
                                    Cancel</a>
                                <a
                                    disabled={this.state.processing}
                                    onClick={this.onSave}
                                    className="btn btn-white">
                                    Save Changes</a>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default UserDialog