import React, {Component} from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import {displayDate} from '../../utils/date.js';

class UserList extends Component {

    constructor(props) {
        super(props);

        this.items = this.props.list.items;

        this.state = {
            data: this.items,
            keyword: '',
            sortName: this.props.list.sortBy,
            sortOrder: this.props.list.sortOrder
        };

        this.onShowMore = this
            .onShowMore
            .bind(this);

        this
            .props
            .actions
            .loadUsers(this.state.keyword, 1, this.props.list.pageSize, this.props.list.sortBy, this.props.list.sortOrder, true);
    }

    componentDidMount() {
        document
            .body
            .classList
            .add('manage-user');
        document
            .body
            .classList
            .add('inner');

        document.title = 'Manage User';
    }

    componentWillUnmount() {
        document
            .body
            .classList
            .remove('manage-user');
        document
            .body
            .classList
            .remove('inner');
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.list.needRefresh) {
            this
                .props
                .actions
                .loadUsers(this.state.keyword, 1, nextProps.list.pageSize, nextProps.list.sortBy, nextProps.list.sortOrder, true);
        } else {
            this.items = nextProps.list.items;

            this.setState({data: this.items, sortName: this.props.list.sortBy, sortOrder: this.props.list.sortOrder});
        }
    }

    onShowMore() {
        if (this.props.list.items.length < this.props.list.totalRecords) {
            this
                .props
                .actions
                .loadUsers(this.state.keyword, ++this.props.list.page, this.props.list.pageSize, this.props.list.sortBy, this.props.list.sortOrder, false);
        }
    }

    onSortChange(sortName, sortOrder) {
        this
            .props
            .actions
            .loadUsers(this.state.keyword, 1, this.props.list.pageSize, sortName, sortOrder, true);
    }

    onSearchChange(searchText, colInfos, multiColumnSearch) {
        this.setState({keyword: searchText});
        this
            .props
            .actions
            .loadUsers(this.state.keyword, 1, this.props.list.pageSize, this.props.list.sortBy, this.props.list.sortOrder, true);
    }

    buttonEdit(cell, row) {
        var f = this.props.layoutActions.openEditUserDialog;
        return (
            <a
                href="javascript:void(0)"
                data-toggle="tooltip"
                title="Edit User"
                onClick={function () {
                f(row);
            }}>
                <img
                    className="icon-svg"
                    src="/img/icons/searchdonaricons/icon-edit.svg"
                    alt="edit"
                    width="18"/></a>
        );
    }

    render() {
        let options = {
            onSortChange: this
                .onSortChange
                .bind(this)
        };

        let table = null;
        let showMore = null;
        if (this.props.list.items) {
            table = <BootstrapTable
                ref='table'
                data={this.state.data}
                options={options}
                search
                tableContainerClass="donortable"
                bordered={false}>
                <TableHeaderColumn dataField='email' isKey dataSort>Email</TableHeaderColumn>
                <TableHeaderColumn dataField='firstName' dataSort>First Name</TableHeaderColumn>
                <TableHeaderColumn dataField='lastName' dataSort>Last Name</TableHeaderColumn>
                <TableHeaderColumn dataField='status' dataSort>Status</TableHeaderColumn>
                <TableHeaderColumn dataField='roleName' dataSort>Role(s)</TableHeaderColumn>
                <TableHeaderColumn dataField='createdAt' dataFormat={displayDate} dataSort>Created On</TableHeaderColumn>
                <TableHeaderColumn dataField='modifiedAt' dataFormat={displayDate} dataSort>Last Modified On</TableHeaderColumn>
                <TableHeaderColumn dataField='modifiedBy' dataSort>Last Modified By</TableHeaderColumn>
                <TableHeaderColumn
                    dataField="button"
                    width='5%'
                    dataFormat={this
                    .buttonEdit
                    .bind(this)}
                    dataAlign="center"
                    headerTitle={false}>&nbsp;</TableHeaderColumn>
            </BootstrapTable>

            if (this.props.list.items.length < this.props.list.totalRecords) {
                showMore = <div className="col-sm-24 text-right showmore hide-table">
                    <a href="javascript:void(0)" onClick={this.onShowMore}>Show More</a>
                </div>
            }

        }
        var o = this.props.layoutActions.openAddUserDialog;
        return (
            <div id="schedule-appointment" className="clearfix">

                <div id="page-breadcrumbs" className="clearfix">
                    <ol className="breadcrumb">
                        <li>
                            <a href="javascript:void(0)">System Administration</a>
                        </li>
                        <li className="active">Manage User</li>
                    </ol>
                </div>

                <div id="section-title" className="clearfix">

                    <div className="pull-left-lg txt-center-xs heading">
                        <h2 className="section-heading">Manage User</h2>
                    </div>
                </div>

                <div className="search-wrapper">
                    <div className="col-sm-24 collapse-sm">
                        <h4 className="icon-container">All Users
                            <a
                                href="javascript:void(0)"
                                title="Add User"
                                id="addUser"
                                onClick={function () {
                                o();
                            }}>
                                <img src="/img/icons/createdonoricons/icon_add.png" alt=""/></a>
                        </h4>
                    </div>
                    <div className="clear"></div>

                    <div
                        className="col-xs-24 col-sm-24 col-md-24 col-lg-24 collapse-sm table-responsive">
                        {table}
                    </div>

                    {showMore}
                    <div className="clear"></div>

                </div>

            </div>

        );
    }

}

export default UserList;