import React, { Component} from 'react';
class UserRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: "Input is invalid",
      errorVisible: false
    };

    this.handleChange = this
      .handleChange
      .bind(this);
    this.handleBlur = this
      .handleBlur
      .bind(this);
    this.validation = this
      .validation
      .bind(this);
  }


 clearError() {
      this.setState({errorVisible: false});
  }

  
  clearData() {
    this.setState({errorVisible: false});
    $('input:radio').each(function () {
      $(this).prop('checked', false);
    })
    $('input:radio')
      .closest('label')
      .removeClass('checked');
  }

  componentWillReceiveProps(nextProps) {
    $('[name=role]').prop('checked', false);
    $('[name=role]').closest('label').removeClass('checked');
    
    var value=nextProps.value;
    if (value){
      var target = $('[name=role][value="' + value + '"]');
      $(target).prop('checked', true);
      $(target)
        .closest('label')
        .addClass('checked');
    }
  }

  handleChange(event) {
  this.setState({
      checked:true
    })

    $('input[name="' + event.target.name + '"]')
      .not(event.target)
      .closest('label')
      .removeClass('checked');
    $(event.target.closest('label')).addClass('checked');

    this.validation(event.target.value);
    // Call onChange method on the parent component for updating it's state If
    // saving this field for final form submission, it gets passed up to the top
    // component for sending to the server
    if (this.props.onChange) {
      this
        .props
        .onChange(event.target.name, event.target.value);
    }
  }

  validation(value) {

    var message = "";
    var valid = true;
    if (this.props.required && !value) {
      // this happens when we have a required field with no text entered in this case,
      // we want the "emptyMessage" error message
      message = this.props.emptyMessage;
      valid = false;
    }

    // setting the state will update the display, causing the error message to
    // display if there is one.
    this.setState({errorMessage: message, errorVisible: !valid});

    if (!valid) {
      if (this.props.onInvalid) {
        this
          .props
          .onInvalid(this.props.name, message);
      }
    } else {
      if (this.props.onValid) {
        this
          .props
          .onValid(this.props.name);
      }
    }

  }

  handleBlur(event) {
     if (!this.state.checked){
      this.setState({errorVisible:true});
      if (this.props.onInvalid) {
        this
          .props
          .onInvalid(this.props.name, this.props.emptyMessage);
      }
   } 
  }


  render() {

    var wrapperClass = "";
    if (this.state.errorVisible) {
      wrapperClass += " has-error";
    }

    return (
      <div className="form-group col-sm-24">
        <div className={wrapperClass}>
          <label
            className="ucase"
            title={this.state.errorVisible
            ? this.state.errorMessage
            : ''}>Role</label>
        </div>
        <div className="role" onChange={this.handleChange} onBlur={this.handleBlur}>
          <label onBlur={this.handleBlur}><input name="role" type="radio" value="1"/>
            Administrator</label>
          <label onBlur={this.handleBlur}><input name="role" type="radio" value="2"/>
            Full View</label>
          <label onBlur={this.handleBlur}><input name="role" type="radio" value="3"/>
            DID Only</label>
          <label onBlur={this.handleBlur}><input name="role" type="radio" value="4"/>
            Donor Request and Reports</label>
          <label onBlur={this.handleBlur}><input name="role" type="radio" value="5"/>
            DID Calendar View</label>
        </div>

      </div>

    )
  }
}

export default UserRole