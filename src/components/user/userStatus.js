import React, { Component} from 'react';
class UserStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: "Input is invalid",
      errorVisible: false
    };

    this.handleChange = this
      .handleChange
      .bind(this);
    this.handleBlur = this
      .handleBlur
      .bind(this);
    this.validation = this
      .validation
      .bind(this);

  }

  clearError() {
      this.setState({errorVisible: false});
  }

  clearData() {
    $('input:radio')
      .each(function () {
        $(this).prop('checked', false);
      })
     this.setState({errorVisible: false});
  }

  componentWillReceiveProps(nextProps) {
    var value=nextProps.value;
    $('[name=status]').prop('checked', false);
    if (value){
       $('[name=status][value="' + value + '"]').prop('checked', true);
    }
   
  }

  
  handleChange(event) {
    this.setState({
      checked: true
    })

    // Call onChange method on the parent component for updating it's state If
    // saving this field for final form submission, it gets passed up to the top
    // component for sending to the server
    if (this.props.onChange) {
      this
        .props
        .onChange(event.target.name, event.target.value);
    }

    this.setState({errorVisible: false});
    if (this.props.onValid) {
      this
        .props
        .onValid(event.target.name);
    }

  }

  validation(value) {

    var message = "";
    var valid = true;
    if (this.props.required && !value) {
      // this happens when we have a required field with no text entered in this case,
      // we want the "emptyMessage" error message
      message = this.props.emptyMessage;
      valid = false;
    }

    // setting the state will update the display, causing the error message to
    // display if there is one.
    this.setState({errorMessage: message, errorVisible: !valid});

    if (!valid) {
      if (this.props.onInvalid) {
        this
          .props
          .onInvalid(this.props.name, message);
      }
    } else {
      if (this.props.onValid) {
        this
          .props
          .onValid(this.props.name);
      }
    }

  }


  handleBlur(event) {
   if (!this.state.checked){
      this.setState({errorVisible:true});
      if (this.props.onInvalid) {
        this
          .props
          .onInvalid(this.props.name, this.props.emptyMessage);
      }
   } 
  }

  render() {
    var wrapperClass = "form-group col-sm-24";
    if (this.state.errorVisible) {
      wrapperClass += " has-error";
    }
    return (

      <div
        className={wrapperClass}
        onChange={this.handleChange}
        onBlur={this.handleBlur}>
        <label
          className="ucase"
          title={this.state.errorVisible
          ? this.state.errorMessage
          : ''}>Status</label>
        <input type="radio" name="status" value="Active"/>
        <span className="radio-text">Active</span>
        <input type="radio"  name="status" value="Inactive"/>
        <span className="radio-text">Inactive</span>

      </div>

    )
  }
}

export default UserStatus