export const CHANG_ROUTE = 'CHANG_ROUTE'
export const OPEN_SUCCESS_DIALOG = 'OPEN_SUCCESS_DIALOG'
export const CLOSE_SUCCESS_DIALOG = 'CLOSE_SUCCESS_DIALOG'

/* User */
export const SAVE_USER = 'SAVE_USER'
export const GET_USER = 'GET_USER'
export const RESET_PASSWORD = 'RESET_PASSWORD'
export const OPEN_ADD_USER_DIALOG = 'OPEN_ADD_USER_DIALOG'
export const OPEN_EDIT_USER_DIALOG = 'OPEN_EDIT_USER_DIALOG'
export const OPEN_ACCOUNT_SETTING_DIALOG = 'OPEN_ACCOUNT_SETTING_DIALOG'
export const CLOSE_DIALOG = 'CLOSE_DIALOG'
export const CLOSE_DIALOG_ACCOUNT_SETTING = 'CLOSE_DIALOG_ACCOUNT_SETTING'
export const CLOSE_APPOINTMENT_DIALOG = 'CLOSE_APPOINTMENT_DIALOG'
export const UPDATE_PROFILE = 'UPDATE_PROFILE'

export const LOAD_USERS = 'LOAD_USERS'
export const RELOAD_USERS = 'RELOAD_USERS'

/* Donor */
export const VIEW_DONOR_QUICK_SEARCH = 'VIEW_DONOR_QUICK_SEARCH'
export const SAVE_DONOR = 'SAVE_DONOR'
export const GET_DONOR = 'GET_DONOR'
export const OPEN_ADD_OUT_REACH_DIALOG = 'OPEN_ADD_OUT_REACH_DIALOG'
export const GET_OUT_REACH_REASON = 'GET_OUT_REACH_REASON'
export const GET_OUT_REACH_OUTCOME = 'GET_OUT_REACH_OUTCOME'
export const SAVE_OUT_REACH = 'SAVE_OUT_REACH'

export const LOAD_DONORS = 'LOAD_DONORS'
export const RELOAD_DONORS = 'RELOAD_DONORS'

export const SEARCH_DONORS = 'SEARCH_DONORS'
export const SEARCH_DONORS_RELOAD = 'SEARCH_DONORS_RELOAD'
export const CLEAR_SEARCH_PARAMS = 'CLEAR_SEARCH_PARAMS'

export const IMPORT_DONORS = 'IMPORT_DONORS'
export const SAVE_PAYMENT_VOLUME = 'SAVE_PAYMENT_VOLUME'
export const PRINT_PDF = 'PRINT_PDF'

/* Account */
export const LOGIN = 'LOGIN'
export const SESSION_EXPIRED = 'SESSION_EXPIRED'
export const LOGOUT = 'LOGOUT'

/*Medical procedure*/
export const LOAD_PROCEDURES = 'LOAD_PROCEDURES'
export const LOAD_ALL_PROCEDURES = 'LOAD_ALL_PROCEDURES'
export const RELOAD_PROCEDURES = 'RELOAD_PROCEDURES'
export const GET_PROCEDURE = 'GET_PROCEDURE'
export const SAVE_PROCEDURE = 'SAVE_PROCEDURE'
export const OPEN_ADD_PROCEDURE_DIALOG = 'OPEN_ADD_PROCEDURE_DIALOG'
export const OPEN_EDIT_PROCEDURE_DIALOG = 'OPEN_EDIT_PROCEDURE_DIALOG'

/* Scheduling request */
export const LOAD_SCHEDULING_REQUEST = 'LOAD_SCHEDULING_REQUEST'
export const RELOAD_SCHEDULING_REQUEST = 'RELOAD_SCHEDULING_REQUEST'
export const SEARCH_SCHEDULING_REQUEST = 'SEARCH_SCHEDULING_REQUEST'
export const SEARCH_SCHEDULING_REQUEST_RELOAD = 'SEARCH_SCHEDULING_REQUEST_RELOAD'
export const GET_SCHEDULING_REQUEST = 'GET_SCHEDULING_REQUEST'
export const SAVE_SCHEDULING_REQUEST = 'SAVE_SCHEDULING_REQUEST'
export const CANCEL_SCHEDULING_REQUEST = 'CANCEL_SCHEDULING_REQUEST'

export const LOAD_SCHEDULING_REQUEST_DONOR_MATCH = 'LOAD_SCHEDULING_REQUEST_DONOR_MATCH'
export const RELOAD_SCHEDULING_REQUEST_DONOR_MATCH = 'RELOAD_SCHEDULING_REQUEST_DONOR_MATCH'

export const MATCH_DONORS = 'MATCH_DONORS'
export const MATCH_DONORS_RELOAD = 'MATCH_DONORS_RELOAD'
export const CLEAR_MATCH_DONORS_PARAMS = 'CLEAR_MATCH_DONORS_PARAMS'
export const MATCH_DONOR_TO_SCHEDULE = 'MATCH_DONOR_TO_SCHEDULE'
export const UNASSIGN_DONOR = 'UNASSIGN_DONOR'

/*Appointment*/
export const GET_APPOINTMENTS = "GET_APPOINTMENTS"
export const GET_APPOINTMENT = 'GET_APPOINTMENT'
export const SEARCH_DONOR_FOR_APPOINTMENT = 'SEARCH_DONOR_FOR_APPOINTMENT'
export const SAVE_APPOINTMENT = 'SAVE_APPOINTMENT'
export const CANCEL_APPOINTMENT = 'CANCEL_APPOINTMENT'
export const CHECK_OUT_APPOINTMENT = 'CHECK_OUT_APPOINTMENT'
export const OPEN_APPOINTMENT_POPUP = 'OPEN_APPOINTMENT_POPUP'
export const OPEN_ADD_APPOINMENT_DIALOG = 'OPEN_ADD_APPOINMENT_DIALOG'
export const OPEN_EDIT_APPOINMENT_DIALOG = 'OPEN_EDIT_APPOINMENT_DIALOG'
export const OPEN_CANCEL_APPOINMENT_DIALOG = 'OPEN_CANCEL_APPOINMENT_DIALOG'
export const CLOSE_APPOINMENT_POPUP = 'CLOSE_APPOINMENT_POPUP'

/* Report */
export const LOAD_REPORT = 'LOAD_REPORT'
export const RELOAD_REPORT = 'RELOAD_REPORT'