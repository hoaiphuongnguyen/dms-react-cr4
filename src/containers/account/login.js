import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as accountActions from '../../actions/accountActions'

import Login from '../../components/account/login'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(accountActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {status: state.account.status, message: state.account.errorMessage, actionType: state.account.actionType, user: state.account.userInfo }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(Login)