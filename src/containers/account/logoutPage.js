import {connect} from 'react-redux'
import LogoutPage from '../../components/account/logoutPage'

const mapStateToProps = (state, ownProps) => {
    return {logout: state.account}
}

export default connect(mapStateToProps)(LogoutPage)