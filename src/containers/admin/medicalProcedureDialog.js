import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as medicalProcedureActions from '../../actions/medicalProcedureActions'
import * as layoutActions from '../../actions/layoutActions'

import MedicalProcedureDialog from '../../components/admin/medicalProcedureDialog'
import dialogHoc from '../../utils/dialogHoc'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(medicalProcedureActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {status: state.medicalProcedure.status, message: state.medicalProcedure.message, actionType: state.medicalProcedure.actionType, layout: state.layout, data: state.layout.dialogData}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(dialogHoc(MedicalProcedureDialog))