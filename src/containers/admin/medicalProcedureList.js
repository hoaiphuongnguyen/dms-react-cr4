import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as medicalProcedureListActions from '../../actions/medicalProcedureListActions'
import * as layoutActions from '../../actions/layoutActions'

import MedicalProcedureList from '../../components/admin/medicalProcedureList'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(medicalProcedureListActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {list: state.medicalProcedureList}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(MedicalProcedureList)