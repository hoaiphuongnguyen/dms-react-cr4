import React, {Component} from 'react'
import {hashHistory, Router, Route} from 'react-router'
import {Provider, connect} from 'react-redux'
import {PATH_PREFIX} from '../common/constants'
import {bindActionCreators} from 'redux';

import Layout from './layout'
import Login from './account/login'
import LogoutPage from './account/logoutPage'
import UserList from './user/userList'
import MedicalProcedureList from './admin/medicalProcedureList';
import ScheduleAppointmentForm from './appointment/scheduleAppointmentForm'
import DonorForm from './donor/donorForm'
import SearchDonorForm from './donor/searchDonorForm'
import ImportDonor from './donor/importDonor'

import SchedulingRequestForm from './schedulingRequest/schedulingRequestForm'
import SchedulingRequestList from './schedulingRequest/schedulingRequestList';
import SearchSchedulingRequestForm from './schedulingRequest/searchSchedulingRequestForm'
import ProcessSchedulingRequestForm from './schedulingRequest/processSchedulingRequestForm'

import DonorStatusReportForm from './report/donorStatusReportForm'
import DonorCancellationReportForm from './report/donorCancellationReportForm'
import AppointmentReportForm from './report/appointmentReportForm'
import PaymentReportForm from './report/paymentReportForm'
import * as layoutActions from '../actions/layoutActions'

class App extends Component {

  render() {
    const {store, routes} = this.props

    return (
      <Provider store={store}>
        <Router
          history={hashHistory}
          onChange={this
          .routeChangeHandler
          .bind(this)}>
          <Route
            component={Layout}
            onChange={this
            .routeChangeHandler
            .bind(this)}>
            <Route path={PATH_PREFIX +"/logout"} component={LogoutPage}/>
            <Route path={PATH_PREFIX + "/"} component={ScheduleAppointmentForm}></Route>
            <Route path={PATH_PREFIX + "/user"} component={UserList}></Route>
            <Route
              path={PATH_PREFIX + "/medical-procedure"}
              component={MedicalProcedureList}></Route>
            <Route path={PATH_PREFIX + "/donor/create-donor"} component={DonorForm}></Route>
            <Route path={PATH_PREFIX + "/donor/edit-donor/:id"} component={DonorForm}></Route>
            <Route path={PATH_PREFIX + "/donor/edit-donor/:id"} component={DonorForm}></Route>
            <Route path={PATH_PREFIX + "/donor/search-donor"} component={SearchDonorForm}></Route>
            <Route path={PATH_PREFIX + "/donor/import-donor"} component={ImportDonor}></Route>

            <Route
              path={PATH_PREFIX + "/schedule-request/process-schedule-request"}
              component={SchedulingRequestList}></Route>
            <Route
              path={PATH_PREFIX + "/schedule-request/process-schedule-request/:id"}
              component={ProcessSchedulingRequestForm}></Route>
            <Route
              path={PATH_PREFIX + "/schedule-request/create-scheduling-request"}
              component={SchedulingRequestForm}></Route>
            <Route
              path={PATH_PREFIX + "/schedule-request/edit-schedule-request/:id"}
              component={SchedulingRequestForm}></Route>
            <Route
              path={PATH_PREFIX + "/schedule-request/search-schedule-request"}
              component={SearchSchedulingRequestForm}></Route>

            <Route
              path={PATH_PREFIX + "/report/donor-status"}
              component={DonorStatusReportForm}></Route>
            <Route
              path={PATH_PREFIX + "/report/donor-cancellation"}
              component={DonorCancellationReportForm}></Route>
            <Route
              path={PATH_PREFIX + "/report/appointment"}
              component={AppointmentReportForm}></Route>
            <Route
              path={PATH_PREFIX + "/report/payment-transaction"}
              component={PaymentReportForm}></Route>

          </Route>

          <Route
            path={PATH_PREFIX + "/login"}
            component={Login}
            onChange={this
            .routeChangeHandler
            .bind(this)}></Route>
        </Router>
      </Provider>
    )
  }

  routeChangeHandler(previousRoute, nextRoute) {
    this
      .props
      .actions
      .changeRoute(previousRoute, nextRoute);
  }

}

const mapDispatchAndActionsToProps = (dispatch) => {
  return {
    actions: bindActionCreators(layoutActions, dispatch)
  }
}

const mapStateToProps = (state, ownProps) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(App)

//export default App