import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as appointmentActions from '../../actions/appointmentActions'
import * as layoutActions from '../../actions/layoutActions'
import {loadAllMedicalProcedures} from '../../actions/medicalProcedureListActions'
import {getSchedulingRequest} from '../../actions/schedulingRequestActions'

import AppointmentDialog from '../../components/appointment/appointmentDialog'
import dialogHoc from '../../utils/dialogHoc'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(appointmentActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch),
        loadAllMedicalProcedures: bindActionCreators(loadAllMedicalProcedures, dispatch),
        getSchedulingRequest: bindActionCreators(getSchedulingRequest, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        status: state.appointment.status,
        message: state.appointment.message,
        actionType: state.appointment.actionType,
        data: state.appointment.data,
        donors: state.appointment.donors,
        layout: state.layout,
        allMedicalProcedures: state.medicalProcedureList.allMedicalProcedures,
        needReloadAllProcedures: state.medicalProcedureList.needReloadAllProcedures,
        scheduleRequest: state.schedulingRequest
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(dialogHoc(AppointmentDialog))