import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as appointmentActions from '../../actions/appointmentActions'
import * as layoutActions from '../../actions/layoutActions'

import AppointmentPopup from '../../components/appointment/appointmentPopup'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(appointmentActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        needRefresh: state.appointment.needRefresh,
        status: state.appointment.status,
        message: state.appointment.message,
        actionType: state.appointment.actionType,
        data: state.appointment.data,
        layout: state.layout
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(AppointmentPopup)