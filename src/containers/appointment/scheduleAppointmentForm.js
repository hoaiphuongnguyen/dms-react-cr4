import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as appointmentActions from '../../actions/appointmentActions'
import * as layoutActions from '../../actions/layoutActions'
import {loadAllMedicalProcedures} from '../../actions/medicalProcedureListActions'

import ScheduleAppointmentForm from '../../components/appointment/scheduleAppointmentForm'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(appointmentActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch),
        loadAllMedicalProcedures: bindActionCreators(loadAllMedicalProcedures, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        needRefresh: state.appointment.needRefresh,
        items: state.appointment.items,
        appointment: state.appointment,
        status: state.appointment.status,
        message: state.appointment.message,
        actionType: state.appointment.actionType,
        user:state.account.userInfo
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(ScheduleAppointmentForm)