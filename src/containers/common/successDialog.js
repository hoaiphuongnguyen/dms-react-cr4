import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as layoutActions from '../../actions/layoutActions'

import SuccessDialog from '../../components/common/successDialog'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        layoutActions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return { layout: state.layout}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(SuccessDialog)