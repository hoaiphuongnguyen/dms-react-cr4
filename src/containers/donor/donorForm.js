import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import {getSchedulingRequest} from '../../actions/schedulingRequestActions'
import {getSchedulingRequestDonorMatch} from '../../actions/schedulingRequestListActions'
import * as processSchedulingRequestActions from '../../actions/processSchedulingRequestActions'
import * as appointmentActions from '../../actions/appointmentActions'
import * as donorActions from '../../actions/donorActions'
import * as layoutActions from '../../actions/layoutActions'

import DonorForm from '../../components/donor/donorForm'
import confirmLeave from '../../utils/confirmLeave'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(donorActions, dispatch),
        processScheduleActions: bindActionCreators(processSchedulingRequestActions, dispatch),
        getSchedulingRequestAction: bindActionCreators(getSchedulingRequest, dispatch),
        appointmentActions: bindActionCreators(appointmentActions, dispatch),
        getSchedulingRequestDonorMatchAction:bindActionCreators(getSchedulingRequestDonorMatch, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        donor: state.donor,
        status: state.donor.status,
        message: state.donor.message,
        actionType: state.donor.actionType,
        user: state.account.userInfo,
        appointments: state.donor.appointments,
        scheduleRequest: state.donor.scheduleRequest
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(confirmLeave(DonorForm))