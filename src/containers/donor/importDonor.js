import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import * as importDonorActions from '../../actions/importDonorActions'

import ImportDonor from '../../components/donor/importDonor'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(importDonorActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        importDonor: state.importDonor
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(ImportDonor)