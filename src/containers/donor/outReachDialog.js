import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as outReachActions from '../../actions/outReachActions'
import * as layoutActions from '../../actions/layoutActions'

import OutReachDialog from '../../components/donor/outreachDialog'
import dialogHoc from '../../utils/dialogHoc'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(outReachActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        status: state.outReach.status,
        message: state.outReach.message,
        actionType: state.outReach.actionType,
        layout: state.layout,
        data: state.outReach.data,
        items: state.outReach.items
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(dialogHoc(OutReachDialog))