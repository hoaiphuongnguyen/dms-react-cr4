import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import * as searchDonorActions from '../../actions/searchDonorActions'
import * as layoutActions from '../../actions/layoutActions'

import SearchDonorForm from '../../components/donor/searchDonorForm'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(searchDonorActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        searchDonor: state.searchDonor,
        user:state.account.userInfo
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(SearchDonorForm)