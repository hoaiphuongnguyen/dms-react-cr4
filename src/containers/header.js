import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as accountActions from '../actions/accountActions'
import {openAccountSettingDialog, getDonorFromQuickSeach} from '../actions/layoutActions'

import Header from '../components/layout/header'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(accountActions, dispatch),
        getDonorFromQuickSeach: bindActionCreators(getDonorFromQuickSeach, dispatch),
        openAccountSettingDialog: bindActionCreators(openAccountSettingDialog, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {account: state.account, status: state.account.status, errorMessage: state.account.errorMessage, actionType: state.account.actionType}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(Header)