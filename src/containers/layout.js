import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import * as layoutActions from '../actions/layoutActions'

import Layout from '../components/layout/layout'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        layout: state.layout,
        user: state.account.userInfo,
        schedulingRequest: state.schedulingRequest
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(Layout)