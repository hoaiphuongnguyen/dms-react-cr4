import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as reportActions from '../../actions/reportActions'

import PaymentReportForm from '../../components/report/paymentReportForm'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(reportActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {report: state.report}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(PaymentReportForm)