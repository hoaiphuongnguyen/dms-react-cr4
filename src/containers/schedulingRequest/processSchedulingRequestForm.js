import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as schedulingRequestActions from '../../actions/schedulingRequestActions'
import * as processSchedulingRequestActions from '../../actions/processSchedulingRequestActions'
import * as layoutActions from '../../actions/layoutActions'
import {loadAllMedicalProcedures} from '../../actions/medicalProcedureListActions'

import ProcessSchedulingRequestForm from '../../components/schedulingRequest/processSchedulingRequestForm'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        scheduleActions: bindActionCreators(schedulingRequestActions, dispatch),
        processScheduleActions: bindActionCreators(processSchedulingRequestActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch),
        loadAllMedicalProcedures: bindActionCreators(loadAllMedicalProcedures, dispatch),
    }
}

const mapStateToProps = (state, ownProps) => {
     return {
        schedulingRequest: state.schedulingRequest, 
        allMedicalProcedures: state.medicalProcedureList.allMedicalProcedures.filter(function(ele){ //filter only categories that are relevant to schedule request
                            if(ele.category == "Whole Blood" || ele.category == "Bone Marrow" ||
                              ele.category == "LeukoPak" || ele.category == "LeukoPak Mobilized" || 
                              ele.category == "Screening & Backup")
                              {
                                return true;
                              }
                              return false;
                        }),
        needReloadAllProcedures: state.medicalProcedureList.needReloadAllProcedures,
        user: state.account.userInfo,
        processScheduleRequest: state.processScheduleRequest
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(ProcessSchedulingRequestForm)