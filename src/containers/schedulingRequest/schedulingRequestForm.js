import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as schedulingRequestActions from '../../actions/schedulingRequestActions'
import * as layoutActions from '../../actions/layoutActions'
import {loadAllMedicalProcedures} from '../../actions/medicalProcedureListActions'
import confirmLeave from '../../utils/confirmLeave'

import SchedulingRequestForm from '../../components/schedulingRequest/schedulingRequestForm'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(schedulingRequestActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch),
        loadAllMedicalProcedures: bindActionCreators(loadAllMedicalProcedures, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        schedulingRequest: state.schedulingRequest,
        status: state.schedulingRequest.status,
        message: state.schedulingRequest.message,
        actionType: state.schedulingRequest.actionType,
        allMedicalProcedures: state
            .medicalProcedureList
            .allMedicalProcedures
            .filter(function (ele) { //filter only categories that are relevant to schedule request
                if (ele.category == "Whole Blood" || ele.category == "Bone Marrow" || ele.category == "LeukoPak" || ele.category == "LeukoPak Mobilized" || ele.category == "Screening & Backup") {
                    return true;
                }
                return false;
            }),
        needReloadAllProcedures: state.medicalProcedureList.needReloadAllProcedures,
        user: state.account.userInfo
    }
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(confirmLeave(SchedulingRequestForm))