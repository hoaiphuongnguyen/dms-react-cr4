import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as schedulingRequestListActions from '../../actions/schedulingRequestListActions'

import SchedulingRequestList from '../../components/schedulingRequest/schedulingRequestList'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(schedulingRequestListActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {list: state.schedulingRequestList}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(SchedulingRequestList)