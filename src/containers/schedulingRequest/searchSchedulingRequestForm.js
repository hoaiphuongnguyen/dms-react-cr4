import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as searchSchedulingRequestActions from '../../actions/searchSchedulingRequestActions'

import SearchSchedulingRequestForm from '../../components/schedulingRequest/searchSchedulingRequestForm'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(searchSchedulingRequestActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {searchSchedulingRequest: state.searchSchedulingRequest,
        user:state.account.userInfo}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(SearchSchedulingRequestForm)