import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as userActions from '../../actions/userActions'
import * as layoutActions from '../../actions/layoutActions'

import UserDialog from '../../components/user/userDialog'
import dialogHoc from '../../utils/dialogHoc'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(userActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {status: state.user.status, message: state.user.message, actionType: state.user.actionType, layout: state.layout, user: state.layout.dialogData}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(dialogHoc(UserDialog))