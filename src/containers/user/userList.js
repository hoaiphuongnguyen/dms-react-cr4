import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import * as userListActions from '../../actions/userListActions'
import * as layoutActions from '../../actions/layoutActions'

import UserList from '../../components/user/userList'

const mapDispatchAndActionsToProps = (dispatch) => {
    return {
        actions: bindActionCreators(userListActions, dispatch),
        layoutActions: bindActionCreators(layoutActions, dispatch)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {list: state.userList}
}

export default connect(mapStateToProps, mapDispatchAndActionsToProps)(UserList)