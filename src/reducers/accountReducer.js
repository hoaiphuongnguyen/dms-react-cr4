import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants'
import {hashHistory} from 'react-router';

const initialState = {
    status: '',
    errorMessage: '',
    userInfo: null
};

export default function accountReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.LOGIN:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    userInfo: action.data,
                    status: action.status,
                    actionType: actionTypes.LOGIN

                });
            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    userInfo: null,
                    status: action.status,
                    actionType: actionTypes.LOGIN
                });
            } else if (action.status == API_STATUS.ERROR) {
                return jQuery.extend(true, {}, state, {
                    userInfo: null,
                    status: action.status,
                    errorMessage: action.errorMessage,
                    actionType: actionTypes.LOGIN

                });
            }
        case actionTypes.LOGOUT:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    userInfo: null,
                    status: API_STATUS.DONE,
                    actionType: actionTypes.LOGOUT

                });
            } else if (action.status == API_STATUS.ERROR) {
                return jQuery.extend(true, {}, state, {
                    status: API_STATUS.ERROR,
                    errorMessage: action.errorMessage,
                    actionType: actionTypes.LOGOUT
                });
            }

        case actionTypes.UPDATE_PROFILE:
            if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {status: action.status});
            } else if (typeof action.data != "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.data.cause,
                    actionType: action.type
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type
                });
            }

        case actionTypes.CLOSE_DIALOG_ACCOUNT_SETTING:
            return jQuery.extend(true, {}, state, {
                actionType: action.type,
                status: ''
            });
        case actionTypes.SESSION_EXPIRED:
            return jQuery.extend(true, {}, initialState, {
                status: API_STATUS.DONE,
                actionType: actionTypes.LOGOUT
            });
        default:
            return state;
    }
}