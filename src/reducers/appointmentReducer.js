import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants'

const initialState = {
    data: {},
    status: '',
    actionType: ''
};

export default function appointmentReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SEARCH_DONOR_FOR_APPOINTMENT:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, initialState, {
                    actionType: action.type,
                    status: action.status,
                    donors: action.items
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    actionType: action.type,
                    status: action.status
                });
            }
        case actionTypes.GET_APPOINTMENTS:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, initialState, {
                    items: action
                        .items
                        .map(function (x) {
                            if (x.donor && x.donor.firstName == null) {
                                x.donor.firstName = "";
                                x.donor.lastName = "";
                            }
                            return x;
                        }),
                    status: action.status,
                    actionType: action.type,
                    needRefresh: false
                });

            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    status: API_STATUS.IN_PROGRESS,
                    actionType: action.type,
                    needRefresh: false
                });
            } else {
                return jQuery.extend(true, {}, initialState, {
                    status: action.status,
                    actionType: action.type,
                    message: action.message,
                    needRefresh: false
                });
            }
        case actionTypes.GET_APPOINTMENT:
        case actionTypes.GET_SCHEDULING_REQUEST:
            if (typeof action.message !== "undefined") {
                return jQuery.extend(true, {}, initialState, {
                    status: action.status,
                    actionType: action.type,
                    data: action.message,
                    needRefresh: false
                });
            } else if (action.status == API_STATUS.IN_PROGRESS || typeof action.data === "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    needRefresh: false
                });
            } else {
                return jQuery.extend(true, {}, initialState, {
                    status: action.status,
                    actionType: action.type,
                    data: action.data,
                    needRefresh: false
                });
            }

        case actionTypes.SAVE_APPOINTMENT:
        case actionTypes.CANCEL_APPOINTMENT:
        case actionTypes.CHECK_OUT_APPOINTMENT:
            if (action.status == API_STATUS.IN_PROGRESS || action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    message: '',
                    status: action.status,
                    data: action.data
                        ? jQuery.extend(true, {}, action.data)
                        : null,
                    actionType: action.type,
                    needRefresh: false
                });
            } else { //Error
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    message: action.data && action.data.cause
                        ? action.data.cause
                        : action.data.error
                            ? action.data.error
                            : ''
                });
            }

        case actionTypes.CLOSE_APPOINTMENT_DIALOG:
        case actionTypes.CLOSE_APPOINMENT_POPUP:
            return jQuery.extend(true, {}, initialState, {
                actionType: action.type,
                status: '',
                needRefresh: action.needRefresh,
                data: action.data
            });

        case actionTypes.OPEN_CANCEL_APPOINMENT_DIALOG:
        case actionTypes.OPEN_ADD_APPOINMENT_DIALOG:
        case actionTypes.OPEN_EDIT_APPOINMENT_DIALOG:
        case actionTypes.OPEN_APPOINTMENT_POPUP:
            return jQuery.extend(true, {}, state, {
                actionType: action.type,
                status: ''
            });

        case actionTypes.CHANG_ROUTE:
            return jQuery.extend(true, {}, initialState, {actionType: action.type});

        default:
            return state;
    }
}