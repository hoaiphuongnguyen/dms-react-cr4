import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants'

const initialState = {
    data: {
        address: {},
        donorMedicalData: {},
        hlaTypings: [],
        donorProcedureAvailabilities: [
            {
                "procedureCategory": "Whole Blood",
                "eligible": false
            }, {
                "procedureCategory": "Bone Marrow",
                "eligible": false
            }, {
                "procedureCategory": "LeukoPak",
                "eligible": false
            }, {
                "procedureCategory": "LeukoPak Mobilized",
                "eligible": false
            }
        ],
        paymentTransactions: [],
        outreachLogs: [],
        recruitment: {}
    },
    appointments: [],
    loadMatchRequests: {
        totalRecords: 0,
        page: 1,
        pageSize: 3,
        sortBy: 'createdAt',
        sortOrder: 'DESC',
        items: []
    },
    status: '',
    actionType: '',
    tabName: '',
    matchedScheduleId: '',
    pdfPrintData: null,
    pdfPrintUrl: null
};

export default function donorReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.VIEW_DONOR_QUICK_SEARCH:
            return jQuery.extend(true, {}, state, {
                donorId: action.donorId,
                actionType: action.type
            });

        case actionTypes.PRINT_PDF:
            if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    pdfPrintData: action.data,
                    pdfPrintUrl: action.url,
                    actionType: action.type
                });
            } else if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    pdfPrintData: null,
                    pdfPrintUrl: null,
                    actionType: action.type
                });
            }
            return state;
        case actionTypes.GET_SCHEDULING_REQUEST:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    scheduleRequest: action.data,
                    status: action.status,
                    actionType: action.type,
                    matchedScheduleId: state.matchedScheduleId //keep this data
                });
            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    status: API_STATUS.IN_PROGRESS,
                    actionType: action.type
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    message: action.message,
                    matchedScheduleId: state.matchedScheduleId //keep this data
                });
            }

        case actionTypes.CHECK_OUT_APPOINTMENT:
        case actionTypes.SAVE_APPOINTMENT:
        case actionTypes.CANCEL_APPOINTMENT:
            if (action.status == API_STATUS.IN_PROGRESS || action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    message: '',
                    status: action.status,
                    actionType: action.type,
                    matchedScheduleId: (action.status == API_STATUS.IN_PROGRESS)
                        ? state.matchedScheduleId
                        : '',
                    confirmSchedule: action.confirmSchedule,
                    needRefresh: false

                });
            } else {
                //Error
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    matchedScheduleId: state.matchedScheduleId, //keep this data
                    confirmSchedule: action.confirmSchedule,
                    message: action.data && action.data.cause
                        ? action.data.cause
                        : action.data.error
                            ? action.data.error
                            : ''
                });
            }

        case actionTypes.CLOSE_APPOINTMENT_DIALOG:
        case actionTypes.CLOSE_APPOINMENT_POPUP:
            return jQuery.extend(true, {}, state, {
                actionType: action.type,
                status: '',
                needRefresh: action.needRefresh
            });

        case actionTypes.GET_APPOINTMENTS:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, initialState, {
                    appointments: action
                        .items
                        .map(function (x) {
                            if (x.donor && x.donor.firstName == null && x.donor.lastName == null) {
                                x.donor.firstName = "";
                                x.donor.lastName = "";
                            }
                            return x;
                        }),
                    status: action.status,
                    actionType: action.type,
                    matchedScheduleId: state.matchedScheduleId,
                    loadMatchRequests: state.loadMatchRequests,
                    data: state.data
                });
            } else {
                return jQuery.extend(true, {}, initialState, {
                    appointments: [],
                    status: action.status,
                    actionType: action.type,
                    matchedScheduleId: state.matchedScheduleId,
                    loadMatchRequests: state.loadMatchRequests,
                    data: state.data
                });
            }

        case actionTypes.SAVE_DONOR:
            if (action.status == API_STATUS.IN_PROGRESS || typeof action.data == "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    tabName: action.tabName
                });
            } else if (action.status == API_STATUS.DONE) {
                var data;
                if (action.tabName == "Profile") {
                    data = action.data;
                    if (!data.donorMedicalData || !data.donorMedicalData.id) {
                        data.donorMedicalData = jQuery.extend(true, {}, state.data.donorMedicalData);
                    }
                    if (!data.donorProcedureAvailabilities || data.donorProcedureAvailabilities.length == 0) {
                        data.donorProcedureAvailabilities = jQuery.extend(true, {}, state.data.donorProcedureAvailabilities);
                    }
                    if (!data.donorViralTest || !data.donorViralTest.id) {
                        data.donorViralTest = jQuery.extend(true, {}, state.data.donorViralTest);
                    }
                    if (!data.recruitment || !data.recruitment.id) {
                        data.recruitment = jQuery.extend(true, {}, state.data.recruitment);
                    }
                } else {
                    var data;
                    if (action.tabName == "Medical") {
                        data = state.data;
                        data.donorMedicalData = action.data.donorMedicalData;
                        data.donorProcedureAvailabilities = action.data.donorProcedureAvailabilities;
                        data.donorViralTest = action.data.donorViralTest;
                    } else if (action.tabName == "Recruitment") {
                        data = state.data;
                        data.recruitment = action.data;
                    } else if (action.tabName == "Payment") {
                        data = state.data;
                        //data.recruitment = action.data;
                    }
                }

                if (!data.donorProcedureAvailabilities || data.donorProcedureAvailabilities.length == 0) {
                    data.donorProcedureAvailabilities = initialState.data.donorProcedureAvailabilities;
                } else {
                    for (var i = 0; i < initialState.data.donorProcedureAvailabilities.length; i++) {
                        var item = initialState.data.donorProcedureAvailabilities[i];
                        for (var j = 0; j < data.donorProcedureAvailabilities.length; j++) {
                            if (item.procedureCategory == data.donorProcedureAvailabilities[j].procedureCategory) {
                                item.eligible = data.donorProcedureAvailabilities[j].eligible;
                                item.wbAccumulatedVol = data.donorProcedureAvailabilities[j].wbAccumulatedVol;
                                item.wbVolLeft = data.donorProcedureAvailabilities[j].wbVolLeft;
                                item.lastProcedureDate = data.donorProcedureAvailabilities[j].lastProcedureDate;
                                item.lastProcedureVol = data.donorProcedureAvailabilities[j].lastProcedureVol;
                                item.currentAvailability = data.donorProcedureAvailabilities[j].currentAvailability;
                                item.nextAvailableDate = data.donorProcedureAvailabilities[j].nextAvailableDate;
                                item.donationCount = data.donorProcedureAvailabilities[j].donationCount;
                                break;
                            }
                        }
                    }
                    data.donorProcedureAvailabilities = initialState.data.donorProcedureAvailabilities;
                }

                if (data.donorMedicalData) {
                    var height = data.donorMedicalData.height;
                    if (height && height != '') {
                        var heights = height.split("-");
                        if (heights.length > 0) 
                            data.donorMedicalData["feet"] = heights[0];
                        if (heights.length > 1) 
                            data.donorMedicalData["inch"] = heights[1];
                        }
                    
                }

                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    tabName: action.tabName,
                    data: data
                });
            } else { //ERROR
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    tabName: action.tabName,
                    data: action.data
                });
            }

        case actionTypes.GET_DONOR:
            if (typeof action.error !== "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    data: action.error
                });
            } else if (action.status == API_STATUS.IN_PROGRESS || typeof action.data === "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type
                });
            } else {
                //Done successfully
                var data = action.data;
                if (!data.donorProcedureAvailabilities || data.donorProcedureAvailabilities.length == 0) {
                    data.donorProcedureAvailabilities = initialState.data.donorProcedureAvailabilities;
                } else {
                    for (var i = 0; i < initialState.data.donorProcedureAvailabilities.length; i++) {
                        var item = initialState.data.donorProcedureAvailabilities[i];
                        for (var j = 0; j < data.donorProcedureAvailabilities.length; j++) {
                            if (item.procedureCategory == data.donorProcedureAvailabilities[j].procedureCategory) {
                                item.id = data.donorProcedureAvailabilities[j].id;
                                item.eligible = data.donorProcedureAvailabilities[j].eligible;
                                item.wbAccumulatedVol = data.donorProcedureAvailabilities[j].wbAccumulatedVol;
                                item.wbVolLeft = data.donorProcedureAvailabilities[j].wbVolLeft;
                                item.lastProcedureDate = data.donorProcedureAvailabilities[j].lastProcedureDate;
                                item.lastProcedureVol = data.donorProcedureAvailabilities[j].lastProcedureVol;
                                item.currentAvailability = data.donorProcedureAvailabilities[j].currentAvailability;
                                item.nextAvailableDate = data.donorProcedureAvailabilities[j].nextAvailableDate;
                                item.donationCount = data.donorProcedureAvailabilities[j].donationCount;
                                break;
                            }
                        }
                    }
                    data.donorProcedureAvailabilities = initialState.data.donorProcedureAvailabilities;
                }
                if (data.donorMedicalData) {
                    var height = data.donorMedicalData.height;
                    if (height && height != '') {
                        var heights = height.split("-");
                        if (heights.length > 0) 
                            data.donorMedicalData["feet"] = heights[0];
                        if (heights.length > 1) 
                            data.donorMedicalData["inch"] = heights[1];
                        }
                    
                }

                return jQuery.extend(true, {}, initialState, {
                    status: action.status,
                    actionType: action.type,
                    matchedScheduleId: state.matchedScheduleId,
                    appointments: state.appointments,
                    loadMatchRequests: state.loadMatchRequests,
                    data: data
                });
            }

        case actionTypes.CLOSE_DIALOG:
            if (action.needRefresh) {
                var data = state.data;
                data
                    .outreachLogs
                    .unshift(action.data);
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    data: data
                });
            }
            return state;
        case actionTypes.CHANG_ROUTE:
            if ((action.prevRoute.location.pathname.indexOf("/schedule-request/process-schedule-request/") >= 0 || action.prevRoute.location.pathname.indexOf("/donor/edit-donor/") >= 0) && action.nextRoute.location.pathname.indexOf("/donor/edit-donor/") >= 0) {
                //reset all data but keep the confirm scheduleId from match donor!
                return jQuery.extend(true, {}, initialState, {
                    actionType: action.type,
                    matchedScheduleId: state.matchedScheduleId
                });
            } else {
                return jQuery.extend(true, {}, initialState, {actionType: action.type});
            }

        case actionTypes.LOAD_SCHEDULING_REQUEST_DONOR_MATCH:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    loadMatchRequests: {
                        page: action.page,
                        sortBy: action.sortBy,
                        sortOrder: action.sortOrder,
                        items: state
                            .loadMatchRequests
                            .items
                            .concat(action.items),
                        totalRecords: action.totalRecords
                    },
                    matchedScheduleId: state.matchedScheduleId, //keep this data
                    status: action.status,
                    actionType: action.type,
                    message: action.errorMessage
                });

            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    matchedScheduleId: state.matchedScheduleId, //keep this data
                    status: API_STATUS.IN_PROGRESS,
                    actionType: action.type
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    matchedScheduleId: state.matchedScheduleId, //keep this data
                    status: action.status,
                    actionType: action.type,
                    message: action.errorMessage
                });
            }

        case actionTypes.RELOAD_SCHEDULING_REQUEST_DONOR_MATCH:
            if (typeof action.items != "undefined") {
                state.loadMatchRequests.items = {}

                return jQuery.extend(true, {}, state, {
                    loadMatchRequests: {
                        page: action.page,
                        sortBy: action.sortBy,
                        sortOrder: action.sortOrder,
                        items: action.items,
                        totalRecords: action.totalRecords
                    },
                    matchedScheduleId: state.matchedScheduleId, //keep this data
                    status: action.status,
                    actionType: action.type,
                    message: action.errorMessage
                });

            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    matchedScheduleId: state.matchedScheduleId, //keep this data
                    status: API_STATUS.IN_PROGRESS,
                    actionType: action.type
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    matchedScheduleId: state.matchedScheduleId, //keep this data
                    status: action.status,
                    actionType: action.type,
                    message: action.errorMessage
                });
            }

        case actionTypes.MATCH_DONOR_TO_SCHEDULE:
            if (action.scheduleId) {
                return jQuery.extend(true, {}, state, {
                    matchedScheduleId: action.scheduleId,
                    hiddenBackScheduleRequest: action.hiddenBackScheduleRequest
                });
            }
            return state;
        case actionTypes.SAVE_PAYMENT_VOLUME:
            return jQuery.extend(true, {}, state, {
                status: action.status,
                actionType: action.type,
                tabName: action.tabName
            });
        case actionTypes.LOGOUT:
            return jQuery.extend(true, {}, state, {
                status: action.status,
                actionType: actionTypes.LOGOUT
            });

        case actionTypes.SESSION_EXPIRED:
            return jQuery.extend(true, {}, initialState, {
                status: API_STATUS.DONE,
                actionType: actionTypes.LOGOUT
            });
        default:
            return state;
    }
}