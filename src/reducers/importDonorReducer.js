import * as actionTypes from '../constants/actionTypes';
import { API_STATUS } from '../common/constants';

const initialState = {
    status:API_STATUS.NOT_STARTED
};

export default function importDonorReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.IMPORT_DONORS:
                 return jQuery.extend(true,{}, state, {  status: action.status,
                                                    totalRecords:action.totalRecords,
                                                    successRecords:action.successRecords,
                                                    failedRecords:action.failedRecords});
        default:
            return state;
    }
}