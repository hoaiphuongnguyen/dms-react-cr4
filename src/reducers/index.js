import {combineReducers} from 'redux';
import accountReducer from './accountReducer';
import userReducer from './userReducer';
import donorReducer from './donorReducer';
import outReachReducer from './outReachReducer';
import userListReducer from './userListReducer';
import importDonorReducer from './importDonorReducer';
import medicalProcedureListReducer from './medicalProcedureListReducer';
import medicalProcedureReducer from './medicalProcedureReducer';
import layoutReducer from './layoutReducer';
import searchDonorReducer from './searchDonorReducer';
import schedulingRequestListReducer from './schedulingRequestListReducer';
import schedulingRequestReducer from './schedulingRequestReducer';
import searchSchedulingRequestReducer from './searchSchedulingRequestReducer';
import appointmentReducer from './appointmentReducer';
import reportReducer from './reportReducer';
import processScheduleRequest from './processScheduleRequestReducer';

const rootReducer = combineReducers({
    account: accountReducer,
    user: userReducer,
    userList: userListReducer,
    donor: donorReducer,
    outReach: outReachReducer,
    layout: layoutReducer,
    searchDonor: searchDonorReducer,
    importDonor: importDonorReducer,
    medicalProcedure: medicalProcedureReducer,
    medicalProcedureList: medicalProcedureListReducer,
    schedulingRequest: schedulingRequestReducer,
    searchSchedulingRequest: searchSchedulingRequestReducer,
    schedulingRequestList: schedulingRequestListReducer,
    appointment: appointmentReducer,
    report: reportReducer,
    processScheduleRequest: processScheduleRequest
});

export default rootReducer;