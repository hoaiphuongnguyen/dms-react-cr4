import * as actionTypes from '../constants/actionTypes';
import {API_STATUS, DIALOG_TYPE} from '../common/constants';

const initialState = {
    successDialogShowing: false,
    accountSettingDialogShowing: false,
    donorProcedureAvailabilityDialogShowing: false,
    appointmentPopupShowing: false,
    appointmentDialogShowing: false,
    modalDialogShowing: false,
    activeDialogType: DIALOG_TYPE.NONE,
    donorId: null,
    dialogData: null
};

export default function layoutReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.OPEN_SUCCESS_DIALOG:
            if (!state.successDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    successDialogShowing: true,
                    modalDialogShowing: false,
                    appointmentDialogShowing: false,
                    donorProcedureAvailabilityDialogShowing: false,
                    accountSettingDialogShowing: false,
                    status: ''
                });
            }
            return state;

          case actionTypes.CLOSE_SUCCESS_DIALOG:
            if (state.successDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    appointmentPopupShowing: state.appointmentPopupShowing,
                    successDialogShowing: false,
                    modalDialogShowing: false,
                    appointmentDialogShowing: state.appointmentDialogShowing,
                    donorProcedureAvailabilityDialogShowing: state.donorProcedureAvailabilityDialogShowing,
                    accountSettingDialogShowing: state.accountSettingDialogShowing,
                    activeDialogType: DIALOG_TYPE.NONE,
                    dialogData: null,
                    status: ''
                });
            }
            return state;     

        case actionTypes.OPEN_ACCOUNT_SETTING_DIALOG:
            if (!state.accountSettingDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    appointmentPopupShowing: state.appointmentPopupShowing,
                    donorProcedureAvailabilityDialogShowing: false,
                    accountSettingDialogShowing: true,
                    successDialogShowing: false,
                    modalDialogShowing: false,
                    appointmentDialogShowing: false,
                    activeDialogType: DIALOG_TYPE.ACCOUNT_SETTING,
                    dialogData: action.dialogData,
                    status: ''
                });
            }
            return state;

        case actionTypes.OPEN_ADD_USER_DIALOG:
        case actionTypes.OPEN_ADD_PROCEDURE_DIALOG:
            if (!state.modalDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    successDialogShowing: false,
                    modalDialogShowing: true,
                    appointmentDialogShowing: false,
                    donorProcedureAvailabilityDialogShowing: false,
                    accountSettingDialogShowing: false,
                    activeDialogType: DIALOG_TYPE.ADD_DATA,
                    dialogData: action.dialogData,
                    status: ''
                });
            }
            return state;

        case actionTypes.OPEN_ADD_OUT_REACH_DIALOG:
            if (!state.modalDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    successDialogShowing: false,
                    modalDialogShowing: true,
                    appointmentDialogShowing: false,
                    donorProcedureAvailabilityDialogShowing: false,
                    accountSettingDialogShowing: false,
                    activeDialogType: DIALOG_TYPE.ADD_DATA,
                    donorId: action.donorId,
                    scheduleId: action.scheduleId,
                    status: ''
                });
            }
            return state;

        case actionTypes.OPEN_EDIT_USER_DIALOG:
        case actionTypes.OPEN_EDIT_PROCEDURE_DIALOG:
            if (!state.modalDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    successDialogShowing: false,
                    modalDialogShowing: true,
                    appointmentPopupShowing: false,
                    donorProcedureAvailabilityDialogShowing: false,
                    accountSettingDialogShowing: false,
                    activeDialogType: DIALOG_TYPE.EDIT_DATA,
                    dialogData: action.dialogData,
                    status: ''
                });
            }

        case actionTypes.OPEN_ADD_APPOINMENT_DIALOG:
            if (!state.appointmentDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    successDialogShowing: false,
                    modalDialogShowing: false,
                    appointmentDialogShowing: true,
                    appointmentPopupShowing: false,
                    donorProcedureAvailabilityDialogShowing: false,
                    accountSettingDialogShowing: false,
                    activeDialogType: DIALOG_TYPE.ADD_DATA,
                    collectionSite: action.collectionSite,
                    donorId: action.donorId,
                    procedureCategory: action.procedureCategory,
                    startTime: action.startTime,
                    endTime: action.endTime,
                    status: ''
                });
            }
            return state;

        case actionTypes.OPEN_EDIT_APPOINMENT_DIALOG:
            if (!state.appointmentDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    successDialogShowing: false,
                    modalDialogShowing: false,
                    appointmentDialogShowing: true,
                    appointmentPopupShowing: true,
                    donorProcedureAvailabilityDialogShowing: false,
                    accountSettingDialogShowing: false,
                    activeDialogType: DIALOG_TYPE.EDIT_DATA,
                    dialogData: action.dialogData,
                    status: ''
                });
            }

        case actionTypes.OPEN_CANCEL_APPOINMENT_DIALOG:
            if (!state.appointmentDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    successDialogShowing: false,
                    modalDialogShowing: false,
                    appointmentDialogShowing: true,
                    appointmentPopupShowing: true,
                    donorProcedureAvailabilityDialogShowing: false,
                    accountSettingDialogShowing: false,
                    activeDialogType: DIALOG_TYPE.CANCEL_APPOINTMENT,
                    dialogData: action.dialogData,
                    status: ''
                });
            }
            return state;
        case actionTypes.CLOSE_DIALOG:
            if (state.modalDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    appointmentPopupShowing: state.appointmentPopupShowing,
                    successDialogShowing: false,
                    modalDialogShowing: false,
                    appointmentDialogShowing: state.appointmentDialogShowing,
                    donorProcedureAvailabilityDialogShowing: state.donorProcedureAvailabilityDialogShowing,
                    accountSettingDialogShowing: state.accountSettingDialogShowing,
                    activeDialogType: DIALOG_TYPE.NONE,
                    dialogData: null,
                    status: ''
                });
            }
            return state;

        case actionTypes.CLOSE_DIALOG_ACCOUNT_SETTING:
            if (state.accountSettingDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    successDialogShowing: false,
                    modalDialogShowing: state.modalDialogShowing,
                    appointmentDialogShowing: state.appointmentDialogShowing,
                    appointmentPopupShowing: state.appointmentPopupShowing,
                    donorProcedureAvailabilityDialogShowing: state.donorProcedureAvailabilityDialogShowing,
                    accountSettingDialogShowing: false,
                    activeDialogType: DIALOG_TYPE.NONE,
                    dialogData: null,
                    status: ''
                });
            }
            return state;

        case actionTypes.CLOSE_APPOINTMENT_DIALOG:
            if (state.appointmentDialogShowing) {
                return jQuery.extend(true, {}, initialState, {
                    appointmentPopupShowing: state.appointmentPopupShowing,
                    successDialogShowing: false,
                    modalDialogShowing: state.modalDialogShowing,
                    appointmentDialogShowing: false,
                    donorProcedureAvailabilityDialogShowing: state.donorProcedureAvailabilityDialogShowing,
                    accountSettingDialogShowing: state.accountSettingDialogShowing,
                    activeDialogType: DIALOG_TYPE.NONE,
                    dialogData: null,
                    status: ''
                });
            }
            return state;

        case actionTypes.OPEN_APPOINTMENT_POPUP:
            return jQuery.extend(true, {}, initialState, {
                successDialogShowing: false,
                appointmentPopupShowing: true,
                activeDialogType: DIALOG_TYPE.APPOINMENT_POPUP,
                dialogData: action.dialogData,
                status: ''
            });

        case actionTypes.CLOSE_APPOINMENT_POPUP:
            if (state.appointmentPopupShowing) {
                return jQuery.extend(true, {}, initialState, {
                    successDialogShowing: false,
                    appointmentPopupShowing: false,
                    status: ''
                });
            }
            return state;

        default:
            return state;
    }
}