import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants'

const initialState = {
    totalRecords: 0,
    page: 1,
    pageSize: 10,
    sortBy: 'createdAt',
    sortOrder: 'DESC',
    items: [],
    status: '',
    message: '',
    needRefresh: false,
    allMedicalProcedures: [],
    needReloadAllProcedures: true
};

export default function medicalProcedureListReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SAVE_PROCEDURE:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {needReloadAllProcedures: true});
            } else 
                return state;
            case actionTypes.LOAD_PROCEDURES:
            if (typeof action.items != "undefined") {
                return jQuery.extend(true, {}, initialState, {
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: state
                        .items
                        .concat(action.items),
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });

            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });
            }
        case actionTypes.RELOAD_PROCEDURES:
            if (typeof action.items != "undefined") {
                return jQuery.extend(true, {}, initialState, {
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: action.items,
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });
            } else {
                return jQuery.extend(true, {}, initialState, {
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });
            }
        case actionTypes.LOAD_ALL_PROCEDURES:
            if (action.status == API_STATUS.DONE) {
                var newState = jQuery.extend(true, {}, state);
                newState.allMedicalProcedures = action.items;
                newState.needReloadAllProcedures = false;
                return newState;
            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {needReloadAllProcedures: false});
            } else if (action.status == API_STATUS.ERROR) {
                return jQuery.extend(true, {}, state, {needReloadAllProcedures: true});
            }

        case actionTypes.CLOSE_DIALOG:
            if (action.needRefresh) {
                return jQuery.extend(true, {}, initialState, {
                    sortBy: state.sortBy,
                    sortOrder: state.sortOrder,
                    needRefresh: action.needRefresh
                });
            }

            return state;

        case actionTypes.LOGOUT:
            if (action.status == API_STATUS.DONE) {
                //reset all data to default after logging out successfully!
                return jQuery.extend(true, {}, initialState);
            }
            return state;
        case actionTypes.SESSION_EXPIRED:
            //reset all data to default if session expired!
            return jQuery.extend(true, {}, initialState);
        default:
            return state;
    }
}