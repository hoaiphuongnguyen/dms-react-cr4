import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants'

const initialState = {
    status: '',
    message: '',
    actionType: ''
};

export default function medicalProcedurerReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SAVE_PROCEDURE:
            if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {status: action.status});
            } else if (typeof action.data != "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.data.cause,
                    actionType: action.type
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type
                });
            }

        case actionTypes.CLOSE_DIALOG:
            return jQuery.extend(true, {}, state, {
                actionType: action.type,
                status: ''
            });

        default:
            return state;
    }
}