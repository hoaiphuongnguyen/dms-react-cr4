import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants'

const initialState = {
    status: '',
    message: '',
    actionType: '',
    items: {}
};

export default function outReachReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SAVE_OUT_REACH:
            if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {status: action.status});
            } else if (typeof action.data != "undefined") {
                if (action.data.cause) {
                    return jQuery.extend(true, {}, state, {
                        status: action.status,
                        message: action.data.cause,
                        actionType: action.type
                    });
                } else {
                    return jQuery.extend(true, {}, state, {
                        status: action.status,
                        data: action.data,
                        actionType: action.type
                    });
                }
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type
                });
            }

        case actionTypes.GET_OUT_REACH_REASON:
        case actionTypes.GET_OUT_REACH_OUTCOME:
            if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {status: action.status});
            } else if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    items: action.items
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    message: action.message
                });
            }

        case actionTypes.CLOSE_DIALOG:
            return jQuery.extend(true, {}, initialState, {
                actionType: action.type,
                status: ''
            });
            
        default:
            return state;
    }
}