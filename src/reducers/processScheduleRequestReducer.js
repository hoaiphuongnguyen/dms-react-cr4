import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants';

const initialState = {
    totalRecords: 0,
    page: 1,
    pageSize: 10,
    sortBy: 'createdAt',
    sortOrder: 'DESC',
    items: [],
    status: '',
    message: '',
    hideFilterIfResultsFound: false,
    actionType: '',
    needRefresh: false
};

export default function processScheduleRequestReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.GET_SCHEDULING_REQUEST:
            return jQuery.extend(true, {}, initialState);
        case actionTypes.MATCH_DONORS:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: state
                        .items
                        .concat(action.items),
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage,
                    actionType: action.type
                });

            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    status: API_STATUS.IN_PROGRESS,
                    actionType: action.type,
                    needRefresh: false
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.errorMessage,
                    actionType: action.type
                });
            }
        case actionTypes.MATCH_DONORS_RELOAD:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, initialState, {
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: action.items,
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage,
                    actionType: action.type,
                    hideFilterIfResultsFound: action.items && action.items.length
                        ? true
                        : false
                });
            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    status: API_STATUS.IN_PROGRESS,
                    actionType: action.type,
                    needRefresh: false
                });
            } else {//Error
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.errorMessage,
                    hideFilterIfResultsFound: false,
                    actionType: action.type
                });
            }
        case actionTypes.CLEAR_MATCH_DONORS_PARAMS:
            return jQuery.extend(true, {}, initialState, {actionType: action.type});
            // case actionTypes.CHANG_ROUTE:    return jQuery.extend(true, {}, initialState,
            // {actionType: action.type});
        case actionTypes.SAVE_OUT_REACH:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    needRefresh: true
                });
            }
            return state;
        default:
            return state;
    }
}