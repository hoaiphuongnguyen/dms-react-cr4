import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants';

const initialState = {
    totalRecords: 0,
    page: 1,
    pageSize: 10,
    sortBy: 'createdAt',
    sortOrder: 'DESC',
    items: [],
    status: '',
    message: '',
    actionType: ''
};

export default function reportReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.LOAD_REPORT:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: state
                        .items
                        .concat(action.items),
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage
                });

            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    status: API_STATUS.IN_PROGRESS,
                    actionType: action.type
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    message: action.errorMessage
                });
            }
        case actionTypes.RELOAD_REPORT:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, initialState, {
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: action.items,
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage
                });
            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    status: API_STATUS.IN_PROGRESS,
                    actionType: action.type
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    message: action.errorMessage
                });
            }
        case actionTypes.CLEAR_SEARCH_PARAMS:
            return jQuery.extend(true, {}, initialState);
        case actionTypes.CHANG_ROUTE:
            return jQuery.extend(true, {}, initialState, {actionType: action.type});
        default:
            return state;
    }
}