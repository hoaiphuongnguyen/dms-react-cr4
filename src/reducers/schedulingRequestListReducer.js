import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants'

const initialState = {
    totalRecords: 0,
    page: 1,
    pageSize: 10,
    sortBy: 'createdAt',
    sortOrder: 'DESC',
    items: [],
    status: '',
    message: '',
    needRefresh: false
};

export default function schedulingRequestListReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.LOAD_SCHEDULING_REQUEST:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, initialState, {
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: state
                        .items
                        .concat(action.items),
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });

            } else { //error
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });
            }
        case actionTypes.RELOAD_SCHEDULING_REQUEST:
            if (typeof action.items != "undefined") {
                return jQuery.extend(true, {}, initialState, {
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: action.items,
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });
            } else { //error
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });
            }

        default:
            return state;
    }
}