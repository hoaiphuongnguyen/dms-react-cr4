import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants'

const initialState = {
    data: {},
    status: '',
    actionType: '',
    tabName: ''
};

export default function schedulingRequestReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SAVE_SCHEDULING_REQUEST:

            if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    tabName: action.tabName
                });
            } else if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: "",
                    data: action.data,
                    actionType: action.type,
                    tabName: action.tabName
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    tabName: action.tabName,
                    message: action.data && action.data.cause
                        ? action.data.cause
                        : ''
                });
            }

        case actionTypes.GET_SCHEDULING_REQUEST:

            if (typeof action.error !== "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    data: action.error
                });
            } else if (action.status == API_STATUS.IN_PROGRESS || typeof action.data === "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type
                });
            } else {

                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    data: action.data
                });
            }

        case actionTypes.CANCEL_SCHEDULING_REQUEST:

            if (typeof action.error !== "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    data: action.error
                });
            } else if (action.status == API_STATUS.IN_PROGRESS || typeof action.data === "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type
                });
            } else {

                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    data: action.data
                });
            }
        case actionTypes.UNASSIGN_DONOR:
            if (typeof action.error !== "undefined") {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    actionType: action.type,
                    data: action.error
                });
            } else {
                var data = jQuery.extend(true, {}, state.data);
                data.donorToScheduleRequests = [];
                return jQuery.extend(true, {}, {
                    status: action.status,
                    actionType: action.type,
                    data: data
                });
            }

        case actionTypes.CHANG_ROUTE:
            return jQuery.extend(true, {}, initialState, {actionType: action.type});

        case actionTypes.SESSION_EXPIRED:
            return jQuery.extend(true, {}, initialState, {
                status: API_STATUS.DONE,
                actionType: actionTypes.LOGOUT
            });

        default:
            return state;
    }
}