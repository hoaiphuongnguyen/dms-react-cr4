import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants';

const initialState = {
    totalRecords: 0,
    page: 1,
    pageSize: 10,
    sortBy: 'firstName',
    sortOrder: 'ASC',
    items: [],
    status: '',
    message: '',
    hideFilterIfResultsFound: false,
    actionType: ''
};

export default function searchDonorReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SEARCH_DONORS:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, state, {
                    actionType: action.type,
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: state
                        .items
                        .concat(action.items),
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage
                });

            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    status: API_STATUS.IN_PROGRESS,
                    actionType: action.type
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.errorMessage
                });
            }
        case actionTypes.SEARCH_DONORS_RELOAD:
            if (action.status == API_STATUS.DONE) {
                return jQuery.extend(true, {}, initialState, {
                    actionType: action.type,
                    page: action.page,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: action.items,
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage,
                    hideFilterIfResultsFound: action.items && action.items.length
                        ? true
                        : false
                });
            } else if (action.status == API_STATUS.IN_PROGRESS) {
                return jQuery.extend(true, {}, state, {
                    actionType: action.type,
                    status: API_STATUS.IN_PROGRESS
                });
            } else {//Error
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.errorMessage,
                    hideFilterIfResultsFound: false
                });
            }

        case actionTypes.CLEAR_SEARCH_PARAMS:
            return jQuery.extend(true, {}, initialState);
        case actionTypes.CHANG_ROUTE:
            return jQuery.extend(true, {}, initialState, {actionType: action.type});

        case actionTypes.LOGOUT:
            return jQuery.extend(true, {}, state, {
                status: action.status,
                actionType: actionTypes.LOGOUT
            });

        default:
            return state;
    }
}