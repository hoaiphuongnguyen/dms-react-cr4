import * as actionTypes from '../constants/actionTypes';
import {API_STATUS} from '../common/constants';

const initialState = {
    totalRecords: 0,
    page: 1,
    pageSize: 10,
    sortBy: 'firstName',
    sortOrder: 'ASC',
    items: [],
    status: '',
    message: '',
    needRefresh: false
};

export default function userListReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.LOAD_USERS:
            if (typeof action.items != "undefined") {
                return jQuery.extend(true, {}, {
                    page: action.page,
                    pageSize: 7,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: state
                        .items
                        .concat(action.items),
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });

            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });
            }
        case actionTypes.RELOAD_USERS:
            if (typeof action.items != "undefined") {

                return jQuery.extend(true, {}, {
                    page: action.page,
                    pageSize: 7,
                    sortBy: action.sortBy,
                    sortOrder: action.sortOrder,
                    items: action.items,
                    totalRecords: action.totalRecords,
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });
            } else {
                return jQuery.extend(true, {}, state, {
                    status: action.status,
                    message: action.errorMessage,
                    needRefresh: false
                });
            }
            
        case actionTypes.CLOSE_DIALOG:
            if (action.needRefresh) {
                return jQuery.extend(true, {}, initialState, {
                    sortBy: state.sortBy,
                    sortOrder: state.sortOrder,
                    needRefresh: action.needRefresh
                });
            }

            return state;

        case actionTypes.CHANG_ROUTE:
            return jQuery.extend(true, {}, initialState, {needRefresh: true});
        default:
            return state;
    }
}