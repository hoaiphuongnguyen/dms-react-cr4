import * as config from '../../config/web.config'
import {API_STATUS} from '../common/constants';
import {localStringToUTCString} from './date'
const apiServerUrl = config.apis[__ENV__].apiServerUrl;
const pdfServiceUrl = config.apis[__ENV__].pdfServiceUrl;

/*Donor*/
export function getDonorUrl() {
    return apiServerUrl + 'donors';
}

export function getOneDonorUrl(donorId) {
    return apiServerUrl + `donors/${donorId}`;
}

export function getSearchDonorForAppointmentUrl(collectionSite, name, email, donorId) {
    var url = apiServerUrl + `donors/searchforappointment?`;
    if (collectionSite != null) {
        url = url + `location=${collectionSite}`;
    }
    if (name != null && name != "") {
        url = url + `&name=${name}`;
    }
    if (email != null && email != "") {
        url = url + `&email=${email}`;
    }
    if (donorId != null && donorId != "") {
        url = url + `&donorId=${donorId}`;
    }
    return url;
}

export function getSearchDonorUrl(pageNumber, pageSize, sortBy, sortOrder) {
    return apiServerUrl + `donors/search?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
}

export function getGlobalSearchDonorUrl(pageNumber, pageSize, sortBy, sortOrder) {
    return apiServerUrl + `donors/searchGlobal?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
}

export function getImportDonorUrl() {
    return apiServerUrl + `donors/files`;
}

export function getOutreachReasonUrl() {
    return apiServerUrl + 'outreachreasons';
}

export function getOutreachOutcomeUrl() {
    return apiServerUrl + 'outreachoutcomes';
}

export function saveDonorProcedureAvailability(donorId) {
    return apiServerUrl + `donors/${donorId}/procedures`;
}

export function getSavePaymentVolumeUrl(paymentId, volume) {
    return apiServerUrl + `payments/${paymentId}/updatewbvol/${volume}`;
}

/* Account APIs */
export function getLoginUrl() {
    return apiServerUrl + 'login';
}
export function getLogoutUrl() {
    return apiServerUrl + 'logout';
}

/* User APIs */
export function getUserUrl() {
    return apiServerUrl + 'users';
}

export function getOneUserUrl(userId) {
    return apiServerUrl + `users/${userId}`;
}

export function getLoadUsersUrl(keyword, pageNumber, pageSize, sortBy, sortOrder) {
    return apiServerUrl + `users?q=${keyword}&page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
}

export function getLoadActiveMedicalProceduresUrl(pageNumber, pageSize, sortBy, sortOrder) {
    return apiServerUrl + `procedures/active?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
}

export function getLoadMedicalProceduresUrl(pageNumber, pageSize, sortBy, sortOrder) {
    return apiServerUrl + `procedures?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
}

export function getProcedureUrl(procedureId) {
    return apiServerUrl + `procedures`;
}
export function getOneProcedureUrl(procedureId) {
    return apiServerUrl + `procedures/${procedureId}`;
}

/* Scheduling Request */
export function getCreateSchedulingRequestUrl() {
    return apiServerUrl + `schedules/internal`;
}
export function getOneSchedulingRequestUrl(schedulingRequestId) {
    return apiServerUrl + `schedules/${schedulingRequestId}`;
}

export function getUnAssignedDonorUrl(scheduleId, donorId) {
    return apiServerUrl + `schedules/${scheduleId}/donors/${donorId}/cancellation`;
}

export function getLoadSchedulingRequestUrl(showOnlyUnprocessedRequest, pageNumber, pageSize, sortBy, sortOrder) {
    return apiServerUrl + `schedules/searching?showOnlyUnprocessedRequest=${showOnlyUnprocessedRequest}&page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
}

export function getSchedulingRequestDonorMatch(donorId, pageNumber, pageSize, sortBy, sortOrder) {
    return apiServerUrl + `schedules/donors/${donorId}?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
}

export function getSearchSchedulingRequestUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder) {
    var url = apiServerUrl + `schedules?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;

    if (searchParams.id !== undefined) 
        url = url + `&id=${searchParams.id}`;
    if (searchParams.wo !== undefined) 
        url = url + `&wo=${searchParams.wo}`;
    
    return url;

}

export function getProcessSchedulingRequestUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder) {
    return apiServerUrl + `donors/matchDonors?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
}

/*Appointment*/
export function getAppointmentUrl() {
    return apiServerUrl + `appointments`;
}

export function getAppointmentsUrl(location, donorId, startTime, endTime) {
    var url = apiServerUrl + `appointments?`;
    if (location !== undefined && location != null) 
        url = url + `&location=${location}`;
    if (donorId !== undefined && donorId != null) 
        url = url + `&donorId=${donorId}`;
    
    url = url + `&fromDate=${startTime}`;
    url = url + `&toDate=${endTime}`;

    return url;
}

export function getOneAppointmentUrl(appointmentId) {
    return apiServerUrl + `appointments/${appointmentId}`;
}

/*Report*/
export function getReportDonorStatusUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder, exportCSV) {
    var url = apiServerUrl;
    if (exportCSV) {
        url = url + `reports/donorStatusExportCSV?sortField=${sortBy}&sort=${sortOrder}`;
    } else {
        url = url + `reports/donorStatus?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
    }

    url = url + `&timezone=` + encodeURIComponent(moment().format("Z"));
    if (searchParams.fromDate !== undefined) 
        url = url + `&fromDate=${localStringToUTCString(searchParams.fromDate)}`;
    if (searchParams.toDate !== undefined) 
        url = url + `&toDate=${localStringToUTCString(searchParams.toDate)}`;
    if (searchParams.collectionSite !== undefined) 
        url = url + `&collectionSite=${searchParams.collectionSite}`;
    if (searchParams.category !== undefined) 
        url = url + `&category=${searchParams.category}`;
    if (searchParams.status !== undefined) 
        url = url + `&status=${searchParams.status}`;
    
    return url;
}

export function getReportDonorCancellationUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder, exportCSV) {
    var url = apiServerUrl;
    if (exportCSV) {
        url = url + `reports/cancelAppointmentsExportCSV?sortField=${sortBy}&sort=${sortOrder}`;
    } else {
        url = url + `reports/cancelAppointments?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
    }

    url = url + `&timezone=` + encodeURIComponent(moment().format("Z"));
    if (searchParams.fromDate !== undefined) 
        url = url + `&fromDate=${localStringToUTCString(searchParams.fromDate)}`;
    if (searchParams.toDate !== undefined) 
        url = url + `&toDate=${localStringToUTCString(searchParams.toDate)}`;
    if (searchParams.collectionSite !== undefined) 
        url = url + `&collectionSite=${searchParams.collectionSite}`;
    if (searchParams.category !== undefined) 
        url = url + `&category=${searchParams.category}`
    if (searchParams.cancellationReason !== undefined) 
        url = url + `&cancellationReason=${searchParams.cancellationReason}`;
    
    return url;
}

export function getReportAppointmentUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder, exportCSV) {
    var url = apiServerUrl;
    if (exportCSV) {
        url = url + `reports/appointmentsExportCSV?sortField=${sortBy}&sort=${sortOrder}`;
    } else {
        url = url + `reports/appointments?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
    }

    url = url + `&timezone=` + encodeURIComponent(moment().format("Z"));
    if (searchParams.fromDate !== undefined) 
        url = url + `&fromDate=${localStringToUTCString(searchParams.fromDate)}`;
    if (searchParams.toDate !== undefined) 
        url = url + `&toDate=${localStringToUTCString(searchParams.toDate)}`;
    if (searchParams.collectionSite !== undefined) 
        url = url + `&collectionSite=${searchParams.collectionSite}`;
    if (searchParams.procedureType !== undefined) 
        url = url + `&procedureType=${searchParams.procedureType}`

    return url;
}

export function getReportPaymentUrl(searchParams, pageNumber, pageSize, sortBy, sortOrder, exportCSV) {
    var url = apiServerUrl;
    if (exportCSV) {
        url = url + `reports/paymentsExportCSV?sortField=${sortBy}&sort=${sortOrder}`;
    } else {
        url = url + `reports/payments?page=${pageNumber}&pageSize=${pageSize}&sortField=${sortBy}&sort=${sortOrder}`;
    }

    url = url + `&timezone=` + encodeURIComponent(moment().format("Z"));

    if (searchParams.fromDate !== undefined) 
        url = url + `&fromDate=${localStringToUTCString(searchParams.fromDate)}`;
    if (searchParams.toDate !== undefined) 
        url = url + `&toDate=${localStringToUTCString(searchParams.toDate)}`;
    if (searchParams.donorId !== undefined) 
        url = url + `&donorId=${searchParams.donorId}`;
    if (searchParams.woNumber !== undefined) 
        url = url + `&woNumber=${searchParams.woNumber}`
    if (searchParams.refNo !== undefined) 
        url = url + `&refNo=${searchParams.refNo}`;
    
    return url;
}

export function getPdfPrintingUrl() {
    return pdfServiceUrl;
}