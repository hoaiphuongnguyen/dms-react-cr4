import React, {Component} from 'react';
import {hashHistory} from 'react-router';

export default function confirmLeave(RouteTargetComponent, data) {

  class ConfirmLeaveHOC extends Component { // HOC = Higher Order Component

    onUnload(event) { // the method that will be used for both add and remove event
      if (this.refs.component && this.refs.component.hasUnsavedChanges()) {
        var e = event || window.event;

        // For IE and Firefox prior to version 4
        if (e) {
          e.returnValue = 'Data you have entered may not be saved';
        }

        // For Safari
        return 'This page is asking you to confirm that you want to leave - data you have entere' +
          'd may not be saved';
      }
    }

    leave = (route) => {
      if (route.pathname == "/login") {
        if (localStorage.getItem("userInfo") === null) { //401 error
          return true; //allow leave
        }
      }

      var form = this.refs.component;
      if (!$(".jconfirm").hasClass("jconfirm-open") && form && form.hasUnsavedChanges()) {
        $.confirm({
          title: 'Confirm leave',
          content: 'Changes were made. Would you like to leave and discard them or stay to save?',
          buttons: {
            /* Yes: {
              text: 'Yes',
              btnClass: 'btn-blue',
              action: function () {
                form.autoSave(route.pathname);
              }
            },*/
            No: { //No save and continue
              text: 'Leave',
              btnClass: 'btn-danger',
              action: function () {
                form.onCancel();
                hashHistory.push(route.pathname);
              }
            },
            cancel: {
              text: 'Stay',
              btnClass: 'btn-blue',
              action: function () {}
            }
          }
        });

        return false;
      }

      return true;
    }

    componentDidUpdate() {
      this
        .props
        .router
        .setRouteLeaveHook(this.props.route, this.leave.bind(this));
    }

    componentDidMount() {
      $(window).off("beforeunload", this.onUnload.bind(this));
      $(window).on("beforeunload", this.onUnload.bind(this));
    }

    componentWillUnmount() {
      $(window).off("beforeunload", this.onUnload.bind(this));
    }

    render() {
      // render the component that requires auth (passed to this wrapper)
      return (<RouteTargetComponent ref="component" {...this.props}/>);
    }
  }

  return ConfirmLeaveHOC;
}