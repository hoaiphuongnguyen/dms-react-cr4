export function displayDate(dateString) {
    return (dateString != null && dateString != '')
        ? moment(dateString).format("MM/DD/YYYY")
        : '';
}

export function displayDate2(internalCollectionTime, collectionDate, collectionByTime) {
    return (collectionByTime == null || collectionByTime == undefined || collectionByTime == '')
        ? collectionDate
        : displayDate(internalCollectionTime);
}

export function displayDate3(cell, row) {
    return (row.collectionByTime == null || row.collectionByTime == undefined || row.collectionByTime == '')
        ? row.collectionDate
        : displayDate(row.internalCollectionTime);
}

export function displayTime(dateString) {
    return (dateString != null && dateString != '')
        ? moment(dateString).format("H:mm")
        : '';
}

export function displayTime2(internalCollectionTime, collectionDate, collectionByTime) {
    return (collectionByTime == null || collectionByTime == undefined || collectionByTime == '')
        ? null
        : displayTime(internalCollectionTime);
}

export function displayDateTime(dateString) {
    return (dateString != null && dateString != '')
        ? moment(dateString).format("MM/DD/YYYY HH:mm")
        : '';

}

export function localDateToUTCString(calendarDate) {
    return (calendarDate != null)
        ? moment(calendarDate)
            .utc()
            .format()
        : '';
}

export function utcStringToLocalDate(utcString) {
    return (utcString != null && utcString != '')
        ? moment(utcString).local()
        : null;
}

export function localStringToUTCString(stringDate) {
    return (stringDate != null && stringDate != '')
        ? moment(stringDate, "MM/DD/YYYY")
            .utc()
            .format()
        : '';
}