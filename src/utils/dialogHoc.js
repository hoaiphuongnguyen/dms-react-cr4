import React, {Component} from 'react';
import {hashHistory} from 'react-router';

export default function dialogHoc(RouteTargetComponent) {

  class DialogHOC extends Component { // HOC = Higher Order Component

    onUnload(event) { // the method that will be used for both add and remove event
      const dialog = this.refs.component;
      if (dialog && dialog.props.dialogShowing && dialog.hasUnsavedChanges()) {
        var e = event || window.event;

        // For IE and Firefox prior to version 4
        if (e) {
          e.returnValue = 'Data you have entered may not be saved';
        }

        // For Safari
        return 'This page is asking you to confirm that you want to leave - data you have entere' +
          'd may not be saved';
      }
    }

    componentDidUpdate() {
      const dialog = this.refs.component;
      if (dialog && dialog.props.dialogShowing) {
        $(window).off("beforeunload", this.onUnload.bind(this));
        $(window).on("beforeunload", this.onUnload.bind(this));
      } else {
        $(window).off("beforeunload", this.onUnload.bind(this));
      }
    }

    componentDidMount() {
      const dialog = this.refs.component;
      let modalId;
      if (dialog.props.modalId) {
        modalId = dialog.props.modalId;
      } else {
        modalId = "myModal";
      }

      let closeDialog;
      if (dialog.props.closeDialog == "closeDialogAccountSetting") 
        closeDialog = dialog.props.layoutActions.closeDialogAccountSetting;
      else if (dialog.props.closeDialog == "closeAppointmentDialog") 
        closeDialog = dialog.props.layoutActions.closeAppointmentDialog;
      else 
        closeDialog = dialog.props.layoutActions.closeDialog;
      
      if (dialog.props.dialogShowing) {
        $('#' + modalId).modal('show');
      } else {
        $('#' + modalId).modal({show: false});
      }

      $('#' + modalId)
        .on('hide.bs.modal', function (e) {
          if (!dialog.closing && !dialog.state.clickSave && !dialog.state.clickCancel && !$(".jconfirm").hasClass("jconfirm-open") && dialog.hasUnsavedChanges()) {
            e.preventDefault();
            e.stopImmediatePropagation();

            $.confirm({
              title: 'Confirm leave',
              content: 'Changes were made. Would you like to leave and discard them or stay to save?',
              buttons: {
                /*  Yes: {
                  text: 'Yes',
                  btnClass: 'btn-blue',
                  action: function () {
                    dialog.autoSave();
                  }
                },*/
                No: {
                  text: 'Leave',
                  btnClass: 'btn-danger',
                  action: function () {
                    closeDialog(false);
                  }
                },
                cancel: {
                  text: 'Stay',
                  btnClass: 'btn-blue',
                  action: function () {}
                }
              }
            });
          } else if (!dialog.closing) {
            e.preventDefault();
            e.stopImmediatePropagation();
            //raise close actions
            var needRefresh = (dialog.state.processing == true);
            if (closeDialog(needRefresh)) {
              dialog.closing = true;
            }
            return false;
          }
          dialog.closing = false;
        });
    }

    componentWillUnmount() {
      $(window).off("beforeunload", this.onUnload.bind(this));
    }

    render() {
      // render the component that requires auth (passed to this wrapper)
      return (<RouteTargetComponent ref="component" {...this.props}/>);
    }
  }

  return DialogHOC;
}