export const did = value => value && !/^[0-9]+$/i.test(value)
    ? false
    : true;
export const number = value => value && !/^[0-9]+$/i.test(value)
    ? false
    : true;
export const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? false
    : true;

export const password = value => value && !/^(?=.*[0-9])(?=.*[@#$%^&+=])(?=\\S+$).{6,30}$/i.test(value)
    ? false
    : true;

export const aplha = value => value && !/^[A-Za-z\s]+$/i.test(value)
    ? false
    : true;

export const phone = value => value && !/^[\\+]?[(]?[0-9]{3}[)]?[-\\s\\.]?[0-9]{3}[-\\s\\.]?[0-9]{4,6}$/i.test(value)
    ? false
    : true;

export const zip = value => value && !/^\d{5}$/.test(value)
    ? false
    : true;

export const age = value => value && !/^\d{1,3}$/i.test(value)
    ? false
    : true;

export const weight = value => value && !/^\d*\.?\d*$/.test(value)
    ? false
    : true;

export const bmi = value => value &&( !/^\d*\.?\d*$/.test(value) || value > 99)
    ? false
    : true;

export const decimal = value => value && !/^\d*\.?\d{0,2}$/.test(value)
    ? false
    : true;

export const date = value => value && !/^(?:(?:(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec))(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:(?:0?2|(?:Feb))(\/|-|\.)(?:29)\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value)
    ? false
    : true;
